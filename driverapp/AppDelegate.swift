//
//  AppDelegate.swift
//  driverapp
//
//  Created by Arben Pnishi on 07/02/2018.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import Firebase
import NMAKit
import HockeySDK
import UserNotifications
import FirebaseMessaging
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        Appearance.setup()
        
        NMAApplicationContext.setAppId(Constants.nmaAppId, appCode: Constants.nmaAppCode, licenseKey: Constants.nmaAppLicense)
        console("REST token: \(AccountManager.userToken ?? "")")

        BITHockeyManager.shared().configure(withIdentifier: "0cd60b8611644fcd9113d1356a3ee92d")
        BITHockeyManager.shared().start()
        BITHockeyManager.shared().authenticator.authenticateInstallation()
        
        FirebaseApp.configure()
        UNService.shared.authorize()
        Fabric.with([Crashlytics.self])
        updateRootWindow()
        application.isIdleTimerDisabled = true
        return true
    }
    
    func updateRootWindow(){
        
        if AccountManager.isLogged{
            if AccountManager.isApplicationTerminated!
               || NavigationManager.vehicleID == 0 {
                switchWindowRoot(to: .chooseTruck)
            } else  {
                switchWindowRoot(to: .home)
                PickupStatusManager.sync()
            }
            
        }else{
            AccountManager.clear()
            NavigationManager.clear()
            switchWindowRoot(to: .login)
            NotificationCenter.default.post(name: Notification.Name.stopNavigation, object: nil)
        }
    }
    
    func switchWindowRoot(to place: ApplicationPlace){
        switch place {
        case .login:
            let controller = UIStoryboard.login.instantiateInitialViewController()!
            self.window?.set(root: controller)
            break
        case .home:
            let controller = UIStoryboard.home.instantiateInitialViewController()!
            self.window?.set(root: controller)
            break
        case .chooseTruck:
            let controller = ChooseTruckViewController.create()
            self.window?.set(root: controller)
            break
        
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNService.shared.configure()
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        if error.localizedDescription != "remote notifications are not supported in the simulator" {
            application.registerForRemoteNotifications()
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        print(#function)
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        print(#function)
        AccountManager.isApplicationTerminated = true
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        print(#function)
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        print(#function)
        AccountManager.isApplicationTerminated = false
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
    }
}

