//
//  RealmSwift.swift
//  driverapp
//
//  Created by Arben Pnishi on 07/02/2018.
//  Copyright © 2017 Zombie Soup. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftKeychainWrapper

protocol DetachableObject: AnyObject {
    func toObject() -> Self
}

extension Object: DetachableObject {
    func toObject() -> Self {
        let detached = type(of: self).init()
        for property in objectSchema.properties {
            guard let value = value(forKey: property.name) else { continue }
            if let detachable = value as? DetachableObject {
                detached.setValue(detachable.toObject(), forKey: property.name)
            } else {
                detached.setValue(value, forKey: property.name)
            }
        }
        return detached
    }
}

extension List: DetachableObject {
    func toObject() -> List<Element> {
        let result = List<Element>()
        forEach {
            result.append($0)
        }
        return result
    }
    
}

extension Object {
    @discardableResult
    func save() -> Success {
        return Realm.save(items: [self])
    }
    
    @discardableResult
    func delete() -> Success {
        return Realm.delete(items: [self])
    }
}

extension Object {
    
    static func find(_ primaryKey: Any) -> Self? {
        return instance().object(ofType: self, forPrimaryKey: primaryKey)
    }
    
    static func all<Element: Object>() -> Results<Element> {
        let items = instance().objects(Element.self)
        return items
    }
    
    static func `where`<Object>(predicate: NSPredicate) -> Results<Object> {
        let items: Results<Object> = instance().objects(self).filter(predicate) as! Results<Object>
        
        return items
    }
    
    @discardableResult
    static func deleteAll() -> Success  {
        let items: Results<Object> = instance().objects(self)
        
        return Realm.delete(items: items.toArray())
    }
    
    fileprivate static func instance() -> Realm {
        return Realm.instance()
    }
}
extension Realm {
    fileprivate static let KEY_REALM_SCHEMA_VERSION = "RGDA_Realm_Schema_Version"
    fileprivate static var schemaNumberVersion: NSNumber{
        get {
            return KeychainWrapper.standard.object(forKey: Realm.KEY_REALM_SCHEMA_VERSION) as? NSNumber ?? NSNumber.init(value: 0)
        }
        set {
            KeychainWrapper.standard.set(newValue, forKey: Realm.KEY_REALM_SCHEMA_VERSION)
        }
    }
    
    fileprivate static func instance() -> Realm {
        do {
            setRealmConfiguration()
            let realm = try Realm()
            return realm
            
        } catch {
            console("Realm Database: Migration Need, database is reseting.")
            Realm.schemaNumberVersion = NSNumber.init(value: Realm.schemaNumberVersion.uint64Value + 1)
            
            return instance()
        }
    }
    
    
    @discardableResult
    static func save(items: [Object]) -> Success {
        let instance = self.instance()
        
        do{
            try instance.write {
                items.forEach({
                    instance.create(type(of: $0), value: $0, update: true)
                })
            }
            return true
        }catch{
            return false
        }
        /*
         instance.beginWrite()
         
         items.forEach({
         instance.dynamicCreate(String(describing: $0.classForCoder), value: $0, update: true)
         })
         
         do {
         try instance.commitWrite()
         return true
         } catch {
         return false
         }*/
    }
    
    @discardableResult
    static func delete(items: [Object]) -> Success {
        let instance = self.instance()
        
        instance.beginWrite()
        
        for item in items {
            guard let primaryKey = item.objectSchema.primaryKeyProperty else { continue }
            guard let value = item.value(forKey: primaryKey.name) else { continue }
            
            if let object = instance.dynamicObject(ofType: String(describing: item.classForCoder), forPrimaryKey: value) {
                instance.delete(object)
            }
        }
        
        do {
            try instance.commitWrite()
            return true
        } catch {
            return false
        }
    }
    
    @discardableResult
    static func deleteAll() -> Success {
        let instance = self.instance()
        
        instance.beginWrite()
        
        instance.deleteAll()
        
        do {
            try instance.commitWrite()
            return true
        } catch {
            return false
        }
    }
    
    ///Increase SchemaVersion always when you change any object on any model!
    /// Set the new schema version. This must be greater than the previously used
    /// version (if you've never set a schema version before, the version is 0).
    private static func setRealmConfiguration(){
        
        let config = Realm.Configuration(
            
            schemaVersion: Realm.schemaNumberVersion.uint64Value,
            
            // Set the block which will be called automatically when opening a Realm with
            // a schema version lower than the one set above
            migrationBlock: { migration, oldSchemaVersion in
                // We haven’t migrated anything yet, so oldSchemaVersion == 0
                console("OLD: REALM SCHEMA VERSION:\(oldSchemaVersion)")
        })
        
        // Tell Realm to use this new configuration object for the default Realm
        Realm.Configuration.defaultConfiguration = config
        console("NEW: REALM SCHEMA VERSION: \(Realm.Configuration.defaultConfiguration.schemaVersion)")
    }
}

extension Array where Element: Object {
    @discardableResult
    func save() -> Success {
        return Realm.save(items: self)
    }
    
    @discardableResult
    func delete() -> Success {
        return Realm.delete(items: self)
    }
}

extension Results where Element: Object {
    func toArray() -> [Element] {
        return self.map({ item -> Element in
            return item.toObject()
        })
    }
}




