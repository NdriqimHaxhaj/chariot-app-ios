//
//  Date.swift
//  driverapp
//
//  Created by Arben Pnishi on 07/02/2018.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import SwiftDate

extension Date {
    
    var toPickupTime: String {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormater.timeZone = TimeZone.init(secondsFromGMT: 0)
        // dateFormater.doesRelativeDateFormat ting = false
        return dateFormater.string(from: self)
    }
    
    static func create(from text: String?) -> Date? {
        
        guard let text = text, !(text.count < 19) else {
            return nil
        }
        
        let index = text.index(text.startIndex, offsetBy: 19)
        let value = text.substring(to: index)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        
        if let date = dateFormatter.date(from: value) {
            let seconds = 0 //TimeZone.current.secondsFromGMT()
            return date.addingTimeInterval(TimeInterval(seconds))
        }
        
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSS'Z'"
        if let date = dateFormatter.date(from: text) {
            let seconds = TimeZone.current.secondsFromGMT()
            return date.addingTimeInterval(TimeInterval(seconds))
        }
        
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSS'Z'"
        if let date = dateFormatter.date(from: text) {
            let seconds = TimeZone.current.secondsFromGMT()
            return date.addingTimeInterval(TimeInterval(seconds))
        }
        
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        if let date = dateFormatter.date(from: text) {
            let seconds = TimeZone.current.secondsFromGMT()
            return date.addingTimeInterval(TimeInterval(seconds))
        }
        
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SS'Z'"
        if let date = dateFormatter.date(from: text) {
            let seconds = TimeZone.current.secondsFromGMT()
            return date.addingTimeInterval(TimeInterval(seconds))
        }
        
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        if let date = dateFormatter.date(from: text) {
            let seconds = TimeZone.current.secondsFromGMT()
            return date.addingTimeInterval(TimeInterval(seconds))
        }
        return nil
    }
    
    
    func isInSameWeek(date: Date) -> Bool {
        return Calendar.current.isDate(self, equalTo: date, toGranularity: .weekOfYear)
    }
    func isInSameMonth(date: Date) -> Bool {
        return Calendar.current.isDate(self, equalTo: date, toGranularity: .month)
    }
    func isInSameYear(date: Date) -> Bool {
        return Calendar.current.isDate(self, equalTo: date, toGranularity: .year)
    }
    func isInSameDay(date: Date) -> Bool {
        return Calendar.current.isDate(self, equalTo: date, toGranularity: .day)
    }
    func hasSame(_ components: Calendar.Component..., as date: Date, using calendar: Calendar = .autoupdatingCurrent) -> Bool {
        return components.filter { calendar.component($0, from: date) != calendar.component($0, from: self) }.isEmpty
    }
    func isNotInThePastFrom(date: Date) -> Bool{
        return self >= date
    }
    var isInThisWeek: Bool {
        return isInSameWeek(date: Date())
    }
    var isInToday: Bool {
        return Calendar.current.isDateInToday(self)
    }
    var isInTheFuture: Bool {
        return Date() < self
    }
    var isInThePast: Bool {
        return self < Date()
    }
    var dayNumberOfWeek: Int {
        return Calendar.current.dateComponents([.weekday], from: self).weekday ?? -8
    }
    var dayNumberOfMonth: Int {
        return Calendar.current.dateComponents([.day], from: self).day ?? -32
    }
    
    static func now() -> Date {
        let seconds = TimeZone.current.secondsFromGMT()
        let d = Date().addingTimeInterval(TimeInterval(seconds))
        return d
    }
    
    func getHoursAndMinutes() -> CGFloat{
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(identifier: "UTC")!
        let hours = calendar.component(.hour, from: self)
        let minutes = calendar.component(.minute, from: self)
        let hoursAndMinutes = CGFloat(hours) + (CGFloat(minutes)/CGFloat(60))
        
        return hoursAndMinutes
    }
    
    func getHMS(from date: Date) -> Date{
        var gregorian = Calendar(identifier: .gregorian)
        gregorian.timeZone = TimeZone(identifier: "UTC")!
        var myComponents = gregorian.dateComponents([.year, .month, .day], from: self)
        var components = gregorian.dateComponents([.hour, .minute, .second], from: date)
        
        // Change the time to 9:30:00 in your locale
        components.year = myComponents.year
        components.month = myComponents.month
        components.day = myComponents.day
        
        return (gregorian.date(from: components))!
    }
    
    func getDMY(from date: Date) -> Date{
        let gregorian = Calendar(identifier: .gregorian)
        var myComponents = gregorian.dateComponents([.hour, .minute, .second], from: self)
        var components = gregorian.dateComponents([.year, .month, .day], from: date)
        
        // Change the time to 9:30:00 in your locale
        components.hour = myComponents.hour
        components.minute = myComponents.minute
        components.second = myComponents.second
        
        return gregorian.date(from: components)!
    }
}

//
//  Date.swift
//  Superintendent
//
//  Created by Arben Pnishi on 01/02/2018.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//



enum Days: Int {
    case sun = 1
    case mon = 2
    case tue = 3
    case wed = 4
    case thu = 5
    case fri = 6
    case sat = 7
    
    var description: String {
        switch self {
        case .mon: return "Monday".localized
        case .tue: return "Tuesday".localized
        case .wed: return "Wednesday".localized
        case .thu: return "Thursday".localized
        case .fri: return "Friday".localized
        case .sat: return "Saturday".localized
        case .sun: return "Sunday".localized
        }
    }
}

let months: [String] = [ "January".localized, "February".localized, "March".localized, "April".localized, "May".localized, "June".localized, "July".localized, "August".localized, "September".localized, "October".localized, "November".localized, "December".localized]
let shortMonths: [String] = [ "Jan".localized, "Feb".localized, "Mar".localized, "Apr".localized, "May".localized, "Jun".localized, "Jul".localized, "Aug".localized, "Sep".localized, "Oct".localized, "Nov".localized, "Dec".localized]

extension Date {
    static func create(day: String) -> Date? {
        
        let calendar = Calendar.current
        
        let year = calendar.component(Calendar.Component.year, from: Date())
        let month = calendar.component(Calendar.Component.month, from: Date())
        let dayOfMonth = calendar.component(Calendar.Component.day, from: Date())
        
        let day: String = "\(year)-\(month)-\(dayOfMonth) ".appending(day)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if let date = dateFormatter.date(from: day) {
            return date
        }
        
        return nil
    }
}

extension Date{
    var toHHMM : String{
        let dateFormater = DateFormatter()
        dateFormater.timeStyle = .short
        
        return dateFormater.string(from: self)
    }
    
    var toReadableDate : String{
        get{
            let calendar = Calendar.current
            let comp = (calendar as NSCalendar).components([.hour, .minute, .day, .month, .year], from: self)
            
            let strHour = String(format: "%02d", comp.hour!)
            let strMinute = String(format: "%02d", comp.minute!)
            
            let month = months[comp.month! - 1]
            
            let thisYear = (Calendar.current as NSCalendar).component(.year, from: Date())
            let year = comp.year! < thisYear ? "\(String(describing: comp.year!))" : ""
            
            if(Calendar.current.isDateInToday(self)){
                return "Today".localized + " \(strHour):\(strMinute)"
            }
            else if(Calendar.current.isDateInYesterday(self)){
                return "Yesterday".localized + " \(strHour):\(strMinute)"
            }
            else{
                return "\(String(describing: comp.day!)) \(month) \(year)"
            }
            
        }
    }
    var toLocalized : String{
        get{
            let calendar = Calendar.current
            let comp = (calendar as NSCalendar).components([.day, .month, .year], from: self)
            
            let month = months[comp.month! - 1]
            
            let thisYear = (Calendar.current as NSCalendar).component(.year, from: Date())
            let year = comp.year! < thisYear ? "\(String(describing: comp.year!))" : "\(String(describing: comp.year!))"
            
            if(Calendar.current.isDateInToday(self)){
                return "Today".localized
            }else if(Calendar.current.isDateInYesterday(self)){
                return "Yesterday".localized
            }else if (Calendar.current.isDateInTomorrow(self)) {
                return "Tomorrow".localized
            }else{
                return "\(String(describing: comp.day!)) \(month) \(year)"
            }
        }
    }
    
    var toEventTime: String {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "HH:mm"
        dateFormater.timeZone = TimeZone.init(secondsFromGMT: 0)
        // dateFormater.doesRelativeDateFormatting = false
        return dateFormater.string(from: self)
    }

    var toShort: String {
        let dateFormater = DateFormatter()
        dateFormater.dateStyle = .long
        dateFormater.doesRelativeDateFormatting = false
        return dateFormater.string(from: self)
    }
    
    func timeAgoSinceDate(numericDates: Bool = false) -> String {
        let calendar = NSCalendar.current
        let unitFlags: Set<Calendar.Component> = [.minute, .hour, .day, .weekOfYear, .month, .year, .second]
        let now = Date()
        let earliest = now < self ? now : self
        let latest = (earliest == now) ? self : now
        let components = calendar.dateComponents(unitFlags, from: earliest,  to: latest)
        
        if (components.year! >= 2) {
            return "\(components.year!) " + "years ago".localized
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago".localized
            } else {
                return "Last year".localized
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) " + "months ago".localized
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month ago".localized
            } else {
                return "Last month".localized
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) " + "weeks ago".localized
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago".localized
            } else {
                return "Last week".localized
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) " + "days ago".localized
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago".localized
            } else {
                return "Yesterday".localized
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) " + "hours ago".localized
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago".localized
            } else {
                return "1 hour ago".localized
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) " + "minutes ago".localized
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 minute ago".localized
            } else {
                return "1 minute ago".localized
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) " + "seconds ago".localized
        } else {
            return "Just now".localized
        }
        
    }
    var toAgo: String {
        return ""
    }
    
    var toUtc: Double {
        return self.timeIntervalSince1970
    }
    
    var toString: String {
        let dateFormater = DateFormatter()
        dateFormater.dateStyle = .medium
        //        dateFormater.timeStyle = .short
        
        return dateFormater.string(from: self)
    }
    
    var toServer: String {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "yyyy-MM-dd"
        
        return dateFormater.string(from: self)
    }
    
    var tomorrow: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: self)!
    }
    
    static func create(from timeInterval: Double) -> Date {
        return Date(timeIntervalSince1970: timeInterval)
    }
    
    func offsetFrom(date : Date) -> String {
        
        let dayHourMinuteSecond: Set<Calendar.Component> = [.day, .hour, .minute, .second]
        let difference = NSCalendar.current.dateComponents(dayHourMinuteSecond, from: date, to: self);
        
//        let seconds = "\(difference.second ?? 0)s"
        let minutes = "\(difference.minute ?? 0)m" // + " " + seconds
        let hours = "\(difference.hour ?? 0)h" + " " + minutes
        let days = "\(difference.day ?? 0)d" + " " + hours
        
        if let day = difference.day, day          > 0 { return days }
        if let hour = difference.hour, hour       > 0 { return hours }
        if let minute = difference.minute, minute > 0 { return minutes }
//        if let second = difference.second, second > 0 { return seconds }
        return ""
    }

}
