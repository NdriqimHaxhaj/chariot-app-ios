//
//  Notifications.swift
//  driverapp
//
//  Created by Arben Pnishi on 07/02/2018.
//  Copyright © 2017 Zombie Soup. All rights reserved.
//

import UIKit

extension Notification.Name {
    static var showSettings = Notification.Name.init("showSettings")
    static var showProfile = Notification.Name.init("showProfile")
    static var showHome = Notification.Name.init("showHome")
    static var signOut = Notification.Name.init("signOut")
    static var changeMapTheme = Notification.Name.init("changeMapTheme")
    static var changeTheme = Notification.Name.init("changeTheme")
    static var downloadLanguage = Notification.Name.init("downloadLanguage")
    static var downloadedLanguage = Notification.Name.init("downloadedLanguage")
    static var refreshMapViewTableView = Notification.Name.init("refreshMapViewTableView")
    static var downloadVoiceLanguageProgress = Notification.Name.init("downloadVoiceLanguageProgress")
    static var uncompressVoiceLanguageProgress = Notification.Name.init("uncompressVoiceLanguageProgress")
    static var installVoiceLanguage = Notification.Name.init("installVoiceLanguage")
    static var requestPickupRequestWithId = Notification.Name.init("getPickupRequestWithId")
    static var acceptPickupRequest = Notification.Name.init("acceptPickupRequest")
    static var navigateToDropOff = Notification.Name.init("navigateToDropOff")
    static var dropOffCompleted = Notification.Name.init("dropOffCompleted")
    static var removeRouteFromMap = Notification.Name.init("removeRouteFromMap")
    static var enableNavigationButtons = Notification.Name.init("enableNavigationButtons")
    static var disableNavigationButtons = Notification.Name.init("disableNavigationButtons")
    static var schedulePickupRequest = Notification.Name.init("schedulePickupRequest")
    static var workingSwitchPressed = Notification.Name.init("workingSwitchPressed")
    static var navigationStarted = Notification.Name.init("navigationStarted")
    static var navigationStopped = Notification.Name.init("navigationStopped")
    static var transferPickups = Notification.Name.init("transferPickups")
    static var nextSliderPickup = Notification.Name.init("nextSliderPickup")
    static var previousSliderPickup = Notification.Name.init("previousSliderPickup")
    static var centerPickupsSlider = Notification.Name.init("centerPickupsSlider")
    static var refreshFavouriteAddresses = Notification.Name.init("refreshFavouriteAddresses")
    static var editPickup = Notification.Name.init("editPickup")
    static var updateNavigationManager = Notification.Name.init("updateNavigationManager")
    static var stopNavigation = Notification.Name.init("stopNavigation")
    static var navigationBoardLocationPressed = Notification.Name.init("navigationBoardLocationPressed")
    static var navigationBoardNotificationsButtonPressed = Notification.Name.init("navigationBoardNotificationsButtonPressed")
    static var navigationBoardSoundsButtonPressed = Notification.Name.init("navigationBoardSoundsButtonPressed")
    static var navigationBoardRoadDetailsPressed = Notification.Name.init("navigationBoardRoadDetailsPressed")
    static var roadDetailsOverviewPressed = Notification.Name.init("roadDetailsOverviewPressed")
    static var roadDetailsCompletedPressed = Notification.Name.init("roadDetailsCompletedPressed")
    static var roadDetailsCancelPressed = Notification.Name.init("roadDetailsCancelPressed")
}

extension UIViewController {
    func registerNotification(notification name: Notification.Name, selector: Selector) {
        NotificationCenter.default.addObserver(self, selector: selector, name: name, object: nil)
    }
    func postNotification(notification name: Notification.Name, object: Any?) {
        NotificationCenter.default.post(name: name, object: object)
    }
    func removeNotifications(){
        NotificationCenter.default.removeObserver(self)
    }
}
