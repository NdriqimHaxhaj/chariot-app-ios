//
//  String.swift
//  driverapp
//
//  Created by Arben Pnishi on 07/02/2018.
//  Copyright © 2017 Zombie Soup. All rights reserved.
//

import UIKit
import Localize_Swift

extension String{
    var localized: String{
        return self.localized()
    }
    var digits: String {
        return components(separatedBy: CharacterSet.decimalDigits.inverted)
            .joined()
    }
    
    static func localized(_ key: String?) -> String{
        var localizedString = ""
        
        if (key == nil) {
            return localizedString
        }
        if key!.isEmpty {
            return localizedString
        }
        localizedString = NSLocalizedString(key!, comment: "")
        if !localizedString.isEmpty{
            return localizedString
        }
        return localizedString
    }
    var trim : String {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    var isEmail: Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    func removingWhitespaces() -> String {
        return components(separatedBy: .whitespaces).joined()
    }
    
}
