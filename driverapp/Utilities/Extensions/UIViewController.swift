//
//  UIViewController.swift
//  driverapp
//
//  Created by Arben Pnishi on 07/02/2018.
//  Copyright © 2017 Zombie Soup. All rights reserved.
//

import UIKit
import MBProgressHUD
import Foundation
import ObjectiveC
import SideMenu

fileprivate var hudKey: UInt8 = 0

enum ApplicationPlace {
    case login
    case home
    case chooseTruck
}

extension UIViewController{
    
    fileprivate var hud: MBProgressHUD?{
        get{
            return objc_getAssociatedObject(self, &hudKey) as? MBProgressHUD
        }
        set{
            objc_setAssociatedObject(self, &hudKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
            newValue?.contentColor = Appearance.greenColor
            newValue?.bezelView.style = .solidColor
            newValue?.bezelView.backgroundColor = ThemeManager.isNightMode() ? Appearance.backgroundViewNight : .white
            newValue?.backgroundColor = .clear
        }
    }
    
    func showHud(_ message: String = ""){
        delay(delay: 0) {
            self.hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        }
    }
    
    func hideHud(){
        delay(delay: 0) {
            self.hud?.hide(animated: true)
        }
    }
    
    func push(_ controller: UIViewController, animated: Bool = true, hideBottomBar : Bool? = nil) {
        if let hbt = hideBottomBar {
            controller.hidesBottomBarWhenPushed = hbt
        }
        
        self.navigationController?.pushViewController(controller, animated: animated)
    }
    @objc func pop(_ animated: Bool = true) {
        if let controller = self.navigationController {
            controller.popViewController(animated: animated)
        }
    }
    func popToRoot(_ animated: Bool = true) {
        if let controller = self.navigationController {
            controller.popToRootViewController(animated: animated)
        }
    }
    
    func showModal(_ controller: UIViewController, animated: Bool = true, completion: (()->Void)? = nil) {
        self.present(controller, animated: animated, completion: completion)
    }
    
    func hideModal(_ animated: Bool = true, completion : (()->Void)? = nil) {
        self.dismiss(animated: animated, completion: completion)
    }
    
    func setTabBarVisible(_ visible:Bool, animated: Bool = true){
        if let tabBar = self.tabBarController?.tabBar {
            // get a frame calculation ready
            let frame = tabBar.frame
            let height = frame.size.height
            let diff = UIScreen.main.bounds.height - tabBar.frame.origin.y - tabBar.frame.height
            let offsetY = (visible ? (diff == 0 ? 0 : -height) : (diff == 0 ? height : 0))
            
            // zero duration means no animation
            let duration: TimeInterval = (animated ? 0.3 : 0.0)
            
            //  animate the tabBar
            UIView.animate(withDuration: duration, animations: {
                self.tabBarController?.tabBar.frame = frame.offsetBy(dx: 0, dy: offsetY)
            }, completion: { (completed) in
                self.tabBarController?.tabBar.isHidden = !visible
            })
        }
    }
    func shareFunction(shareContent: String) {
        
        let objectsToShare = [shareContent] as [Any]
        
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        activityVC.excludedActivityTypes = []
        if let wPPC = activityVC.popoverPresentationController {
            wPPC.sourceView = UIView.init()
            wPPC.sourceRect = CGRect.init()
        }
        let buttonTintBackup = UIButton.appearance().tintColor
        
        UIButton.appearance().tintColor = Appearance.greenColor
        
        self.present(activityVC, animated: true) {
            UIButton.appearance().tintColor = buttonTintBackup
        }
    }
    
    func switchWindowRoot(to place: ApplicationPlace){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.switchWindowRoot(to: place)
    }
    
    func setupSideMenu() {
        guard let navigation = self.navigationController else { return }
        SideMenuManager.defaultManager.menuLeftNavigationController = UIStoryboard.menu.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? UISideMenuNavigationController
        SideMenuManager.defaultManager.menuAnimationOptions = .curveEaseIn
        
        SideMenuManager.defaultManager.menuPresentMode = .menuSlideIn
        SideMenuManager.defaultManager.menuFadeStatusBar = false
        SideMenuManager.defaultManager.menuWidth = Constants.leftMenuWidth
        SideMenuManager.defaultManager.menuAddPanGestureToPresent(toView: navigation.navigationBar)
        SideMenuManager.defaultManager.menuAddScreenEdgePanGesturesToPresent(toView: navigation.view)
    }
    
    // MARK: - History Addresses
    func getHistoryAddresses()->[Address]{
        if let unarchivedData =  UserDefaults.standard.object(forKey: "addresses-history") as? Data {
            if let addresses = NSKeyedUnarchiver.unarchiveObject(with: unarchivedData) as? [Address] {
                return addresses
            } else {
                return []
            }
        } else {
            return []
        }
    }
    
    func saveHistoryAddresses(_ addresses:[Address]){
        let archivedData = NSKeyedArchiver.archivedData(withRootObject: addresses)
        UserDefaults.standard.setValue(archivedData, forKey: "addresses-history")
    }
    
    func clearAddressHistory(){
        UserDefaults.standard.removeObject(forKey: "addresses-history")
    }
    
    // MARK: Favourite Addresses
    func getFavouriteAddresses()->[FavouriteAddress]{
        if let unarchivedData =  UserDefaults.standard.object(forKey: "addresses-favourite") as? Data {
            if let addresses = NSKeyedUnarchiver.unarchiveObject(with: unarchivedData) as? [FavouriteAddress] {
                return addresses
            } else {
                return []
            }
        } else {
            return []
        }
    }
    
    func saveFavouriteAddresses(_ addresses:[FavouriteAddress]){
        let archivedData = NSKeyedArchiver.archivedData(withRootObject: addresses)
        UserDefaults.standard.setValue(archivedData, forKey: "addresses-favourite")
    }
    
    func clearFavouriteHistory(){
        UserDefaults.standard.removeObject(forKey: "addresses-favourite")
    }
    
    func checkThemeMode(){
        // If it's Auto mode
        if NavigationManager.mapView == MapViewType.Auto.rawValue {
            let date = Date()
            let calendar = Calendar.current
            let hour = calendar.component(.hour, from: date)
            if hour >= 7 && hour < 19 {
                ThemeManager.themeMode = Mode.Day.rawValue
                NavigationManager.mapTheme = 0
            } else {
                ThemeManager.themeMode = Mode.Night.rawValue
                NavigationManager.mapTheme = 1
            }
        } else {
            if NavigationManager.mapTheme == 0 {
                ThemeManager.themeMode = Mode.Day.rawValue
            } else if NavigationManager.mapTheme == 1{
                ThemeManager.themeMode = Mode.Night.rawValue
            }
        }
    }
    
    
}
