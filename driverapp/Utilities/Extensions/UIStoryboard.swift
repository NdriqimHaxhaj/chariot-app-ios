//
//  UIStoryboard.swift
//  driverapp
//
//  Created by Arben Pnishi on 07/02/2018.
//  Copyright © 2017 Zombie Soup. All rights reserved.
//

import UIKit

extension UIStoryboard {
    static var login: UIStoryboard {
        return UIStoryboard(name: "Login", bundle: nil)
    }
    static var profile: UIStoryboard {
        return UIStoryboard(name: "Profile", bundle: nil)
    }
    static var menu: UIStoryboard {
        return UIStoryboard(name: "Menu", bundle: nil)
    }
    static var settings: UIStoryboard {
        return UIStoryboard(name: "Settings", bundle: nil)
    }
    static var home: UIStoryboard {
        return UIStoryboard(name: "Home", bundle: nil)
    }
    static var schedule: UIStoryboard {
        return UIStoryboard(name: "Schedule", bundle: nil)
    }
}

extension UIStoryboard {
    func instantiate<T: UIViewController>(_ : T.Type, identifier: String? = nil) -> T {
        return self.instantiateViewController(withIdentifier: identifier ?? String(describing: T.self)) as! T
    }
}
