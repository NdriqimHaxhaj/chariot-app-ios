//
//  AccountManager.swift
//  driverapp
//
//  Created by Arben Pnishi on 07/02/2018.
//  Copyright © 2017 Zombie Soup. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper
import RealmSwift

typealias UserToken = String

class AccountManager: NSObject {
    fileprivate static let KEY_USER_ID = "RGD_user_id"
    fileprivate static let KEY_TOKEN  = "RGD_token"
    fileprivate static let KEY_BUILDING_ID = "RGD_building_id"
    fileprivate static let KEY_ACCOUNT_MANAGER_PHONE_NUMBER = "RGD_phone_number"
    fileprivate static let KEY_LAST_USED_EMAIL = "RGSI_last_used_email"
    fileprivate static let KEY_NOTIFICATIONS_SOUNDS = "Notifications_Sounds"
    fileprivate static let KEY_FIREBASE_TOKEN = "Firebase_token"
    fileprivate static let KEY_IS_APPLICATION_TERMINATED = "Is_application_terminated"
    fileprivate static let KEY_SHOW_SPEEDOMETER = "Show_speedometer"
    fileprivate static let KEY_SHOW_SPEED_LIMIT = "Show_speed_limit"
    fileprivate static let KEY_SPEED_ALERT_SOUND = "Speed_alert_sound"
    fileprivate static let KEY_WHEN_TO_SHOW_SPEED_ALERT = "When_to_show_speed_alert"
    
    static var currentUser: User? {
        get{
            let id = AccountManager.userId
            return  User.find(id)?.toObject()
        }
        set{
            newValue?.save()
        }
    }
    
    static var currentFirebaseToken:String?{
        get{
            return KeychainWrapper.standard.string(forKey: AccountManager.KEY_FIREBASE_TOKEN) ?? ""
        }
        set{
            KeychainWrapper.standard.set(newValue ?? "", forKey: AccountManager.KEY_FIREBASE_TOKEN)
        }
    }
    
    static var phoneNumber: String? {
        get{
            return KeychainWrapper.standard.string(forKey: AccountManager.KEY_ACCOUNT_MANAGER_PHONE_NUMBER)
        }
        set{
            KeychainWrapper.standard.set(newValue ?? "", forKey: AccountManager.KEY_ACCOUNT_MANAGER_PHONE_NUMBER)
        }
    }
    
    static var lastUsedEmail: String? {
        get{
            return KeychainWrapper.standard.string(forKey: AccountManager.KEY_LAST_USED_EMAIL)
        }
        set{
            KeychainWrapper.standard.set(newValue ?? "", forKey: AccountManager.KEY_LAST_USED_EMAIL)
        }
    }
    
    static var userToken: String? {
        get {
            return KeychainWrapper.standard.string(forKey: AccountManager.KEY_TOKEN)
        }
        set {
            KeychainWrapper.standard.set(newValue ?? "", forKey: AccountManager.KEY_TOKEN)
        }
    }
    
    static var userId: Int {
        get {
            if let id = KeychainWrapper.standard.integer(forKey: AccountManager.KEY_USER_ID){
                return id
            }else{
                return 0
            }
        }
        set {
            KeychainWrapper.standard.set(newValue, forKey: AccountManager.KEY_USER_ID)
        }
    }
    
    static var buildingId: Int {
        get {
            if let id = KeychainWrapper.standard.integer(forKey: AccountManager.KEY_BUILDING_ID){
                return id
            }else{
                return 0
            }
        }
        set {
            KeychainWrapper.standard.set(newValue, forKey: AccountManager.KEY_BUILDING_ID)
        }
    }
    
    static var isLogged: Bool {
        
        guard let token = userToken else {
            return false
        }
        
        if token.isEmpty {
            return false
        }
        
        if currentUser == nil {
            return false
        }

        if userId == 0 {
            return false
        }
        
        return true
    }
    
    
    static var notifications: Int? {
        get{
            return KeychainWrapper.standard.integer(forKey: AccountManager.KEY_NOTIFICATIONS_SOUNDS) ?? 1
        }
        set{
            KeychainWrapper.standard.set(newValue ?? 0, forKey: AccountManager.KEY_NOTIFICATIONS_SOUNDS)
        }
    }
    
    static var isApplicationTerminated: Bool? {
        get{
            return KeychainWrapper.standard.bool(forKey: KEY_IS_APPLICATION_TERMINATED) ?? false
        }
        set{
            KeychainWrapper.standard.set(newValue ?? false, forKey: KEY_IS_APPLICATION_TERMINATED)
        }
    }
    
    static var showSpeedometer: Bool? {
        get{
            return KeychainWrapper.standard.bool(forKey: KEY_SHOW_SPEEDOMETER) ?? true
        }
        set{
            KeychainWrapper.standard.set(newValue ?? true, forKey: KEY_SHOW_SPEEDOMETER)
        }
    }
    
    static var showSpeedLimit:String?{
        get{
            return KeychainWrapper.standard.string(forKey: AccountManager.KEY_SHOW_SPEED_LIMIT) ?? "Don't show"
        }
        set{
            KeychainWrapper.standard.set(newValue ?? "Don't show", forKey: AccountManager.KEY_SHOW_SPEED_LIMIT)
        }
    }
    
    static var playSpeedAlertSound: Bool? {
        get{
            return KeychainWrapper.standard.bool(forKey: KEY_SPEED_ALERT_SOUND) ?? true
        }
        set{
            KeychainWrapper.standard.set(newValue ?? true, forKey: KEY_SPEED_ALERT_SOUND)
        }
    }
    
    static var whenToShowSpeedAlert:Int? {
        get {
            return KeychainWrapper.standard.integer(forKey: AccountManager.KEY_WHEN_TO_SHOW_SPEED_ALERT) ?? 0
        }
        set {
            KeychainWrapper.standard.set(newValue ?? 0, forKey: AccountManager.KEY_WHEN_TO_SHOW_SPEED_ALERT)
        }
    }
    
    class func clear(){
        AccountManager.userToken = ""
        AccountManager.currentFirebaseToken = nil
        AccountManager.userId = 0
        AccountManager.buildingId = 0
        AccountManager.currentUser?.delete()
        AccountManager.showSpeedometer = true
        AccountManager.showSpeedLimit = "Don't show"
    }
    
    class func updateRootWindow(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.updateRootWindow()
    }
    
    //MARK: Other
    class func setValue(_ token: String, key: String){
        UserDefaults.standard.set(token, forKey: key)
        UserDefaults.standard.synchronize()
    }
    class func getValue(key : String) -> String{
        if(UserDefaults.standard.string(forKey: key) != nil){
            return UserDefaults.standard.string(forKey: key)!
        }
        return ""
    }
    
}
