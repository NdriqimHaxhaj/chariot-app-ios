//
//  NotificationsManager.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 7/31/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper


class NotificationsManager: NSObject {
    fileprivate static let KEY_ALLOW_ALL_NOTIFICATIONS = "Allow_all_notifications"
//    fileprivate static let KEY_ALLOW_ONE_HOUR_LEFT_NOTIFICATION = "Allow_one_hour_left_notification"
    fileprivate static let KEY_REVIEW_CLIENT_NOTIFICATION = "Review_client_notification"
    
    static var allowAllNotifications: Bool? {
        get{
            return KeychainWrapper.standard.bool(forKey: KEY_ALLOW_ALL_NOTIFICATIONS) ?? true
        }
        set{
            KeychainWrapper.standard.set(newValue ?? true, forKey: KEY_ALLOW_ALL_NOTIFICATIONS)
        }
    }
    
    static var reviewClientNotification: Bool? {
        get{
            return KeychainWrapper.standard.bool(forKey: KEY_REVIEW_CLIENT_NOTIFICATION) ?? true
        }
        set{
            KeychainWrapper.standard.set(newValue ?? true, forKey: KEY_REVIEW_CLIENT_NOTIFICATION)
        }
    }
    
//    static var allowOneHourLeftNotification: Bool? {
//        get{
//            return KeychainWrapper.standard.bool(forKey: KEY_ALLOW_ONE_HOUR_LEFT_NOTIFICATION) ?? true
//        }
//        set{
//            KeychainWrapper.standard.set(newValue ?? true, forKey: KEY_ALLOW_ONE_HOUR_LEFT_NOTIFICATION)
//        }
//    }
}
