//
//  NavigationManager.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 4/24/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import NMAKit
import SwiftKeychainWrapper
import SwiftyJSON

class NavigationManager: NSObject {
    // MARK: - Properties
    fileprivate static let KEY_FIRST_NAVIGATION_SETUP = "First_navigation_setup"
    fileprivate static let KEY_MAP_THEME = "Map_theme"
    fileprivate static let KEY_MAP_VIEW = "Map_view"
    fileprivate static let KEY_ROUTING_TYPE = "Routing_type"
    fileprivate static let KEY_TRANSPORT_MODE = "Transport_mode"
    // Routing options keys
    fileprivate static let KEY_AVOID_BOAT_FERRY = "Avoid_boat_ferry"
    fileprivate static let KEY_AVOID_DIRT_ROAD = "Avoid_dirt_road"
    fileprivate static let KEY_AVOID_HIGHWAY = "Avoid_highway"
    fileprivate static let KEY_AVOID_PARK = "Avoid_park"
    fileprivate static let KEY_AVOID_TOLL_ROAD = "Avoid_toll_road"
    fileprivate static let KEY_AVOID_TUNNEL = "Avoid_tunnel"
    fileprivate static let KEY_AVOID_CAR_SHUTTLE_TRAIN = "Avoid_car_shuttle_train"
    fileprivate static let KEY_CARPOOL = "Avoid_carpool"
    fileprivate static let KEY_ACTUAL_LANGUAGE_ID = "Actual_language_id"
    fileprivate static let KEY_IS_GOING_TO_DROP_OFF = "Is_going_to_drop_off"
    fileprivate static let KEY_LAST_DROP_OFF = "Last_drop_off_id"
    fileprivate static let KEY_OFFLINE_REASON_ID = "Offline_reason_id"
    fileprivate static let KEY_TRUCK_NAME = "Truck_name"
    fileprivate static let KEY_TRUCK_ID = "Truck_id"
    fileprivate static let KEY_SHOULD_REMEMBER_LAST_TRUCK = "Should_remember_last_truck"
    fileprivate static let KEY_TRUCK_TYPE_ID = "Truck_type_id"
    fileprivate static let KEY_TRUCK_HEIGHT = "Truck_height"
    fileprivate static let KEY_TRUCK_LENGTH = "Truck_length"
    fileprivate static let KEY_TRAFFIC_ENABLED = "Traffic_Enabled"
    /**
     
     Shortest = 0,
     Fastest = 1,
     Balanced = 3
     
     **/
    
    static var isFirstNavigationSetup: Bool {
        get {
            return KeychainWrapper.standard.bool(forKey: NavigationManager.KEY_FIRST_NAVIGATION_SETUP) ?? true
        }
        set {
            KeychainWrapper.standard.set(newValue, forKey: NavigationManager.KEY_FIRST_NAVIGATION_SETUP)
        }
    }
    
    static var isGoingToDropOff: Bool {
        get {
            return KeychainWrapper.standard.bool(forKey: NavigationManager.KEY_IS_GOING_TO_DROP_OFF) ?? false
        }
        set {
            KeychainWrapper.standard.set(newValue, forKey: NavigationManager.KEY_IS_GOING_TO_DROP_OFF)
        }
    }
    
    
    static var lastDropOffId: Int? {
        get {
            return KeychainWrapper.standard.integer(forKey: NavigationManager.KEY_LAST_DROP_OFF) ?? nil
        }
        set {
            KeychainWrapper.standard.set(newValue!, forKey: NavigationManager.KEY_LAST_DROP_OFF)
        }
    }
    
    /*
     1 - Take a break
     2 - Full capacity / Drop off
     3 - Truck inoperable
     4 - Leave truck / Shift over
     
     */
    static var offlineReasonId: Int {
        get {
            return KeychainWrapper.standard.integer(forKey: NavigationManager.KEY_OFFLINE_REASON_ID) ?? 0
        }
        set {
            KeychainWrapper.standard.set(newValue, forKey: NavigationManager.KEY_OFFLINE_REASON_ID)
        }
    }
    
    /**
     
     mapTheme options
     0 - Day
     1 - Night
     
    **/
    static var mapTheme: Int {
        get {
            return KeychainWrapper.standard.integer(forKey: NavigationManager.KEY_MAP_THEME)!
        }
        set {
            KeychainWrapper.standard.set(newValue, forKey: NavigationManager.KEY_MAP_THEME)
        }
    }
    // 0 - Auto
    // 1 - Manual
    static var mapView: Int {
        get {
            return KeychainWrapper.standard.integer(forKey: NavigationManager.KEY_MAP_VIEW) ?? 0
        }
        set {
            KeychainWrapper.standard.set(newValue, forKey: NavigationManager.KEY_MAP_VIEW)
        }
    }
    
    static var routingType: Int {
        get {
            return KeychainWrapper.standard.integer(forKey: NavigationManager.KEY_ROUTING_TYPE)!
        }
        set {
            KeychainWrapper.standard.set(newValue, forKey: NavigationManager.KEY_ROUTING_TYPE)
        }
    }
    
    static var trafficEnabled: Int {
        get {
            return KeychainWrapper.standard.integer(forKey: NavigationManager.KEY_TRAFFIC_ENABLED) ?? 1
        }
        set {
            KeychainWrapper.standard.set(newValue, forKey: NavigationManager.KEY_TRAFFIC_ENABLED)
        }
    }
    
     /**
     
     Car = 0,
     Pedestrian = 1,
     PublicTransport = 2,
     Track __attribute__((deprecated)) = 3,
     Truck = 5,
     UrbanMobility = 6,
     Bike = 7
     Scooter = 8
     **/
    
    static var transportMode: Int {
        get {
            return KeychainWrapper.standard.integer(forKey: NavigationManager.KEY_TRANSPORT_MODE)!
        }
        set {
            KeychainWrapper.standard.set(newValue, forKey: NavigationManager.KEY_TRANSPORT_MODE)
        }
    }
    
     /**
     AvoidBoatFerry          = 1 << 0, 1
     AvoidDirtRoad          = 1 << 1, 2
     AvoidHighway            = 1 << 2, 4
     AvoidPark               = 1 << 3, 8
     AvoidTollRoad           = 1 << 4, 16
     AvoidTunnel             = 1 << 5, 32
     AvoidCarShuttleTrain    = 1 << 6, 64
     AvoidCarpool            = 1 << 7, 128
     **/

    // Routing options variables
    static var avoidBoatFerry : Bool {
        get {
            return KeychainWrapper.standard.bool(forKey: NavigationManager.KEY_AVOID_BOAT_FERRY) ?? false
        }
        set {
            KeychainWrapper.standard.set(newValue, forKey: NavigationManager.KEY_AVOID_BOAT_FERRY)
        }
    }
    
    static var avoidDirtRoad : Bool {
        get {
            return KeychainWrapper.standard.bool(forKey: NavigationManager.KEY_AVOID_DIRT_ROAD) ?? false
        }
        set {
            KeychainWrapper.standard.set(newValue, forKey: NavigationManager.KEY_AVOID_DIRT_ROAD)
        }
    }
    
    static var avoidHighway : Bool {
        get {
            return KeychainWrapper.standard.bool(forKey: NavigationManager.KEY_AVOID_HIGHWAY) ?? false
        }
        set {
            KeychainWrapper.standard.set(newValue, forKey: NavigationManager.KEY_AVOID_HIGHWAY)
        }
    }
    
    static var avoidPark : Bool {
        get {
            return KeychainWrapper.standard.bool(forKey: NavigationManager.KEY_AVOID_PARK) ?? false
        }
        set {
            KeychainWrapper.standard.set(newValue, forKey: NavigationManager.KEY_AVOID_PARK)
        }
    }
    
    static var avoidTollRoad : Bool {
        get {
            return KeychainWrapper.standard.bool(forKey: NavigationManager.KEY_AVOID_TOLL_ROAD) ?? false
        }
        set {
            KeychainWrapper.standard.set(newValue, forKey: NavigationManager.KEY_AVOID_TOLL_ROAD)
        }
    }
    
    static var avoidTunnel : Bool {
        get {
            return KeychainWrapper.standard.bool(forKey: NavigationManager.KEY_AVOID_TUNNEL) ?? false
        }
        set {
            KeychainWrapper.standard.set(newValue, forKey: NavigationManager.KEY_AVOID_TUNNEL)
        }
    }
    
    static var avoidCarShuttleTrain : Bool {
        get {
            return KeychainWrapper.standard.bool(forKey: NavigationManager.KEY_AVOID_CAR_SHUTTLE_TRAIN) ?? false
        }
        set {
            KeychainWrapper.standard.set(newValue, forKey: NavigationManager.KEY_AVOID_CAR_SHUTTLE_TRAIN)
        }
    }
    
    static var avoidCarpool : Bool {
        get {
            return KeychainWrapper.standard.bool(forKey: NavigationManager.KEY_CARPOOL) ?? false
        }
        set {
            KeychainWrapper.standard.set(newValue, forKey: NavigationManager.KEY_CARPOOL)
        }
    }
    
    static var actualLanguageId : Int {
        get {
            return KeychainWrapper.standard.integer(forKey: NavigationManager.KEY_ACTUAL_LANGUAGE_ID) ?? 0
        }
        set {
            KeychainWrapper.standard.set(newValue, forKey: NavigationManager.KEY_ACTUAL_LANGUAGE_ID)
        }
    }
    
    // This will save the selected truck name on Home View Controller
    static var vehicleName: String {
        get {
            return KeychainWrapper.standard.string(forKey: NavigationManager.KEY_TRUCK_NAME) ?? ""
        }
        set {
            KeychainWrapper.standard.set(newValue, forKey: NavigationManager.KEY_TRUCK_NAME)
        }
    }
    
    static var vehicleID: Int {
        get {
            if let id = KeychainWrapper.standard.integer(forKey: NavigationManager.KEY_TRUCK_ID){
                return id
            }else{
                return 0
            }
        }
        set {
            KeychainWrapper.standard.set(newValue, forKey: NavigationManager.KEY_TRUCK_ID)
        }
    }
    
    static var shouldRememberLastTruck: Bool {
        get {
            return KeychainWrapper.standard.bool(forKey: NavigationManager.KEY_SHOULD_REMEMBER_LAST_TRUCK) ?? true
        }
        set {
            KeychainWrapper.standard.set(newValue, forKey: NavigationManager.KEY_SHOULD_REMEMBER_LAST_TRUCK)
        }
    }
    
    /*
         Truck types:
         Recyclable = "1"
         Garbage = "2"
     */
    
    static var vehicleTypeID: String {
        get {
            return KeychainWrapper.standard.string(forKey: NavigationManager.KEY_TRUCK_TYPE_ID) ?? ""
        }
        set {
            KeychainWrapper.standard.set(newValue, forKey: NavigationManager.KEY_TRUCK_TYPE_ID)
        }
    }
    
    static var truckHeight : Int {
        get {
            return KeychainWrapper.standard.integer(forKey: NavigationManager.KEY_TRUCK_HEIGHT) ?? 0
        }
        set {
            KeychainWrapper.standard.set(newValue, forKey: NavigationManager.KEY_TRUCK_HEIGHT)
        }
    }
    
    static var truckLength : Int {
        get {
            return KeychainWrapper.standard.integer(forKey: NavigationManager.KEY_TRUCK_LENGTH) ?? 0
        }
        set {
            KeychainWrapper.standard.set(newValue, forKey: NavigationManager.KEY_TRUCK_LENGTH)
        }
    }
    
    public static func defaultOptions(){
        mapTheme = 0
        mapView = 0
        routingType = 1
        transportMode = 5
        trafficEnabled = 1
        avoidBoatFerry = false
        avoidDirtRoad = false
        avoidHighway = false
        avoidPark  = false
        avoidTollRoad = true
        avoidTunnel = false
        avoidCarShuttleTrain = false
        avoidCarpool = false
    }
    
    public static func getRoutingOptions()->NMARoutingOption{
        
        if avoidBoatFerry {
            return NMARoutingOption.avoidBoatFerry
        }
        if avoidDirtRoad {
            return NMARoutingOption.avoidDirtRoad
        }
        if avoidHighway {
            return NMARoutingOption.avoidHighway
        }
        if avoidPark {
            return NMARoutingOption.avoidPark
        }
        if avoidTollRoad {
            return NMARoutingOption.avoidTollRoad
        }
        if avoidTunnel {
            return NMARoutingOption.avoidTunnel
        }
        if avoidCarShuttleTrain {
            return NMARoutingOption.avoidCarShuttleTrain
        }
        if avoidCarpool {
            return NMARoutingOption.avoidCarpool
        }
        
        return NMARoutingOption.init(rawValue: 0)
    }
    
    public static func getRoutineType(){
        
    }
    
    public static func clear(){
        self.vehicleID = 0
        self.vehicleName = ""
        self.shouldRememberLastTruck = false
        self.vehicleTypeID = ""
        self.truckHeight = 0
        self.truckLength = 0
    }
    
    class func applySettings(from json: JSON){
        if let avoidBoatFerry = json["avoidBoatFerry"].bool {
            self.avoidBoatFerry = avoidBoatFerry
        } else {
            self.avoidBoatFerry = false
        }
        
        if let avoidDirtRoad = json["avoidDirtRoad"].bool {
            self.avoidDirtRoad = avoidDirtRoad
        } else {
            self.avoidDirtRoad = false
        }
        
        if let avoidHighway = json["avoidHighway"].bool {
            self.avoidHighway = avoidHighway
        } else {
            self.avoidHighway = false
        }
        
        if let avoidPark = json["avoidPark"].bool {
            self.avoidPark = avoidPark
        } else {
            self.avoidPark  = false
        }
        
        if let avoidTollRoad = json["avoidTollRoad"].bool {
            self.avoidTollRoad = avoidTollRoad
        } else {
            self.avoidTollRoad = true
        }
        if let avoidTunnel = json["avoidTunnel"].bool {
          self.avoidTunnel = avoidTunnel
        } else {
            self.avoidTunnel = false
        }
        
        if let avoidCarShuttleTrain = json["avoidCarShuttleTrain"].bool {
            self.avoidCarShuttleTrain = avoidCarShuttleTrain
        } else {
            self.avoidCarShuttleTrain = false
        }
        
        if let avoidCarpool = json["avoidCarpool"].bool {
            self.avoidCarpool = avoidCarpool
        } else {
            self.avoidCarpool = false
        }
    }
    
    class func applyAttributes(from json: JSON){
        if let heightString = json["height"].string {
            let height = NSString(string: heightString).integerValue
            self.truckHeight = height
        }
        if let lengthString = json["length"].string {
            let length = NSString(string: lengthString).integerValue
            self.truckLength = length
        }
    }
    
}












