//
//  PickupStatusManager.swift
//  driverapp
//
//  Created by Arben Pnishi on 26/02/2018.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

class PickupStatusManager: NSObject {
    static let shared = PickupStatusManager()
    var statuses: [PickupStatus] = []
    
    static func sync(){
        PickupStatusManager.shared.statuses = PickupStatus.all().toArray()
        getStatuses()
    }
    
    fileprivate static func getStatuses(){
        PickupREST.getStatuses { (statuses, error) in
            if let statuses = statuses{
                PickupStatusManager.shared.statuses = statuses
                PickupStatusManager.shared.statuses.save()
            }
        }
    }
    
    static func getStatusesOf(type: PickupType) -> [PickupStatus] {
        return PickupStatusManager.shared.statuses.filter({ (item) -> Bool in
            return item.pType == type
        })
    }
}
