//
//  LanguageManager.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 4/20/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import Localize_Swift

enum LanguageType: String{
    case english = "en"
    case spanish = "es"
    
    var description: String{
        switch self {
        case .english:
            return "English"
            
        case .spanish:
            return "Español"
        }
    }
}

class LanguageManager: NSObject {
    static var current: LanguageType{
        return LanguageType.init(rawValue: Localize.currentLanguage())!
    }
    
    static func setLanguage(to language: String, reload: Bool = true){
        guard language != Localize.currentLanguage() else { return }
        
        Localize.setCurrentLanguage(language)
        UserDefaults.standard.set([language], forKey: "AppleLanguages")
        UserDefaults.standard.synchronize()
        
        if reload{
            AccountManager.updateRootWindow()
        }
    }
    
    static func setSystemsLanguage() {
        let userDefaults = UserDefaults.standard
        let value = userDefaults.bool(forKey: "languageSetBefore")
        if value == false {
            userDefaults.set(true, forKey: "languageSetBefore")
            userDefaults.synchronize()
            
            if let systemLanguage = Locale.current.languageCode, let langType = LanguageType(rawValue: systemLanguage){
                LanguageManager.setLanguage(to: langType.rawValue, reload: true)
            }
        }
    }
}

