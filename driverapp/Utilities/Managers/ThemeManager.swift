//
//  ThemeManager.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 5/25/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

enum Mode:Int {
    case Day = 0
    case Night = 1
}

class ThemeManager: NSObject {
    fileprivate static let KEY_THEME = "Theme"
    
    static var themeMode: Int {
        get {
            return KeychainWrapper.standard.integer(forKey: ThemeManager.KEY_THEME) ?? 0
        }
        set {
            KeychainWrapper.standard.set(newValue, forKey: ThemeManager.KEY_THEME)
        }
    }
    
    class func isNightMode()->Bool{
        return self.themeMode == Mode.Night.rawValue
    }
    
    class func isDayMode()->Bool{
        return self.themeMode == Mode.Day.rawValue
    }
    
    class func autoCheck(){
        let date = Date()
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        if hour >= 7 && hour < 19 {
            self.themeMode = Mode.Day.rawValue
        } else {
            self.themeMode = Mode.Night.rawValue
        }
    }
}
