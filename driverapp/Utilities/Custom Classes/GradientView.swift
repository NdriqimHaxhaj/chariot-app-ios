//
//  GradientView.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 10/16/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

@IBDesignable
class GradientView: UIView {
    
    private var gradientLayer = CAGradientLayer()
    private var vertical: Bool = false
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        // Drawing code
        
        //fill view with gradient layer
        gradientLayer.frame = self.bounds
        
        //style and insert layer if not already inserted
        if gradientLayer.superlayer == nil {
            
            gradientLayer.startPoint = CGPoint(x: 0, y: 0)
            gradientLayer.endPoint = vertical ? CGPoint(x: 0, y: 1) : CGPoint(x: 1, y: 0)
            let color1 = UIColor(red: 10/255, green: 128/255, blue: 54/255, alpha: 1)
            let color2 = UIColor(red: 62/255, green: 193/255, blue: 79/255, alpha: 1)
            gradientLayer.colors = [color1, color2]
            gradientLayer.locations = [0.0, 1.0]
            
            self.layer.insertSublayer(gradientLayer, at: 0)
        }
    }
    
}
