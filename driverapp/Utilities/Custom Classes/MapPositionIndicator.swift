//
//  MapPositionIndicator.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 10/8/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

class MapPositionIndicator: UIView {
    
    // MARK: - Properties
    var mainCircle = UIView()
    var secondCircle = UIView()
    var thirdCircle = UIView()
    var fourthCircle = UIView()
    
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        mainCircle = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        mainCircle.center = self.center
        mainCircle.backgroundColor = Appearance.greenColor
        mainCircle.layer.borderWidth = 0.1
        mainCircle.layer.borderColor = Appearance.groupTableGray.cgColor
        
        let arrowImage = #imageLiteral(resourceName: "navigation arrow")
        let arrowImageViewFrame = CGRect(x: 0,
                                         y: 0,
                                         width: 8,
                                         height: 6)
        let arrowImageView = UIImageView(frame: arrowImageViewFrame)
        arrowImageView.image = arrowImage
        arrowImageView.center = CGPoint(x: mainCircle.bounds.midX,
                                        y: mainCircle.bounds.midY)
        mainCircle.addSubview(arrowImageView)
        mainCircle.bringSubview(toFront: arrowImageView)
        
        
        
        mainCircle.layer.cornerRadius = mainCircle.frame.height/2
        let secondCircle = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.width-60, height: self.frame.height-60))
        secondCircle.backgroundColor = Appearance.greenColor
        secondCircle.layer.cornerRadius = secondCircle.frame.height/2
        secondCircle.center = self.center
        
        let thirdCircle = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.width-30, height: self.frame.height-30))
        thirdCircle.backgroundColor = Appearance.greenColor
        thirdCircle.layer.cornerRadius = thirdCircle.frame.height/2
        thirdCircle.center = self.center
        
        let fourthCircle = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height))
        fourthCircle.backgroundColor = Appearance.greenColor
        fourthCircle.layer.cornerRadius = fourthCircle.frame.height/2
        fourthCircle.center = self.center
        
        secondCircle.alpha = 0.5
        thirdCircle.alpha = 0.5
        fourthCircle.alpha = 0.5
        
        addSubview(fourthCircle)
        addSubview(thirdCircle)
        addSubview(secondCircle)
        addSubview(mainCircle)
        animate(secondCircle, duration: 2)
        
        animate(thirdCircle,
                duration: 2,
                scaleAnimatiomFromValue: 0,
                scaleAnimationToValue: 1,
                alphaAnimationFromValue: 0,
                alphaAnimationToValue: 0.4)
        
        animate(fourthCircle,
                duration: 2,
                scaleAnimatiomFromValue: 0,
                scaleAnimationToValue: 1,
                alphaAnimationFromValue: 0.5,
                alphaAnimationToValue: 0.6)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    // MARK: - Functions
    func animate(_ firstView:UIView,
                 duration:CFTimeInterval,
                 scaleAnimatiomFromValue: CGFloat = 0,
                 scaleAnimationToValue:CGFloat = 1,
                 alphaAnimationFromValue: CGFloat = 0,
                 alphaAnimationToValue: CGFloat = 1){
        let scaleAnimation = CABasicAnimation(keyPath: "transform.scale")
        scaleAnimation.fromValue = scaleAnimatiomFromValue
        scaleAnimation.toValue = scaleAnimationToValue
        
        let alphaAnimation = CABasicAnimation(keyPath: "opacity")
        alphaAnimation.fromValue = 1
        alphaAnimation.toValue = 0
        
        let animations = CAAnimationGroup()
        animations.duration = duration
        animations.repeatCount = Float.infinity
        animations.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        animations.animations = [scaleAnimation, alphaAnimation]
        
        firstView.layer.add(animations, forKey: "animations")
    }
}
