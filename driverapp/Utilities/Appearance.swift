//
//  Appearance.swift
//  driverapp
//
//  Created by Arben Pnishi on 07/02/2018.
//  Copyright © 2017 Zombie Soup. All rights reserved.
//

import UIKit

class Appearance: NSObject {
    static var groupTableGray = UIColor(red255: 242, green255: 242, blue255: 242)
    static var darkOrangeColor = UIColor(red255: 208, green255: 92, blue255: 21)
    static var orangeColor = UIColor.orange
    static var greenColor = UIColor(red255: 62, green255: 193, blue255: 79)
    static var redColor = UIColor(red255: 255, green255: 107, blue255: 107)
    static var darkGrayColor = UIColor(red255: 92, green255: 104, blue255: 102)
    static var grayColor = UIColor(red255: 86, green255: 86, blue255: 86)
    static var lightGray = UIColor(red255: 235, green255: 235, blue255: 235)
    static var salmonColor = UIColor(red255: 255, green255: 107, blue255: 107)
    static var receivedMessageTextColor = UIColor(red255: 71, green255: 73, blue255: 85)
    static var receivedMessageBackgroundColor = UIColor(red255: 217, green255: 217, blue255: 217)
    static var cancellPickupGray = UIColor(red255: 134, green255: 134, blue255: 134)
    static var canceledRed = UIColor(red255: 255, green255: 107, blue255: 107)
    static var textFieldNight = UIColor(red255: 93, green255: 93, blue255: 100)
    static var backgroundViewNight = UIColor(red255: 45, green255: 45, blue255: 46)
    static var darkGrayNight = UIColor(red255:61, green255:61, blue255:70)
    static var onColor = UIColor(red255: 134, green255: 134, blue255: 134)
    static var offColor = UIColor(red255: 182, green255: 182, blue255: 182)
    static var selectedTimeLabelColor = UIColor(red255: 240, green255: 240, blue255: 240)
    static var grayOutText = UIColor(red255: 182, green255: 182, blue255: 182)
    static var grayText = UIColor(red255: 134, green255: 134, blue255: 134)
    static var completedPickupGreen = UIColor(red255: 186, green255: 233, blue255: 192)
    static var completedGreenFont = UIColor(red255: 10, green255: 128, blue255: 54)
    static var defaultGrayBorder = UIColor(red255: 217, green255: 217, blue255: 217)
    
    
    static func setup() {
        
        let nav = UINavigationBar.appearance()
        let isNightMode = ThemeManager.isNightMode()
        
        nav.isTranslucent = false
        
        if isNightMode {
            nav.barTintColor = Appearance.darkGrayNight
        }
        
        nav.titleTextAttributes = [NSAttributedStringKey.font: Font.robotoBoldFont(size: 17), NSAttributedStringKey.foregroundColor: isNightMode ? UIColor.white : UIColor.darkGray]
        
        
        let barAppearance = UIBarButtonItem.appearance()
        barAppearance.setBackButtonTitlePositionAdjustment(UIOffsetMake(0, 0), for:UIBarMetrics.default)
    }
}
