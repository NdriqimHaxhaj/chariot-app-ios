//
//  Preferences.swift
//  driverapp
//
//  Created by Arben Pnishi on 23/02/2018.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

class Preferences: NSObject {
    fileprivate static let KEY_IS_DRIVER_WORKING = "Is_user_working"
    
    static var isWorkingStatusOn: Bool {
        get {
            return KeychainWrapper.standard.bool(forKey: Preferences.KEY_IS_DRIVER_WORKING) ?? true
        }
        set {
            KeychainWrapper.standard.set(newValue, forKey: Preferences.KEY_IS_DRIVER_WORKING)
        }
    }
}
