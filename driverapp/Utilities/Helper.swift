//
//  Helper.swift
//  driverapp
//
//  Created by Arben Pnishi on 07/02/2018.
//  Copyright © 2017 Zombie Soup. All rights reserved.
//

import UIKit

func console(_ items: Any..., separator: String = " ", terminator: String = "\n") {
    #if DEBUG
        print(items[0], separator: separator, terminator: terminator)
    #endif
}

func delay(delay: Double, closure: @escaping ()->()) {
    DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
        closure()
    }
}

func dispatch(_ closure: @escaping ()->()) {
    delay(delay: 0.0, closure: closure)
}


class Helper: NSObject {
    class func showAlertView(title: String, message: String, viewController: UIViewController){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler:{
            (ACTION :UIAlertAction!)in
        }))
        viewController.present(alert, animated: true, completion: nil)
    }
    
    class func getCurrentMillis() -> Int64 {
        return Int64(Date().timeIntervalSince1970 * 1000)
    }
}
