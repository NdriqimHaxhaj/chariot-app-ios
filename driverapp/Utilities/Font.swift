//
//  Font.swift
//  driverapp
//
//  Created by Arben Pnishi on 07/02/2018.
//  Copyright © 2017 Zombie Soup. All rights reserved.
//

import UIKit

class Font: NSObject {
    static func robotoRegularFont(size: CGFloat = 16) -> UIFont {
        if let roboto = UIFont(name: "Roboto-Regular", size: size) {
            return roboto
        }
        
        return UIFont.systemFont(ofSize: size)
    }
    
    static func robotoMediumFont(size: CGFloat = 16) -> UIFont {
        if let roboto = UIFont(name: "Roboto-Medium", size: size) {
            return roboto
        }
        
        return UIFont.systemFont(ofSize: size)
    }
    
    static func robotoBoldFont(size: CGFloat = 16) -> UIFont {
        if let roboto = UIFont(name: "Roboto-Bold", size: size) {
            return roboto
        }
        
        return UIFont.systemFont(ofSize: size)
    }
}
