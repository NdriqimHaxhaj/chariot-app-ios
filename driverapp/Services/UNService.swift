//
//  UNService.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 7/17/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import FirebaseMessaging
import UserNotifications


class UNService: NSObject {
    
    // MARK: - Helper Properties
    var homeController:HomeController? {
        guard let homeController = window?.rootViewController?.childViewControllers[0] as? HomeController else {
            return nil
        }
        return homeController
    }
    
    // MARK: - Properties
    private override init() {}
    static let shared = UNService()
    let userNotificationCenter = UNUserNotificationCenter.current()
    var window:UIWindow?
    
    
    // MARK: - Functions
    func authorize(){
        let options : UNAuthorizationOptions = [.alert, .badge, .sound]
        userNotificationCenter.requestAuthorization(options: options) { (granted, error) in
            print(error ?? "No authorization error")
            guard granted else {return}
            DispatchQueue.main.async {
                self.configure()
            }
        }
    }
    
    func configure(){
        userNotificationCenter.delegate = self
        Messaging.messaging().delegate = self
        
        let application = UIApplication.shared
        application.registerForRemoteNotifications()
        
        //Get AppDelegate window reference
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        window = appDelegate.window
    }
}
@available(iOS 10, *)
extension UNService: UNUserNotificationCenterDelegate {
    // Firebase notification received

    func userNotificationCenter(_ center: UNUserNotificationCenter,  willPresent notification: UNNotification, withCompletionHandler   completionHandler: @escaping (_ options:   UNNotificationPresentationOptions) -> Void) {
        // custom code to handle push while app is in the foreground
        print("Handle push from foreground, received: \n \(notification.request.content)")
        
        if let notificationId = (notification.request.content.userInfo["id_notification_type"] as? NSString)?.integerValue {
            if NotificationTypeList.init(notificationId) != .rateDriver {
                refreshPickupsOnNotificationReceive()
            }
        }
        
        guard let dict = notification.request.content.userInfo["aps"] as? NSDictionary else { return }
        if let alert = dict["alert"] as? [String: String] {
            let body = alert["body"]!
            let title = alert["title"]!
            print("Title:\(title) + body:\(body)")
            putBadgeOnNotificationsButton(title)
            //            self.showAlertAppDelegate(title: title, message: body, buttonTitle: "ok", window: self.window!)
        } else if let alert = dict["alert"] as? String {
            print("Text: \(alert)")
//            self.showAlertAppDelegate(title: alert, message: "", buttonTitle: "ok", window: self.window!)
        }
        guard let idNotificationType = (notification.request.content.userInfo["id_notification_type"] as? NSString)?.integerValue else {
            return
        }
        if NotificationTypeList.init(idNotificationType) == .rateDriver {
            handleNotification(with: idNotificationType,
                               data: notification.request.content.userInfo)
        }
    }
    
    func handleNotification(with id:Int, data: [AnyHashable:Any]){
        let notificationType = NotificationTypeList.init(id)
        switch notificationType {
//        case .reschedulePickup:
//            print("Pickup Rescheduled")
//            break
        case .replyFeedback:
            print("Feedback Reply")
            break
        case .droppedOffPickup:
            print("Dropped Off Pickup")
            break
        case .oneHourLeft:
            print("One Hour Left")
            break
        case .pickupAccepted:
            print("Pickup Accepted")
            break
            case .driverAssignPickup:
                if let id = (data["id"] as? NSString)?.integerValue {
                    self.openPickupProfile(id)
                }
            print("Driver Assign Pickup")
        case .newMessageDriver:
            self.openMessageCenter()
            print("New message to Driver")
        case .pickupRescheduledDriver:
            if let id = (data["id_pickup_handler"] as? NSString)?.integerValue {
                self.openPickupProfile(id)
            }
            print("Pickup rescheduled")
        case .newPickupNoteDriver:
            if let id = (data["id"] as? NSString)?.integerValue {
                self.openPickupNotesViewController(id)
            }
            print("New Pickup Note Driver")
        case .rateDriver:
            delay(delay: 1) {
                self.showRatingViewController(with: data)
            }
            break
        default:
            break
        }
    }
    
    func openPickupNotesViewController(_ pickupId:Int){
        homeController?.openPickupNotesViewVontroller(pickupId)
    }
    
    func openPickupProfile(_ pickupId:Int){
     homeController?.openPickupProfile(pickupId)
    }
    
    func openMessageCenter(){
        homeController?.openMessageCenter()
    }
    
    func showRatingViewController(with data: [AnyHashable:Any]){
        homeController?.showReviewForm(with: data)
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        print("Handle tapped push from background, received: \n \(response.notification.request.content)")
        refreshPickupsOnNotificationReceive()
        guard let idNotificationType = (response.notification.request.content.userInfo["id_notification_type"] as? NSString)?.integerValue else {
            return
        }
        handleNotification(with: idNotificationType,
                           data: response.notification.request.content.userInfo)
        completionHandler()
    }
    
    func showAlertAppDelegate(title: String, message: String, buttonTitle: String, window: UIWindow) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: buttonTitle, style: .default, handler: nil))
        window.rootViewController?.present(alert, animated: false, completion: nil)
    }
    
    func refreshPickupsOnNotificationReceive(){
        
        // Refresh pickups when a notification is received
        guard let homeListController = (window?.rootViewController as? UINavigationController)?.viewControllers[0].childViewControllers[1] as? HomeListController else {
            print("Couldn't get homeListController")
            return
        }
        homeListController.getPickups { (success) in
            print("Notification received. Pickups refreshed successfully.")
        }
    }
    
    func putBadgeOnNotificationsButton(_ text:String){
        // Put a badge on Notifications when a notification is received
        guard let homeMapController = (window?.rootViewController as? UINavigationController)?.viewControllers[0].childViewControllers[0] as? HomeMapController else {
            print("Couldn't get homeMapController")
            return
        }
        
        homeMapController.notificationButton.badgeString = "1"
        homeMapController.notificationButton.badgeEdgeInsets = UIEdgeInsetsMake(20, 5, 0, 20)
        homeMapController.showNotificationAlert(text)
    }
}

//Firebase Messaging Delegate
extension UNService: MessagingDelegate {
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        NSLog("[RemoteNotification] didRefreshRegistrationToken: \(fcmToken)")
        guard let token = AccountManager.currentFirebaseToken else {
            return
        }
        if token != fcmToken {
            AccountManager.currentFirebaseToken = fcmToken
            UserREST.updateFirebaseToken(token: fcmToken) {
                print("Token updated")
            }
        }
    }
    
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
        guard let token = AccountManager.currentFirebaseToken else {return}
        if token.isEmpty || token != fcmToken {
            AccountManager.currentFirebaseToken = fcmToken
            UserREST.updateFirebaseToken(token: fcmToken) {
                print("Token updated")
            }
        }
    }
    // [END refresh_token]
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    // [END ios_10_data_message]
    
    func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
        print("Notification '\(notification)")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        print("Userinfo \(userInfo)")
    }
    
    // The callback to handle data message received via FCM for devices running iOS 10 or above.
    func application(received remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage.appData)
    }
    
    func application(application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        Messaging.messaging().apnsToken = deviceToken as Data
    }
}

