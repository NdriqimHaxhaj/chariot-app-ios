//
//  FirebaseService.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 3/14/19.
//  Copyright © 2019 Zombie Soup. All rights reserved.
//

import UIKit
import Firebase
import FirebaseMessaging

class FirebaseService: NSObject {
    
    // MARK: - Properties
    private override init() {}
    static let shared = FirebaseService()
    var window:UIWindow?

    // MARK: - Functions
    func removeFirebaseToken(){
        guard let id = FirebaseApp.app()?.options.gcmSenderID else {
            return
        }
        Messaging.messaging().deleteFCMToken(forSenderID: id, completion: { (error) in
            print("Delete FCM Token, error: \(error.debugDescription)")
        })
    }
}
