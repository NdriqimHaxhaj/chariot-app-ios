//
//  User.swift
//  driverapp
//
//  Created by Arben Pnishi on 07/02/2018.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import RealmSwift

enum UserRole: String {
    case superIntendent = "super_intendent"
    case superIntendentAdmin = "super_intendent_admin"
    case tenant = "tenant"
    case pmcUser = "pmc_user"
    case driver = "driver_app_user"
}

class User: Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String = ""
    @objc dynamic var lastname: String = ""
    @objc dynamic var email: String = ""
    @objc dynamic var phone: String = ""
    @objc dynamic var photo: String = ""
    @objc dynamic var role: String = UserRole.driver.rawValue

    static func create(from json: JSON) -> User? {
        if let id = json["id"].int, let email = json["email"].string {
            
            let user = User()
            user.id = id
            user.email = email
            user.name = json["name"].string ?? ""
            user.lastname = json["last_name"].string ?? ""
            user.phone = json["phone"].string ?? ""
            user.photo = json["image_url"].string ?? ""
            
            return user
        }
        return nil
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    var userRole: UserRole {
        return UserRole(rawValue: role) ?? .driver
    }
    
    override static func ignoredProperties() -> [String] {
        return ["userRole"]
    }
}
