//
//  PickupStatus.swift
//  driverapp
//
//  Created by Arben Pnishi on 26/02/2018.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON

enum PickupType: String{
    case occurring = "pickup_occurring_status"
    case pickup = "pickup_status"
    case cancelation = "pickup_cancelation_reason"
    case driverAvailability = "driver_availability"
}

enum PickupStatusType: Int{
    case unassigned = 1
    case assigned = 2
    case canceled = 3
    case paused = 4
    case completedWithDelays = 5
    case completedWithoutDelays = 6
    case deleted = 7
    case ongoing = 8
    case overdue = 9
    case dropeedOff = 10
    case postponed = 20
    case notSpecified = 21
    
    init(_ statusCode: Int){
        switch statusCode {
        case 1:
            self = .unassigned
        case 2:
            self = .assigned
        case 3:
            self = .canceled
        case 4:
            self = .paused
        case 5:
            self = .completedWithDelays
        case 6:
            self = .completedWithoutDelays
        case 7:
            self = .deleted
        case 8:
            self = .ongoing
        case 9:
            self = .overdue
        case 10:
            self = .dropeedOff
        case 20:
            self = .postponed
        default:
            self = .notSpecified
        }
    }
}

enum PickupOccurringType: Int{
    case inbound = 1
    case outbound = 2
    case outboundAfterSchedule = 3
    
    var description: String{
        switch self {
        case .inbound:
            return "Inbound"
            
        case .outbound:
            return "Outbound"
            
        case .outboundAfterSchedule:
            return "outbound after schedule"
        }
    }
}

enum PickupCreator:Int {
    case dispatcher = 2
    case me = 3
    case notSpecified = 4
    
    init(_ int:Int){
        switch int {
        case 1,2:
            self = .dispatcher
        case 3:
            self = .me
        default:
            self = .notSpecified
        }
    }
}

class PickupStatus: Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var code: Int = 0
    @objc dynamic var name: String = ""
    @objc dynamic var pDescription: String = ""
    @objc dynamic fileprivate var statusType: String = ""

    var pType: PickupType?{
        return PickupType.init(rawValue: statusType)
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    override static func ignoredProperties() -> [String] {
        return ["pType"]
    }
    
    static func create(from json: JSON) -> PickupStatus? {
        if let id = json["id"].int, let code = json["status_code"].int, let statusType = json["status_type"].string, let _ = PickupType.init(rawValue: statusType) {
            
            let p = PickupStatus()
            p.id = id
            p.code = code
            p.statusType = statusType
            p.name = json["name"].string ?? ""
            p.pDescription = json["description"].string ?? ""

        }
        
        return nil
    }
    
    static func createArray(from jsonArray: [JSON]) -> [PickupStatus]{
        var array: [PickupStatus] = []
        array = jsonArray.map({ (item) -> PickupStatus in
            return PickupStatus.create(from: item) ?? PickupStatus()
        }).filter({ (item) -> Bool in
            return item.id != PickupStatus().id
        })
        
        return array
    }
}
