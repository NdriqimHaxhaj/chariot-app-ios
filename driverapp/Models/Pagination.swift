//
//  Pagination.swift
//  driverapp
//
//  Created by Arben Pnishi on 07/02/2018.
//  Copyright © 2017 Zombie Soup. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON

class Pagination: NSObject {
    var totalItems: Int = 0
    var currentPage: Int = 0
    var pageItems: Int = 0
    var totalPages: Int = 0
    
    static func create(from json: JSON) -> Pagination? {
        if let totalItems = json["total"].int, let currentPage = json["current_page"].int, let totalPages = json["last_page"].int {
            
            let p = Pagination()
            p.totalItems = totalItems
            p.currentPage = currentPage
            if let pageItems = json["per_page"].int{
                p.pageItems = pageItems
            }else if let pageItems = json["per_page"].string{
                p.pageItems = Int(pageItems) ?? 0
            }
            p.totalPages = totalPages
            return p
        }
        
        return nil
    }
    
    var nextPage: Int {
        return currentPage + 1
    }
    
    var hasNext: Bool {
        return nextPage <= totalPages
    }
    
    var isInFirstPage: Bool{
        return currentPage == 0
    }
}

