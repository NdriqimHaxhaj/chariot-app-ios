//
//  PickupNote.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 2/23/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//



import UIKit
import SwiftyJSON

class PickupNote: NSObject {
    
    //MARK: Properties
    var id:Int?
    var idPickupHandler:Int?
    var idDispatcher:Int?
    var dispatcherName:String?
    var idDriver:Int?
    var dispatcherDriver:Int?
    var notes:String?
    var status:Int?
    var deletedAt:Date?
    var createdAt:Date?
    var updatedAt:Date?
    var messageType:MessageType?
    
    //Helper property
    lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter
    }()
    
    //MARK: Initializers
    override init() {}
    
    init (_ json: JSON) {
        super.init()
        if !json.isEmpty{
            id = json["id"].int
            idPickupHandler = json["id_pickup_handler"].int
            idDispatcher = json["id_dispatcher"].int
            dispatcherName = json["dispatcher_name"].string
            if dispatcherName == nil {
                dispatcherName = "Property Manager"
            }
            idDriver = json["id_driver"].int
            dispatcherDriver = json["dispatcher_driver"].int
            notes = json["notes"].string
            status = json["status"].int
            if json["deleted_at"] != JSON.null {
                self.deletedAt = dateFormatter.date(from: json["deleted_at"].string!)
            }
            if json["created_at"] != JSON.null {
                self.createdAt = dateFormatter.date(from: json["created_at"].string!)
            }
            if json["updated_at"] != JSON.null {
                self.updatedAt = dateFormatter.date(from: json["updated_at"].string!)
            }
        }
    }
    
    //MARK: Class functions
    class func PickupNoteArray(_ json:JSON)->[PickupNote]?{
        if !json.isEmpty{
            var pickups = [PickupNote]()
            for item in json.array! {
                let pickup:PickupNote = PickupNote(item)
                pickups.append(pickup)
            }
            return pickups
        }
        return nil
    }
    
    //MARK: Helper Functions
    
    
}
