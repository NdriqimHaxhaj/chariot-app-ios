//
//  OfflineReason.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 6/19/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import SwiftyJSON

enum OfflineReasonType:Int {
    case takeBrake = 1
    case atFullCapacity = 2
    case truckInoperable = 3
    case leaveTruck = 4
    case notSpecified = 5
    
    init (_ offlineReasonId: Int) {
        switch offlineReasonId {
        case 1:
            self = .takeBrake
        case 2:
            self = .atFullCapacity
        case 3:
            self = .truckInoperable
        case 4:
            self = .leaveTruck
        default:
            self = .notSpecified
        }
    }
}

class OfflineReason: NSObject {
    var id:Int?
    var name:String?
    var type:OfflineReasonType?
    
    
    // MARK: - Initializers
    init(_ id:Int, _ name:String, _ type:OfflineReasonType) {
        self.id = id
        self.name = name
        self.type = type
    }
    
    init(_ json:JSON){
        if !json.isEmpty {
            if let id = json["id"].int {
                self.id = id
            }
            if let name = json["name"].string {
                self.name = name
            }
            if let id = json["id"].int{
                self.type = OfflineReasonType(id)
            }
            
        }
    }
    
    class func createOfflineReasonsArray(_ json:JSON) -> [OfflineReason]{
        var offlineReasonsArray:[OfflineReason] = []
        if let array = json.array{
            for offlineReason in array {
                let offlineReason:OfflineReason = OfflineReason(offlineReason)
                offlineReasonsArray.append(offlineReason)
            }
        }
        return offlineReasonsArray
    }
    
}
