//
//  PMPickupsRequest.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 7/18/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

enum PickupRequestStatus: Int{
    case Denied = 0
    case Pending = 1
    case Accepted = 2
    
}

enum PickupLocationType: String{
    case Inside = "inside"
    case Outside = "outside"
    case Curbside = "curbside"
    
    var intValue: Int{
        switch self {
        case .Inside:
            return 1
        case .Outside:
            return 2
        case .Curbside:
            return 3
        }
    }
}

enum PickupLoadType: String{
    case Recycable = "recyclable"
    case EWaste = "ewaste"
    case Garbage = "garbage"
    case Scrap = "scrap"
    
    var intValue: Int{
        switch self {
        case .Recycable, .Scrap:
            return 1
        case .EWaste:
            return 2
        case .Garbage:
            return 3
        }
    }
}

class PMPickupRequest: NSObject {
    override init() {}
    
    var id: Int = 0
    var buildingId: Int = 0
    var buildingName: String = ""
    var buildingAddress:String = ""
    var longitude:Double = 0
    var latitude:Double = 0
    var zipCode:String = ""
    var startPickupTime: Date = Date()
    var endPickupTime: Date = Date()
    var bagCount: Int = -1
    var loadSize: Int = 0
    var loadSubtypes:[Int] = []
    var name: String = ""
    var recurringFrequency: Int = -1
    var loadType: String = ""
    var status: PickupRequestStatus = .Pending
    var locationType = -1
    var pickupTypes: [Int] = []
    var scheduleFlag:Int?
    var reschedulePickupStartTime:Date?
    var reschedulePickupEndTime:Date?
    var pickupStatus:Int?
    var pickupStatusName:String?
    var recurringDays:[Int] = []
    var buildingCreatedBy:PickupCreator?
    
    init(_ fromPickup:PickupRequest) {
        if let id = fromPickup.id {
            self.id = id
        }
        self.buildingName = fromPickup.buildingName
        self.buildingAddress = fromPickup.buildingAddress
        if let latitude = fromPickup.latitude {
            self.latitude = Double(latitude)
        }
        if let longitude = fromPickup.longitude {
            self.longitude = Double(longitude)
        }
        if let zipCode = fromPickup.zipCode {
            self.zipCode = "\(zipCode)"
        }
        if let startPickupTime = fromPickup.startPickupTime {
            self.startPickupTime = startPickupTime
        }
        if let endPickupTime = fromPickup.endPickupTime {
            self.endPickupTime = endPickupTime
        }
        if let loadLocation = fromPickup.loadLocation {
            self.locationType = loadLocation
        }
        if let bagCount = fromPickup.bagCount {
            self.loadSize = bagCount
        }
        self.loadSubtypes = fromPickup.loadSubtypes
        self.recurringDays = fromPickup.recurringDays
        if let recurringFrequencyString = fromPickup.recurringFrequency {
            self.recurringFrequency = NSString(string: recurringFrequencyString).integerValue
        }
        
        
    }
    
    static func create(from json: JSON) -> PMPickupRequest?{
        if let id = json["id"].int, let startTime = json["start_pickup_time"].string, let endTime = json["end_pickup_time"].string {
            let p = PMPickupRequest()
            p.id = id
            p.buildingId = json["id_building"].int ?? 0
            p.buildingName = json["building_name"].string ?? ""
            p.startPickupTime = Date.create(from: startTime) ?? Date()
            p.endPickupTime = Date.create(from: endTime) ?? Date()
            p.bagCount = json["bag_count"].int ?? 0
            p.loadSize = json["load_size"].int ?? 0
            p.name = json["name"].string ?? ""
            p.recurringFrequency = json["recurring_frequency"].int ?? -1
            p.loadType = json["load_type"].string ?? " "
            if let status = json["status"].int {
                p.status = PickupRequestStatus(rawValue: status)!
            }
            p.locationType = p.parseLocationType(from: p.name)
            p.pickupTypes = p.parseLoadType(from: p.loadType)
            if let scheduleFlag = json["schedule_flag"].int {
                p.scheduleFlag = scheduleFlag
            }
            if let reschedulePickupStartTime = json["reschedule_pickup_start_time"].string {
                p.reschedulePickupStartTime = Date.create(from: reschedulePickupStartTime) ?? Date()
            }
            if let reschedulePickupEndTime = json["reschedule_pickup_end_time"].string {
                p.reschedulePickupEndTime = Date.create(from: reschedulePickupEndTime) ?? Date()
            }
            if let pickupStatus = json["pickup_status"].int {
                p.pickupStatus = pickupStatus
            }
            if let pickupStatusName = json["pickup_status_name"].string {
                p.pickupStatusName = pickupStatusName
            }
            if let loadSubtypes = json["load_subtypes"].string {
                p.loadSubtypes = intArrayFromCommaSeperatesString(loadSubtypes)
            }
            if let recurringDays = json["recurring_days"].string {
                p.recurringDays = intArrayFromCommaSeperatesString(recurringDays)
            }
            
            return p
        }
        return nil
    }
    
    private func parseLocationType(from string: String) -> Int{
        guard let type = PickupLocationType.init(rawValue: string) else { return -1 }
        return type.intValue
    }
    
    private func parseLoadType(from string: String) -> [Int]{
        return string.split{$0 == ","}.map(String.init).compactMap({ (string) -> Int in
            guard let type = PickupLoadType.init(rawValue: string) else { return -1 }
            return type.intValue
        }).filter { (value) -> Bool in
            return value >= 1
        }
    }
    
    static func createArray(from jsonArray: [JSON]) -> [PMPickupRequest]{
        var array: [PMPickupRequest] = []
        array = jsonArray.map({ (item) -> PMPickupRequest in
            return PMPickupRequest.create(from: item) ?? PMPickupRequest()
        }).filter({ (item) -> Bool in
            return item.id != 0
        })
        
        return array
    }
    
    static func intArrayFromCommaSeperatesString(_ string:String)->[Int]{
        let stringArray = string.components(separatedBy: ",")
        var intArray = [Int]()
        for str in stringArray {
            let intNumber:Int = Int(str)!
            intArray.append(intNumber)
        }
        return intArray
    }
    
    func getValuesFrom(pickup: PMPickupRequest){
        self.id = pickup.id
        self.buildingId =  pickup.buildingId
        self.buildingName = pickup.buildingName
        self.buildingAddress = pickup.buildingAddress
        self.startPickupTime =  pickup.startPickupTime
        self.endPickupTime =  pickup.endPickupTime
        self.bagCount =  pickup.loadSize
        self.loadSize =  pickup.loadSize
        self.name =  pickup.name
        self.recurringFrequency =  pickup.recurringFrequency
        self.recurringDays = pickup.recurringDays
        self.loadSubtypes = pickup.loadSubtypes
        self.loadType =  pickup.loadType
        self.status =  pickup.status
        self.locationType =  pickup.locationType
        self.pickupTypes =  pickup.pickupTypes
        self.zipCode = pickup.zipCode
    }
    
    func toDictionary() -> Parameters {
        var parameters: Parameters = [
            "address"    : buildingAddress,
            "latitude"   : "\(latitude)",
            "longitude"  : "\(longitude)",
            "zip_code"   : zipCode,
            "start_pickup_time": startPickupTime.toPickupTime,
            "end_pickup_time"  : endPickupTime.toPickupTime,
            "bag_count" : bagCount,
            "load_size" : loadSize,
            "load_type" : pickupTypes.map{ String($0) }.joined(separator: ","),
            "load_location" : locationType,
            "recurring_frequency" : recurringFrequency,
            "load_subtypes" : loadSubtypes.map{ String($0) }.joined(separator: ","),
            "recurring_days" : recurringDays.map{ String($0) }.joined(separator: ",")
        ]
        if buildingCreatedBy != nil, buildingCreatedBy == .dispatcher {
            parameters["building_id"] = buildingId
        }
        
        return parameters
    }

    
}
