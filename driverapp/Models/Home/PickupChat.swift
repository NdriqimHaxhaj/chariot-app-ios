//
//  PickupChat.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 4/17/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import SwiftyJSON

class PickupChat: NSObject {
    
    //MARK: Properties
    var id:Int?
    var idTransportationCompany:Int?
    var idUser:Int?
    var dispatcherName:String?
    var idDriver:Int?
    var chatText :String?
    var status:Int?
    var createdAt:Date?
    
    //Helper property
    lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter
    }()
    
    //MARK: Initializers/Constructors
    override init() {
    }
    
    init (_ json: JSON) {
        super.init()
        if !json.isEmpty{
            self.id = json["id"].int
            self.idTransportationCompany = json["id_transportation_company"].int
            self.idUser = json["id_user"].int
            let dispatcherName = json["dispatcher_name"].string
            self.dispatcherName = dispatcherName != nil ? dispatcherName : ""
            self.chatText = json["text"].string
            self.status = json["status"].int
            if json["created_at"] != JSON.null {
                self.createdAt = dateFormatter.date(from: json["created_at"].string!)
            }
        }
    }
    
    //MARK: Class functions
    class func PickupChatArray(_ json:JSON)->[PickupChat]?{
        if !json.isEmpty{
            var pickups = [PickupChat]()
            for item in json.array! {
                let pickup:PickupChat = PickupChat(item)
                pickups.append(pickup)
            }
            return pickups
        }
        return nil
    }
}
