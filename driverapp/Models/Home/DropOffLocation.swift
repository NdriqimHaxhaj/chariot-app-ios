//
//  DropOffLocation.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 6/19/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import SwiftyJSON

class DropOffLocation: NSObject {
    
    
    var id:Int?
    var idTransportCompany:Int?
    var name:String?
    var address:String?
    var zipCode:String?
    var latitude:String?
    var longitude:String?
    var deletedAt:Date?
    var createdAt:Date?
    var updatedAt:Date?
    
    //Helper property
    lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter
    }()
    
    init(_ json:JSON){
        super.init()
        if !json.isEmpty {
            id = json["id"].int
            idTransportCompany = json["id_transport_company"].int
            name = json["name"].string
            address = json["address"].string
            zipCode = json["zip_code"].string
            latitude = json["latitude"].string
            longitude = json["longitude"].string
            if let date = json["deleted_at"].string {
                deletedAt = dateFormatter.date(from: date)
            }
            if let date = json["created_at"].string {
                deletedAt = dateFormatter.date(from: date)
            }
            if let date = json["updated_at"].string {
                deletedAt = dateFormatter.date(from: date)
            }
        }
    }
    
    class func createDropsArray(_ json:JSON) -> [DropOffLocation]{
        var dropsArray:[DropOffLocation] = []
        if let array = json.array{
            for dropOffLocationJSON in array {
                let dropLocation:DropOffLocation = DropOffLocation(dropOffLocationJSON)
                dropsArray.append(dropLocation)
            }
        }
        return dropsArray
    }
    
}
