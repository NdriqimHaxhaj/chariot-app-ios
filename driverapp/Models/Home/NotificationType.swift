//
//  NotificationType.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 5/18/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import SwiftyJSON

enum NotificationTypeList: Int{
    case allNotifications = 0
    case activityFeed = 1
    case criticalAlert = 2
    case notificationsPane = 3
    case broadcast = 4
    case newPickupNoteDriver = 5
    case driverStartPickup = 6
    case driverCompletePickup = 7
    case driverAssignPickup = 8
    case pickupOverdue = 9
    case pickupCancel = 10
    case pickupRescheduleSuper = 11
    case pickupRemoveAssignDriver = 12
    case uncancelPickupDriver = 14
    case driverAssignPickupDispatcher = 15
    case pickupReassignDriverDispatcher = 16
    case driverPickupCancelDispatcher = 17
    case uncancelPickupDispatcher = 18
    case unassignedPickupRequestDispatcher = 19
    case superNewPickupDispatcher = 20
    case acceptedPickupRescheduleDispatcher = 21
    case changePickupRescheduleDispatcher = 22
    case pickupRescheduledDriver = 23
    case newMessageDriver = 24
    case reschedulePickup = 26
    case replyFeedback = 27
    case droppedOffPickup = 29
    case oneHourLeft = 30
    case pickupAccepted = 31
    case newPickupNoteDispatcher = 33
    case newPickupNotePM = 34
    case rateDriver = 35
    case notSpecified = 36
    
    init(_ statusCode: Int){
        switch statusCode {
        case 1:
            self = .activityFeed
            break
        case 2:
            self = .criticalAlert
            break
        case 3:
            self = .notificationsPane
            break
        case 4:
            self = .broadcast
            break
        case 5:
            self = .newPickupNoteDriver
            break
        case 6:
            self = .driverStartPickup
            break
        case 7:
            self = .driverCompletePickup
            break
        case 8:
            self = .driverAssignPickup
            break
        case 9:
            self = .pickupOverdue
            break
        case 10:
            self = .pickupCancel
            break
        case 11:
            self = .pickupRescheduleSuper
            break
        case 12:
            self = .pickupRemoveAssignDriver
            break
        case 14:
            self = .uncancelPickupDispatcher
            break
        case 15:
            self = .driverAssignPickupDispatcher
            break
        case 16:
            self = .pickupReassignDriverDispatcher
            break
        case 17:
            self = .driverPickupCancelDispatcher
        case 18:
            self = .uncancelPickupDispatcher
            break
        case 19:
            self = .unassignedPickupRequestDispatcher
            break
        case 20:
            self = .superNewPickupDispatcher
            break
        case 21:
            self = .acceptedPickupRescheduleDispatcher
            break
        case 22:
            self = .changePickupRescheduleDispatcher
            break
        case 23:
            self = .pickupRescheduledDriver
            break
        case 24:
            self = .newMessageDriver
            break
        case 26:
            self = .reschedulePickup
            break
        case 27:
            self = .replyFeedback
            break
        case 29:
            self = .droppedOffPickup
            break
        case 30:
            self = .oneHourLeft
            break
        case 31:
            self = .pickupAccepted
            break
        case 33:
            self = .newPickupNoteDispatcher
            break
        case 34 :
            self = .newPickupNotePM
            break
        case 35:
            self = .rateDriver
            break
        default:
            self = .notSpecified
        }
    }
}

class NotificationType: NSObject {
    // MARK: - Properties
    var notificationType:NotificationTypeList = .notSpecified
    var text:String = ""
    var status:Int?
    var params:[String:Any]?
    var createdAt:Date?
    
    var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter
    }()
    
    
    init(_ json: JSON) {
        if !json.isEmpty && json != JSON.null {
            notificationType = NotificationTypeList(json["id_notification_type"].int!)
            status = json["status"].int
            text = json["text"].string!
            // Get string from JSON and convert it to Dictionary[String:String]
            if let data = json["params"].string?.data(using: .utf8){
                do {
                    params =  try JSONSerialization.jsonObject(with: data, options: []) as? [String:Any]
                } catch let error as NSError {
                    print(error)
                }
            }
            if let date = json["created_at"].string {
                createdAt = dateFormatter.date(from: date)
            }
        }
    }
    
    class func createNotificationsArray(_ json:JSON) -> [NotificationType]{
        var notificationArray:[NotificationType] = []
        if let array = json.array{
            for notificationJSON in array {
                let notification:NotificationType = NotificationType(notificationJSON)
                notificationArray.append(notification)
            }
        }
        return notificationArray
    }
    
}
