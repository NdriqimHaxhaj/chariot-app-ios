//
//  PickupRequest.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 2/22/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import SwiftyJSON

class PickupRequest: NSObject {
    
    //MARK: Properties
    var id: Int?
    var idDriver: Int?
    var buildingName: String = ""
    var buildingAddress: String = ""
    var bagCount: Int?
    var longitude: Float?
    var latitude: Float?
    var startPickupTime: Date?
    var endPickupTime: Date?
    var loadLocation: Int?
    var loadLocationName: String = ""
    var loadTypesNames: [String] = []
    var loadTypes: [Int] = []
    var pickupStatus: PickupStatusType = .notSpecified
    var pickupStatusName: String?
    var superintendentPhone: String?
    var createdAt: Date?
    var zipCode:String?
    var recurringFrequency:String?
    var requestedBy:PickupCreator?
    var loadSubtypes:[Int] = []
    var recurringDays:[Int] = []
    
    //Helper property
    lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter
    }()
    
    //MARK: Initializers/Constructors
    override init() {}
    
    init (_ json: JSON) {
        super.init()
        if !json.isEmpty {
            if let id = json["id"].int {
                self.id = id
            }
            if let idDriver = json["id_driver"].int {
                self.idDriver = idDriver
            }
            if let buildingName = json["building_name"].string {
                self.buildingName = buildingName
            }
            if let buildingAddress = json["building_address"].string {
                self.buildingAddress = buildingAddress
            }
            
            if let bagCount = json["bag_count"].int {
                self.bagCount = bagCount
            }
            if let longitude = json["longitude"].string {
                self.longitude = NSString(string: longitude).floatValue
            }
            if let latitude = json["latitude"].string {
                self.latitude = NSString(string: latitude).floatValue
            }
            if let startPickupTime = json["start_pickup_time"].string {
                self.startPickupTime = dateFormatter.date(from: startPickupTime)
            }
            if let endPickupTime = json["end_pickup_time"].string {
                self.endPickupTime = dateFormatter.date(from: endPickupTime)
            }
            if let loadLocation = json["load_location"].int {
                self.loadLocation = loadLocation
            }
            if let loadLocationName = json["load_location_name"].string {
                self.loadLocationName = loadLocationName
            }
//            "request_created_by": 3,
            if let loadTypesNames = json["load_types_names"].string {
                self.loadTypesNames = stringArrayFromCommaSeperatedString(loadTypesNames)
            }
            if let loadTypes = json["load_types"].string {
                self.loadTypes = intArrayFromCommaSeperatesString(loadTypes)
            }
            if let pickupStatus = json["pickup_status"].int {
                self.pickupStatus = PickupStatusType(pickupStatus)
            }
            if let pickupStatusName = json["pickup_status_name"].string {
                self.pickupStatusName = pickupStatusName
            }
            if let superintendentPhone = json["superintendent_phone"].string {
                self.superintendentPhone = superintendentPhone
            }
            if let createdAt = json["created_at"].string {
                self.createdAt = dateFormatter.date(from: createdAt)
            }
            if let zipCode = json["zip_code"].string {
                self.zipCode = zipCode
            }
            if let recurringFrequency = json["recurring_frequency"].string {
                self.recurringFrequency = recurringFrequency
            }
            if let requestedBy = json["request_created_by"].int {
                // 1,2 - Dispatcher
                // 3 - Driver
                self.requestedBy = PickupCreator(requestedBy)
            }
            if let loadSubtypes = json["load_subtypes"].string {
                self.loadSubtypes = intArrayFromCommaSeperatesString(loadSubtypes)
            }
            if let recurringDays = json["recurring_days"].string {
                self.recurringDays = intArrayFromCommaSeperatesString(recurringDays)
            }
        }
    }
    
    //MARK: Class functions
    class func createPickupsArray(_ json:JSON)->[PickupRequest]{
        if !json.isEmpty{
            var pickups = [PickupRequest]()
            for item in json.array! {
                let pickup:PickupRequest = PickupRequest(item)
                pickups.append(pickup)
            }
            return pickups
        }
        return []
    }
    
    func toPMPickupRequest()->PMPickupRequest{
        let pickup = PMPickupRequest(self)
        return pickup
    }
    
    
    //MARK: Helper Functions
    func stringArrayFromCommaSeperatedString(_ string: String)->[String]{
        return string.components(separatedBy: ",")
    }
    
    func intArrayFromCommaSeperatesString(_ string:String)->[Int]{
        let stringArray = string.components(separatedBy: ",").filter{$0.isEmpty == false}
        var intArray = [Int]()
        for str in stringArray {
            if let intNumber:Int = Int(str) {
                intArray.append(intNumber)
            }
        }
        return intArray
    }
}
