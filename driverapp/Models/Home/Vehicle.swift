//
//  Vehicle.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 9/3/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import SwiftyJSON

class Vehicle: NSObject {

    // MARK : - Properties
    var id:Int?
    var idTransportationCompany:Int?
    var name:String?
    var vinNumber:String?
    var vehicleTypeID: Int?
    var createdAt:Date?
    var updatedAt:Date?
    var truckTypeName:String?
    var truckTypeID:Int?
    var status:Int?
    var statusName:String?
    var currentIdDriver:Int?
    var vehicleType:VehicleType?
    var vehicleParentID:Int?
    
    //Helper property
    lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter
    }()
    
    // MARK: - Initializers
    init(_ json:JSON) {
        super.init()
        if let id = json["id"].int {
            self.id = id
        }
        if let idTransportationCompany = json["id_transportation_company"].int {
            self.idTransportationCompany = idTransportationCompany
        }
        if let name = json["name"].string {
            self.name = name
        }
        if let vinNumber = json["vin_number"].string {
            self.vinNumber = vinNumber
        }
        if let truckTypeID = json["id_truck_type"].int {
            self.truckTypeID = truckTypeID
        }
        if let truckTypeName = json["truck_type_name"].string {
            self.truckTypeName = truckTypeName
        }
        if let createdAt = json["created_at"].string {
            self.createdAt = dateFormatter.date(from: createdAt)
        }
        if let updatedAt = json["updated_at"].string {
            self.createdAt = dateFormatter.date(from: updatedAt)
        }
        if let status = json["status"].int {
            self.status = status
        }
        if let statusName = json["status_name"].string {
            self.statusName = statusName
        }
        if let currentIdDriver = json["current_id_driver"].int {
            self.currentIdDriver = currentIdDriver
        }
        if let vehicleType = json["id_vehicle_type"].int {
            self.vehicleType = VehicleType(type: vehicleType)
        }
        if let vehicleParentID = json["id_parent_vehicle"].int {
            self.vehicleParentID = vehicleParentID
        }
        if let vehicleTypeID = json["id_vehicle_type"].int {
            self.vehicleTypeID = vehicleTypeID
        }
    }
    
    // MARK: - Functions
    class func createTrucksArray(_ json:JSON)->[Vehicle]{
        if !json.isEmpty{
            var trucks = [Vehicle]()
            for item in json.array! {
                let truck:Vehicle = Vehicle(item)
                trucks.append(truck)
            }
            return trucks
        }
        return []
    }
    
}



enum VehicleType:Int {
    case truck = 1
    case car = 2
    case bike = 3
    case bicycle = 4
    case frontloaderTruck = 5
    case rearloaderTruck = 6
    case rollOffTruck = 7
    case sideLoaderTruck = 8
    case notSpecified = 21
    
    init( type:Int){
        switch type {
        case 1:
            self = .truck
        case 2:
            self = .car
        case 3:
            self = .bike
        case 4:
            self = .bicycle
        case 5:
            self = .frontloaderTruck
        case 6:
            self = .rearloaderTruck
        case 7:
            self = .rollOffTruck
        case 8:
            self = .sideLoaderTruck
        default:
            self = .notSpecified
        }
    }
}
