//
//  File.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 11/30/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class MaterialSubtype: NSObject {
   
    // MARK: - Properties
    var id:Int?
    var transportationCompanyID:Int?
    var name:String?
    var scrapTypeID:Int?
    var active:Int?
    var createdAt:Date?
    var updatedAt:Date?
    
    // MARK: - Helper properties
    lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter
    }()
    
    init(_ json:JSON){
        super.init()
        
        if let id = json["id"].int {
            self.id = id
        }
        if let transportationCompanyID = json["id_transportation_company"].int {
            self.transportationCompanyID = transportationCompanyID
        }
        if let name = json["name"].string {
            self.name = name
        }
        if let scrapTypeID = json["id_scrap_type"].int {
            self.scrapTypeID = scrapTypeID
        }
        if let active = json["active"].int {
            self.active = active
        }
        
        if let createdAt = json["created_at"].string {
            self.createdAt = dateFormatter.date(from: createdAt)
        }
        if let updatedAt = json["updated_at"].string {
            self.createdAt = dateFormatter.date(from: updatedAt)
        }
        
    }
    
    //MARK: Class functions
    class func createMaterialSubtypesArray(_ jsonArray:[JSON])->[MaterialSubtype]{
        if !jsonArray.isEmpty {
            var materials = [MaterialSubtype]()
            for item in jsonArray {
                let materialSubtype:MaterialSubtype = MaterialSubtype(item)
                materials.append(materialSubtype)
            }
            return materials
        }
        return []
    }
    
    
}
