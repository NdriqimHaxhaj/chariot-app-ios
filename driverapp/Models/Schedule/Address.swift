//
//  Address.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 10/23/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import SwiftyJSON

class Address: NSObject, NSCoding {
    
    // MARK: - Properties
    var buildingId:Int = -1
    var buildingName:String = ""
    var buildingAddress:String = ""
    var latitude:Double = 0
    var longitude:Double = 0
    var zipCode:Int = 0
    
    // MARK: - Encoding/Decoding
    func encode(with aCoder: NSCoder) {
        aCoder.encode(buildingId, forKey: "id")
        aCoder.encode(buildingName, forKey: "buildingName")
        aCoder.encode(buildingAddress, forKey: "buldingAddress")
        aCoder.encode(latitude, forKey: "latitude")
        aCoder.encode(longitude, forKey: "longitude")
        aCoder.encode(zipCode, forKey: "zipCode")
    }
    
    public convenience required init?(coder aDecoder: NSCoder) {
        let id = aDecoder.decodeInteger(forKey: "id")
        let buildingName = aDecoder.decodeObject(forKey: "buildingName") as! String
        let buildingAddress = aDecoder.decodeObject(forKey: "buldingAddress") as! String
        
        let latitude = aDecoder.decodeDouble(forKey: "latitude")
        let longitude = aDecoder.decodeDouble(forKey: "longitude")
        let zipCode = aDecoder.decodeInteger(forKey: "zipCode")
        self.init(id: id,
                  buildingName: buildingName,
                  buildingAddress: buildingAddress,
                  latitude: latitude,
                  longitude: longitude,
                  zipCode: zipCode)
    }
    
    // MARK: - Initializers
    
    init(_ json:JSON) {
        super.init()
        if !json.isEmpty {
            if let buildingId = json["id_building"].int {
                self.buildingId = buildingId
            }
            if let buildingName = json["building_name"].string {
                self.buildingName = buildingName
            }
            if let buldingAddress = json["building_address"].string {
                self.buildingAddress = buldingAddress
            }
            if let latitude = json["latitude"].string {
                self.latitude = NSString(string: latitude).doubleValue
            }
            if let longitude = json["longitude"].string {
                self.longitude = NSString(string: longitude).doubleValue
            }
            if let zipCode = json["zip_code"].string {
                self.zipCode = NSString(string: zipCode).integerValue
            }
        }
    }
    
    init(id:Int, buildingName:String, buildingAddress:String, latitude:Double, longitude:Double, zipCode:Int) {
        self.buildingId = id
        self.buildingName = buildingName
        self.buildingAddress = buildingAddress
        self.latitude = latitude
        self.longitude = longitude
        self.zipCode = zipCode
    }
    
    // MARK: - Helper functions
    class func createAddressesArray(_ json:[JSON])->[Address]{
        var addressesArray:[Address] = []
        if !json.isEmpty {
            for jsonObject in json {
                let address = Address(jsonObject)
                addressesArray.append(address)
            }
        }
        return addressesArray
    }
}
