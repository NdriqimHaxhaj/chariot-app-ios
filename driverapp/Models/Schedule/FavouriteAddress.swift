//
//  FavouriteAddress.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 10/26/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import SwiftyJSON

class FavouriteAddress: NSObject, NSCoding {

    
    // MARK: - Properties
    var id:Int?
    var userId:Int?
    var buildingName:String?
    var buildingAddress:String?
    var latitude:Double?
    var longitude:Double?
    var priority:Int?
    var zipCode:Int?

    // MARK: - Encoding/Decoding
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(userId, forKey: "userId")
        aCoder.encode(buildingName, forKey: "buildingName")
        aCoder.encode(buildingAddress, forKey: "buldingAddress")
        aCoder.encode(latitude, forKey: "latitude")
        aCoder.encode(longitude, forKey: "longitude")
        aCoder.encode(priority, forKey: "priority")
        aCoder.encode(zipCode, forKey: "zipCode")
    }
    
    public convenience required init?(coder aDecoder: NSCoder) {
        let id = aDecoder.decodeObject(forKey: "id") as! Int
        let userId = aDecoder.decodeObject(forKey: "userId") as! Int
        let buildingName = aDecoder.decodeObject(forKey: "buildingName") as! String
        let buildingAddress = aDecoder.decodeObject(forKey: "buldingAddress") as! String
        let latitude = aDecoder.decodeObject(forKey: "latitude") as! Double
        let longitude = aDecoder.decodeObject(forKey: "longitude") as! Double
        let zipCode = aDecoder.decodeObject(forKey: "zipCode") as! Int
        let priority = aDecoder.decodeObject(forKey: "priority") as! Int
        self.init(id: id,
                  userId: userId,
                  buildingName: buildingName,
                  buildingAddress: buildingAddress,
                  latitude: latitude,
                  longitude: longitude,
                  priority: priority,
                  zipCode: zipCode)
    }
    
    // MARK: - Initializers
    
    init(_ json:JSON) {
        super.init()
        if !json.isEmpty {
            if let id = json["id"].int {
                self.id = id
            }
            if let userId = json["id_user"].int {
                self.userId = userId
            }
            if let buildingName = json["name"].string {
                self.buildingName = buildingName
            }
            if let buldingAddress = json["address"].string {
                self.buildingAddress = buldingAddress
            }
            if let latitude = json["latitude"].string {
                self.latitude = NSString(string: latitude).doubleValue
            }
            if let longitude = json["longitude"].string {
                self.longitude = NSString(string: longitude).doubleValue
            }
            if let priority = json["priority"].int {
                self.priority = priority
            }
            if let zipCode = json["zip_code"].string {
                self.zipCode = NSString(string: zipCode).integerValue
            }
        }
    }
    
    init(id:Int, userId:Int,  buildingName:String, buildingAddress:String, latitude:Double, longitude:Double, priority:Int, zipCode:Int) {
        self.id = id
        self.userId = userId
        self.buildingName = buildingName
        self.buildingAddress = buildingAddress
        self.latitude = latitude
        self.longitude = longitude
        self.priority = priority
        self.zipCode = zipCode
    }
    
    // MARK: - Helper functions
    class func createAddressesArray(_ json:[JSON])->[FavouriteAddress]{
        var addressesArray:[FavouriteAddress] = []
        if !json.isEmpty {
            for jsonObject in json {
                let address = FavouriteAddress(jsonObject)
                addressesArray.append(address)
            }
        }
        return addressesArray
    }
}
