//
//  ChatREST.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 4/17/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

class ChatREST: NSObject {

    class func getChatMessages(completionHandler: @escaping (_ status:Bool,_ chats:[PickupChat]?,_ error:APError)->()){
        
        let request = RequestREST(resource: "da/dispatcherchats", method: .get, parameters: nil)
        
        ServiceREST.request(with: request) { (response) in
            let error = response.apError(message: response.errorMessage ?? "Could not update availibilty right now!")
            
            if response.isHttpSuccess{
                guard let jsonArray = response.json.array else {
                    completionHandler(false, nil, error)
                    return
                }
                
                var messages = [PickupChat]()
                for item in jsonArray {
                    let message = PickupChat(item)
                    messages.append(message)
                }
                
                completionHandler(true, messages, error)
                
            }else {
                completionHandler(false, nil, error)
            }
        }
    }
    
    
    
    class func chatUpdate(_ id:Int, completionHandler: @escaping (_ status:Bool,_ error:APError)->()){
        
        let request = RequestREST(resource: "da/chatstatus/\(id)", method: .get, parameters: nil)
        
        ServiceREST.request(with: request) { (response) in
            let error = response.apError(message: response.errorMessage ?? "Could not update availibilty right now!")
            
            if response.isHttpSuccess{
                completionHandler(true, error)
            }else {
                completionHandler(false, error)
            }
        }
    }
    
    class func chatReply(_ text:String, completionHandler: @escaping (_ status:Bool,_ error:APError)->()){
        
        let parameters:[String:String] = [
            "text":text
        ]
        
        let request = RequestREST(resource: "da/addchat", method: .put, parameters: parameters)
        
        ServiceREST.request(with: request) { (response) in
            let error = response.apError(message: response.errorMessage ?? "Could not update availibilty right now!")
            
            if response.isHttpSuccess{
                completionHandler(true, error)
            }else {
                completionHandler(false, error)
            }
        }
    }
}
