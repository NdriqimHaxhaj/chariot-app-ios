//
//  NotificationsREST.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 5/18/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import SwiftyJSON


class NotificationsREST: NSObject {
    
    class func getNotifications(completion: @escaping (_ success:Bool, _ notifications:[NotificationType]?)->Void){
        let path = "da/getnotifications"
        let request = RequestREST(resource: path, method: .get, parameters: nil)
        
        ServiceREST.request(with: request) { (response) in
            let jsonArray = response.json
            let notificationsArray = NotificationType.createNotificationsArray(jsonArray)
            if notificationsArray.count > 0 {
                completion(true,notificationsArray)
            } else {
                completion(false,[])
            }
        }
    }
    
    class func markAllNotificationsAsRead(completion: @escaping (_ success:Bool)->Void){
        let path = "da/marknotificationsread"
        let request = RequestREST(resource: path, method: .get, parameters: nil)
        
        ServiceREST.request(with: request) { (response) in
            if response.isHttpSuccess {
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    class func getNotificationsSettings(completion: @escaping (_ success:Bool,_ error:APError?)->Void){
        let path = "notificationsettings"
        let request = RequestREST(resource: path, method: .get, parameters: nil)
        
        ServiceREST.request(with: request) { (response) in
            let error  = response.apError(message: response.errorMessage ?? "Could not get notifications settings!")
            
            if let jsonArray = response.json.array {
                for item in jsonArray {
                    if let idNotificationType = item["id_notification_type"].int {
                        switch idNotificationType {
                        case NotificationTypeList.allNotifications.rawValue:
                            let status = item["status"].int == 1 ? true : false
                            NotificationsManager.allowAllNotifications = status
                            break
                        case NotificationTypeList.rateDriver.rawValue:
                            let status = item["status"].int == 1 ? true : false
                            NotificationsManager.reviewClientNotification = status
                            break
                        default:
                            break
                        }
                    }
                }
                completion(true,nil)
            } else {
                completion(false,error)
            }
        }
    }
    
    class func setNotificationsSettings(_ notificationType:Int, _ status:Int,completion: @escaping (_ success:Bool,_ error:APError?)->Void){
        let path = "notificationsettings"
        let parameters: [String:Any] = [
            "id_notification_type": notificationType,
            "status": status
        ]
        
        let request = RequestREST(resource: path, method: .put, parameters: parameters)
        
        ServiceREST.request(with: request) { (response) in
            let error  = response.apError(message: response.errorMessage ?? "Could not set notifications settings!")
            let json = response.json
            if json["message"].string == "Settings updated!" {
                completion(true, nil)
            } else {
                completion(false, error)
            }
        }
    }
    

}
