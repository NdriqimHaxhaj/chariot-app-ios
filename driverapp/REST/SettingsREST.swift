//
//  SettingsREST.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 4/20/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import SwiftyJSON

class SettingsREST: NSObject {
    class func getHtmlString(termsControllerType: TermsControllerType, language: String = "eng", completionHandler: @escaping (_ htmlString: String? , _ error: APError?) -> Void){
        let parameters: [String:Any] = [
            "lang": language
        ]
        let path = termsControllerType == .terms ? "si/termsconditions" : "si/privacypolicy"
        let request = RequestREST(resource: path, method: .get, parameters: parameters)
        
        ServiceREST.request(with: request) { (response) in
            let error  = response.apError(message: response.errorMessage ?? "Could not get terms!")
            if let array = response.json.array, array.count > 0{
                let jsonItem = array[0]
                if let htmlString = jsonItem["text"].string{
                    completionHandler(htmlString, nil)
                }
            }else{
                completionHandler(nil, error)
            }
        }
    }
}
