//
//  PickupRest.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 2/22/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class PickupREST: NSObject {
    class func create(pickup: PMPickupRequest, completionHandler: @escaping (_ success: Bool, _ error: APError?) -> Void){
        let request = RequestREST(resource: "da/addpickuprequest", method: .post, parameters: pickup.toDictionary())
        
        ServiceREST.request(with: request) { (response) in
            let error  = response.apError(message: response.errorMessage ?? "Could not create pickup request!")
            guard let status = response.json["status"].string, status == "successfully added" else {
                completionHandler(false, error)
                return
            }
            completionHandler(true, nil)
        }
    }
    
    class func edit(pickup: PMPickupRequest, completionHandler: @escaping (_ success: Bool, _ pickup: PMPickupRequest?, _ error: APError?) -> Void){
        let request = RequestREST(resource: "da/editpickuprequest/\(pickup.id)", method: .put, parameters: pickup.toDictionary())
        
        ServiceREST.request(with: request) { (response) in
            let error  = response.apError(message: response.errorMessage ?? "Could not edit pickup request!")
            guard let status = response.json["status"].string, status == "successfully added" else {
                completionHandler(false, nil, error)
                return
            }
            completionHandler(true, nil, nil)
        }
    }
    
    class func getPickupRequests(_ time:PickupRequestsTime, completionHandler: @escaping (_ success: Bool,  _ pickupRequests: [PickupRequest]?, _ error: APError?) -> Void){
        let path = time == .current ? "da/transportrequests" : "da/alltodotransportrequests"
        let request = RequestREST(resource: path, method: .get, parameters: nil)
        
        ServiceREST.request(with: request) { (response) in
            let error = response.apError(message: response.errorMessage ?? "Could not get pickups right now!")
            
            if response.isHttpSuccess{
                guard let jsonArray = response.json.array else {
                    completionHandler(false, nil, error)
                    return
                }
                
                var pickupRequests = [PickupRequest]()
                for item in jsonArray {
                    let pickupRequest = PickupRequest(item)
                    pickupRequests.append(pickupRequest)
                }
                
                completionHandler(true, pickupRequests, error)
                
            }else {
                completionHandler(false, nil, error)
            }
        }
    }
    
    
    class func getPickupNotes (_ id:Int,_ communicationType:Int, completionHandler: @escaping (_ success: Bool,_ pickupNotes:[PickupNote]?,_ error:APError?)->Void){
        let request = RequestREST(resource: "da/pickupnotes/\(id)?communication_type=\(communicationType)", method: .get, parameters: nil)
        
        ServiceREST.request(with: request) { (response) in
            let error = response.apError(message: response.errorMessage ?? "Could not get notes right now !")
            
            if response.isHttpSuccess {
                guard let jsonArray = response.json.array else {
                    completionHandler(false, nil, error)
                    return
                }
                var pickupNotes = [PickupNote]()
                for item in jsonArray {
                    let pickupNote = PickupNote(item)
                    pickupNotes.append(pickupNote)
                }
                completionHandler(true,pickupNotes,nil)
            } else {
                completionHandler(false, nil, error)
            }
        }
    }
    
    class func addNote(_ id: Int,_ communicationType:Int, _ note:String, completionHandler: @escaping(_ success:Bool, _ error: APError?)->Void){
        let parameters:[String:Any] = [
            "id_pickup_handler":id,
            "communication_type":communicationType,
            "notes":note
            ]
        let request = RequestREST(resource: "da/pickupnotes", method: .put, parameters: parameters)
        
        ServiceREST.request(with: request) { (response) in
            let error = response.apError(message: response.errorMessage ?? "Couldn't add note")
            
            if response.isHttpSuccess {
                completionHandler(true,nil)
            } else {
                completionHandler(false, error)
            }
        }
    }
    
    class func getStatuses( completionHandler: @escaping (_ requests: [PickupStatus]?, _ error: APError?) -> Void){
        
        let request = RequestREST(resource: "da/pickupstatuses", method: .get, parameters: nil)
        
        ServiceREST.request(with: request) { (response) in
            let error  = response.apError(message: response.errorMessage ?? "Could not get data!")
            if let array = response.json.array{
                let statuses = PickupStatus.createArray(from: array)
                completionHandler(statuses, nil)
            }else{
                completionHandler( nil, error)
            }
        }
    }
    
    class func resumePickup(_ id:String, completionHandler: @escaping (_ success:Bool,_ error:APError?) -> Void){
        
        let parameters:[String:Any] = [
            "id_pickup_handler":id
        ]
        
        let request = RequestREST(resource: "da/resumetransportrequest", method: .put, parameters: parameters)
        
        ServiceREST.request(with: request) { (response) in
            let error = response.apError(message: response.errorMessage ?? "Could not resume pickup!")
            
            if response.isHttpSuccess {
                completionHandler(true,nil)
            } else {
                completionHandler(false,error)
            }
        }
    }
    
    
    class func completePickup(_ id:String,_ route:String, completionHandler: @escaping (_ success:Bool,_ error:APError?) -> Void){
        
        let parameters:[String:Any] = [
            "id_pickup_handler":id,
            "route": route
        ]
        
        let request = RequestREST(resource: "da/donetransportrequest", method: .put, parameters: parameters)
        
        ServiceREST.request(with: request) { (response) in
            let error = response.apError(message: response.errorMessage ?? "Could not cancel pickup!")
            
            if response.isHttpSuccess {
                if response.json["status"] != "error" {
                    completionHandler(true,nil)
                } else {
                    completionHandler(false,error)
                }
            } else {
                completionHandler(false,error)
            }
        }
    }
    
    class func cancelPickup(_ id:Int,_ cancelReason:Int,_ cancelDetails:String, completionHandler: @escaping (_ success:Bool,_ error:APError?) -> Void){
        
        let parameters:[String:Any] = [
            "id_pickup_handler":"\(id)",
            "pickup_cancelation_reason":cancelReason,
            "description":cancelDetails
        ]
        
        let request = RequestREST(resource: "da/canceltransportrequest", method: .put, parameters: parameters)
        
        ServiceREST.request(with: request) { (response) in
            let error = response.apError(message: response.errorMessage ?? "Could not cancel pickup!")
        
            if response.json["status"] == "success" {
                completionHandler(true,nil)
            } else {
                completionHandler(false,error)
            }
        }
    }
    
    class func undoCancelPickup(_ id:Int, completionHandler: @escaping (_ success:Bool, _ error:APError?)->Void){
        let parameters:[String:String] = [
            "id_pickup_handler":"\(id)"
        ]
        let request = RequestREST(resource: "da/undocanceltransportrequest", method: .put, parameters: parameters)
        
        ServiceREST.request(with: request) { (response) in
            let error = response.apError(message: response.errorMessage ?? "Could not undo cancelation")
            if response.isHttpSuccess {
                completionHandler(true, nil)
            } else {
                completionHandler(false, error)
            }
        }
    }
    
    class func uncompletePickup(_ id:Int, completionHandler: @escaping (_ success:Bool, _ error:APError?)->Void){
        let parameters:[String:String] = [
            "id_pickup_handler":"\(id)"
        ]
        let request = RequestREST(resource: "da/uncompletedtransportrequest", method: .put, parameters: parameters)
        
        ServiceREST.request(with: request) { (response) in
            let error = response.apError(message: response.errorMessage ?? "Could not undo completion")
            if response.isHttpSuccess {
                completionHandler(true, nil)
            } else {
                completionHandler(false, error)
            }
        }
    }
    
    class func startPickup(_ id:Int, _ route:String, completionHandler: @escaping (_ success:Bool, _ error:APError?)->Void){
        let parameters:[String:String] = [
            "id_pickup_handler":"\(id)",
            "route": route
        ]
        let request = RequestREST(resource: "da/startpickuprequest", method: .put, parameters: parameters)
        
        ServiceREST.request(with: request) { (response) in
            let error = response.apError(message: response.errorMessage ?? "Could not start pickup")
            if response.isHttpSuccess {
                completionHandler(true, nil)
            } else {
                completionHandler(false, error)
            }
        }
    }
    
    class func startAnotherPickup(_ startPickupId:Int,_ cancelPickupId:Int, completionHandler: @escaping (_ success:Bool, _ error:APError?)->Void){
        let parameters:[String:String] = [
            "id_current_pickup":"\(cancelPickupId)",
            "id_next_pickup":"\(startPickupId)"
        ]
        let request = RequestREST(resource: "da/startanotherpickup", method: .put, parameters: parameters)
        
        ServiceREST.request(with: request) { (response) in
            let error = response.apError(message: response.errorMessage ?? "Could not start pickup")
            if response.isHttpSuccess {
                completionHandler(true, nil)
            } else {
                completionHandler(false, error)
            }
        }
    }
    
    class func getCompletedPickups(completionHandler: @escaping (_ success:Bool,_ pickups:[PickupRequest]?,_ error:APError?)->Void){
        let request = RequestREST(resource: "da/getcompletedpickups", method: .get, parameters: nil)
        ServiceREST.request(with: request) { (response) in
            let error = response.apError(message: response.errorMessage ?? "Could not get completed pickups")
            if response.isHttpSuccess {
                let pickups = PickupRequest.createPickupsArray(response.json)
                completionHandler(true, pickups, nil)
            } else {
                completionHandler(false, nil, error)
            }
        }
    }
    
    class func completeDropOff(completedPickupsIds:String, completionHandler: @escaping (_ success:Bool, _ error:APError?)->Void){
        let parameters:Parameters = [
            "id": completedPickupsIds
        ]
        let request = RequestREST(resource: "da/completedropoff", method: .put, parameters: parameters)
        ServiceREST.request(with: request) { (response) in
            let error = response.apError(message: response.errorMessage ?? "Could not send completed pickups")
            if response.isHttpSuccess {
                completionHandler(true,nil)
            } else {
                completionHandler(false,error)
            }
        }
    }
    
    class func getMaterialSubtypes( completionHandler: @escaping (_ success:Bool, _ requests: [MaterialSubtype]?, _ error: APError?) -> Void){
        
        let request = RequestREST(resource: "da/materialsubtypes", method: .get, parameters: nil)
        
        ServiceREST.request(with: request) { (response) in
            let error  = response.apError(message: response.errorMessage ?? "Could not get data!")
            if let array = response.json.array {
                let materialSubtypes = MaterialSubtype.createMaterialSubtypesArray(array)
                completionHandler(true, materialSubtypes, nil)
            } else {
                completionHandler( false, nil, error)
            }
        }
    }
    
    class func changePickupsPriority(_ pickups:[String],_ priority:[String], completionHandler: @escaping (_ success:Bool, _ error: APError?) -> Void){
        
        let parameters:Parameters = [
            "id_pickup_handler": pickups.joined(separator: ","),
            "priority": priority.joined(separator: ",")
        ]
        
        let request = RequestREST(resource: "da/changepickupriority", method: .put, parameters: parameters)
        
        ServiceREST.request(with: request) { (response) in
            let error  = response.apError(message: response.errorMessage ?? "Could not get data!")
            if response.isHttpSuccess {
                completionHandler(true, nil)
            } else {
                completionHandler( false, error)
            }
        }
    }
    
}
