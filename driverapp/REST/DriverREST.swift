//
//  DriverRest.swift
//  driverapp
//
//  Created by Arben Pnishi on 23/02/2018.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class DriverREST: NSObject {
    class func changeAvailability(isOnline: Int, reason: Int, completionHandler: @escaping (_ success: Bool, _ error: APError?) -> Void){
        let parameters: Parameters = [
            "driver_availability" : isOnline,
            "reason": reason
        ]
        let request = RequestREST(resource: "da/onlineoffline", method: .put, parameters: parameters)
        
        ServiceREST.request(with: request) { (response) in
            let error = response.apError(message: response.errorMessage ?? "Could not update availibilty right now!")
            
            if response.isHttpSuccess{
                completionHandler(true, nil)
            }else {
                completionHandler(false, error)
            }
        }
    }
    
    class func sendLocation(_ latitude:Double, _ longitude:Double, completionHandler: @escaping (_ success:Bool, _ error:APError?)->Void){
        let parameters:[String:String] = [
            "latitude":"\(latitude)",
            "longitude":"\(longitude)"
        ]
        
        let request = RequestREST(resource: "da/locationupdate", method: .put, parameters: parameters)
        
        ServiceREST.request(with: request) { (response) in
            let error = response.apError(message: response.errorMessage ?? "Could not send location")
            if response.isHttpSuccess {
                completionHandler(true, nil)
            } else {
                completionHandler(false, error)
            }
        }
    }
    
    class func getVehicles(completionHandler: @escaping (_ success:Bool,_ trucks:[Vehicle]?, _ error:APError?)->Void){
        
        let request = RequestREST(resource: "da/trucks", method: .get, parameters: nil)
        
        ServiceREST.request(with: request) { (response) in
            let error = response.apError(message: response.errorMessage ?? "Could not send location")
            
            if response.isHttpSuccess {
                var trucks:[Vehicle] = [] 
                trucks = Vehicle.createTrucksArray(response.json)
                completionHandler(true, trucks, nil)
            } else {
                completionHandler(false, nil, error)
            }
        }
    }
    
    class func chooseVehicle(_ vehidleID:Int, completionHandler: @escaping(_ success:Bool,_ error:APError?)->Void){
        let parameters:[String:Int] = [
            "id_truck": vehidleID
        ]
        let request = RequestREST(resource: "da/choosetruck", method: .post, parameters: parameters)
        
        ServiceREST.request(with: request) { (response) in
            let error = response.apError(message: response.errorMessage ?? "Could not send location")
            if response.isHttpSuccess {
                if let settingsString = response.json["settings"].string {
                    let convertedSettingsString = settingsString.replacingOccurrences(of: "'", with: "\"")
                    let settingsJSON = JSON.init(parseJSON: convertedSettingsString)
                    NavigationManager.applySettings(from: settingsJSON)
                }
                if let attributesString = response.json["attributes"].string {
                    let convertedAttributesString = attributesString.replacingOccurrences(of: "'", with: "\"")
                    let attributesJSON = JSON.init(parseJSON: convertedAttributesString)
                    NavigationManager.applyAttributes(from: attributesJSON)
                }
                completionHandler(true, nil)
            } else {
                completionHandler(false, error)
            }
        }
    }
    
    class func leaveVehicle(_ vehidleID:Int, completionHandler: @escaping(_ success:Bool,_ error:APError?)->Void){

        let request = RequestREST(resource: "da/leavetruck", method: .put, parameters: nil)
        
        ServiceREST.request(with: request) { (response) in
            let error = response.apError(message: response.errorMessage ?? "Could not leave vehicle")
            if response.isHttpSuccess {
                completionHandler(true, nil)
            } else {
                completionHandler(false, error)
            }
        }
    }
}
