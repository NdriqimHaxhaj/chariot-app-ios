//
//  UserRest.swift
//  driverapp
//
//  Created by Arben Pnishi on 22/02/2018.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class UserREST: NSObject {
    
    class func loginWith(email: String, password: String, completionHandler: @escaping (_ success: Bool, _ token: String?, _ firstLogin: Bool, _ role: UserRole?, _ idUser:Int?, _ error: APError?) -> Void){
        let parameters: Parameters = [
            "email": email,
            "password": password
        ]
        let request = RequestREST(resource: "login", method: .get, parameters: parameters)
        
        ServiceREST.request(with: request) { (response) in
            let error = response.apError(message: response.errorMessage ?? "Could not login right now!")
            
            if response.isHttpSuccess{
                guard let status = response.json["status"].string, status == "success", let token = response.json["remember_token"].string, let role = response.json["role"].string, let userRole = UserRole(rawValue: role),let idUser = response.json["id_user"].int, userRole == .driver else{
                    completionHandler(false, nil, false, nil, nil, error)
                    return
                }
                
                let firstLogin = response.json["first_login"].bool ?? false
                
                completionHandler(true, token, firstLogin, userRole, idUser, nil)
                
            }else {
                completionHandler(false, nil, false, nil,nil, error)
            }
        }
    }
    
    class func getUserProfile(with userRole: UserRole?, completionHandler: @escaping (_ user: User?, _ error: APError?) -> Void){
        let request = RequestREST(resource: "profile", method: .get, parameters: nil)
        
        ServiceREST.request(with: request) { (response) in
            let error  = response.apError(message: response.errorMessage ?? "Could not login right now!")
            
            guard response.json["email"].string != nil else{
                completionHandler(nil, error)
                return
            }
            let role = userRole ?? .driver
            if let user = User.create(from: response.json){
                user.role = role.rawValue
                completionHandler(user, nil)
            }else{
                completionHandler(nil, error)
            }
        }
    }
    
    class func updateProfile(with name: String, phone: String, completionHandler: @escaping (Success, APError?) -> Void){
        let parameters: Parameters = [
            "name": name,
            "phone": phone
        ]
        let request = RequestREST(resource: "profiledata", method: .put, parameters: parameters)
        
        ServiceREST.request(with: request) { (response) in
            let error  = response.apError(message: response.errorMessage ?? "Could not update profile right now!")
            
            guard let status = response.json["status"].string, status == "success" else{
                completionHandler(false, error)
                return
            }
            let user = AccountManager.currentUser
            user?.name = name
            user?.phone = phone
            AccountManager.currentUser = user
            completionHandler(true, nil)
        }
    }
    
    class func changePassword(with current: String, new: String, completionHandler: @escaping (Success, APError?) -> Void){
        let parameters: Parameters = [
            "current_password": current,
            "new_password": new,
            "new_password_confirm": new
        ]
        let request = RequestREST(resource: "changepassword", method: .put, parameters: parameters)
        
        ServiceREST.request(with: request) { (response) in
            let error  = response.apError(message: response.errorMessage ?? "Could not change password right now!")
            
            guard let status = response.json["status"].string, status == "success" else{
                completionHandler(false, error)
                return
            }
            completionHandler(true, nil)
        }
    }
    
    class func changeEmail(with email: String, completionHandler: @escaping (Success, APError?) -> Void){
        let parameters: Parameters = [
            "new_email": email
        ]
        let request = RequestREST(resource: "changeemail", method: .put, parameters: parameters)
        
        ServiceREST.request(with: request) { (response) in
            let error  = response.apError(message: response.errorMessage ?? "Could not change email right now!")
            
            guard let status = response.json["status"].string, status == "success" else{
                completionHandler(false, error)
                return
            }
            let user = AccountManager.currentUser
            user?.email = email
            AccountManager.currentUser = user
            completionHandler(true, nil)
        }
    }
    
    class func logOut(completionHandler: @escaping () -> Void){
        let request = RequestREST(resource: "logout", method: .get, parameters: nil)
        
        ServiceREST.request(with: request) { (response) in
            //doesnt need to check if the response is OK or FAIL. Log the user out
            completionHandler()
        }
    }
    
    class func deleteProfilePicture(completionHandler: @escaping () -> Void){
        let parameters: Parameters = [
            "image_url": ""
        ]
        let request = RequestREST(resource: "profileimage", method: .put, parameters: parameters)
        
        ServiceREST.request(with: request) { (response) in
            //doesnt need to check if the response is OK or FAIL. Log the user out
            completionHandler()
        }
    }
    
    class func forgotPassword(email: String, completionHandler: @escaping (Bool) -> Void){
        let parameters: Parameters = [
            "email": email
        ]
        let request = RequestREST(resource: "forgetpassword", method: .get, parameters: parameters)
        
        ServiceREST.request(with: request) { (response) in
            completionHandler(response.isHttpSuccess)
        }
    }
    
    class func uploadProfilePicture(image: UIImage, completionHandler: @escaping (_ imageUrl: String?, _ error: APError?) -> Void){
        let url = URL(string: "profileimage", relativeTo: URL(string: Constants.baseURL))!
        
        let headers: HTTPHeaders = [
            "Accept": "application/json",
            "Authorization": "Bearer \(AccountManager.userToken ?? "")"
        ]
        let request: URLRequest = try! URLRequest(url: url, method: .post, headers: headers)
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            let imageData = UIImageJPEGRepresentation(image, 0.5);
            let fileName = ("file_\(Helper.getCurrentMillis()).jpg")
            multipartFormData.append(imageData!, withName: "file", fileName: fileName, mimeType: "image/jpg")
        }, with: request) { (encodingResult) in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    if(upload.response?.statusCode == 200){
                        console("Upload response: \(response.description)")
                        let serverPathTrimmed: String = response.description.replacingOccurrences(of: "SUCCESS: ", with: "", options: NSString.CompareOptions.literal, range:nil)
                        completionHandler(serverPathTrimmed, nil)
                    }else{
                        completionHandler(nil, APError(message: "Error while uploading image!", code: 11))
                    }
                }
            case .failure(_):
                completionHandler(nil, APError(message: "Error while uploading image!", code: -1))
            }
        }
    }
    
    class func updateFirebaseToken(token: String, completionHandler: @escaping () -> Void){
        let parameters: Parameters = [
            "token": token
        ]
        let request = RequestREST(resource: "da/updatefirebasetoken", method: .put, parameters: parameters)
        
        ServiceREST.request(with: request) { (response) in
            
            completionHandler()
        }
    }
    
    class func getDropOffLocations(completionHandler: @escaping (_ success: Bool,_ drops:[DropOffLocation]?, _ error: APError?) -> Void){
        
        let request = RequestREST(resource: "da/dropofflocations", method: .get, parameters: nil)
        
        ServiceREST.request(with: request) { (response) in
            let error  = response.apError(message: response.errorMessage ?? "Could not change password right now!")
            
            if response.isHttpSuccess{
                guard let jsonArray = response.json.array else {
                    completionHandler(false, nil, error)
                    return
                }
                
                var dropsArray = [DropOffLocation]()
                for item in jsonArray {
                    let drop = DropOffLocation(item)
                    dropsArray.append(drop)
                }
                
                completionHandler(true, dropsArray, error)
                
            }else {
                completionHandler(false, nil, error)
            }
        }
    }
    
    class func getOfflineReasons(completionHandler: @escaping (_ success: Bool,_ drops:[OfflineReason]?, _ error: APError?) -> Void){
        
        let request = RequestREST(resource: "da/getofflinereasons", method: .get, parameters: nil)
        
        ServiceREST.request(with: request) { (response) in
            let error  = response.apError(message: response.errorMessage ?? "Could not change password right now!")
            
            if response.isHttpSuccess{
                guard let jsonArray = response.json.array else {
                    completionHandler(false, nil, error)
                    return
                }
                
                var offlineReasonsArray = [OfflineReason]()
                for item in jsonArray {
                    let offlineReason = OfflineReason(item)
                    offlineReasonsArray.append(offlineReason)
                }
                
                completionHandler(true, offlineReasonsArray, error)
                
            }else {
                completionHandler(false, nil, error)
            }
        }
    }
    
    class func reviewUser(_ buildingId:Int, _ toIdUser:Int, _ rating:Int, _ description:String, completionHandler: @escaping (_ status:Bool,_ error:APError?)-> Void){
        
        let parameters: Parameters = [
            "id_building":buildingId,
            "to_id_user":toIdUser,
            "rating":rating,
            "description": description
        ]
        
        let request = RequestREST(resource: "si/userrating", method: .post, parameters: parameters)
        
        ServiceREST.request(with: request) { (response) in
            let error  = response.apError(message: response.errorMessage ?? "Could not change password right now!")
            if response.isSuccess {
                completionHandler(true,nil)
            } else {
                completionHandler(false,error)
            }
        }
    }
    
    class func leaveTruck(completionHandler: @escaping (_ status:Bool, _ error:APError?)->Void){
        let request = RequestREST(resource: "da/leavetruck", method: .put, parameters: nil)
        
        ServiceREST.request(with: request) { (response) in
            let error  = response.apError(message: response.errorMessage ?? "Could not leave truck right now!")
            
            if response.json["status"].string == "successfully updated!" {
                completionHandler(true, nil)
            } else {
                completionHandler(false, error)
            }
        }
    }
    
    class func getAddresses(_ addressType: AddressType, completionHandler: @escaping (_ success: Bool,_ addresses:[Address], _ error: APError?) -> Void){
        
        
        let resource = "da/favoritebuildings?type=\(addressType.rawValue)"
        
        let request = RequestREST(resource: resource, method: .get, parameters: nil)
        
        ServiceREST.request(with: request) { (response) in
            let error  = response.apError(message: response.errorMessage ?? "Could not get addreses right now!")
            
            if response.isHttpSuccess{
                guard let jsonArray = response.json.array else {
                    completionHandler(false, [], error)
                    return
                }
                
                let addresses = Address.createAddressesArray(jsonArray)
                completionHandler(true, addresses, error)
                
            }else {
                completionHandler(false, [], error)
            }
        }
    }
    
    class func getFavouriteAddresses( completionHandler: @escaping (_ success: Bool,_ addresses:[FavouriteAddress], _ error: APError?) -> Void){
        
        
        let resource = "da/favoritebuildings?type=\(AddressType.favourite.rawValue)"
        
        let request = RequestREST(resource: resource, method: .get, parameters: nil)
        
        ServiceREST.request(with: request) { (response) in
            let error  = response.apError(message: response.errorMessage ?? "Could not get addreses right now!")
            
            if response.isHttpSuccess{
                guard let jsonArray = response.json.array else {
                    completionHandler(false, [], error)
                    return
                }
                
                let addresses = FavouriteAddress.createAddressesArray(jsonArray)
                completionHandler(true, addresses, error)
                
            }else {
                completionHandler(false, [], error)
            }
        }
    }
    
    class func editFavouriteAddresses(_ id:[String],_ priority:[String],_ deleted:[String], completionHandler: @escaping (_ success: Bool, _ error: APError?) -> Void){
        
        let parameters:Parameters = [
            "id": id.joined(separator:","),
            "priority":priority.joined(separator:","),
            "deleted":deleted.joined(separator:",")
        ]
        
        let resource = "da/changefavoritebuilding"
        
        let request = RequestREST(resource: resource, method: .put, parameters: parameters)
        
        ServiceREST.request(with: request) { (response) in
            let error  = response.apError(message: response.errorMessage ?? "Could not get addreses right now!")
            
            if response.isHttpSuccess{
                completionHandler(true, nil)
                
            }else {
                completionHandler(false, error)
            }
        }
    }
    
    
    
    //da/addfavoritebuilding
    class func addFavouriteAddress(_ address: Address, completionHandler: @escaping (_ success: Bool, _ error: APError?) -> Void){
        
        
        
        let parameters:Parameters = [
            "name" : address.buildingName,
            "address" : address.buildingAddress,
            "latitude" : "\(address.latitude)",
            "longitude" : "\(address.longitude)",
            "zip_code": "\(address.zipCode)"
        ]
        
        let request = RequestREST(resource: "da/addfavoritebuilding", method: .post, parameters: parameters)
        
        ServiceREST.request(with: request) { (response) in
            let error  = response.apError(message: response.errorMessage ?? "Could not add favourite address!")
            
            if response.isHttpSuccess{
                completionHandler(true, nil)
                
            }else {
                completionHandler(false, error)
            }
        }
    }
    
}

