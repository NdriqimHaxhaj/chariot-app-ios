//
//  SelectTruckView.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 9/7/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

protocol SelectVehicleDelegate:class {
    func shouldOpenTrucksList(_ status:Bool)
}

class SelectTruckView: UIView {
    
    // MARK: - IBOulets
    var view:UIView!
    @IBOutlet weak var truckTitle: UILabel!
    @IBOutlet weak var arrowImage: UIImageView!
    
    // MARK: - Properties
    var isTrucksListOpen = false
    var vehicleName = "" {
        didSet{
            truckTitle.text = vehicleName
            self.setNeedsDisplay()
        }
    }
    var delegate:SelectVehicleDelegate?
    
    // MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Functions
    @IBAction func viewTapped(_ sender: UITapGestureRecognizer) {
        isTrucksListOpen = !isTrucksListOpen
        arrowImage.image = isTrucksListOpen ? #imageLiteral(resourceName: "Up Arrow") : #imageLiteral(resourceName: "Down Arrow")
        delegate?.shouldOpenTrucksList(isTrucksListOpen)
    }
    
}

extension SelectTruckView {
    // MARK: - Initial functions
    @objc func setup(){
        view = loadViewFromNib()
        view?.frame = bounds
        view?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view!)
        if ThemeManager.isNightMode(){
            view.backgroundColor = Appearance.darkGrayNight
            truckTitle.textColor = UIColor.white
        }
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(setup),
                                               name: Notification.Name.changeTheme,
                                               object: nil)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        return UINib(nibName: "SelectTruckView", bundle: bundle).instantiate(withOwner: self, options: nil)[0] as! UIView
    }
}
