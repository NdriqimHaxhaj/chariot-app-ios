//
//  DropOffLocationCell.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 6/20/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

class DropOffLocationCell: UITableViewCell {
    
    //MARK: - IBOutlets
    @IBOutlet weak var icon: UIView!
    @IBOutlet weak var iconWrapper: UIView!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var buildingNameLabel: UILabel!
    
    
    //MARK: - Properties
    var drop:DropOffLocation!{
        didSet{
            setupUI()
            setValues()
        }
    }
    var isDropSelected = false {
        didSet{
           setupUI()
        }
    }
    
    //MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }

    //MARK: - Functions
    func setupUI(){
        icon.backgroundColor = isDropSelected ? Appearance.greenColor : UIColor.white
        iconWrapper.borderColor = isDropSelected ? Appearance.greenColor : UIColor.lightGray
        if ThemeManager.isNightMode() {
            locationLabel.textColor = UIColor.white
            buildingNameLabel.textColor = UIColor.white
        }
    }
    
    func setValues(){
        locationLabel.text = drop.name
        buildingNameLabel.text = drop.address
    }
    
    
}
