//
//  PickupNoteCell.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 2/26/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

enum MessageType {
    case sentMessage
    case receivedMessage
}

class PickupNoteCell: UITableViewCell {
    
    // MARK: - Properties
    var note:PickupNote!{
        didSet{
            self.setInfo()
        }
    }
    
    //Helper property
    lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter
    }()
    

    // MARK: - IBOutlets
    @IBOutlet weak var senderLabel: UILabel!
    @IBOutlet weak var receivedTimeLabel: UILabel!
    @IBOutlet weak var messageViewContainer: UIView!
    @IBOutlet weak var messageTextView: UITextView!
    
    // MARK: - Constraints
    @IBOutlet weak var senderTitleTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var senderTitleHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var messageTimeHeightConstraint: NSLayoutConstraint!
      @IBOutlet weak var messageContainerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var messageTimeBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var messageContainerLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var messageContainerTrailingConstraint: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setInfo(){
        let dispatcherName = note.dispatcherName
        self.senderLabel.text = note.idDriver == AccountManager.userId ? "You" : dispatcherName
        dateFormatter.dateFormat = "hh:mm a"
        self.receivedTimeLabel.text = "\(dateFormatter.string(from: note.createdAt!))"
        self.messageTextView.text = note.notes
        self.updateTexts()
        updateUI()
        
    }
    
    func updateUI(){
        let size = CGSize(width: self.frame.width, height: 1000)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        let estimatedFrame = NSString(string: note.notes!).boundingRect(with: size, options: options, attributes: [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 14)], context: nil)
        self.messageContainerHeightConstraint.constant = estimatedFrame.height + 30
        print("estimatedFrame.height : \(estimatedFrame.height+30)")
        self.layoutIfNeeded()
    }
    
    func updateTexts(){
        let isNightMode = ThemeManager.themeMode == Mode.Night.rawValue
        
        senderLabel.textColor = isNightMode ? UIColor.white : UIColor.darkGray
        receivedTimeLabel.textColor = isNightMode ? UIColor.white : UIColor.darkGray
        
        if note.idDriver == AccountManager.userId { // If the sender is Driver
            senderLabel.text = "You"
            senderLabel.textAlignment = .right
            receivedTimeLabel.textAlignment = .right
            messageViewContainer.backgroundColor = isNightMode ? UIColor.white : Appearance.greenColor
            messageTextView.textColor = isNightMode ? UIColor.black : UIColor.white
            messageContainerLeadingConstraint.constant = 100
            messageContainerTrailingConstraint.constant = -20
        } else { // If the sender is Dispatcher
            senderLabel.text = note.dispatcherName
            senderLabel.textAlignment = .left
            receivedTimeLabel.textAlignment = .left
            messageViewContainer.backgroundColor = isNightMode ? Appearance.darkGrayNight : Appearance.receivedMessageBackgroundColor
            messageTextView.textColor = isNightMode ? UIColor.white : Appearance.receivedMessageTextColor
            messageContainerTrailingConstraint.constant = -100
            messageContainerLeadingConstraint.constant = 20
        }
        self.layoutIfNeeded()
    }
    
}
