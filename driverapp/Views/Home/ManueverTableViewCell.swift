//
//  ManueverTableViewCell.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 10/6/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import NMAKit

class ManueverTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var manueverIconView: UIImageView!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var streetLabel: UILabel!
    @IBOutlet weak var currentManueverIconView: UIImageView!
    
    // MARK: - Properties
    var manuever:NMAManeuver!{
        didSet{
            setValues()
        }
    }
    var isCurrentManeuver = false {
        didSet{
            currentManueverIconView.isHidden = isCurrentManeuver ? false : true
        }
    }
    var isOddCell:Bool!{
        didSet{
            mainView.backgroundColor = isOddCell ? ThemeManager.isNightMode() ? UIColor.darkGray : UIColor(red255: 248, green255: 248, blue255: 248) : UIColor.clear
            setupUI()
        }
    }
    
    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    // MARK: - Functions
    func setupUI(){
        if ThemeManager.isNightMode() {
            distanceLabel.textColor = .white
        }
    }
    
    func setValues(){
        manueverIconView.image = getImageFor(Int(manuever.icon.rawValue))
        
        let distance = Double(manuever.distanceFromStart)*0.00062137
        distanceLabel.text = "\(String(format: "%.2f", distance)) miles"
        
        if let roadName = manuever.roadName{
            streetLabel.text = String(roadName)
        }
    }
    
    fileprivate func getImageFor(_ value:Int)->UIImage? {
        var imageName = ThemeManager.isNightMode() ? "\(value)-white" : "\(value)"
        guard let image = UIImage(named: "\(imageName)") else {
            print("Couldn't get signpost for this direction")
            print("DEBUG: Signpost value: \(value)")
            return nil
        }
        return image
    }
    
}
