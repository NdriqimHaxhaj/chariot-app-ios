//
//  PickupNoteCell.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 2/26/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

class MessageCell: UITableViewCell {
    
    // MARK: - Properties
    var message:PickupChat!{ // This will be MessageNote
        didSet{
            self.setInfo()
        }
    }
    
    //Helper property
    lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter
    }()
    
    // MARK: - IBOutlets
    @IBOutlet weak var senderLabel: UILabel!
    @IBOutlet weak var receivedTimeLabel: UILabel!
    @IBOutlet weak var messageViewContainer: UIView!
    @IBOutlet weak var messageTextView: UITextView!
    
    // MARK: - Constraints
    @IBOutlet weak var senderTitleTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var senderTitleHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var messageTimeHeightConstraint: NSLayoutConstraint!
      @IBOutlet weak var messageContainerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var messageTimeBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var messageContainerLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var messageContainerTrailingConstraint: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        // Testing branches
    }
    
    func setInfo(){
        self.senderLabel.text = "YOU"
        dateFormatter.dateFormat = "hh:mm a"
        self.receivedTimeLabel.text = "\(dateFormatter.string(from: message.createdAt!))"
        self.messageTextView.text = message.chatText
        self.updateTexts()
        updateUI()
    }
    
    func updateUI(){
        let size = CGSize(width: self.frame.width-150, height: 1000)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        let estimatedFrame = NSString(string: message.chatText!).boundingRect(with: size, options: options, attributes: [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 14)], context: nil)
        self.messageContainerHeightConstraint.constant = estimatedFrame.height + 30
        
        print("estimatedFrame.height : \(estimatedFrame.height+30)")
        self.layoutIfNeeded()
    }
    
    func updateTexts(){
        let isNightMode = ThemeManager.isNightMode()
        if message.idUser == 0 {
            senderLabel.textAlignment = .right
            senderLabel.textColor = isNightMode ? UIColor.white : UIColor.darkGray
            receivedTimeLabel.textAlignment = .right
            receivedTimeLabel.textColor = isNightMode ? UIColor.white : UIColor.darkGray
            messageViewContainer.backgroundColor = isNightMode ? UIColor.white : Appearance.greenColor
            messageTextView.textColor = isNightMode ? UIColor.black : UIColor.white
            messageContainerLeadingConstraint.constant = 100
            messageContainerTrailingConstraint.constant = -20
            
        } else {
            let name = message.dispatcherName
            senderLabel.text = name != "" ? name : "Dispatcher"
            senderLabel.textColor = isNightMode ? UIColor.white : UIColor.darkGray
            senderLabel.textAlignment = .left
            receivedTimeLabel.textAlignment = .left
            receivedTimeLabel.textColor = isNightMode ? UIColor.white : UIColor.darkGray
            messageViewContainer.backgroundColor = isNightMode ? Appearance.darkGrayNight : Appearance.receivedMessageBackgroundColor
            messageTextView.textColor = isNightMode ? UIColor.white : Appearance.receivedMessageTextColor
            messageContainerTrailingConstraint.constant = -100
            messageContainerLeadingConstraint.constant = 20
        }
    }
}
