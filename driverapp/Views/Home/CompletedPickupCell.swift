//
//  CompletedPickupCell.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 7/5/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

class CompletedPickupCell: UITableViewCell {
    
    //MARK: - IBOutlets
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var tickView: UIView!
    @IBOutlet weak var tickIcon: UIImageView!
    @IBOutlet weak var destinationLabel: UILabel!
    @IBOutlet weak var buildingNameLabel: UILabel!
    
    //MARK: - Properties
    var pickupRequest:PickupRequest! {
        didSet{
            destinationLabel.text = pickupRequest.buildingAddress
            buildingNameLabel.text = pickupRequest.buildingName
        }
    }
    override var isSelected: Bool {
        didSet{
            fillCheck(isSelected)
        }
    }
    
    //MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }

    //MARK: - Functions
    fileprivate func setupUI(){
        tickView.borderColor = isSelected ? Appearance.greenColor : UIColor(red255: 196, green255: 196, blue255: 196)
        if ThemeManager.isNightMode() {
            cellView.backgroundColor = Appearance.darkGrayNight
            destinationLabel.textColor = UIColor.white
            cellView.borderColor = UIColor.white
        }
    }
    
    fileprivate func fillCheck(_ bool: Bool){
        cellView.borderColor = bool ? Appearance.greenColor : UIColor(red255: 196, green255: 196, blue255: 196)
        tickView.borderColor = bool ? Appearance.greenColor : UIColor(red255: 196, green255: 196, blue255: 196)
        tickIcon.alpha = bool ? 1 : 0
    }
    
}
