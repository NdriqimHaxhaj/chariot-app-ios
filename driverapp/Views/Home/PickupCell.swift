//
//  PickupCell.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 2/20/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

enum FirstPickupStatus {
    case onGoingPickup
    case waitingForActionPickup
    case postponed
}

protocol PickupDelegate:class {
    func triggerNavigation(_ id:Int, _ route:String)
    func refreshPickupDelegateFromCell()
    func editPickup(_ pickup: PickupRequest)
}

class PickupCell: UITableViewCell {
    
    //MARK: - IBOutlets
    @IBOutlet weak var startPickup: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imageContainerView: UIView!
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var editButton: UIButton!
    
    //MARK: Labels
    @IBOutlet weak var buildingNameLabel: UILabel!
    @IBOutlet weak var buildingAddressLabel: UILabel!
    @IBOutlet weak var bagCount: UILabel!
    @IBOutlet weak var bagCountLabel: UILabel!
    @IBOutlet weak var preferredPickupTime: UILabel!
    @IBOutlet weak var preferredPickupTimeLabel: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var pickupType: UILabel!
    @IBOutlet weak var pickupStatusLabel: UILabel!
    
    
    //MARK: - Properties
    var pickupRequest: PickupRequest!{
        didSet{
            setInfos()
            handlePickupType()
        }
    }
    var completedIcon = UIImage(named: "completed")
    var canceledIcon = UIImage(named: "canceled pickup")
    var overdueIcon = UIImage(named: "overdue pickup")
    var isFirstPickup = false
    var isLastPickup = false
    var pickupStatus: FirstPickupStatus = .waitingForActionPickup {
        didSet{
            var title = ""
            switch pickupStatus {
            case .waitingForActionPickup:
                startPickup.backgroundColor = Appearance.greenColor
                title = "Start Pickup"
                break
            case .onGoingPickup:
                title = "Mark as completed"
                startPickup.backgroundColor = Appearance.greenColor
                break
            case .postponed:
                startPickup.backgroundColor = Appearance.canceledRed
                title = "Resume Pickup"
            }
            startPickup.setTitle(title, for: .normal)
        }
    }
    weak var delegate:PickupDelegate?
    
    //MARK: Helper variables
    lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter
    }()
    
    // MARK: - Constraints
    @IBOutlet weak var containerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomContainerViewConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        setInfos()
        handlePickupType()
    }
    
    // MARK - IBActions
    @IBAction func startPickup(_ sender: UIButton) {
        print("Pickup started")
        switch pickupStatus {
            case .onGoingPickup:
                let id = "\(pickupRequest.id!)"
                PickupREST.completePickup(id, "[]") { (success, error) in
                    if success {
                        self.delegate?.refreshPickupDelegateFromCell()
                    }
                }
                break
            case .waitingForActionPickup:
                let route = "\(self.pickupRequest.latitude!), \(self.pickupRequest.longitude!)"
                self.delegate?.triggerNavigation(self.pickupRequest.id!, route)
                self.pickupStatus = .onGoingPickup
                break
            case .postponed:
                if startPickup.titleLabel?.text == "Mark as completed" {
                    let id = "\(pickupRequest.id!)"
                    PickupREST.completePickup(id, "[]") { (success, error) in
                        if success {
                            self.delegate?.refreshPickupDelegateFromCell()
                        }
                    }
                } else {
                    let route = "\(self.pickupRequest.latitude!), \(self.pickupRequest.longitude!)"
                    self.delegate?.triggerNavigation(self.pickupRequest.id!, route)
                    self.pickupStatus = .onGoingPickup
                }
                break
        }
    }
    
    @IBAction func editButtonPressed(_ sender: UIButton) {
        delegate?.editPickup(pickupRequest)
    }
    
    //MARK: Helper functions
    func setInfos(){
        buildingNameLabel.text = pickupRequest.buildingName
        buildingAddressLabel.text = pickupRequest.buildingAddress
        self.bagCountLabel.text = "\(pickupRequest.bagCount!)"
        dateFormatter.dateFormat = "h:mm a"
        let startPickupTime = dateFormatter.string(from: pickupRequest.startPickupTime!)
        let endPickupTime = dateFormatter.string(from: pickupRequest.endPickupTime!)
        self.preferredPickupTimeLabel.text = "\(startPickupTime)-\(endPickupTime)"
        self.locationLabel.text = pickupRequest.loadLocationName
        self.pickupStatusLabel.text = pickupRequest.pickupStatusName?.replacingOccurrences(of: "_", with: " ")
    }
    
    //MARK: Functions
    private let cellHeight: CGFloat = 200
    private let expandedCellHeight: CGFloat = 200
    
    private let bottomSpace: CGFloat = 20
    private let expanedBottomSpace: CGFloat = 20
    private let lastCellExpanedBottomSpace: CGFloat = 40
    
    func handlePickupType(){
        let isNightMode = ThemeManager.isNightMode()
        editButton.alpha = 0
        switch pickupRequest.pickupStatus {
        case .assigned:
            if pickupRequest.requestedBy == PickupCreator.me {
                editButton.alpha = 1
            }
            containerViewHeightConstraint.constant = isFirstPickup ? expandedCellHeight : cellHeight
            bottomContainerViewConstraint.constant = isLastPickup ? lastCellExpanedBottomSpace : isFirstPickup ? expanedBottomSpace : bottomSpace
            containerView.borderColor = isNightMode ? Appearance.darkGrayNight : isFirstPickup ? Appearance.greenColor : Appearance.lightGray
            startPickup.alpha = isFirstPickup ? 1 : 0
            containerView.backgroundColor = isNightMode ? Appearance.darkGrayNight : UIColor.white
            changeLabelsColors(color: isNightMode ? "FFFFFF" : "868686")
            buildingNameLabel.textColor = Appearance.greenColor
            buildingAddressLabel.textColor = Appearance.greenColor
            imageContainerView.isHidden = true
            pickupStatus = .waitingForActionPickup
            break
        case .completedWithDelays:
            containerViewHeightConstraint.constant = cellHeight
            containerView.borderColor = Appearance.greenColor
            containerView.backgroundColor = isNightMode ? Appearance.darkGrayNight : UIColor.HexToColor("bae9c0")
            changeLabelsColors(color: isNightMode ? "FFFFFF" : "0a8036")
            bottomContainerViewConstraint.constant = isLastPickup ? lastCellExpanedBottomSpace : isFirstPickup ? expanedBottomSpace : bottomSpace
            startPickup.alpha = 0
            imageContainerView.isHidden = true
            iconImage.image = completedIcon
            pickupStatus = .waitingForActionPickup
            break
        case .completedWithoutDelays, .dropeedOff:
            containerViewHeightConstraint.constant = cellHeight
            containerView.borderColor = Appearance.greenColor
            startPickup.alpha = 0
            containerView.backgroundColor = isNightMode ? Appearance.darkGrayNight : UIColor.HexToColor("bae9c0")
            changeLabelsColors(color: isNightMode ? "FFFFFF" : "0a8036")
            imageContainerView.isHidden = false
            imageContainerView.backgroundColor = Appearance.greenColor
            iconImage.image = completedIcon
            pickupStatus = .waitingForActionPickup
            break
        case .canceled, .postponed, .paused:
            containerViewHeightConstraint.constant = cellHeight
            containerView.backgroundColor = isNightMode ? Appearance.darkGrayNight : UIColor.white
            containerView.borderColor = UIColor.HexToColor("ff6b6b")
            startPickup.alpha = 0
            changeLabelsColors(color: isNightMode ? "FFFFFF" : "ff6b6b")
            imageContainerView.isHidden = false
            imageContainerView.backgroundColor = UIColor.HexToColor("ff6b6b")
            iconImage.image = canceledIcon
            if pickupRequest.pickupStatus == .postponed ||
               pickupRequest.pickupStatus == .paused {
                startPickup.backgroundColor = Appearance.canceledRed
                pickupStatus = .postponed
                startPickup.alpha = 1
                containerViewHeightConstraint.constant = expandedCellHeight
                bottomContainerViewConstraint.constant = lastCellExpanedBottomSpace
            } else {
                pickupStatus = .waitingForActionPickup
            }
            
            break
        case .overdue:
            containerViewHeightConstraint.constant = isFirstPickup ? expandedCellHeight : cellHeight
            containerView.backgroundColor = isNightMode ? Appearance.darkGrayNight : UIColor.white
            containerView.borderColor = UIColor.HexToColor("f16e2f")
            bottomContainerViewConstraint.constant = isLastPickup ? lastCellExpanedBottomSpace : isFirstPickup ? expanedBottomSpace : bottomSpace
            startPickup.alpha = isFirstPickup ? 1 : 0
            changeLabelsColors(color: isNightMode ? "FFFFFF" : "f16e2f")
            imageContainerView.isHidden = false
            imageContainerView.backgroundColor = UIColor.HexToColor("f16e2f")
            iconImage.image = overdueIcon
            pickupStatus = .waitingForActionPickup
            break
        case .ongoing:
            containerViewHeightConstraint.constant = isFirstPickup ? expandedCellHeight : cellHeight
            bottomContainerViewConstraint.constant = expanedBottomSpace
            containerView.backgroundColor = isNightMode ? Appearance.darkGrayNight : UIColor.white
            containerView.borderColor = Appearance.greenColor
            startPickup.alpha = 1
            changeLabelsColors(color: isNightMode ? "FFFFFF" : "868686")
            buildingNameLabel.textColor = Appearance.greenColor
            buildingAddressLabel.textColor = Appearance.greenColor
            imageContainerView.isHidden = true
            pickupStatus = .onGoingPickup
            break
        default:
            
            console("hello default")
            break
        }
        layoutIfNeeded()
    }
    
    func changeLabelsColors(color: String){
        let color = UIColor.HexToColor(color)
        buildingNameLabel.textColor = color
        buildingAddressLabel.textColor = color
        bagCount.textColor = color
        bagCountLabel.textColor = color
        preferredPickupTime.textColor = color
        preferredPickupTimeLabel.textColor = color
        location.textColor = color
        locationLabel.textColor = color
        pickupType.textColor = color
        pickupStatusLabel.textColor = color
    }
}
