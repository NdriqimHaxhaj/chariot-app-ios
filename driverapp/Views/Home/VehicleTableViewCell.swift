//
//  TruckTableViewCell.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 9/3/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

protocol VehicleCellDelegate:class {
    func didSelect(vehicle: Vehicle, status: Bool)
}

class VehicleTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var borderedView: UIView!
    @IBOutlet weak var yourSelectionLabel: UILabel!
    @IBOutlet weak var tickButton: UIButton!
    @IBOutlet weak var vehicleName: UILabel!
    @IBOutlet weak var vehicleSubtype: UILabel!
    @IBOutlet weak var licensePlateNumber: UILabel!
    @IBOutlet weak var typeOfTruck: UILabel!
    @IBOutlet weak var availabilityLabel: UILabel!
    
    // MARK: - Constraints
    @IBOutlet weak var mainViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var vehicleNameYConstraint: NSLayoutConstraint!
    
    // MARK: - Properties
    var delegate:VehicleCellDelegate?
    var vehicle:Vehicle! {
        didSet{
            setValues()
        }
    }
    var isVehicleSelected:Bool = false {
        didSet{
            tickButton.isSelected = isVehicleSelected
            borderedView.borderColor = isVehicleSelected ? Appearance.greenColor : UIColor(red255: 216, green255: 216, blue255: 216)
        }
    }
    var lastTruckIDSelected:Int?
    
    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
        checkTheme()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        setValues()
    }
    
    // MARK: - Initial functions
    func setup(){
        // Add tap to Tick Button
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tickTapped))
        tickButton.addGestureRecognizer(tapGesture)
        NotificationCenter.default.addObserver(self, selector: #selector(self.checkTheme), name: Notification.Name.changeTheme, object: nil)
        
    }
    @objc func checkTheme(){
        if ThemeManager.isNightMode(){
            vehicleName.textColor = UIColor.white
            borderedView.backgroundColor = Appearance.darkGrayNight
            tickButton.tintColor = UIColor.clear
        } else {
            borderedView.backgroundColor = UIColor.clear
        }
    }
    @objc func tickTapped(){
        if vehicle.status != 0 || vehicle.id == NavigationManager.vehicleID{
            isVehicleSelected = !isVehicleSelected
            delegate?.didSelect(vehicle: vehicle, status: isVehicleSelected)
        } else {
            shake(availabilityLabel)
        }
    }
    
    func shake(_ label: UILabel){
        dispatch {
            let animation = CABasicAnimation(keyPath: "position")
            animation.duration = 0.07
            animation.repeatCount = 3
            animation.autoreverses = true
            animation.fromValue = NSValue(cgPoint: CGPoint(x: label.center.x - 10,
                                                           y: label.center.y))
            animation.toValue = NSValue(cgPoint: CGPoint(x: label.center.x + 10,
                                                         y: label.center.y))
            label.layer.add(animation, forKey: "position")
        }
    }
    
    // MARK: - Functions
    func setValues(){
        vehicleName.text = vehicle.name
        if vehicle.vehicleParentID != nil {
            let vehicleType:String
            switch vehicle.vehicleType {
            case .frontloaderTruck?:
                vehicleType = "Front Loader Truck"
            case .rearloaderTruck?:
                vehicleType = "Rear Loader Truck"
            case .rollOffTruck?:
                vehicleType = "Roll Off Truck"
            case .sideLoaderTruck?:
                vehicleType = "Side Loader Truck"
            default:
                vehicleType = "Truck"
            }
            vehicleSubtype.text = vehicleType
            mainViewHeightConstraint.constant = 140
            vehicleNameYConstraint.constant = -30
        } else {
            vehicleSubtype.text = ""
            vehicleSubtype.frame = CGRect.zero
            mainViewHeightConstraint.constant = 120
            vehicleNameYConstraint.constant = -20
        }
        licensePlateNumber.text = vehicle.vinNumber
        typeOfTruck.text = vehicle.truckTypeName
        typeOfTruck.textColor = vehicle.truckTypeID == 1 ? Appearance.greenColor : Appearance.redColor
        availabilityLabel.text = vehicle.statusName?.capitalized
        availabilityLabel.textColor = vehicle.status == 1 ? Appearance.greenColor : Appearance.redColor
        yourSelectionLabel.alpha = vehicle.status == 0 && NavigationManager.vehicleID == vehicle.id ? 1 : 0
    }
}
