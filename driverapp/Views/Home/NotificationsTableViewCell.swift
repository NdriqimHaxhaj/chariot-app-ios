//
//  NotificationsTableViewCell.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 5/18/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

class NotificationsTableViewCell: UITableViewCell {
    
    var notification:NotificationType! {
        didSet {
            setupUI()
        }
    }
    
    // MARK: - IBOutlets
    @IBOutlet weak var background: UIView!
    @IBOutlet weak var notificationView: UIView!
    @IBOutlet weak var notificationTypeLabel: UILabel!
    @IBOutlet weak var streetNameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    
    var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter
    }()
    
    
    // MARK: Constraints
    @IBOutlet weak var cellHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var notificationsTypeTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var notificationsTypeBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var streetNameTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var streetNameHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var streetNameBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var descriptionTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var descriptionHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var descriptionBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var timeTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var timeHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var timeBottomConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func prepareForReuse() {
        setupUI()
        resizeDescriptionField()
        calculateCellHeight()
        changeLabelsColor()
    }
    
    func setupUI(){
        iconImageView.alpha = 1
        streetNameHeightConstraint.constant = 0
        streetNameLabel.alpha = 0
        notificationView.borderColor = Appearance.darkGrayNight
        if notification.createdAt != nil {
            if let date = notification.createdAt {
                dateFormatter.dateFormat = "hh:mm a"
                timeLabel.text = dateFormatter.string(from: date)
            }
        } else {
            timeHeightConstraint.constant = 0
            timeLabel.alpha = 0
        }
        
        descriptionLabel.text = notification.text.replacingOccurrences(of: "\\n", with: "\n")
        
        switch notification.notificationType {
        case .driverAssignPickup:
            notificationTypeLabel.text = "Pickup assigned"
            iconImageView.alpha = 0
            break
        case .pickupOverdue:
            notificationTypeLabel.text = "Pickup overdue"
            iconImageView.image = #imageLiteral(resourceName: "overdue pickup")
            break
        case .newPickupNoteDriver:
            notificationTypeLabel.text = "New pickup note"
            iconImageView.alpha = 0
            break
        case .uncancelPickupDispatcher:
            notificationTypeLabel.text = "Pickup uncanceled from dispatcher"
            iconImageView.alpha = 0
            break
        case .broadcast:
            
            if let broadcastType = notification.params!["broadcast_type"] as? String {
                notificationTypeLabel.text = broadcastType
            } else {
                notificationTypeLabel.text = "Road closed"
            }
            if let streetName = notification.params!["address"] as? String {
                streetNameLabel.alpha = 1
                streetNameLabel.text = streetName
            } else {
                streetNameLabel.text = ""
            }
            if let message = notification.params!["message"] as? String {
                descriptionLabel.text = message
            }
            iconImageView.image = #imageLiteral(resourceName: "Broadcast")
            break
        case .pickupRemoveAssignDriver:
            iconImageView.alpha = 0
            break
        case .driverCompletePickup:
            notificationTypeLabel.text = "Pickup completed from Driver"
            iconImageView.alpha = 0
            break
        case .pickupRescheduledDriver:
            notificationTypeLabel.text = "Pickup rescheduled"
            iconImageView.alpha = 0
            break
        default:
            notificationTypeLabel.text = ""
            iconImageView.alpha = 0
            break
        }
        
        resizeDescriptionField()
        resizeStreetNameField()
        calculateCellHeight()
        changeLabelsColor()
        self.layoutIfNeeded()
    }
    
    func calculateCellHeight(){
        cellHeightConstraint.constant = notificationsTypeTopConstraint.constant
        cellHeightConstraint.constant += notificationTypeLabel.frame.height
        cellHeightConstraint.constant += notificationsTypeBottomConstraint.constant
        cellHeightConstraint.constant += streetNameTopConstraint.constant
        cellHeightConstraint.constant += streetNameLabel.frame.height
        cellHeightConstraint.constant += streetNameBottomConstraint.constant
        cellHeightConstraint.constant += descriptionTopConstraint.constant
        cellHeightConstraint.constant += descriptionLabel.frame.height
        cellHeightConstraint.constant += descriptionBottomConstraint.constant
        cellHeightConstraint.constant += timeTopConstraint.constant
        cellHeightConstraint.constant += timeLabel.frame.height
        cellHeightConstraint.constant += timeBottomConstraint.constant
    }
    
   
    
    func changeLabelsColor(){
        let isNightMode = ThemeManager.isNightMode()
        
        switch notification.notificationType {

        case .pickupOverdue:
            background.backgroundColor = isNightMode ? Appearance.darkGrayNight : UIColor(red255: 255, green255: 210, blue255: 210)
            notificationTypeLabel.textColor =  isNightMode ? UIColor.white : UIColor(red255: 181, green255: 62, blue255: 62)
            streetNameLabel.textColor =  isNightMode ? UIColor.white : UIColor(red255: 255, green255: 210, blue255: 210)
            descriptionLabel.textColor = isNightMode ? UIColor.white : UIColor(red255: 255, green255: 210, blue255: 210)
            timeLabel.textColor = isNightMode ? UIColor.white : UIColor(red255: 255, green255: 210, blue255: 210)
            background.borderWidth = 0
            break
        case .broadcast, .pickupRescheduledDriver:
            background.backgroundColor = isNightMode ? Appearance.darkGrayNight : UIColor(red255: 237, green255: 218, blue255: 208)
            notificationTypeLabel.textColor = isNightMode ? UIColor.white : UIColor(red255: 241, green255: 110, blue255: 47)
            streetNameLabel.textColor = isNightMode ? UIColor.white : UIColor(red255: 241, green255: 110, blue255: 47)
            descriptionLabel.textColor = isNightMode ? UIColor.white : UIColor(red255: 241, green255: 110, blue255: 47)
            timeLabel.textColor = isNightMode ? UIColor.white : UIColor(red255: 241, green255: 110, blue255: 47)
            background.borderWidth = 0
            break
            
        case .uncancelPickupDispatcher, .driverAssignPickup:
            background.backgroundColor = isNightMode ? Appearance.darkGrayNight : UIColor(red255: 186, green255: 233, blue255: 192)
            notificationTypeLabel.textColor = isNightMode ? UIColor.white : UIColor(red255: 10, green255: 128, blue255: 54)
            streetNameLabel.textColor = isNightMode ? UIColor.white : UIColor(red255: 10, green255: 128, blue255: 54)
            descriptionLabel.textColor = isNightMode ? UIColor.white : UIColor(red255: 10, green255: 128, blue255: 54)
            timeLabel.textColor = isNightMode ? UIColor.white : UIColor(red255: 62, green255: 193, blue255: 79)
            background.borderWidth = 0
            break
        default:
            background.backgroundColor = isNightMode ? Appearance.darkGrayNight : UIColor.white
            notificationTypeLabel.textColor = isNightMode ? UIColor.white : UIColor.black
            descriptionLabel.textColor = isNightMode ? UIColor.white : UIColor(red255: 134, green255: 134, blue255: 134)
            streetNameLabel.textColor = isNightMode ? UIColor.white : UIColor(red255: 134, green255: 134, blue255: 134)
            descriptionLabel.textColor = isNightMode ? UIColor.white : UIColor(red255: 134, green255: 134, blue255: 134)
            timeLabel.textColor = isNightMode ? UIColor.white : UIColor(red255: 134, green255: 134, blue255: 134)
            background.borderWidth = 1
            break
        }
        
    }
    
    // MARK: - Helper functions
    func resizeDescriptionField(){
        let size = CGSize(width: self.frame.width, height: 1000)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        let estimatedFrame = NSString(string: notification.text).boundingRect(with: size, options: options, attributes: [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 14)], context: nil)
        self.descriptionLabel.sizeToFit()
        self.descriptionHeightConstraint.constant = estimatedFrame.height + 10
        self.layoutIfNeeded()
    }
    
    func resizeStreetNameField(){
        if streetNameLabel.text! == "Street name" {
            streetNameHeightConstraint.constant = 0
            streetNameTopConstraint.constant = 0
            streetNameBottomConstraint.constant = 0
        } else {
            let size = CGSize(width: self.frame.width, height: 1000)
            let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
            let estimatedFrame = NSString(string: streetNameLabel.text ?? "").boundingRect(with: size, options: options, attributes: [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 14)], context: nil)
            streetNameLabel.sizeToFit()
            streetNameHeightConstraint.constant = estimatedFrame.height + 20
            streetNameTopConstraint.constant = 5
            streetNameBottomConstraint.constant = 5
        }
        
        self.layoutIfNeeded()
    }
}
