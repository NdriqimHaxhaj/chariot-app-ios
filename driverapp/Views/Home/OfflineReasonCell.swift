//
//  OfflineReasonCell.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 6/19/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

class OfflineReasonCell: UITableViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var icon: UIView!
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var offlineReasonLabel: UILabel!
    
    
    // MARK: - Properties
    var reason:OfflineReason!{
        didSet{
            setupUI()
            setValues()
        }
    }
    var isReasonSelected = false {
        didSet {
            setupUI()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    func setupUI(){
//        icon.backgroundColor = isReasonSelected ? UIColor(red255: 154, green255: 154, blue255: 154): UIColor.white
        iconView.image = isReasonSelected ? #imageLiteral(resourceName: "done") : #imageLiteral(resourceName: "undone") 
        if ThemeManager.isNightMode(){
            offlineReasonLabel.textColor = UIColor.white
        }
    }
    
    func setValues(){
        offlineReasonLabel.text = reason.name?.capitalized
    }
    
    
}
