//
//  SubtypeTableViewCell.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 11/30/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

protocol SubTypeTableViewCellDelegate {
    func selectedCell(shouldSelect: Bool, id: Int)
}

class SubtypeTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var tickButton: UIButton!
    @IBOutlet weak var materialName: UILabel!
    
    // MARK: - Properties
    var material:MaterialSubtype! {
        didSet{
            setValues()
        }
    }
    var isMaterialSelected = false {
        didSet{
            tickButton.isSelected = isMaterialSelected
        }
    }
    var delegate:SubTypeTableViewCellDelegate?
    // MARK: - Lifecycle

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func prepareForReuse() {
        if material != nil {
            setValues()
        }
    }
    
    // MARK: - IBActions
    @IBAction func tickButtonPressed(_ sender: UIButton) {
        tap()
    }
    
    
    // MARK: - Functions
    func tap(){
        isMaterialSelected = !isMaterialSelected
        if let id = material.id {
            delegate?.selectedCell(shouldSelect: isMaterialSelected, id: id)
        }
    }
    
    func setValues(){
        materialName.text = material.name
        materialName.textColor = ThemeManager.isNightMode() ? .white : .darkGray
    }
    
}
