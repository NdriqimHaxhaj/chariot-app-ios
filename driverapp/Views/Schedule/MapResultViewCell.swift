//
//  MapResultViewCellTableViewCell.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 7/19/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import NMAKit

enum MapResultType {
    case random
    case favourite
}

class MapResultViewCell: UITableViewCell {
    
    //MARK: - IBOutlets
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet var formattedAddressLabel: UILabel!
    @IBOutlet weak var favouriteOptionView: UIView!
    @IBOutlet weak var favouriteDescriptionLabel: UILabel!
    @IBOutlet weak var addToFavouriteButton: UIButton!
    
    //MARK: - Properties
    var cellType:MapResultType = .random {
        didSet{
            checkIfIsAlreadyAdded()
        }
    }
    var addressText:String! {
        didSet {
            addressLabel.text = addressText
        }
    }
    var formattedAddressText:String!{
        didSet {
            formattedAddressLabel.text = formattedAddressText
        }
    }
    var place:NMAPlace!
    
    // MARK: - Constraints
    @IBOutlet weak var favouriteOptionViewWidthConstraint: NSLayoutConstraint!
    
    
    //MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
        checkIfIsAlreadyAdded()
    }
    
    override func prepareForReuse() {
        checkIfIsAlreadyAdded()
    }
    
    // MARK: - IBActions
    @IBAction func addToFavouritePressed(_ sender: UIButton) {
        guard let buildingName = place.name,
            let buildingAddress = place.location?.address?.formattedAddress?.replacingOccurrences(of: "<br/>", with: ", "),
            let zipCode = place.location?.address?.postalCode,
            let latitude = place.location?.position?.latitude,
            let longitude = place.location?.position?.longitude else {
                return
        }
        
        let address = Address(id: 0,
                              buildingName: buildingName,
                              buildingAddress: buildingAddress,
                              latitude: latitude,
                              longitude: longitude,
                              zipCode: NSString(string: zipCode).integerValue)
        UserREST.addFavouriteAddress(address) { (success, error) in
            if success {
                UserREST.getFavouriteAddresses(completionHandler: { (success, addresses, error) in
                    self.saveFavouriteAddresses(addresses)
                    self.checkIfIsAlreadyAdded()
                    NotificationCenter.default.post(name: Notification.Name.refreshFavouriteAddresses, object: nil)
                })
            }
        }
    }
    
    
    // MARK: - Initial Functions
    
    func setupUI(){
        if ThemeManager.isNightMode(){
            addressLabel.textColor = UIColor.white
        }
    }
    
    // MARK: - Functions
    
    func checkIfIsAlreadyAdded(){
        if cellType == .favourite {
            let favouriteAddresses = getFavouriteAddresses()
            for favouriteAddress in favouriteAddresses {
                if favouriteAddress.buildingName == addressText {
                    favouriteOptionView.alpha = 0.5
                    addToFavouriteButton.isEnabled = false
                    favouriteOptionViewWidthConstraint.constant = 80
                    favouriteDescriptionLabel.text = "Added"
                    return
                }
            }
            favouriteOptionView.alpha = 1
            addToFavouriteButton.isEnabled = true
            favouriteDescriptionLabel.text = "Add"
            favouriteOptionViewWidthConstraint.constant = 65
        } else {
            favouriteOptionView.alpha = 0
            addToFavouriteButton.isEnabled = false
            favouriteOptionViewWidthConstraint.constant = 0
        }
    }
    
    func getFavouriteAddresses()->[FavouriteAddress]{
        if let unarchivedData =  UserDefaults.standard.object(forKey: "addresses-favourite") as? Data {
            if let addresses = NSKeyedUnarchiver.unarchiveObject(with: unarchivedData) as? [FavouriteAddress] {
                return addresses
            } else {
                return []
            }
        } else {
            return []
        }
    }
    
    func saveFavouriteAddresses(_ addresses:[FavouriteAddress]){
        let archivedData = NSKeyedArchiver.archivedData(withRootObject: addresses)
        UserDefaults.standard.setValue(archivedData, forKey: "addresses-favourite")
    }
}
