//
//  ChooseAddressTableViewCell.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 10/16/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

class ChooseAddressTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var topDivider: UIView!
    @IBOutlet weak var bottomDivider: UIView!
    @IBOutlet weak var arrowImageView: UIImageView!
    
    
    // MARK: - Properties
    var tempText:String!{
        didSet{
            setValues()
        }
    }
    
    // MARK: - Lifecycle

    override func awakeFromNib() {
        super.awakeFromNib()
        setValues()
    }
    
    func setValues(){
        descriptionLabel.text = tempText
        switch tempText {
        case "Favorites":
            iconImageView.image = UIImage(named: "star")
            break
        case "Created by Dispatcher":
            topDivider.alpha = 0
            iconImageView.image = UIImage(named: "dispatcher")
            break
        case "History":
            topDivider.alpha = 0
            iconImageView.image = UIImage(named: "history")
            break
        default:
            break
        }
        if ThemeManager.isNightMode() {
            iconImageView.image = iconImageView.image?.tint(with: .white)
            arrowImageView.image = iconImageView.image?.tint(with: .white)
            descriptionLabel.textColor = .white
        }
    }

    
}
