//
//  LanguageCell.swift
//  Superintendent
//
//  Created by Drenushë Imeraj on 9/18/17.
//  Copyright © 2017 Zombie Soup. All rights reserved.
//

import UIKit

class LanguageCell: UITableViewCell {

    @IBOutlet weak var languageNameLabel: UILabel!
    @IBOutlet weak var selectedLanguageImage: UIImageView!
    @IBOutlet weak var dividerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setValues(from language: LanguageType){
        setupUI()
        languageNameLabel.text = language.description
        let isSelectedLanguage = language == LanguageManager.current
        selectedLanguageImage.image = isSelectedLanguage ? #imageLiteral(resourceName: "tick Icon") : nil
    }
    
    func setupUI(){
        let isNightMode = ThemeManager.isNightMode()
        languageNameLabel.tintColor = isNightMode ? UIColor.white : UIColor(red255: 85, green255: 85, blue255: 85)
        dividerView.backgroundColor = isNightMode ? UIColor.white : UIColor(red255: 230, green255: 230, blue255: 230)
    }
}
