//
//  MapViewTableViewCell.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 5/9/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

enum MapViewType:Int {
    case Auto = 0
    case Manual = 1
}

class MapViewTableViewCell: UITableViewCell {

    // MARK: - Outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tickImage: UIImageView!
    @IBOutlet weak var dividerView: UIView!
    
    // MARK: - Properties
    var cellType:MapViewType?{
        didSet{
            updateCell()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
        updateCell()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        setupUI()
        updateCell()
    }
    
    func setupUI(){
        let isNightMode = ThemeManager.isNightMode()
        titleLabel.textColor = isNightMode ? UIColor.white : UIColor(red255: 85, green255: 85, blue255: 85)
    }
    
    func updateCell(){
        switch cellType {
        case .Auto?:
            titleLabel.text = "Auto"
            tickImage.alpha = NavigationManager.mapView == 0 ? 1 : 0
            dividerView.alpha = 1
            break
        case .Manual?:
            titleLabel.text = "Manual"
            tickImage.alpha = NavigationManager.mapView == 1 ? 1 : 0
            dividerView.alpha = 1
            break
        default:
            tickImage.alpha = 0
            break
        }
        setupUI()
    }
    
    func press(){
        switch cellType {
        case .Auto?:
            NavigationManager.mapView = 0
            let date = Date()
            let calendar = Calendar.current
            let hour = calendar.component(.hour, from: date)
            if hour >= 7 && hour < 19 {
                ThemeManager.themeMode = Mode.Day.rawValue
            } else {
                ThemeManager.themeMode = Mode.Night.rawValue
            }
            break
        case .Manual?:
            NavigationManager.mapView = 1
            break
        
        default:
            break
        }
        NotificationCenter.default.post(Notification(name: Notification.Name.refreshMapViewTableView))
    }
    
}
