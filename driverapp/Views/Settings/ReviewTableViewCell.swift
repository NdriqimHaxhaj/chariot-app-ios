//
//  ReviewTableViewCell.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 9/3/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

protocol ReviewCellDelegate:class {
    func reviewClients(_ status:Bool)
}

class ReviewTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var reviewClientsLabel: UILabel!
    @IBOutlet weak var reviewClientsNotificationSwitch: UISwitch!
    @IBOutlet weak var yesLabel: UILabel!
    @IBOutlet weak var noLabel: UILabel!
    
    // MARK: - Properties
    var delegate:ReviewCellDelegate?
    let isNightMode =  { return ThemeManager.isNightMode() }()
    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
        getNotificationsSettings()
    }
    
    // MARK: - IBActions
    @IBAction func reviewClientsNotificationsPressed(_ sender: UISwitch) {
        let type = NotificationTypeList.rateDriver.rawValue
        let status = sender.isOn ? 1 : 0
        yesLabel.textColor = sender.isOn ? isNightMode ? .white : Appearance.onColor : Appearance.offColor
        noLabel.textColor = sender.isOn ? Appearance.offColor : isNightMode ? .white : Appearance.onColor
        delegate?.reviewClients(sender.isOn)
        NotificationsREST.setNotificationsSettings(type, status) { (success, error) in
            if success {
                self.getNotificationsSettings()
            }
        }
    }
    
    // MARK: - Functions
    
    func setupUI(){
        reviewClientsLabel.textColor = isNightMode ? .white : Appearance.grayText
    }

    func setValues(){
        reviewClientsLabel.text = "review_client".localized
        let status = NotificationsManager.reviewClientNotification!
        reviewClientsNotificationSwitch.setOn(status, animated: true)
        yesLabel.textColor = status ? isNightMode ? .white : Appearance.onColor : Appearance.offColor
        noLabel.textColor = status ? Appearance.offColor : isNightMode ? .white : Appearance.onColor
        reviewClientsLabel.alpha = status ? 1 : 0.5
    }
    
    func getNotificationsSettings(){
        NotificationsREST.getNotificationsSettings { (success, error) in
            self.setValues()
        }
    }

    
    
}
