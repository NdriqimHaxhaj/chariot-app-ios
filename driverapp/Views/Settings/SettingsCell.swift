//
//  SettingsCell.swift
//  Superintendent
//
//  Created by Drenushë Imeraj on 9/18/17.
//  Copyright © 2017 Zombie Soup. All rights reserved.
//

import UIKit

class SettingsCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dividerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        NotificationCenter.default.addObserver(self, selector: #selector(self.setupUI), name: Notification.Name.changeTheme, object: nil)
        setupUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @objc func setupUI(){
        let isNightMode = ThemeManager.isNightMode()
        nameLabel.textColor = isNightMode ? UIColor.white : UIColor(red255: 85, green255: 85, blue255: 85)
        dividerView.backgroundColor = isNightMode ? UIColor.white : UIColor(red255: 230, green255: 230, blue255: 230)
    }
    
    func displayCellName(name: String){
        setupUI()
        nameLabel.text = name
    }
    
}
