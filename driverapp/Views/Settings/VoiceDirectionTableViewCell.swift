//
//  VoiceDricetionTableViewCell.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 4/27/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

class VoiceDirectionTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var tickIcon: UIImageView!
    @IBOutlet weak var voiceLanguageLabel: UILabel!
    @IBOutlet weak var progressBarView: UIProgressView!
    @IBOutlet weak var languageStatusLabel: UILabel!
    
    @IBOutlet weak var yCenterProgressViewConstraint: NSLayoutConstraint!
    
    // MARK: - Properties
    var isCellSelected = false {
        didSet{
            updateCell()
        }
    }
    var languageId:Int!
    var tempVoiceLanguageLabel:String! {
        didSet {
            updateCell()
        }
    }
    
    
    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        addObservers()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    // MARK: - Functions
    func updateCell(){
        if isCellSelected {
            tickIcon.alpha = 1
            voiceLanguageLabel.textColor = Appearance.greenColor
        } else {
            tickIcon.alpha = 0
            let grayColor = UIColor(red255: 134, green255: 134, blue255: 134)
            voiceLanguageLabel.textColor = grayColor
        }
        voiceLanguageLabel.text = tempVoiceLanguageLabel
        
        layoutIfNeeded()
    }
    
    func addObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.downloadProgress(_:)), name: Notification.Name.downloadVoiceLanguageProgress, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.uncompressProgress(_:)), name: Notification.Name.uncompressVoiceLanguageProgress, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.installCompleted(_:)), name: Notification.Name.installVoiceLanguage, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.setupUI), name: Notification.Name.changeTheme, object: nil)
    }
    @objc func setupUI(){
        let isNightMode = ThemeManager.isNightMode()
        voiceLanguageLabel.textColor = isNightMode ? UIColor.white : UIColor(red255: 85, green255: 85, blue255: 85)
    }
    
    func press(){
        isCellSelected = !isCellSelected
        if isCellSelected {
            NotificationCenter.default.post(name: Notification.Name.downloadLanguage, object: nil, userInfo: ["id":languageId])
            tickIcon.alpha = 0
        }
    }
    
    @objc func downloadProgress(_ not: Notification) {
        if let userInfo = not.userInfo {
            guard let progress = userInfo["progress"] as? Float else {return}
            if UInt(self.languageId) == userInfo["id"] as? UInt {
                progressBarView.alpha = 1
                languageStatusLabel.alpha = 1
                languageStatusLabel.text = "Downloading"
                progressBarView.setProgress(progress/100, animated: false)
                layoutIfNeeded()
            }
        }
    }
    
    @objc func uncompressProgress(_ not: Notification) {
        if let userInfo = not.userInfo {
            guard let progress = userInfo["progress"] as? Float
                else {return}
            if UInt(self.languageId) == userInfo["id"] as? UInt {
                languageStatusLabel.text = "Uncompressing"
                progressBarView.setProgress(progress/100, animated: false)
                layoutIfNeeded()
            }
           
        }
    }
    
    @objc func installCompleted(_ not: Notification) {
        if let userInfo = not.userInfo {
            guard let state = userInfo["state"] as? Bool else {return }
            if UInt(self.languageId) == userInfo["id"] as? UInt {
                if state {
                    languageStatusLabel.text = "Installed"
                    progressBarView.setProgress(1, animated: false)
                    yCenterProgressViewConstraint.constant = 10
                } else {
                    languageStatusLabel.text = "Error"
                    progressBarView.setProgress(0, animated: false)
                    yCenterProgressViewConstraint.constant = 10
                }
                progressBarView.alpha = 0
                languageStatusLabel.alpha = 0
                tickIcon.alpha = 1
                layoutIfNeeded()
            }
        }
    }
}
