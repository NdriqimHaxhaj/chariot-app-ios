//
//  VoiceDricetionTableViewCell.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 4/27/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

class VoiceDirectionTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var tickIcon: UIImageView!
    @IBOutlet weak var voiceLanguageLabel: UILabel!
    
    // MARK: - Properties
    var isCellSelected = false {
        didSet{
            updateCell()
        }
    }
    var languageId:Int!
    var tempVoiceLanguageLabel:String! {
        didSet {
            updateCell()
        }
    }
   
    
    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    
    // MARK: - Functions
    func updateCell(){
        if isCellSelected {
            tickIcon.alpha = 1
            voiceLanguageLabel.textColor = Appearance.greenColor
        } else {
            tickIcon.alpha = 0
            let grayColor = UIColor(red255: 134, green255: 134, blue255: 134)
            voiceLanguageLabel.textColor = grayColor
        }
        voiceLanguageLabel.text = tempVoiceLanguageLabel
        layoutIfNeeded()
    }
    
    func press(){
        isCellSelected = !isCellSelected
        if isCellSelected {
            NotificationCenter.default.post(name: Notification.Name.downloadLanguage, object: nil, userInfo: ["id":languageId])
        }
    }
}

extension VoiceDirectionTableViewCell: VoiceLanguageDelegate {
    
}
