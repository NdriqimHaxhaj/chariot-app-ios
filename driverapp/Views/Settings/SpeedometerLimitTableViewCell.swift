//
//  SpeedometerOptionTableViewcell.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 10/17/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

class SpeedometerLimitTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var tickImage: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    // MARK: - Properties
    var tempText:String!{
        didSet{
            setValues()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setValues(){
        let isCellOptionSelected = tempText == AccountManager.showSpeedLimit
        descriptionLabel.text = tempText
        descriptionLabel.textColor = isCellOptionSelected ? Appearance.greenColor : Appearance.grayText
        tickImage.alpha = isCellOptionSelected ?  1 : 0
    }
    
}
