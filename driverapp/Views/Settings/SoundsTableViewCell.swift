//
//  SoundsTableViewCellTableViewCell.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 4/27/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

enum SoundsCellType {
    case Notification
    case Voice
}

class SoundsTableViewCell: UITableViewCell {

    
    // MARK: - Outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var notificationsSwitch: UISwitch!
    @IBOutlet weak var arrowImage: UIImageView!
    
    // MARK: - Properties
    var cellType:SoundsCellType! {
        didSet{
            updateCell()
        }
    }
    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        updateCell()
        addObservers()
        updateSettings()
    }
    
    func addObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.setupUI), name: Notification.Name.changeTheme, object: nil)
    }
    
    func updateSettings(){
        NotificationsREST.getNotificationsSettings { (success, error) in
            if success {
                self.notificationsSwitch.setOn(NotificationsManager.allowAllNotifications!, animated: true)
            }
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        updateCell()
    }
    
    // MARK: - Functions
    @objc func setupUI(){
        let isNightMode = ThemeManager.isNightMode()
        titleLabel.textColor = isNightMode ? UIColor.white : UIColor(red255: 85, green255: 85, blue255: 85)
    }
    @IBAction func notificationsButtonPressed(_ sender: UISwitch) {
        let status = sender.isOn ? 1 : 0
        NotificationsREST.setNotificationsSettings(NotificationTypeList.allNotifications.rawValue, status) { (success, error) in
            if success {
                print("Settings updated succesfully")
            } else {
                print(error?.message ?? "Couldn't set notifications settings")
            }
        }
    }
    
    func updateCell(){
        setupUI()
        if cellType == .Notification {
            notificationsSwitch.alpha = 1
            let notificationsSounds = AccountManager.notifications == 1
            notificationsSwitch.setOn(notificationsSounds, animated: true)
            arrowImage.alpha = 0
            titleLabel.text = "Notifications"
        } else if cellType == .Voice {
            titleLabel.text = "Sounds"
            notificationsSwitch.alpha = 0
            arrowImage.alpha = 1
            arrowImage.image = #imageLiteral(resourceName: "left arrow").rotate(radians: .pi)
        }
    }
    
    
    
}
