//
//  MenuCell.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 20/02/2018.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var greenView: UIView!
    
    var isCellSelected:Bool!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        NotificationCenter.default.addObserver(self, selector: #selector(self.setupUI), name: Notification.Name.changeTheme, object: nil)
    }
    
    func updateState(){
        titleLabel.textColor = isCellSelected ? Appearance.greenColor : ThemeManager.isNightMode() ? UIColor.white : Appearance.grayColor
        greenView.alpha = isCellSelected ? 1 : 0
    }
    
    @objc func setupUI(){
        titleLabel.textColor = isCellSelected ? Appearance.greenColor : ThemeManager.isNightMode() ? UIColor.white : Appearance.grayColor
        greenView.alpha = isCellSelected ? 1 : 0
    }
}
