//
//  SliderPickupCollectionViewCell.swift
//  PickupsSlider
//
//  Created by Ndriqim Haxhaj on 10/18/18.
//  Copyright © 2018 RecycleGO. All rights reserved.
//

import UIKit

protocol SliderPickupCellDelegate:class {
    func editPickup(_ pickup:PickupRequest)
}

class SliderPickupCollectionViewCell: UICollectionViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var pickupView: UIView!
    @IBOutlet weak var pickupIcon: UIImageView!
    @IBOutlet weak var markerIcon: UIImageView!
    @IBOutlet weak var buildingAddressLabel: UILabel!
    @IBOutlet weak var bagCountStaticLabel: UILabel!
    @IBOutlet weak var bagCountLabel: UILabel!
    @IBOutlet weak var pickupTimeStaticLabel: UILabel!
    @IBOutlet weak var pickupTimeLabel: UILabel!
    @IBOutlet weak var locationStaticLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var editButton: UIButton!
    
    // MARK: - Properties
    var pickupRequest:PickupRequest! {
        didSet{
            setupPickupView()
        }
    }
    lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "h:mm a"
        return dateFormatter
    }()
    weak var delegate:SliderPickupCellDelegate?
    // MARK: - Lifecycle

    override func awakeFromNib() {
        super.awakeFromNib()
        NotificationCenter.default.addObserver(self, selector: #selector(self.setupPickupView), name: Notification.Name.changeTheme, object: nil)
    }
    
    override func prepareForReuse() {
        setupPickupView()
    }
    
    // MARK: - IBActions
    @IBAction func editPickupPressed(_ sender: UIButton) {
        NotificationCenter.default.post(name: Notification.Name.editPickup, object: nil, userInfo: ["pickup": self.pickupRequest])
    }
    
    
    // MARK: - Functions
    @objc func setupPickupView(){
        setPickupIcon()
        setMarkerIcon()
        setPickupView()
        setValues()
        setFontsColor()
    }
    
    func setPickupView(){
        setupPickupViewBorder()
        setupPickupViewBackground()
    }
    
    func setPickupIcon(){
        var mapMarker:UIImage?
        switch pickupRequest.pickupStatus {
        case .overdue,.postponed:
            mapMarker = #imageLiteral(resourceName: "overdue pickup")
        case .completedWithDelays, .completedWithoutDelays, .dropeedOff:
            mapMarker = #imageLiteral(resourceName: "new assigned pickup")
        case .canceled, .paused:
            mapMarker = #imageLiteral(resourceName: "canceled pickup")
        default:
            mapMarker = nil
            break
        }
        pickupIcon.image = mapMarker
    }
    
    func setMarkerIcon(){
        var mapMarker:UIImage?
        editButton.alpha = 0
        switch pickupRequest.pickupStatus {
        case .assigned:
            if pickupRequest.requestedBy == PickupCreator.me {
                editButton.alpha = 1
            }
            mapMarker = #imageLiteral(resourceName: "pickup pin")
        case .overdue:
            mapMarker = #imageLiteral(resourceName: "overdue pickup pin")
        case .completedWithDelays, .completedWithoutDelays, .dropeedOff:
            mapMarker = #imageLiteral(resourceName: "completed pickup pin")
        case .postponed, .paused:
            mapMarker = #imageLiteral(resourceName: "canceled pickup pin")
        default:
            mapMarker = #imageLiteral(resourceName: "pickup pin")
            break
        }
        markerIcon.image = mapMarker
    }
    
    func setupPickupViewBorder(){
        var borderColor:UIColor?
        switch pickupRequest.pickupStatus {
        case .overdue:
            borderColor = Appearance.orangeColor
        case .postponed, .paused, .canceled:
            borderColor = Appearance.canceledRed
        default:
            borderColor = Appearance.greenColor
            break
        }
        pickupView.borderColor = borderColor
        
    }
    
    func setupPickupViewBackground(){
        var backgroundColor:UIColor?
        switch pickupRequest.pickupStatus {
        case .completedWithDelays, .completedWithoutDelays, .dropeedOff:
            backgroundColor = ThemeManager.isNightMode() ? Appearance.darkGrayNight : Appearance.completedPickupGreen
        default:
            backgroundColor = ThemeManager.isNightMode() ? Appearance.darkGrayNight : UIColor.white
            break
        }
        pickupView.backgroundColor = backgroundColor
    }
    
    func setValues(){
        buildingAddressLabel.text = pickupRequest.buildingName
        bagCountLabel.text = "\(pickupRequest?.bagCount ?? 0)"
        pickupTimeLabel.text = dateFormatter.string(from: pickupRequest.startPickupTime ?? Date.now())
        locationLabel.text = pickupRequest.loadLocationName.capitalized
        
    }
    
    func setFontsColor(){
        let isNightMode = ThemeManager.isNightMode()
        var textColor:UIColor?
        switch pickupRequest.pickupStatus {
        case.overdue, .postponed, .paused :
            textColor = Appearance.canceledRed
        case .completedWithDelays, .completedWithoutDelays, .dropeedOff:
            textColor = isNightMode ? Appearance.greenColor : Appearance.completedGreenFont
        default:
            textColor = isNightMode ? .white : Appearance.grayText
            break
        }
        
        buildingAddressLabel.textColor = textColor
        
        if pickupRequest.pickupStatus == .completedWithDelays || pickupRequest.pickupStatus == .completedWithoutDelays || pickupRequest.pickupStatus == .dropeedOff {
            let textColor = Appearance.completedGreenFont
            bagCountLabel.textColor         = isNightMode ? .white : textColor
            bagCountStaticLabel.textColor   = isNightMode ? Appearance.grayOutText : textColor
            pickupTimeLabel.textColor       = isNightMode ? .white : textColor
            pickupTimeStaticLabel.textColor = isNightMode ? Appearance.grayOutText : textColor
            locationLabel.textColor         = isNightMode ? .white : textColor
            locationStaticLabel.textColor   = isNightMode ? Appearance.grayOutText : textColor
        } else {
            bagCountStaticLabel.textColor   = Appearance.grayOutText
            bagCountLabel.textColor         = isNightMode ? .white : Appearance.grayText
            
            pickupTimeStaticLabel.textColor = Appearance.grayOutText
            pickupTimeLabel.textColor       = isNightMode ? .white : Appearance.grayText
           
            locationStaticLabel.textColor   = Appearance.grayOutText
            locationLabel.textColor         = isNightMode ? .white : Appearance.grayText
        }
    }
    
}
