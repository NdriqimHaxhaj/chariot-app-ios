//
//  NavigationBoardHeader.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 10/5/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

protocol NavigationBoardHeaderDelegate:class {
    func showManeuvers()
}

class NavigationBoardHeader: UIView {

    // MARK: - IBOutlets
    @IBOutlet weak var navigationBoardHeaderView: UIView!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var distanceToNextManeuver: UILabel!
    @IBOutlet weak var currentRoad: UILabel!
    @IBOutlet weak var buildingName: UILabel!
    @IBOutlet weak var buildingAddress: UILabel!
    
    // MARK: - Properties
    weak var delegate:NavigationBoardHeaderDelegate?
    var isLandscape = false
    var previousOrientiation:UIDeviceOrientation = UIDevice.current.orientation
    
    // MARK: - Constraints
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet var iconImageVerticalConstraint: NSLayoutConstraint!
    
    // MARK: - Livecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
        addObservers()
    }
    
    // MARK: - IBActions
    @IBAction func showManueversTap(_ sender: UITapGestureRecognizer) {
        delegate?.showManeuvers()
    }
    
    // MARK: - Initial functions
    @objc func setupUI(){
        self.navigationBoardHeaderView.backgroundColor = ThemeManager.isNightMode() ? Appearance.darkGrayNight : UIColor.white
        if ThemeManager.isNightMode() {
            distanceToNextManeuver.textColor = UIColor.white
            buildingName.textColor = UIColor.white
        }
    }
    
    // MARK: - Functions
    func set(distance: Double, currentRoad: String, buildingName: String, buildingAddress:String, iconValue: Int){
        self.distanceToNextManeuver.text = "\(String(format: "%.2f", distance)) miles"
        self.currentRoad.text = currentRoad
        self.buildingName.text = buildingName
        self.buildingAddress.text = buildingAddress
        self.iconImageView.image = getImageFor(iconValue)
        setupUI()
    }
    
    func addObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.setupUI), name: Notification.Name.changeTheme, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(viewRotated), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
    }
    
    fileprivate func getImageFor(_ value:Int)->UIImage?{
        var imageName = ThemeManager.isNightMode() ? "\(value)-white" : "\(value)"
        guard let image = UIImage(named: "\(imageName)") else {
            print("Couldn't get signpost for this direction")
            print("DEBUG: Signpost value: \(value)")
            return nil
        }
        return image
    }
    
    func changeHeight(to: Int){
        heightConstraint.constant = CGFloat(to)
        UIView.animate(withDuration: 0.1) {
            self.layoutIfNeeded()
        }
    }
    
    @objc func viewRotated(){
        checkOrientation()
        layoutSelf()
    }
    
    func layoutSelf(){
        iconImageVerticalConstraint.constant = isLandscape ? 0 : 10
        UIView.animate(withDuration: 0.2) {
            self.layoutIfNeeded()
        }
    }
    
    func checkOrientation(){
        switch UIDevice.current.orientation {
        case .faceDown, .faceUp:
            if previousOrientiation == .landscapeLeft || previousOrientiation == .landscapeRight {
                isLandscape = true
            } else {
                isLandscape = false
            }
        case .portrait, .portraitUpsideDown:
            isLandscape = false
            previousOrientiation = UIDevice.current.orientation
        case .landscapeLeft, .landscapeRight:
            isLandscape = true
            previousOrientiation = UIDevice.current.orientation
        case .unknown:
            // Do nothing
            break
        }
    }
}
