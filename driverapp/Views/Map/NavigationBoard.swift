//
//  NavigationBoard.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 3/19/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

protocol NavigationBoardDelegate:class {
    func buttonPressed(_ sender:UIButton)
}

class NavigationBoard: PassthroughView {
    // MARK: - IBOutlets
    @IBOutlet weak var navigationBoardView: PassthroughView!
    @IBOutlet weak var arrivalTime: UILabel!
    @IBOutlet weak var durationAndDistanceLabel: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var completedButton: UIButton!
    @IBOutlet weak var overViewButton: UIButton!
    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var roadOptionsButton: NSLayoutConstraint!
    @IBOutlet weak var roadDetailsView: UIView!
    @IBOutlet weak var locationButton: UIButton!
    @IBOutlet weak var notificationsButton: UIButton!
    @IBOutlet weak var soundsButton: UIButton!
    
    // MARK : - Constraints
    @IBOutlet weak var roadOptionsButtonBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var roadDetailsViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var moreButtonLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var moreButtonBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var locationButtonLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var locationButtonBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var notificationsButtonLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var notificationsButtonBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var soundsButtonLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var soundsButtonBottomConstraint: NSLayoutConstraint!
    
    // MARK: - Properties
    weak var delegate:NavigationBoardDelegate?
    var previousOrientiation:UIDeviceOrientation = UIDevice.current.orientation
    var isLandscape = false
    var shouldShowButtons = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
        addObservers()
        checkOrientation()
    }
    
    // MARK: IBActions
    @IBAction func cancelPickupPressed(_ sender: UIButton) {
        print(#function)
        // sender.tag = 1
        delegate?.buttonPressed(sender)
    }
    
    @IBAction func completedPressed(_ sender: UIButton) {
        print(#function)
        // sender.tag = 2
        delegate?.buttonPressed(sender)
    }
    @IBAction func overviewPressed(_ sender: UIButton) {
        print(#function)
        // sender.tag = 3
        delegate?.buttonPressed(sender)
    }
    @IBAction func moreButtonPressed(_ sender: UIButton) {
        checkOrientation()
        shouldShowButtons = !shouldShowButtons
        layoutButtons()
    }
    @IBAction func locationButtonPressed(_ sender: UIButton) {
        NotificationCenter.default.post(name: Notification.Name.navigationBoardLocationPressed, object: nil)
    }
    @IBAction func notificationsButtonPressed(_ sender: UIButton) {
        NotificationCenter.default.post(name: Notification.Name.navigationBoardNotificationsButtonPressed, object: nil)
    }
    @IBAction func soundsButtonPressed(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name.navigationBoardSoundsButtonPressed, object: nil)
    }
    @IBAction func roadDetailsButtonPressed(_ sender: UIButton) {
        NotificationCenter.default.post(name: Notification.Name.navigationBoardRoadDetailsPressed, object: nil)
    }
    
    
    // MARK: - Functions
    @objc func setupUI(){
//        self.navigationBoardView.backgroundColor = ThemeManager.isNightMode() ? Appearance.darkGrayNight : UIColor.white
        self.navigationBoardView.backgroundColor = UIColor.clear
        if ThemeManager.isNightMode() {
            arrivalTime.textColor = UIColor.white
            durationAndDistanceLabel.textColor = UIColor.white
            roadDetailsView.backgroundColor = Appearance.darkGrayNight
        }
    }
    
    func addObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.setupUI), name: Notification.Name.changeTheme, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(viewRotated), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
    }
    
    @objc func viewRotated(){
        checkOrientation()
        layoutButtons()
    }
    
    func checkOrientation(){
        switch UIDevice.current.orientation {
        case .faceDown, .faceUp:
            if previousOrientiation == .landscapeLeft || previousOrientiation == .landscapeRight {
                isLandscape = true
            } else {
                isLandscape = false
            }
        case .portrait, .portraitUpsideDown:
            isLandscape = false
            previousOrientiation = UIDevice.current.orientation
        case .landscapeLeft, .landscapeRight:
            isLandscape = true
            previousOrientiation = UIDevice.current.orientation
        case .unknown:
            // Do nothing
            break
        }
        
    }
    
    public func set(arrivalTime: NSDate, durationAndDistance:String){
        setupUI()
        let time = getArrival(date: arrivalTime)
        self.arrivalTime.text = time
        self.durationAndDistanceLabel.text = durationAndDistance
    }
    
    fileprivate func getArrival(date: NSDate)->String{
        print("Date: \(date)")
        let formatter = DateFormatter()
        formatter.locale = NSLocale.system
        formatter.dateFormat = "h:mm a"
        print("Date: \(date)")
        return "\(formatter.string(from: date as Date)) | "
    }
    
    func hideCancelButtons(){
//        cancelButton.isHidden = true
//        completedButton.setTitle("Drop Off Completed", for: .normal)
//        arrivalTime.alpha = 1
//        durationAndDistanceLabel.alpha = 1
    }
    
    func showCancelButton(){
//        cancelButton.isHidden = false
//        completedButton.setTitle("Complete", for: .normal)
        arrivalTime.alpha = 1
        durationAndDistanceLabel.alpha = 1
    }
    
    func showButtons(_ status:Bool){
        locationButton.alpha = status ? 1 : 0
        notificationsButton.alpha = status ? 1 : 0
        soundsButton.alpha = status ? 1 : 0
    }
    
    func layoutButtons(){
        moreButton.isEnabled = false
        let origImage = #imageLiteral(resourceName: "add gray")
        let tintedImage = origImage.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        if shouldShowButtons {
            showButtons(shouldShowButtons)
            locationButtonBottomConstraint.constant = isLandscape ? 20 : 150
            locationButtonLeftConstraint.constant = isLandscape ? 100 : 20
            UIView.animate(withDuration: 0.2, delay: 0.2, options: [.curveEaseOut], animations: {
                self.roadDetailsView.alpha = self.isLandscape && self.shouldShowButtons ? 0.5 : 1
                self.layoutIfNeeded()
            }, completion: {(_) in
                self.moreButton.isEnabled = true
            })
            notificationsButtonBottomConstraint.constant = isLandscape ? 20 : 230
            notificationsButtonLeftConstraint.constant = isLandscape ? 180 : 20
            UIView.animate(withDuration: 0.2, delay: 0.1, options: [.curveEaseOut], animations: {
                self.layoutIfNeeded()
            }, completion: nil)
            soundsButtonBottomConstraint.constant = isLandscape ? 20 : 310
            soundsButtonLeftConstraint.constant = isLandscape ? 260 : 20
            moreButtonBottomConstraint.constant = isLandscape ? 20 : 70
            roadOptionsButton.constant = isLandscape ? 20 : 70
            roadDetailsViewBottomConstraint.constant = isLandscape ? 30 : 20
            
            UIView.animate(withDuration: 0.2, delay: 0, options: [.curveEaseOut], animations: {
                self.moreButton.transform = CGAffineTransform.init(rotationAngle: CGFloat.pi / 4)
                self.moreButton.backgroundColor = UIColor.white
                self.moreButton.setImage(tintedImage, for: .normal)
                self.moreButton.tintColor = UIColor.darkGray
                self.layoutIfNeeded()
            }, completion: nil)
        } else {
            soundsButtonBottomConstraint.constant = isLandscape ? 20 : 70
            soundsButtonLeftConstraint.constant = isLandscape ? shouldShowButtons ? 260 : 20 : 20
            UIView.animate(withDuration: 0.2, delay: 0.2, options: [.curveEaseIn], animations: {
                self.roadDetailsView.alpha = self.isLandscape && self.shouldShowButtons ? 0.5 : 1
                self.layoutIfNeeded()
            }, completion: {(_) in
                self.moreButton.isEnabled = true
                self.showButtons(self.shouldShowButtons)
            })
            notificationsButtonBottomConstraint.constant = isLandscape ? 20 : 70
            notificationsButtonLeftConstraint.constant = isLandscape ? shouldShowButtons ?  180 : 20 : 20
            UIView.animate(withDuration: 0.2, delay: 0.1, options: [.curveEaseIn], animations: {
                self.layoutIfNeeded()
            }, completion: nil)
            locationButtonBottomConstraint.constant = isLandscape ? 20 : 70
            locationButtonLeftConstraint.constant = isLandscape ? shouldShowButtons ?  100 : 20 : 20
            moreButtonBottomConstraint.constant = isLandscape ? 20 : 70
            roadOptionsButton.constant = isLandscape ? 20 : 70
            roadDetailsViewBottomConstraint.constant = isLandscape ? 30 : 20
            UIView.animate(withDuration: 0.2, delay: 0.0, options: [.curveEaseIn], animations: {
                self.moreButton.transform = CGAffineTransform.identity
                self.moreButton.backgroundColor = Appearance.greenColor
                self.moreButton.setImage(tintedImage, for: .normal)
                self.moreButton.tintColor = UIColor.white
                self.layoutIfNeeded()
            }, completion: nil)
        }
    }
}

class PassthroughView: UIView {
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let view = super.hitTest(point, with: event)
        return view == self ? nil : view
    }
}
