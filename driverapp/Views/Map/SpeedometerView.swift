//
//  SpeedometerView.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 10/11/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

enum SpeedometerViewMode {
    case allowed
    case exceeded
}

class SpeedometerView: UIView {
    
    // MARK: - IBOutlets
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var speedLabel: UILabel!
    @IBOutlet weak var mphLabel: UILabel!
    
    // MARK: - Properties
    var mode: SpeedometerViewMode = .allowed {
        didSet{
            updateUI()
        }
    }
    // MARK: - Lifecycle

    override func awakeFromNib() {
        mainView.layer.cornerRadius = self.frame.height/2
        updateUI()
    }
    
    // MARK: - Initialization functions
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
//        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Functions
    func updateUI(){
        if mode == .allowed {
            let isNightMode = ThemeManager.isNightMode()
            mainView.backgroundColor = isNightMode ? Appearance.backgroundViewNight : UIColor.white
            mainView.borderColor = isNightMode ? Appearance.backgroundViewNight : UIColor(red255: 217, green255: 217, blue255: 217)
            speedLabel.textColor = isNightMode ? .white : UIColor(red255: 71, green255: 73, blue255: 85)
            mphLabel.textColor = isNightMode ? .darkGray :  UIColor(red255: 115, green255: 115, blue255: 115)
        } else {
            mainView.backgroundColor = UIColor(red255: 255, green255: 210, blue255: 210)
            mainView.borderColor = UIColor(red255: 255, green255: 107, blue255: 107)
            speedLabel.textColor = UIColor(red255: 71, green255: 73, blue255: 85)
            mphLabel.textColor = UIColor(red255: 115, green255: 115, blue255: 115)
        }
        setNeedsDisplay()
    }
    
    func set(currentSpeed: Float, limitedSpeed: Float){
        let currentSpeedInMiles = currentSpeed / 1.609
        let limitedSpeedInMiles = limitedSpeed / 1.609
        switch AccountManager.showSpeedLimit {
        case "Don’t show":
            mode = .allowed
            break
        case "Show when exceeding speed limit":
            mode = currentSpeedInMiles + Float(AccountManager.whenToShowSpeedAlert!)  > limitedSpeedInMiles ? .allowed : .exceeded
            break
        case "Always show speed limit":
            mode = currentSpeedInMiles  > limitedSpeedInMiles ? .allowed : .exceeded
            break
        default:
            break
        }
        
        speedLabel.text = "\(Int(currentSpeedInMiles))"
        updateUI()
    }
}
