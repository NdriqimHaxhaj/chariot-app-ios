//
//  SliderView.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 4/5/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

enum SliderType {
    case isSlider
    case isSwitch
}

class SliderView: UIView {

    // MARK: - IBOutlets
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var dragger: UIView!
    
    var sliderType = SliderType.isSlider
    var didSliderSetup = false
    
    // MARK: - Properties
    var view:UIView!
    weak var delegate:SliderDelegate?
    
    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    override func layoutSubviews() {
        setupScrollView()
        scrollToPage(1)
    }
    
    
    // MARK: - Functions
    private func setupScrollView(){
        scrollView.contentOffset.x = view1.frame.width
    }
    
    func setup(){
        view = loadViewFromNib()
        view?.frame = bounds
        view?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view!)
        setupScrollView()
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        return UINib(nibName: "SliderView", bundle: bundle).instantiate(withOwner: self, options: nil)[0] as! UIView
    }
    
    @IBAction func onViewGesture(_ sender: UIPanGestureRecognizer) {
        print("gesture: \(sender.state.rawValue)")
    }
}

extension SliderView: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let total = ( scrollView.contentSize.width / 2 ) - 35
        let outlinedTotal = scrollView.contentOffset.x
        let progress = Int((1-(outlinedTotal/total))*100)
        print("Progress: \(progress)%")
        
        if progress == 100 {
            if didSliderSetup {
                delegate?.progressCompleted()
            } else {
                scrollToPage(1)
            }
            didSliderSetup = true
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print(#function)
        print("scrollViewDidEndDecelerating: \(scrollView.currentPage)")
    }
    
    func scrollToPage(_ page: Int) {
        print(#function)
        UIView.animate(withDuration: 0.3) {
            self.scrollView.contentOffset.x = self.view1.frame.width * CGFloat(page)
            self.view.setNeedsDisplay()
            self.view.layoutIfNeeded()
        }
    }
}

protocol SliderDelegate:class {
    func progressCompleted()
}
