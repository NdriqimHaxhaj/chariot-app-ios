//
//  MapViewViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 5/9/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

class MapViewViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    
    // MARK: - Properties
    var items = ["Auto", "Manual"]
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        addObservers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupUI()
    }
    
    // MARK: - Functions
    func setupUI(){
        if ThemeManager.isNightMode() {
            backgroundView.backgroundColor = Appearance.backgroundViewNight
        }
    }
    
    func setupTableView(){
        tableView.register(MapViewTableViewCell.self)
        tableView.estimatedRowHeight = 80
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    func addObservers(){
        registerNotification(notification: Notification.Name.refreshMapViewTableView, selector: #selector(self.reloadTableData))
    }
    
    @objc func reloadTableData(){
        tableView.reloadData()
    }
    
}

//MARK: - Extensions
extension MapViewViewController{
    static func create() -> MapViewViewController{
        return UIStoryboard.settings.instantiate(MapViewViewController.self)
    }
}

// MARK: - Table view extension
extension MapViewViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
            case 0:
                let cell = tableView.dequeue(MapViewTableViewCell.self, for: indexPath)
                cell.titleLabel.text = items[indexPath.row]
                cell.cellType = .Auto
                return cell
            case 1:
                let cell = tableView.dequeue(MapViewTableViewCell.self, for: indexPath)
                cell.titleLabel.text = items[indexPath.row]
                cell.cellType = .Manual
                return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let cell = tableView.cellForRow(at: indexPath) as? MapViewTableViewCell else {return}
        cell.press()
        postNotification(notification: Notification.Name.changeTheme, object: nil)
        self.pop()
    }
    
}

