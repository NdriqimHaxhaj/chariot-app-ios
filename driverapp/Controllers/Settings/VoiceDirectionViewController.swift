//
//  VoiceDirectionViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 4/27/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

class VoiceDirectionViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet var backtroundView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Properties
    var languageids = [206,10101,405000,83101]
    var languages:[Int:String] = [206:    "English (US) - Female",
                                  10101:  "English (US) - Male",
                                  405000: "Español (América Latina) - Female",
                                  83101:  "Español (México) - Male"]
    
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        addObservers()
        setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupUI()
    }
    
    // MARK: - Functions
    func addObservers(){
        registerNotification(notification: Notification.Name.downloadLanguage, selector: #selector(self.downloadLanguage))
        registerNotification(notification: Notification.Name.downloadedLanguage, selector: #selector(self.downloadedLanguage))
    }
    func setupUI(){
        if ThemeManager.isNightMode() {
            backtroundView.backgroundColor = Appearance.backgroundViewNight
        }
    }
    
    func setupTableView(){
        tableView.register(VoiceDirectionTableViewCell.self)
        tableView.estimatedRowHeight = 80
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    @objc func downloadLanguage(){
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        view.isUserInteractionEnabled = false
        navigationController?.navigationBar.isUserInteractionEnabled = false
        title = "Please wait..."
    }
    
    @objc func downloadedLanguage(){
        tableView.reloadData()
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        view.isUserInteractionEnabled = true
        navigationController?.navigationBar.isUserInteractionEnabled = true
        
        self.title = "Voice Languages"
    }

}

extension VoiceDirectionViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return languages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(VoiceDirectionTableViewCell.self, for: indexPath)
        let languageId = languageids[indexPath.row]
        cell.languageId = languageId
        cell.tempVoiceLanguageLabel = languages[languageids[indexPath.row]]
        cell.isCellSelected = NavigationManager.actualLanguageId == languageId
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let cell = tableView.cellForRow(at: indexPath) as? VoiceDirectionTableViewCell
        cell?.press()
    }
}

//MARK: - Extensions
extension VoiceDirectionViewController{
    static func create() -> VoiceDirectionViewController{
        return UIStoryboard.settings.instantiate(VoiceDirectionViewController.self)
    }
}
