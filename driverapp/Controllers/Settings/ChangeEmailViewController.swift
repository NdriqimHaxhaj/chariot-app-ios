//
//  ChangeEmailViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 4/25/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

class ChangeEmailViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var currentEmailTextfield: UITextField!
    @IBOutlet weak var newEmailTextfield: UITextField!
    
    // MARK: - Properties
    var shouldProceed = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupUI()
    }

    // MARK: - IBActions
    @IBAction func changeEmailButtonPressed(_ sender: UIButton) {
        changeEmail()
    }
    @IBAction func viewTapped(_ sender: UITapGestureRecognizer) {
        endEditing()
    }
    
    // MARK: Functions
    func setupUI(){
        if ThemeManager.isNightMode() {
            backgroundView.backgroundColor = Appearance.backgroundViewNight
            currentEmailTextfield.backgroundColor = Appearance.textFieldNight
            currentEmailTextfield.textColor = UIColor.white
            currentEmailTextfield.placeHolderColor = UIColor.lightGray
            newEmailTextfield.backgroundColor = Appearance.textFieldNight
            newEmailTextfield.textColor = UIColor.white
            newEmailTextfield.placeHolderColor = UIColor.lightGray
        }
    }
    
    func changeEmail(){
        validateTextfields()
        if shouldProceed {
            guard let email = newEmailTextfield.text else {return}
            UserREST.changeEmail(with: email)  { (success, error) in
                if success {
                    self.pop()
                } else {
                    print("Error: \(String(describing: error))")
                }
            }
        }
    }
    
    func validateTextfields(){
        if currentEmailTextfield.text == "Current E-Mail" || currentEmailTextfield.text == "" {
            highlightMissingField(currentEmailTextfield)
            shouldProceed = false
        } else {
            shouldProceed = true
        }
        
        if newEmailTextfield.text == "New E-Mail" || newEmailTextfield.text == "" {
            highlightMissingField(newEmailTextfield)
            shouldProceed = false
        } else {
            shouldProceed = true
        }
    }
    
    func highlightMissingField(_ field:UITextField){
        field.borderColor = Appearance.darkOrangeColor
    }
    
    func resetTextFieldsColors(){
        let color = UIColor(red255: 230, green255: 230, blue255: 230)
        newEmailTextfield.borderColor = color
        currentEmailTextfield.borderColor = color
    }
    
    func endEditing(){
        newEmailTextfield.resignFirstResponder()
        currentEmailTextfield.resignFirstResponder()
    }
}

//MARK: - Extensions
extension ChangeEmailViewController{
    static func create() -> ChangeEmailViewController{
        return UIStoryboard.settings.instantiate(ChangeEmailViewController.self)
    }
}

extension ChangeEmailViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        endEditing()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        resetTextFieldsColors()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        validateTextfields()
    }
}
