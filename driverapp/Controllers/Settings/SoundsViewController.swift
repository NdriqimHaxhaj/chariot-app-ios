//
//  MapVoiceViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 4/27/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

class SoundsViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var backgroundView: UIView!
    
    
    // MARK: - Properties
    var items = ["Notifications", "Voice Directions"]
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupUI()
    }
    
    // MARK: - Functions
    func setupUI(){
        if ThemeManager.isNightMode() {
            backgroundView.backgroundColor = Appearance.backgroundViewNight
            
        }
    }
    
    func setupTableView(){
        tableView.register(SoundsTableViewCell.self)
        tableView.estimatedRowHeight = 80
        tableView.rowHeight = UITableViewAutomaticDimension
    }
}


//MARK: - Extensions
extension SoundsViewController{
    static func create() -> SoundsViewController{
        return UIStoryboard.settings.instantiate(SoundsViewController.self)
    }
}

// MARK: - Table view extension
extension SoundsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
            
        case 0:
            let cell = tableView.dequeue(SoundsTableViewCell.self, for: indexPath)
            cell.cellType = .Notification
            cell.selectionStyle = .none
            return cell
        case 1:
            let cell = tableView.dequeue(SoundsTableViewCell.self, for: indexPath)
            cell.cellType = .Voice
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch indexPath.row {
        case 1:
            let vc = VoiceDirectionViewController.create()
            self.push(vc)
        default:
            return
        }
        
    }
    
}
