//
//  WhenToShowAlertViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 10/17/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

class WhenToShowAlertViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Properties
    let whenToShowAlertOptions = ["At speed limit", "5 MPH above speed limit", "10 MPH above speed limit", "15 MPH above speed limit", "20 MPH above speed limit" ]
    let whenToShowAlertIntOptions = [0,5,10,15,20]
    
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupUI()
    }
    
    // MARK: - Functions
    func setupUI(){
        if ThemeManager.isNightMode() {
            mainView.backgroundColor = Appearance.backgroundViewNight
        }
    }
    
    func setupTableView(){
        tableView.register(SpeedometerAlertTableViewCell.self)
        tableView.estimatedRowHeight = 60
        tableView.rowHeight = UITableViewAutomaticDimension
    }
}

// MARK: - Table view extension
extension WhenToShowAlertViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return whenToShowAlertOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(SpeedometerAlertTableViewCell.self, for: indexPath)
        let option = whenToShowAlertOptions[indexPath.row]
        cell.tempText = option
        cell.whenToShowAlert = whenToShowAlertIntOptions[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? SpeedometerAlertTableViewCell, let whenToShowAlert = cell.whenToShowAlert  {
            AccountManager.whenToShowSpeedAlert = whenToShowAlert
            tableView.reloadData()
        }
    }
}

// MARK: - Create exentension
extension WhenToShowAlertViewController {
    static func create() -> WhenToShowAlertViewController{
        return UIStoryboard.settings.instantiate(WhenToShowAlertViewController.self)
    }
}
