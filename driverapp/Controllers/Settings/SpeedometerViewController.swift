//
//  SpeedometerViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 10/17/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

class SpeedometerViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var showSpeedoMeterView: UIView!
    @IBOutlet weak var showSpeedometerLabel: UILabel!
    @IBOutlet weak var speedometerNoLabel: UILabel!
    @IBOutlet weak var speedometerSwitch: UISwitch!
    @IBOutlet weak var speedometerYesLabel: UILabel!
    @IBOutlet weak var showSpeedLimitView: UIView!
    @IBOutlet weak var showSpeedLimitLabel: UILabel!
    @IBOutlet weak var showSpeedLimitOptionLabel: UILabel!
    @IBOutlet weak var playAlertSoundView: UIView!
    @IBOutlet weak var playAlertSoundLabel: UILabel!
    @IBOutlet weak var playAlertNoLabel: UILabel!
    @IBOutlet weak var playAlertSwitch: UISwitch!
    @IBOutlet weak var playAlertYesLabel: UILabel!
    @IBOutlet weak var whenToShowAlertView: UIView!
    @IBOutlet weak var whenToShowAlertLabel: UILabel!
    @IBOutlet weak var whenToShowAlertOptionLabel: UILabel!
    
    // MARK: - Properties
    let isNightMode = { return ThemeManager.isNightMode() }()
    // MARK: - Constraints
    @IBOutlet weak var showSpeedLimitTopConstraint: NSLayoutConstraint!
    
    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        checkSpeedometerOptionsView()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setValues()
        setupUI()
    }
    
    // MARK: - IBActions
    @IBAction func showSpeedometerSwitchPressed(_ sender: UISwitch) {
        speedometerNoLabel.textColor = sender.isOn ? Appearance.grayOutText : isNightMode ? .white : Appearance.grayText
        speedometerYesLabel.textColor = sender.isOn ? isNightMode ? .white : Appearance.grayText : Appearance.grayOutText
        AccountManager.showSpeedometer = sender.isOn
        showSpeedLimitTopConstraint.constant = sender.isOn ? 0 : -140
        UIView.animate(withDuration: 0.2) {
            self.showSpeedLimitView.alpha = sender.isOn ? 1 : 0
            self.playAlertSoundView.alpha = sender.isOn ? 1 : 0
            self.whenToShowAlertView.alpha = sender.isOn ? 1 : 0
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func showSpeedLimitPressed(_ sender: UITapGestureRecognizer) {
        let showSpeedLimitViewController = ShowSpeedometerLimitViewController.create()
        push(showSpeedLimitViewController)
    }
    
    
    @IBAction func playAlertSoundSwitchPressed(_ sender: UISwitch) {
        playAlertNoLabel.textColor = sender.isOn ? Appearance.grayOutText : isNightMode ? .white : Appearance.grayText
        playAlertYesLabel.textColor = sender.isOn ? isNightMode ? .white : Appearance.grayText : Appearance.grayOutText
        AccountManager.playSpeedAlertSound = sender.isOn
    }
    
    @IBAction func whenToShowAlertPressed(_ sender: UITapGestureRecognizer) {
        let whenToShowAlertViewController = WhenToShowAlertViewController.create()
        push(whenToShowAlertViewController)
    }
    
    
    
    // MARK: - Functions
    fileprivate func setupUI(){
        if ThemeManager.isNightMode() {
            mainView.backgroundColor = Appearance.backgroundViewNight
            showSpeedoMeterView.backgroundColor = Appearance.backgroundViewNight
            showSpeedometerLabel.textColor = .white
            showSpeedLimitLabel.textColor = .white
            playAlertSoundLabel.textColor = .white
            whenToShowAlertLabel.textColor = .white
        }
    }
    
    fileprivate func checkSpeedometerOptionsView(){
        showSpeedLimitTopConstraint.constant = AccountManager.showSpeedometer! ? 0 : -200
        self.view.layoutIfNeeded()
    }
    
    fileprivate func setValues(){
        speedometerSwitch.setOn( AccountManager.showSpeedometer!, animated: false)
        speedometerNoLabel.textColor = AccountManager.showSpeedometer! ? Appearance.grayOutText : isNightMode ? .white : Appearance.grayText
        speedometerYesLabel.textColor = AccountManager.showSpeedometer! ? isNightMode ? .white : Appearance.grayText : Appearance.grayOutText
        showSpeedLimitOptionLabel.text = AccountManager.showSpeedLimit
        playAlertSwitch.setOn(AccountManager.playSpeedAlertSound!, animated: false)
        playAlertNoLabel.textColor = AccountManager.playSpeedAlertSound! ? Appearance.grayOutText : isNightMode ? .white : Appearance.grayText
        playAlertYesLabel.textColor = AccountManager.playSpeedAlertSound! ? isNightMode ? .white : Appearance.grayText : Appearance.grayOutText
        switch AccountManager.whenToShowSpeedAlert {
        case 0:
            whenToShowAlertOptionLabel.text = "At speed limit"
            break
        default:
            whenToShowAlertOptionLabel.text = "\(AccountManager.whenToShowSpeedAlert!) MPH above speed limit"
        }
    }
}

extension SpeedometerViewController {
    static func create() -> SpeedometerViewController{
        return UIStoryboard.settings.instantiate(SpeedometerViewController.self)
    }
}
