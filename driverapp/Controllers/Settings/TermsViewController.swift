//
//  TermsViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 4/20/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import WebKit

enum TermsControllerType: String {
    case terms = "terms_of_use"
    case privacy = "privacy_policy"
}

class TermsViewController: UIViewController {
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet var backroundView: UIView!
    @IBOutlet weak var textTypeLabel: UILabel!
    @IBOutlet weak var webViewHolder: UIView!
    
    var type: TermsControllerType = .terms
    var wkWebView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = type.rawValue.localized
        if(type == .terms){
            textTypeLabel.text = type.rawValue.localized
        }else {
            textTypeLabel.text = type.rawValue.localized
        }
        setWebView()
        getHtmlString()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupUI()
    }
    
    // MARK: - Functions
    func setupUI(){
        if ThemeManager.isNightMode() {
            logo.image = UIImage(named: "nightLogo")
            backroundView.backgroundColor = Appearance.backgroundViewNight
            textTypeLabel.textColor = UIColor.white
        }
    }
    
    func getHtmlString(){
        SettingsREST.getHtmlString(termsControllerType: type) { [weak self] (htmlString, error) in
            guard let `self` = self else { return }
            
                if ThemeManager.isNightMode() {
                    if let receivedHTML = htmlString {
                        let html = "<html><head><style>*[style], *{color: white!important;background-color: #2D2D2E!important;background:  #2D2D2E !important;}</style><head><body>\(receivedHTML)</body></html>"
                        self.wkWebView.loadHTMLString(html, baseURL: nil)
                    }
                } else {
                    if let html = htmlString{
                        self.wkWebView.loadHTMLString(html, baseURL: nil)
                    }
                }
        }
    }
    
    func constrainView(view: UIView, toView contentView: UIView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        view.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
    }
    
    func setWebView(){
        wkWebView = WKWebView(frame: CGRect.zero)
        webViewHolder.addSubview(wkWebView)
        wkWebView.navigationDelegate = self
        constrainView(view: wkWebView, toView: webViewHolder)
        if ThemeManager.isNightMode() {
            wkWebView.backgroundColor = Appearance.backgroundViewNight
            webViewHolder.backgroundColor = Appearance.backgroundViewNight
        }
        wkWebView.alpha = 0
        webViewHolder.alpha = 0
        
    }
}

//MARK: - Extensions
extension TermsViewController{
    static func create(withType: TermsControllerType) -> TermsViewController{
        let vc = UIStoryboard.settings.instantiate(TermsViewController.self)
        vc.type = withType
        return vc
    }
}

extension TermsViewController: WKNavigationDelegate{
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("didFinish navigation")
        UIView.animate(withDuration: 0.2) {
            self.wkWebView.alpha = 1
            self.webViewHolder.alpha = 1
        }
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        print("didFail navigation")
    }
}

