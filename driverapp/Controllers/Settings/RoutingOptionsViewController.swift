//
//  RoutingOptionsViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 4/24/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

class RoutingOptionsViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet var backgroundView: UIView!
    
    // Routing type
    @IBOutlet weak var fastestIconView: UIImageView!
    @IBOutlet weak var fastestLabel: UILabel!
    @IBOutlet weak var shortestIconView: UIImageView!
    @IBOutlet weak var shortestLabel: UILabel!
    @IBOutlet weak var balancedIconView: UIImageView!
    @IBOutlet weak var balancedLabel: UILabel!
    @IBOutlet weak var enabledIconView: UIImageView!
    @IBOutlet weak var enabledLabel: UILabel!
    @IBOutlet weak var disabledIconView: UIImageView!
    @IBOutlet weak var disabledLabel: UILabel!
    
    // Switch
    @IBOutlet weak var tollRoadSwitch: UISwitch!
    @IBOutlet weak var highWaySwitch: UISwitch!
    @IBOutlet weak var tunnelsSwitch: UISwitch!
    @IBOutlet weak var parksSwitch: UISwitch!
    @IBOutlet weak var dirtroadSwitch: UISwitch!
    @IBOutlet weak var carpoolSwitch: UISwitch!
    @IBOutlet weak var boatFerrySwitch: UISwitch!
    @IBOutlet weak var carShuttleTrainSwitch: UISwitch!
        // Labels
    @IBOutlet weak var tollRoadLabel: UILabel!
    @IBOutlet weak var highWayLabel: UILabel!
    @IBOutlet weak var tunnelsLabel: UILabel!
    @IBOutlet weak var parksLabel: UILabel!
    @IBOutlet weak var dirtroadLabel: UILabel!
    @IBOutlet weak var carpoolLabel: UILabel!
    @IBOutlet weak var boatFerryLabel: UILabel!
    @IBOutlet weak var carShuttleTrainLabel: UILabel!
        // Labels Yes or No
    @IBOutlet weak var tollRoadNo: UILabel!
    @IBOutlet weak var tollRoadYes: UILabel!
    @IBOutlet weak var highwayNo: UILabel!
    @IBOutlet weak var highwayYes: UILabel!
    @IBOutlet weak var tunnelsNo: UILabel!
    @IBOutlet weak var tunnelsYes: UILabel!
    @IBOutlet weak var parksNo: UILabel!
    @IBOutlet weak var parksYes: UILabel!
    @IBOutlet weak var dirtRoadNo: UILabel!
    @IBOutlet weak var dirtRoadYes: UILabel!
    @IBOutlet weak var carpoolNo: UILabel!
    @IBOutlet weak var carpoolYes: UILabel!
    @IBOutlet weak var ferryNo: UILabel!
    @IBOutlet weak var ferryYes: UILabel!
    @IBOutlet weak var shuttleTrainNo: UILabel!
    @IBOutlet weak var shuttleTrainYes: UILabel!
    
    // MARK: - Properties
    //...

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupUI()
        checkSwitches()
        checkRoutingAttributes()
    }
    
    // MARK: - IBActions
    @IBAction func routingTypePressed(_ sender: UITapGestureRecognizer) {
        guard let sender = sender.view else {return}
        resetRoutingTypes()
        switch sender.tag {
        case 1:
            fastestIconView.alpha = 1
            fastestLabel.textColor = Appearance.greenColor
            NavigationManager.routingType = 1
            break
        case 2:
            shortestIconView.alpha = 1
            shortestLabel.textColor = Appearance.greenColor
            NavigationManager.routingType = 0
            break
        case 3:
            balancedIconView.alpha = 1
            balancedLabel.textColor = Appearance.greenColor
            NavigationManager.routingType = 3
            break
        default:
            break
        }
    }
    @IBAction func trafficModePressed(_ sender: UITapGestureRecognizer) {
        guard let sender = sender.view else {return}
        resetTrafficModes()
        switch sender.tag {
        case 4:
            enabledIconView.alpha = 1
            enabledLabel.textColor = Appearance.greenColor
            NavigationManager.trafficEnabled = 1
            break
        case 5:
            disabledIconView.alpha = 1
            disabledLabel.textColor = Appearance.greenColor
            NavigationManager.trafficEnabled = 0
            break
        default:
            break
        }
        postNotification(notification: Notification.Name.updateNavigationManager, object: nil)
    }
    
    @IBAction func switchPressed(_ sender: UISwitch) {
        if sender.isOn == false {
            sender.setOn(true, animated: false)
            return
        } else {
            resetSwitchesExcluding(sender)
            
            switch sender {
            case tollRoadSwitch:
                NavigationManager.avoidTollRoad = sender.isOn ? true : false
                break
            case highWaySwitch:
                NavigationManager.avoidHighway = sender.isOn ? true : false
                break
            case tunnelsSwitch:
                NavigationManager.avoidTunnel = sender.isOn ? true : false
                break
            case parksSwitch:
                NavigationManager.avoidPark = sender.isOn ? true : false
                break
            case dirtroadSwitch:
                NavigationManager.avoidDirtRoad = sender.isOn ? true : false
                break
            case carpoolSwitch:
                NavigationManager.avoidCarpool = sender.isOn ? true : false
                break
            case boatFerrySwitch:
                NavigationManager.avoidBoatFerry = sender.isOn ? true : false
                break
            case carShuttleTrainSwitch:
                NavigationManager.avoidCarShuttleTrain = sender.isOn ? true : false
                break
            default:
                break
            }
            checkSwitches()
        }
    }
    
    // MARK: - Functions
    func setupUI(){
        if ThemeManager.isNightMode() {
            backgroundView.backgroundColor = Appearance.backgroundViewNight
            tollRoadLabel.textColor = UIColor.white
            highWayLabel.textColor = UIColor.white
            tunnelsLabel.textColor = UIColor.white
            parksLabel.textColor = UIColor.white
            dirtroadLabel.textColor = UIColor.white
            carpoolLabel.textColor = UIColor.white
            boatFerryLabel.textColor = UIColor.white
            carShuttleTrainLabel.textColor = UIColor.white
        } else {
            self.view.setNeedsDisplay()
        }
    }
    
    func resetRoutingTypes(){
        fastestIconView.alpha = 0
        fastestLabel.textColor = Appearance.cancellPickupGray
        shortestIconView.alpha = 0
        shortestLabel.textColor = Appearance.cancellPickupGray
        balancedIconView.alpha = 0
        balancedLabel.textColor = Appearance.cancellPickupGray
    }
    
    func resetTrafficModes(){
        enabledIconView.alpha = 0
        enabledLabel.textColor = Appearance.cancellPickupGray
        disabledIconView.alpha = 0
        disabledLabel.textColor = Appearance.cancellPickupGray
    }
    
    func checkSwitches(){
        tollRoadSwitch.setOn(NavigationManager.avoidTollRoad, animated: false)
        tollRoadLabel.alpha = NavigationManager.avoidTollRoad ? 1 : 0.5
        tollRoadYes.textColor = NavigationManager.avoidTollRoad ? Appearance.onColor : Appearance.offColor
        tollRoadNo.textColor = NavigationManager.avoidTollRoad ? Appearance.offColor : Appearance.onColor
        
        highWaySwitch.setOn(NavigationManager.avoidHighway, animated: false)
        highWayLabel.alpha = NavigationManager.avoidHighway ? 1 : 0.5
        highwayYes.textColor = NavigationManager.avoidHighway ? Appearance.onColor : Appearance.offColor
        highwayNo.textColor = NavigationManager.avoidHighway ? Appearance.offColor : Appearance.onColor
        
        tunnelsSwitch.setOn(NavigationManager.avoidTunnel, animated: false)
        tunnelsLabel.alpha = NavigationManager.avoidTunnel ? 1 : 0.5
        tunnelsYes.textColor = NavigationManager.avoidTunnel ? Appearance.onColor : Appearance.offColor
        tunnelsNo.textColor = NavigationManager.avoidTunnel ? Appearance.offColor : Appearance.onColor
        
        parksSwitch.setOn(NavigationManager.avoidPark, animated: false)
        parksLabel.alpha = NavigationManager.avoidPark ? 1 : 0.5
        parksYes.textColor = NavigationManager.avoidPark ? Appearance.onColor : Appearance.offColor
        parksNo.textColor = NavigationManager.avoidPark ? Appearance.offColor : Appearance.onColor
        
        dirtroadSwitch.setOn(NavigationManager.avoidDirtRoad, animated: false)
        dirtroadLabel.alpha = NavigationManager.avoidDirtRoad ? 1 : 0.5
        dirtRoadYes.textColor = NavigationManager.avoidDirtRoad ? Appearance.onColor : Appearance.offColor
        dirtRoadNo.textColor = NavigationManager.avoidDirtRoad ? Appearance.offColor : Appearance.onColor
        
        carpoolSwitch.setOn(NavigationManager.avoidCarpool, animated: false)
        carpoolLabel.alpha = NavigationManager.avoidCarpool ? 1 : 0.5
        carpoolYes.textColor = NavigationManager.avoidCarpool ? Appearance.onColor : Appearance.offColor
        carpoolNo.textColor = NavigationManager.avoidCarpool ? Appearance.offColor : Appearance.onColor
        
        boatFerrySwitch.setOn(NavigationManager.avoidBoatFerry, animated: false)
        boatFerryLabel.alpha = NavigationManager.avoidBoatFerry ? 1 : 0.5
        ferryYes.textColor = NavigationManager.avoidBoatFerry ? Appearance.onColor : Appearance.offColor
        ferryNo.textColor = NavigationManager.avoidBoatFerry ? Appearance.offColor : Appearance.onColor
        
        carShuttleTrainSwitch.setOn(NavigationManager.avoidCarShuttleTrain, animated: false)
        carShuttleTrainLabel.alpha = NavigationManager.avoidCarShuttleTrain ? 1 : 0.5
        shuttleTrainYes.textColor = NavigationManager.avoidCarShuttleTrain ? Appearance.onColor : Appearance.offColor
        shuttleTrainNo.textColor = NavigationManager.avoidCarShuttleTrain ? Appearance.offColor : Appearance.onColor
    }
    
    func checkRoutingAttributes(){
        resetRoutingTypes()
        resetTrafficModes()
        switch NavigationManager.routingType {
        case 0:
            shortestIconView.alpha = 1
            shortestLabel.textColor = Appearance.greenColor
        case 1:
            fastestIconView.alpha = 1
            fastestLabel.textColor = Appearance.greenColor
        case 3:
            balancedIconView.alpha = 1
            balancedLabel.textColor = Appearance.greenColor
        default:
            break
        }
        
        switch NavigationManager.trafficEnabled {
        case 0:
            disabledIconView.alpha = 1
            disabledLabel.textColor = Appearance.greenColor
        case 1:
            enabledIconView.alpha = 1
            enabledLabel.textColor = Appearance.greenColor
        default:
            break
        }
        
    }
    
    
    func resetSwitchesExcluding(_ sender:UISwitch){
        if tollRoadSwitch != sender {
            NavigationManager.avoidTollRoad = false
        }
        if highWaySwitch != sender {
            NavigationManager.avoidHighway = false
        }
        
        if tunnelsSwitch != sender {
            NavigationManager.avoidTunnel = false
        }
        
        if parksSwitch != sender {
            NavigationManager.avoidPark = false
        }
        
        if dirtroadSwitch != sender {
            NavigationManager.avoidDirtRoad = false
        }
        
        if carpoolSwitch != sender {
            NavigationManager.avoidCarpool = false
        }
        
        if boatFerrySwitch != sender {
            NavigationManager.avoidBoatFerry = false
        }
        
        if carShuttleTrainSwitch != sender {
            NavigationManager.avoidCarShuttleTrain = false
        }
    }
}

//MARK: - Extensions
extension RoutingOptionsViewController{
    static func create() -> RoutingOptionsViewController{
        return UIStoryboard.settings.instantiate(RoutingOptionsViewController.self)
    }
}
