//
//  ShowSpeedometerLimitViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 10/17/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

class ShowSpeedometerLimitViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Properties
    let showSpeedometerLimitOptions = ["Don't show", "Show when exceeding speed limit", "Always show speed limit"]
    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupUI()
    }

    // MARK: - Functions
    func setupUI(){
        if ThemeManager.isNightMode() {
            mainView.backgroundColor = Appearance.backgroundViewNight
        }
    }
    
    func setupTableView(){
        tableView.register(SpeedometerLimitTableViewCell.self)
        tableView.estimatedRowHeight = 60
        tableView.rowHeight = UITableViewAutomaticDimension
    }
}

// MARK: - Table view extension
extension ShowSpeedometerLimitViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return showSpeedometerLimitOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(SpeedometerLimitTableViewCell.self, for: indexPath)
        let option = showSpeedometerLimitOptions[indexPath.row]
        cell.tempText = option
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? SpeedometerLimitTableViewCell, let showSpeedLimit = cell.descriptionLabel.text  {
            AccountManager.showSpeedLimit = showSpeedLimit
            tableView.reloadData()
        }
    }
}

// MARK: - Create exentension
extension ShowSpeedometerLimitViewController {
    static func create() -> ShowSpeedometerLimitViewController{
        return UIStoryboard.settings.instantiate(ShowSpeedometerLimitViewController.self)
    }
}
