//
//  SettingsViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 4/20/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
    
    // MARK: IBOutlets
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var settingsTable: UITableView!
    
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        addObservers()
        initTableView()
        navigationItem.title = "settings".localized
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupUI()
    }
    
    // MARK: Functions
    func addObservers(){
        registerNotification(notification: Notification.Name.changeTheme, selector: #selector(self.setupUI))
    }
    
    @objc func setupUI(){
        setupNavigationController()
        let isNightMode = ThemeManager.isNightMode()
        backgroundView.backgroundColor = isNightMode ?  Appearance.backgroundViewNight : UIColor(red255: 242, green255: 242, blue255: 242)
        
    }
    
    func setupNavigationController(){
        let isNightMode = ThemeManager.isNightMode()
        guard let navigationBar = self.navigationController?.navigationBar else {
            return
        }
        
        navigationBar.barTintColor = isNightMode ? Appearance.darkGrayNight : UIColor.white
        navigationBar.titleTextAttributes = [NSAttributedStringKey.font: Font.robotoBoldFont(size: 17), NSAttributedStringKey.foregroundColor: isNightMode ? UIColor.white : UIColor.darkGray]
        navigationBar.tintColor = isNightMode ? UIColor.white : Appearance.greenColor
    }
    
    func initTableView(){
        settingsTable.register(SettingsCell.self)
        settingsTable.estimatedRowHeight = 80
        settingsTable.rowHeight = UITableViewAutomaticDimension
        settingsTable.delegate = self
        settingsTable.dataSource = self
    }
}

//MARK: - Extensions
extension SettingsViewController{
    static func create() -> SettingsViewController{
        return UIStoryboard.settings.instantiate(SettingsViewController.self)
    }
}

extension SettingsViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 8
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: SettingsCell = tableView.dequeue(SettingsCell.self, for: indexPath)
        
        switch indexPath.row {
        case 0:
            cell.displayCellName(name: "routing_settings".localized)
            break
        case 1:
            cell.displayCellName(name: "sounds".localized)
            break
        case 2:
            cell.displayCellName(name: "map_view".localized)
            break
        case 3:
            cell.displayCellName(name: "speedometer".localized)
            break
        case 4:
            cell.displayCellName(name: "change_password".localized)
            break
        case 5:
            cell.displayCellName(name: "change_email".localized)
            break
        case 6:
            cell.displayCellName(name: "terms_of_use".localized)
            break
        case 7:
            cell.displayCellName(name: "privacy_policy".localized)
            break
        case 8:
            cell.displayCellName(name: "review_client".localized)
            break
        case 9:
            cell.displayCellName(name: "build_details".localized)
            break
        default:
            cell.displayCellName(name: "notifications".localized)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch indexPath.row {
        case 0:
            let vc = RoutingOptionsViewController.create()
            self.push(vc)
            break
        case 1:
            let vc = SoundsViewController.create()
            self.push(vc)
            break
        case 2:
            let vc = MapViewViewController.create()
            self.push(vc)
            break
        case 3:
            let vc = SpeedometerViewController.create()
            self.push(vc)
            break
        case 4:
            let vc = ChangePasswordViewController.create(isFirstLogin: false, currentPassword: "")
            self.push(vc)
            break
        case 5:
            let vc = ChangeEmailViewController.create()
            self.push(vc)
            break
        case 6:
            let vc = TermsViewController.create(withType: .terms)
            self.push(vc)
            break
        case 7:
            let vc = TermsViewController.create(withType: .privacy)
            self.push(vc)
            break
        case 8:
//            let vc = ReviewClientViewController.create()
//            self.push(vc)
            break
        case 9:
            // Create build version Controller
            
            break
        default:
            let vc = NotificationsViewController.create()
            self.push(vc)
        }
    }
}

