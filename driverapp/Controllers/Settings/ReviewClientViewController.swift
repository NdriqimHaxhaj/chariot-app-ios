//
//  ReviewClientViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 9/3/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

class ReviewClientViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var informationLabel: UILabel!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupUI()
    }
    
    // MARK: - Initital functions
    func setupUI(){
        if ThemeManager.isNightMode() {
            mainView.backgroundColor = Appearance.backgroundViewNight
        }
    }
    
    func setupTableView(){
        tableView.register(ReviewTableViewCell.self)
        tableView.estimatedRowHeight = 80
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    // MARK : - IBActions
    
    // MARK: - Functions
}


extension ReviewClientViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(ReviewTableViewCell.self, for: indexPath)
        cell.delegate = self
        return cell
    }
}

extension ReviewClientViewController: ReviewCellDelegate {
    func reviewClients(_ status: Bool) {
        if status {
            UIView.animate(withDuration: 0.5) {
                self.informationLabel.alpha = 0
            }
        } else {
            UIView.animate(withDuration: 0.5) {
                self.informationLabel.alpha = 1
            }
        }
    }
}

extension ReviewClientViewController {
    static func create() -> ReviewClientViewController{
        return UIStoryboard.settings.instantiate(ReviewClientViewController.self)
    }
}
