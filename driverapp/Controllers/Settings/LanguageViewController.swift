//
//  LanguageViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 4/20/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

class LanguageViewController: UIViewController {
    
    @IBOutlet weak var languagesTable: UITableView!
    
    var languages: [LanguageType] = [.english, .spanish]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initLanguagesTable()
        navigationItem.title = "language".localized
    }
    
    func initLanguagesTable(){
        languagesTable.register(LanguageCell.self)
        languagesTable.estimatedRowHeight = 80
        languagesTable.rowHeight = UITableViewAutomaticDimension
        languagesTable.delegate = self
        languagesTable.dataSource = self
        languagesTable.reloadData()
    }
}

//MARK: - Extensions
extension LanguageViewController{
    static func create() -> LanguageViewController{
        return UIStoryboard.settings.instantiate(LanguageViewController.self)
    }
}

extension LanguageViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: LanguageCell = tableView.dequeue(LanguageCell.self, for: indexPath)
        
        let language = languages[indexPath.row]
        cell.setValues(from: language)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let selectedLanguage = languages[indexPath.row]
        if LanguageManager.current != selectedLanguage{
            LanguageManager.setLanguage(to: selectedLanguage.rawValue, reload: true)
        }
    }
}

