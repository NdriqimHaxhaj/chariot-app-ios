//
//  ChangePasswordViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 4/19/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import SCLAlertView

class ChangePasswordViewController: UIViewController {
    
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var currentPasswordTextField: UITextField!
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var confirmNewPasswordTextField: UITextField!
    @IBOutlet weak var strengthBar: UIProgressView!
    @IBOutlet weak var saveChangesButton: UIButton!
    @IBOutlet weak var passwordTypeLabel: UILabel!
    @IBOutlet weak var passwordMatchLabel: UILabel!
    
    fileprivate var isFirstLogin = false
    fileprivate var currentPassword = ""
    
    let validator = Validator()
    override func viewDidLoad() {
        super.viewDidLoad()
        setTexts()
        checkForFirstLogin()
        setupUI()
    }
    
    func setupUI(){
        if ThemeManager.isNightMode() {
            backgroundView.backgroundColor = Appearance.backgroundViewNight
            currentPasswordTextField.backgroundColor = Appearance.textFieldNight
            currentPasswordTextField.textColor = UIColor.white
            currentPasswordTextField.placeHolderColor = UIColor.lightGray
            newPasswordTextField.backgroundColor = Appearance.textFieldNight
            newPasswordTextField.textColor = UIColor.white
            newPasswordTextField.placeHolderColor = UIColor.lightGray
            confirmNewPasswordTextField.backgroundColor = Appearance.textFieldNight
            confirmNewPasswordTextField.textColor = UIColor.white
            confirmNewPasswordTextField.placeHolderColor = UIColor.lightGray
            strengthBar.trackTintColor = UIColor.white
            passwordTypeLabel.textColor = UIColor.white
            passwordMatchLabel.textColor = UIColor.white
            self.view.layoutIfNeeded()
        }
    }
    
    func setTexts(){
        navigationItem.title = "change_password".localized
        saveChangesButton.setTitle("save_changes".localized, for: .normal)
        currentPasswordTextField.placeholder = "current_password".localized
        newPasswordTextField.placeholder = "new_password".localized
        confirmNewPasswordTextField.placeholder = "confirm_new_password".localized
    }
    
    func checkForFirstLogin(){
        if isFirstLogin {
            currentPasswordTextField.text = currentPassword
            currentPasswordTextField.isHidden = true
        }
    }
    
    @IBAction func saveChangesButtonPressed(_ sender: UIButton) {
        currentPasswordTextField.text = currentPasswordTextField.text?.trim
        newPasswordTextField.text = newPasswordTextField.text?.trim
        confirmNewPasswordTextField.text = confirmNewPasswordTextField.text?.trim
        
        guard let current = currentPasswordTextField.text, current.count >= 6 else {
            currentPasswordTextField.shake()
            return
        }
        guard let new = newPasswordTextField.text, new.count >= 6 else {
            newPasswordTextField.shake()
            return
        }
        guard let confirm = confirmNewPasswordTextField.text, confirm == new else {
            confirmNewPasswordTextField.shake()
            return
        }
        
        currentPasswordTextField.resignFirstResponder()
        newPasswordTextField.resignFirstResponder()
        confirmNewPasswordTextField.resignFirstResponder()
        
        let termsAndConditionVC = TermsAndConditionsViewController.create(isFirstLogin: true, currentPassword: current, newPassword: new)
        self.showModal(termsAndConditionVC)
    }
    
    func updateGuidelines(progressBarValue: Float, _ password: String) {
        
        var color:UIColor!
        switch progressBarValue {
        case 0.33:
            color = Appearance.darkOrangeColor
            break
        case 0.66:
            color = Appearance.orangeColor
            break
        case 1:
            color = Appearance.greenColor
            break
        default:
            color = ThemeManager.themeMode == Mode.Day.rawValue ? UIColor.black : UIColor.white
            break
        }
        
        strengthBar.progress = progressBarValue
        strengthBar.tintColor = color
        passwordTypeLabel.text = Strength(password: password).description
        passwordTypeLabel.textColor =  color
    }
    
    func validateFieldsForStrength(_ password: String) {
        let result = validator.validate(password)
        switch result {
        case .ok:
            print("Strength: \(Strength(password: password).description)")
            let strength = Strength(password: password)
            switch strength {
            case .reasonable:
                updateGuidelines(progressBarValue: 0.33, password)
                break
            case .strong:
                updateGuidelines(progressBarValue: 0.66, password)
                break
            case .veryStrong:
                updateGuidelines(progressBarValue: 1, password)
                break
            default:
                break
            }
            break
            
        case .failure(let failingRules):
            failingRules.forEach { rule in
                passwordTypeLabel.text = "Minimum 8 characters"
                passwordTypeLabel.textColor = ThemeManager.themeMode == Mode.Day.rawValue ? UIColor.black : UIColor.white
                strengthBar.progress = 0
            }
        }
    }
    func validateFieldsForMatch(_ password: String, _ confirm: String){
        guard !password.isEmpty || !confirm.isEmpty || password.count == confirm.count else {
            passwordMatchLabel.text = ""
            saveChangesButton.isUserInteractionEnabled = false
            saveChangesButton.isEnabled = false
            return
        }
        if password == confirm {
            passwordMatchLabel.text = "Passwords match"
            saveChangesButton.isUserInteractionEnabled = true
            saveChangesButton.isEnabled = true
        } else {
            passwordMatchLabel.text = "Passwords don't match"
            saveChangesButton.isUserInteractionEnabled = false
            saveChangesButton.isEnabled = false
        }
    }
    
    @IBAction func textfieldEditingChanged(_ sender: UITextField) {
        guard let pass = newPasswordTextField.text, let confirm = confirmNewPasswordTextField.text else { return }
        if confirm.isEmpty{
            validateFieldsForStrength(pass)
        }else{
            validateFieldsForStrength(pass)
            validateFieldsForMatch(pass, confirm)
        }
    }
}

extension ChangePasswordViewController: UITextFieldDelegate{
    
}

//MARK: - Extensions
extension ChangePasswordViewController{
    static func create(isFirstLogin: Bool, currentPassword: String) -> ChangePasswordViewController{
        let cp = UIStoryboard.login.instantiate(ChangePasswordViewController.self)
        cp.isFirstLogin = isFirstLogin
        cp.currentPassword = currentPassword
        return cp
    }
}
