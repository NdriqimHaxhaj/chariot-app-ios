//
//  LoginController.swift
//  driverapp
//
//  Created by Arben Pnishi on 07/02/2018.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import SCLAlertView
import FirebaseMessaging
import SwiftKeychainWrapper

class LoginController: UIViewController {

    // MARK: - Outlets
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var emailTextField: CustomTextField!
    @IBOutlet weak var passwordTextField: CustomTextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    @IBOutlet weak var loginLabel: UILabel!
    @IBOutlet weak var showPasswordButton: UIButton!
    @IBOutlet weak var savePasswordButton: UIButton!
    
    // MARK: - Properties
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        checkThemeMode()
        setupUI()
        setTexts()
        checkForSavedPassword()
    }
    
    func setupUI(){
        if ThemeManager.isNightMode() {
            backgroundView.backgroundColor = Appearance.backgroundViewNight
            emailTextField.backgroundColor = Appearance.textFieldNight
            emailTextField.textColor = UIColor.white
            emailTextField.placeHolderColor = UIColor.lightGray
            passwordTextField.backgroundColor = Appearance.textFieldNight
            passwordTextField.textColor = UIColor.white
            passwordTextField.placeHolderColor = UIColor.lightGray
            loginLabel.textColor = UIColor.white
            forgotPasswordButton.setTitleColor(UIColor.white, for: .normal)
             self.view.layoutIfNeeded()
        }
    }
    
    
    @IBAction func showButtonPressed(_ sender: UITapGestureRecognizer) {
        showPasswordButton.isSelected = !showPasswordButton.isSelected
        passwordTextField.isSecureTextEntry = showPasswordButton.isSelected ? false : true
    }
    
    @IBAction func savePasswordPressed(_ sender: UIButton) {
        savePasswordButton.isSelected = !savePasswordButton.isSelected
    }
    @IBAction func savePasswordLabelPressed(_ sender: UITapGestureRecognizer) {
        savePasswordButton.isSelected = !savePasswordButton.isSelected
    }
    
    // MARK: - Functions
    func setTexts(){
        emailTextField.text = AccountManager.lastUsedEmail
        loginLabel.text = "login_label".localized
        loginButton.setTitle("login_label".localized, for: .normal)
        forgotPasswordButton.setTitle("forgot_your_password".localized, for: .normal)
        emailTextField.placeholder = "Email".localized
        passwordTextField.placeholder = "password".localized
    }
    
    func triggerLoginService(){
        emailTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
        
        
        guard let email = emailTextField.text, !email.isEmpty else{
            SCLAlertView().showError("Error", subTitle: "Please add email.") // Error
            return
        }
        guard let password = passwordTextField.text, password.count > 5 else{
            SCLAlertView().showError("Error", subTitle: "Please add password.") // Error
            return
        }
        getUserToken(email: email, password: password)
    }
    
    
    
    func getUserToken(email: String, password: String){
        showHud()
        UserREST.loginWith(email: email, password: password) { [weak self] (success, token, firstLogin, userRole, userId, error) in
            guard let `self` = self else { return }

            if success{
                if let token = token {
                    AccountManager.userToken = token
                    AccountManager.lastUsedEmail = email
                    if let userId = userId {
                        AccountManager.userId = userId
                    }
                    self.getUserProfile(with: userRole, isFirstLogin: firstLogin, password: password)
                }else{
                    self.hideHud()
                    SCLAlertView().showNotice("Alert", subTitle: "Wrong Email/Password. Please try again.") // Error
                }
            }else{
                self.hideHud()
                SCLAlertView().showNotice("Alert", subTitle: "Wrong Email/Password. Please try again.") // Error
            }
        }
    }
    
    func getUserProfile(with role: UserRole?, isFirstLogin: Bool, password: String){
        UserREST.getUserProfile(with: role) { [weak self] (user, error) in
            guard let `self` = self else { return }

            self.hideHud()
            if let user = user{
                AccountManager.userId = user.id
                AccountManager.currentUser = user
                if NavigationManager.isFirstNavigationSetup {
                    // Set default routing options for navigation
                    NavigationManager.defaultOptions()
                }
                if isFirstLogin {
                    let cp = ChangePasswordViewController.create(isFirstLogin: isFirstLogin, currentPassword: password)
                    self.push(cp)
                } else {
                    let token = Messaging.messaging().fcmToken
                    UserREST.leaveTruck { (success, error) in
                        print("Truck left")
                    }
                    AccountManager.currentFirebaseToken = token
                    print("AccountManager.currentFirebaseToken: \(AccountManager.currentFirebaseToken!)")
                    self.saveUserCredentials()
                    AccountManager.updateRootWindow()
                }
            }
            if let error = error{
                SCLAlertView().showNotice("Alert", subTitle: error.message)
            }
        }
    }
    
    func saveUserCredentials(){
        // Save or remove user credentials
        if savePasswordButton.isSelected {
            if let password = passwordTextField.text {
                if let email = emailTextField.text {
                    KeychainWrapper.standard.set(password, forKey: email)
                }
            }
        } else {
            if let email = emailTextField.text {
                if KeychainWrapper.standard.string(forKey: email) != nil {
                    KeychainWrapper.standard.removeObject(forKey: email)
                }
            }
        }
    }
    
    // MARK: - IBActions
    @IBAction func loginButtonPressed(_ sender: UIButton) {
        triggerLoginService()
    }
    @IBAction func forgotButtonPressed(_ sender: UIButton) {
        let forgotPassVC = ForgotPasswordController.create()
        push(forgotPassVC)
    }
}

extension LoginController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == emailTextField {
            passwordTextField.becomeFirstResponder()
        } else {
            passwordTextField.resignFirstResponder()
            
        }
        
        return false
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == passwordTextField {
            checkForSavedPassword()
        }
    }
    
    func checkForSavedPassword(){
        if let email = emailTextField.text {
            if let password = KeychainWrapper.standard.string(forKey: email) {
                passwordTextField.text = password
                savePasswordButton.isSelected = true
            } else {
                passwordTextField.text = ""
            }
        }
    }
}
