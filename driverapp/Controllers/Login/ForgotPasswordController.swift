//
//  ForgotPasswordController.swift
//  driverapp
//
//  Created by Arben Pnishi on 07/02/2018.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

class ForgotPasswordController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var forgotLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    
    // MARK: - Properties
    //
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setTexts()
        setupUI()
    }
    
    func setupUI(){
        if ThemeManager.isNightMode() {
            forgotLabel.textColor = UIColor.white
            emailTextField.backgroundColor = Appearance.textFieldNight
            emailTextField.textColor = UIColor.white
            emailTextField.placeHolderColor = UIColor.lightGray
            backButton.setTitleColor(UIColor.white, for: .normal)
            self.view.layoutIfNeeded()
        }
    }
    
    func setTexts(){
        forgotLabel.text = "forgot_password".localized
        sendButton.setTitle("send".localized, for: .normal)
    }
    
    @IBAction func sendButtonPressed(_ sender: UIButton) {
        emailTextField.resignFirstResponder()
        guard let email = emailTextField.text, email.isEmail else {
            emailTextField.shake()
            return
        }
        showHud()
        UserREST.forgotPassword(email: email) { [weak self] (success) in
            guard let `self` = self else { return }
            self.hideHud()
            
            var message = ""
            if success{
                message = "Check your email!"
            }else{
                message = "Could not send recover link to your email!"
            }
            let alert = UIAlertController(title: "forgot_password".localized, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
                if success{
                    self.pop()
                }
            }))

            alert.view.tintColor = Appearance.greenColor
            self.present(alert, animated: true, completion: nil)
            alert.view.tintColor = Appearance.greenColor
        }
    }
    @IBAction func backButtonPressed(_ sender: UIButton) {
        pop()
    }

}
//MARK: - Extensions
extension ForgotPasswordController{
    static func create() -> ForgotPasswordController{
        return UIStoryboard.login.instantiate(ForgotPasswordController.self)
    }
}
