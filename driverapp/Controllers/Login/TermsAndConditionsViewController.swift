//
//  TermsAndConditionsViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 4/20/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import SCLAlertView
import WebKit

class TermsAndConditionsViewController: UIViewController {
    
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var termsLabel: UILabel!
    @IBOutlet weak var agreeToAllButton: UIButton!
    @IBOutlet weak var declineButton: UIButton!
    @IBOutlet weak var agreeButton: UIButton!
    @IBOutlet weak var tickButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setTexts()
        getHtmlString()
    }
    
    // MARK: - Properties
    var currentPassword:String!
    var newPassword:String!
    var isFirstLogin:Bool!
    
    
    // MARK: Functions
    func setupUI(){
        if ThemeManager.isNightMode() {
            backgroundView.backgroundColor = Appearance.backgroundViewNight
            termsLabel.textColor = UIColor.white
            agreeToAllButton.setTitleColor(UIColor.white, for: .normal)
            
        }
    
    }
    
    func setTexts(){
        agreeToAllButton.setTitle("agree_to_all".localized, for: .normal)
        declineButton.setTitle("decline".localized, for: .normal)
        agreeButton.setTitle("agree".localized, for: .normal)
    }
    
    
    func getHtmlString(){
        SettingsREST.getHtmlString(termsControllerType: .terms) { [weak self] (htmlString, error) in
            guard let `self` = self else { return }
            
            if let html = htmlString{
                self.webView.loadHTMLString(html, baseURL: nil)
            }
        }
    }
    
    @IBAction func agreeToAllButtonPressed(_ sender: UIButton) {
        tickButton.isSelected = !tickButton.isSelected
        agreeButton.isEnabled = tickButton.isSelected
    }
    
    @IBAction func agreeButton(_ sender: Any) {
        showHud()
        UserREST.changePassword(with: currentPassword, new: newPassword) { (success, error) in
            self.hideHud()
            if success{
                if self.isFirstLogin{
                    self.hideModal()
                    AccountManager.updateRootWindow()
                }else{
                    self.pop()
                }
            }else if let error = error{
                SCLAlertView().showNotice("Error!", subTitle: error.message)
            }
        }
    }
    
    @IBAction func declineButton(_ sender: Any) {
        
    }
}

//MARK: - Extensions
extension TermsAndConditionsViewController{
    static func create(isFirstLogin: Bool, currentPassword: String, newPassword: String) -> TermsAndConditionsViewController{
        let cp = UIStoryboard.login.instantiate(TermsAndConditionsViewController.self)
        cp.isFirstLogin = isFirstLogin
        cp.currentPassword = currentPassword
        cp.newPassword = newPassword
        return cp
    }
}
