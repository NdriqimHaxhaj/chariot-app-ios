//
//  NotificationsViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 3/9/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

class DispatcherChatViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var messageTextField: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    
    // MARK: - Properties
    var messages:[PickupChat] = []
    weak var delegate:DispatcherChatDelegate?
    // MARK: - Constraints
    @IBOutlet weak var textFieldBottomConstraint: NSLayoutConstraint!
    
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        getMessages()
        addPaddingToTextField()
        setupKeyboardObservers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupUI()
    }

    // MARK: - IBActions
    @IBAction func postButtonPressed(_ sender: UIButton) {
        guard let message = messageTextField.text, message != "" else {return}
        self.view.endEditing(true)
        self.postReply(message)
        messageTextField.text = ""
    }
    
    // MARK: - Functions
    func setupUI(){
        if ThemeManager.isNightMode() {
            backgroundView.backgroundColor = Appearance.backgroundViewNight
            messageTextField.backgroundColor = Appearance.darkGrayNight
            messageTextField.placeHolderColor = UIColor.lightGray
            messageTextField.borderColor = Appearance.darkGrayNight
            messageTextField.textColor = UIColor.white
            sendButton.imageView?.image = sendButton.imageView?.image?.tint(with: UIColor.white)
            
        }
    }
    
    func setupTableView(){
        tableView.register(MessageCell.self, reuseIdentifier: "MessageCell")
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 300
    }
    
    func addPaddingToTextField(){
        let leftView = UIView(frame: CGRect.init(x: 0, y: 0, width: 15, height: self.messageTextField.frame.height))
        let rightView = UIView(frame: CGRect.init(x: 0, y: 0, width: 60, height: self.messageTextField.frame.height))
        messageTextField.leftView = leftView
        messageTextField.rightView = rightView
        messageTextField.leftViewMode = UITextFieldViewMode.always
        messageTextField.rightViewMode = UITextFieldViewMode.always
    }
    
    func getMessages(){
        showHud()
        ChatREST.getChatMessages { (success, data, error) in
            self.hideHud()
            if success {
                self.messages = data!
                self.tableView.reloadData()
                self.scrollToBottom()
                self.updateChat()
            } else {
                let alert = UIAlertController(title: "Warning", message: "Something went wrong while getting messages", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.showModal(alert)
            }
        }
    }
    
    func updateChat(){
        showHud()
        guard let id = messages.last?.id, messages.last?.idUser != 0, messages.last?.status == 0 else {
                self.hideHud()
                print("Chat couldn't be updated")
                return
            }
        
        ChatREST.chatUpdate(id) { (success, error) in
            self.hideHud()
            if success {
                self.delegate?.chatUpdates()
                print("Chat updated succesfully")
            } else {
                print("Chat couldn't be updated")
            }
        }
        
    }
    
    func postReply(_ reply: String){
        showHud()    
        ChatREST.chatReply(reply) { (success, error) in
            self.hideHud()
            if success {
                print("Reply posted")
                self.getMessages()
            } else {
                print("Error posting reply")
            }
        }
    }
    
    func scrollToBottom(){
        if self.tableView.numberOfRows(inSection: 0) > 0 {
            let numberOfRows = self.tableView.numberOfRows(inSection: 0)
            let numberOfSections = self.tableView.numberOfSections
            let ipath = IndexPath(row: numberOfRows - 1, section: numberOfSections - 1)
            self.tableView.scrollToRow(at: ipath, at: .bottom, animated: false)
        }
    }
    
    // MARK: - Keyboard functions
    func setupKeyboardObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardDidHide, object: nil)
    }
    
    @objc func keyboardWillShow(notification:NSNotification){
        let keyboardFrame = notification.userInfo![UIKeyboardFrameEndUserInfoKey] as! CGRect
        
        let keyboardDuration = notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! Double
        
        self.textFieldBottomConstraint.constant = keyboardFrame.height
        UIView.animate(withDuration: keyboardDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(notification:NSNotification){
        let keyboardDuration = notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! Double
        
        self.textFieldBottomConstraint.constant = 0
        UIView.animate(withDuration: keyboardDuration) {
            self.view.layoutIfNeeded()
        }
    }

}

// MARK: - View controller extension
extension DispatcherChatViewController{
    static func create() -> DispatcherChatViewController{
        return UIStoryboard.home.instantiate(DispatcherChatViewController.self)
    }
}

// MARK: Table view extensions
extension DispatcherChatViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let message = messages[indexPath.row]
        let cell = tableView.dequeue(MessageCell.self, for: indexPath)
        cell.message = message
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let numberOfRows = self.tableView.numberOfRows(inSection: 0)
        if  numberOfRows == 0 {
            let footerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 30))
            footerView.backgroundColor = UIColor.clear
            let label = UILabel(frame: footerView.frame)
            label.textAlignment = .center
            label.font = UIFont.systemFont(ofSize: 14)
            label.textColor = UIColor.lightGray
            label.backgroundColor = UIColor.clear
            label.text = "There isn't any note for this pickpup"
            footerView.addSubview(label)
            return footerView
        } else {
            return UIView(frame: CGRect.zero)
        }
    }
}

//MARK: Text field extensions
extension DispatcherChatViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        guard let message = textField.text, message != "" else {return true}
        self.postReply(message)
        // Empty textfield
        // Scroll table view to bottom if there is at least one row
        textField.text = ""
        if self.tableView.numberOfRows(inSection: 0) > 0 {
            tableView.scrollToRow(at: IndexPath.init(row: messages.count-1, section: 0), at: .bottom, animated: true)
        }
        return true
    }
    
}

protocol DispatcherChatDelegate:class {
    func chatUpdates()
}
