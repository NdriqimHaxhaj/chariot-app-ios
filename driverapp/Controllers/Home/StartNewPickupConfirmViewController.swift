//
//  StartNewPickupConfirmViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 4/23/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

protocol StartNewPickupDelegate:class {
    func startNewPickup(_ state:Bool, _ newPickupId:Int, _ cancelPickupId:Int)
}

class StartNewPickupConfirmViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var closeButtonView: UIView!
    @IBOutlet weak var closeIcon: UIImageView!
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    // MARK: - Constraints
    @IBOutlet weak var yAxisPopupConstraint: NSLayoutConstraint!
    
    
    // MARK: - Properties
    var currentPickupId:Int!
    var newPickupId:Int!
    weak var delegate:StartNewPickupDelegate?
    
    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        prepareForAnimation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        showSelf()
    }
    
    // MARK - IBActions
    @IBAction func yesButtonPressed(_ sender: UIButton) {
        startNewPickup()
    }
    
    @IBAction func noButtonPressed(_ sender: UIButton) {
        hideSelf(delegatingPickup: false)
    }
    
    @IBAction func closeButtonPressed(_ sender: UITapGestureRecognizer) {
        hideSelf(delegatingPickup: false)
    }
    
    @IBAction func backgroundButtonPressed(_ sender: UITapGestureRecognizer) {
        hideSelf(delegatingPickup: false)
    }
    
    // MARK: - Functions
    func setupUI(){
        let isNightMode = ThemeManager.isNightMode()
        closeIcon.image = isNightMode ? #imageLiteral(resourceName: "X") : #imageLiteral(resourceName: "XGreen")
        if isNightMode {
            popupView.backgroundColor = Appearance.darkGrayNight
            closeButtonView.borderColor = Appearance.darkGrayNight
            closeButtonView.backgroundColor = Appearance.darkGrayNight
            descriptionLabel.textColor = UIColor.white
        }
    }
    
    func startNewPickup(){
        PickupREST.startAnotherPickup(newPickupId, currentPickupId, completionHandler: { (success, error) in
            self.hideHud()
            if success {
                print("The new pickup successfully started. Canceled the one ongoing")
                self.hideSelf(delegatingPickup: true)
            } else {
                print(#function)
                print("Error on startAnotherPickup : \(String(describing: error))")
                self.delegate?.startNewPickup(false, -1, -1)
                self.hideSelf(delegatingPickup: false)
            }
        })
    }
    
    
    
    func prepareForAnimation(){
        yAxisPopupConstraint.constant = self.view.frame.height
        backgroundView.alpha = 0
        view.layoutIfNeeded()
    }
    
    func showSelf(){
        yAxisPopupConstraint.constant = 0
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 10, options: [.curveEaseIn], animations: {
            self.backgroundView.alpha = 0.5
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    func hideSelf(delegatingPickup:Bool){
        yAxisPopupConstraint.constant = self.view.frame.height
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0, options: [.curveEaseIn], animations: {
            self.backgroundView.alpha = 0
            self.view.layoutIfNeeded()
        }, completion: { _ in
            self.dismiss(animated: false, completion: {
                print("Dissmised StartNewPickupConfirmViewController")
                if delegatingPickup {
                    self.delegate?.startNewPickup(true, self.newPickupId, self.currentPickupId)
                }
            })
        })
    }

}

// MARK: - View controller extension
extension StartNewPickupConfirmViewController{
    static func create() -> StartNewPickupConfirmViewController{
        return UIStoryboard.home.instantiate(StartNewPickupConfirmViewController.self)
    }
}
