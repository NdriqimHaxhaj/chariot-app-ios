//
//  PropertyManagerViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 8/3/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import SCLAlertView

class PropertyManagerDispatcherNotesViewController: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - Properties
    var pickupRequest:PickupRequest!{
        didSet{
            getNotes()
        }
    }
    var messages:[PickupNote] = []
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.getNotes), for: .valueChanged)
        return refreshControl
    }()
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    //MARK: - Functions
    @objc func getNotes(){
        PickupREST.getPickupNotes(pickupRequest.id!, CommunicationType.DriverPropertyManager.rawValue) { (success, notes, error) in
            self.refreshControl.endRefreshing()
            if success {
                self.messages = notes!
                self.tableView.reloadData()
                self.scrollToBottom()
            } else {
                SCLAlertView().showNotice("Alert", subTitle: "Could not fetch the notes. Please try again.") // Error
                print(error!)
                return
            }
        }
    }
    
    func setupTableView(){
        tableView.register(PickupNoteCell.self, reuseIdentifier: "PickupNoteCell")
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 300
        tableView.addSubview(refreshControl)
    }
    
    func scrollToBottom(){
        if self.tableView.numberOfRows(inSection: 0) > 0 {
            let numberOfRows = self.tableView.numberOfRows(inSection: 0)
            let numberOfSections = self.tableView.numberOfSections
            let ipath = IndexPath(row: numberOfRows - 1, section: numberOfSections - 1)
            self.tableView.scrollToRow(at: ipath, at: .bottom, animated: false)
        }
    }
}

extension PropertyManagerDispatcherNotesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let note = messages[indexPath.row]
        let cell = tableView.dequeue(PickupNoteCell.self, for: indexPath)
        cell.note = note
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let numberOfRows = self.tableView.numberOfRows(inSection: 0)
        if  numberOfRows == 0 {
            let footerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 30))
            footerView.backgroundColor = UIColor.clear
            let label = UILabel(frame: footerView.frame)
            label.textAlignment = .center
            label.font = UIFont.systemFont(ofSize: 14)
            label.textColor = UIColor.lightGray
            label.backgroundColor = UIColor.clear
            label.text = "There isn't any note for this pickpup"
            footerView.addSubview(label)
            return footerView
        } else {
            return UIView(frame: CGRect.zero)
        }
    }
}
