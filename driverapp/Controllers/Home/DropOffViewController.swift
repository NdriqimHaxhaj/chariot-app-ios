//
//  DropOffViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 6/11/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import UserNotifications

class DropOffViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - Properties
    var drops:[DropOffLocation] = []
    var reason:OfflineReason!
    var currentDrop:DropOffLocation?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupTableView()
        getDropOffLocations()
    }

    @IBAction func doneAndStartPressed(_ sender: UIButton) {
        
        Preferences.isWorkingStatusOn = false
        guard let longitude = self.currentDrop?.longitude else {return}
        guard let latitude = self.currentDrop?.latitude else {return}
        guard let buildingName = self.currentDrop?.name else {return}
        guard let id = self.currentDrop?.id else {return}
        let longitudeFloat = NSString(string: longitude).floatValue
        let latitudeFloat = NSString(string: latitude).floatValue
        
        NotificationCenter.default.post(name: Notification.Name.navigateToDropOff,
                                        object: nil,
                                        userInfo: [
                                            "longitude": longitudeFloat,
                                            "latitude": latitudeFloat,
                                            "building_name": buildingName,
                                            "id":id])
        self.hideModal()
        
//        let reasonString = "\(reason?.id ?? -1)"
//        goOnline(reason: reasonString) { (success, error) in
//            if success {
//
//            } else {
//                Preferences.shared.isWorkingStatusOn = true
//            }
//
//        }
    }
    
    fileprivate func goOnline(reason: Int ,completion: @escaping (_ success: Bool, _ error:APError?)->()){
        DriverREST.changeAvailability(isOnline: 1, reason: reason) { (success, error) in
            if success{
                completion(true,nil)
            }else{
                completion(false,error)
            }
        }
    }
    
    @IBAction func closeButtonPressed(_ sender: UIButton) {
        Preferences.isWorkingStatusOn = true
        self.hideModal()
    }
    
    func setupUI(){
        let isNightMode = ThemeManager.isNightMode()
        if isNightMode {
            titleLabel.textColor = UIColor.white
            mainView.backgroundColor = Appearance.backgroundViewNight
        }
    }
    
    func setupTableView(){
        tableView.register(DropOffLocationCell.self)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 120
        //Delegate and Data Source set from IB
    }
    
    func getDropOffLocations(){
        UserREST.getDropOffLocations { (success, drops, error) in
            if success {
                self.drops = drops!
                self.currentDrop = drops?.first
                self.tableView.reloadData(with: .automatic)
            }
        }
    }
}

// MARK: - Tableview extension
extension DropOffViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return drops.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(DropOffLocationCell.self, for: indexPath) as DropOffLocationCell
        let drop = drops[indexPath.row]
        cell.drop = drop
        cell.isDropSelected = currentDrop == drop ? true : false
        cell.selectionStyle = .none
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let drop = drops[indexPath.row]
        currentDrop = drop
        tableView.reloadData()
    }
}

//MARK: EXTENSIONS
extension DropOffViewController{
    static func create() -> DropOffViewController{
        return UIStoryboard.home.instantiate(DropOffViewController.self)
    }
}
