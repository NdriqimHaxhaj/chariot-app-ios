//
//  ChooseTruckViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 9/3/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

protocol ChooseVehicleDelegate:class {
    func enableButtons(_ status: Bool)
}

class ChooseTruckViewController: UIViewController {

    
    // MARK: - IBOutlets
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var navigationBarBackgroundView: UIView!
    @IBOutlet weak var selectVehicleLabel: UILabel!
    @IBOutlet weak var vehiclesListTableView: UITableView!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var tickButton: UIButton!
    @IBOutlet weak var rememberMyChoiceLabel: UILabel!
    @IBOutlet weak var doneButton: UIButton!
    
    // MARK - Properties
    var vehicles:[Vehicle] = []
    var currentVehicle:Vehicle?
    var vehicleID:Int? {
        didSet{
            doneButton.alpha = 1
            doneButton.isEnabled = true
        }
    }
    var delegate:ChooseVehicleDelegate?
    fileprivate var trucks:[Vehicle] = []
    fileprivate var cars:[Vehicle] = []
    fileprivate var bikes:[Vehicle] = []
    fileprivate var bicycles:[Vehicle] = []
    fileprivate var separatedVehicles:[[Vehicle]] = [[]]
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        getVehicles()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupUI()
    }
    
    // MARK: - Initial functions
    
    func setupUI(){
        if ThemeManager.isNightMode() {
            mainView.backgroundColor = Appearance.backgroundViewNight
            selectVehicleLabel.textColor = UIColor.white
            rememberMyChoiceLabel.textColor = UIColor.white
            footerView.backgroundColor = Appearance.darkGrayNight
            tickButton.tintColor = UIColor.clear
            navigationBar.barTintColor = Appearance.darkGrayNight
            navigationBar.tintColor = .white
            navigationBarBackgroundView.backgroundColor = Appearance.darkGrayNight
        }
    }
    func getVehicles(){
        DriverREST.getVehicles { (success, vehicles, error) in
            if success {
                if let vehicles = vehicles {
                    self.vehicles = vehicles
                    self.separateVehiclesIntoSections()
                    self.vehiclesListTableView.alpha = self.vehicles.count > 0 ? 1 : 0
                    self.vehiclesListTableView.reloadData()
                }
            } else {
                print("Error getting trucks")
            }
        }
    }
    
    func separateVehiclesIntoSections(){
        trucks.removeAll()
        cars.removeAll()
        bikes.removeAll()
        bicycles.removeAll()
        separatedVehicles.removeAll()
        for vehicle in self.vehicles {
            switch vehicle.vehicleType {
            case .truck?, .frontloaderTruck?, .rearloaderTruck?, .sideLoaderTruck?, .rollOffTruck?  :
                trucks.append(vehicle)
                break
            case .car?:
                cars.append(vehicle)
                break
            case .bike?:
                bikes.append(vehicle)
                break
            case .bicycle?:
                bicycles.append(vehicle)
            default :
                break
            }
        }
        if !trucks.isEmpty {
            separatedVehicles.append(trucks)
        }
        if !cars.isEmpty {
            separatedVehicles.append(cars)
        }
        if !bikes.isEmpty {
            separatedVehicles.append(bikes)
        }
        if !bicycles.isEmpty {
            separatedVehicles.append(bicycles)
        }
    }
    
    func setupTableView(){
        vehiclesListTableView.register(VehicleTableViewCell.self)
        vehiclesListTableView.estimatedRowHeight = 100
        vehiclesListTableView.rowHeight = UITableViewAutomaticDimension
        
    }
    
    // MARK: - IBActions
    @IBAction func backButtonPressed(_ sender: UIBarButtonItem) {
        switchWindowRoot(to: .login)
    }
    
    @IBAction func rememberMyChoicePressed(_ sender: UIButton) {
        rememberMyChoice()
    }
    @IBAction func rememberMyChoiceLabelTapped(_ sender: UITapGestureRecognizer) {
        rememberMyChoice()
    }
    
    @IBAction func donePressed(_ sender: UIButton) {
        print("Done pressed")
        self.showHud()
        if vehicleID != nil {
            if let vehicleID = vehicleID {
                DriverREST.changeAvailability(isOnline: 1, reason: 1, completionHandler: { (success, error) in
                    Preferences.isWorkingStatusOn = true
                    NavigationManager.offlineReasonId = OfflineReasonType.notSpecified.rawValue
                    DriverREST.chooseVehicle(vehicleID) { (success, error) in
                        self.hideHud()
                        if success {
                            let shouldRememberLastTruck = self.tickButton.isSelected
                            NavigationManager.shouldRememberLastTruck = shouldRememberLastTruck
                            if let vehicleTypeID = self.currentVehicle?.truckTypeID {
                                NavigationManager.vehicleTypeID = "\(vehicleTypeID)"
                            }
                            AccountManager.isApplicationTerminated = false
                            NavigationManager.vehicleID = vehicleID
                            self.delegate?.enableButtons(true)
                            self.switchWindowRoot(to: .home)
                        }
                    }
                })
            }
        }
    }
    
    // MARK - Functions

    func rememberMyChoice(){
        tickButton.isSelected = !tickButton.isSelected
    }

}

extension ChooseTruckViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if NavigationManager.shouldRememberLastTruck
            && NavigationManager.vehicleID != 0
            && vehicles.count > 0 {
            return 1 + separatedVehicles.count
        } else {
            return separatedVehicles.count
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if NavigationManager.shouldRememberLastTruck && vehicles.count > 0 {
            if section == 0 {
                return 1
            } else {
                return separatedVehicles[section].count
            }
        } else {
            return separatedVehicles[section].count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(VehicleTableViewCell.self, for: indexPath)
        cell.delegate = self
        if NavigationManager.shouldRememberLastTruck && vehicles.count > 0 {
            if indexPath.section == 0 {
                var selectedVehicle:Vehicle?
                for vehicle in vehicles {
                    if vehicle.id == NavigationManager.vehicleID {
                        selectedVehicle = vehicle
                    }
                }
                if selectedVehicle != nil {
                    cell.vehicle = selectedVehicle
                    cell.isVehicleSelected = cell.vehicle.id == vehicleID
                } else {
                    
                }
            } else {
                let vehicle = separatedVehicles[indexPath.section][indexPath.row]
                cell.vehicle = vehicle
                cell.isVehicleSelected = cell.vehicle.id == vehicleID
            }
            cell.selectionStyle = .none
            return cell
        } else {
            if separatedVehicles[indexPath.section].count > indexPath.row {
                let vehicle = separatedVehicles[indexPath.section][indexPath.row]
                cell.vehicle = vehicle
                cell.isVehicleSelected = cell.vehicle.id == vehicleID
                cell.selectionStyle = .none
                return cell
            } else {
                return UITableViewCell()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let backgroundColor = mainView.backgroundColor
        let containerView = UIView(frame: CGRect(x: 10, y: 0, width: view.frame.width-10, height: 30))
        containerView.backgroundColor = ThemeManager.isNightMode() ? Appearance.backgroundViewNight : backgroundColor
        let label = UILabel(frame: CGRect(x: 10, y: 0, width: 80, height: containerView.frame.height))
        label.textColor = UIColor(red255: 71, green255: 73, blue255: 85)
        let horizontalLine = UIView(frame: CGRect(x: 90, y: 14, width: view.frame.width-100, height: 1))
        horizontalLine.backgroundColor = UIColor.lightGray
        containerView.addSubview(horizontalLine)
        
        let vehicleType:String
        switch separatedVehicles[section].first?.vehicleType {
        case .truck?, .rearloaderTruck?, .frontloaderTruck?, .sideLoaderTruck?, .rollOffTruck?:
            vehicleType = "Trucks"
        case .car?:
            vehicleType = "Cars"
        case .bike?:
            vehicleType = "Bikes"
        case .bicycle?:
            vehicleType = "Bicycles"
        default:
            vehicleType = ""
        }
        
        if NavigationManager.shouldRememberLastTruck && vehicles.count > 0 {
            if section == 0 {
                label.text = "Your last truck:"
                containerView.addSubview(label)
                return containerView
            } else {
                label.text = vehicleType
                containerView.addSubview(label)
                return containerView
            }
        } else {
            label.text = vehicleType
            containerView.addSubview(label)
            return containerView
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! VehicleTableViewCell
        // If truck is not taken or selected before from current driver
        if cell.vehicle.status != 0 && cell.vehicle.id != NavigationManager.vehicleID {
            select(vehicle: cell.vehicle)
        } else {
            deselect(vehicle: cell.vehicle)
        }
         cell.tickTapped()
    }
    
    func select( vehicle:Vehicle) {
        // Fetch truck ID and save
        if let vehicleID = vehicle.id {
            self.vehicleID = vehicleID
        }
        // Save current selected truck as Current Truck
        currentVehicle = vehicle
        
        // Fetch truck name and save
        if let vehicleName = vehicle.name {
            NavigationManager.vehicleName = vehicleName
        }
        vehiclesListTableView.reloadData()
    }
    
    func deselect(vehicle:Vehicle){
        vehicleID = nil
        currentVehicle = nil
        NavigationManager.vehicleName = ""
        vehiclesListTableView.reloadData()
    }
    
}

extension ChooseTruckViewController: VehicleCellDelegate {
    func didSelect(vehicle: Vehicle, status: Bool) {
        if status {
           select(vehicle: vehicle)
        } else {
            deselect(vehicle: vehicle)
        }
    }
}

// MARK: - View controller extension
extension ChooseTruckViewController{
    static func create() -> ChooseTruckViewController{
        return UIStoryboard.home.instantiate(ChooseTruckViewController.self)
    }
}
