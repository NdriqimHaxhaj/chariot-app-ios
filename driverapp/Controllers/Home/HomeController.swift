//
//  HomeController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 20/02/2018.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import SideMenu
import FirebaseMessaging
import Firebase

enum HomeControllerPosition {
    case toMap
    case toPickupsList
}

class HomeController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var vehiclesListTableView: UITableView!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var tickButton: UIButton!
    @IBOutlet weak var menuBarButton: UIBarButtonItem!
    @IBOutlet var listBarButton: UIBarButtonItem!
    @IBOutlet var mapBarButton: UIBarButtonItem!
    @IBOutlet var emptyBarButton: UIBarButtonItem!
    @IBOutlet var notificationsBarButton: UIBarButtonItem!
    @IBOutlet weak var rememberMyChoiceLabel: UILabel!
    @IBOutlet weak var doneButton: UIButton!
    
    // MARK: - Properties
    weak var mapMarkersDelegate:MapMarkersDelegate?
    var pickupViewController:UIViewController?
    var menuVC:UISideMenuNavigationController?
    var navigationBoardHeader:NavigationBoardHeader?
    var navigationBoardHeaderHeight:CGFloat = 0
    var selectVehicleView:SelectTruckView?
    var vehicles:[Vehicle] = []
    var vehicleID:Int? {
        didSet{
            doneButton.alpha = 1
            doneButton.isEnabled = true
        }
    }
    weak var pickupDelegate:PickupDelegate?
    var currentVehicle:Vehicle?
    var listController: HomeListController? {
        let homeListController = childViewControllers.compactMap({ $0 as? HomeListController })[0]
        return homeListController
    }
    
    var mapController: HomeMapController? {
        let homeMapController = childViewControllers.compactMap({ $0 as? HomeMapController })[0]
        return homeMapController
    }
    
    var position: HomeControllerPosition{
        get { return getScrollPosition() }
    }
    
    var isScrollEnabled: Bool{
        get{ return scroll.isScrollEnabled }
        set{ scroll.isScrollEnabled = newValue }
    }
    var currentScrollPosition:HomeControllerPosition = .toMap {
        didSet{
            updateCurrentPosition()
        }
    }
    fileprivate var didLayoutSubviews = false
    fileprivate var trucks:[Vehicle] = []
    fileprivate var cars:[Vehicle] = []
    fileprivate var bikes:[Vehicle] = []
    fileprivate var bicycles:[Vehicle] = []
    fileprivate var separatedVehicles:[[Vehicle]] = [[]]
    
    // MARK: - LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        listController?.setupSideMenu()
        setupDelegates()
        addObservers()
        checkDriverAvailability()
        initializeTrucksTableView()
        setupNavigationBoardHeader()
    }

    override func viewDidLayoutSubviews() {
        if !didLayoutSubviews {
            didLayoutSubviews = true
            currentScrollPosition = .toMap
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.isStatusBarHidden = false
        UIApplication.shared.statusBarStyle = .lightContent
        let nav = self.navigationController
        nav?.navigationBar.tintColor = UIColor.white
        if let navigationController = self.navigationController {
            for subview in navigationController.navigationBar.subviews {
                if subview is UIProgressView {
                    subview.removeFromSuperview()
                }
            }
        }
        setupUI()
    }
    
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        scroll.contentSize = CGSize(width: size.width*2, height: size.height)
        view.frame.size = size
        let height = calculateNavigationBoardHeaderViewHeight()
        navigationBoardHeader?.changeHeight(to: height)
        
        delay(delay: 0.1) {
            self.updateCurrentPosition()
        }
        delay(delay: 0.15) {
            self.listController?.scrollToPage((self.listController?.currentScrollPosition)!)
        }
        delay(delay: 0.4) {
            self.postNotification(notification: Notification.Name.centerPickupsSlider, object: nil)
        }
    }
    
    // MARK: - Functions
    @objc func setupUI(){
        let isNightMode = ThemeManager.isNightMode()
        menuBarButton.tintColor = isNightMode ? UIColor.white : Appearance.greenColor
        listBarButton.tintColor = isNightMode ? UIColor.white : Appearance.greenColor
        mapBarButton.tintColor = isNightMode ? UIColor.white : Appearance.greenColor
        notificationsBarButton.tintColor = isNightMode ? UIColor.white : Appearance.greenColor
        footerView.backgroundColor = isNightMode ? Appearance.darkGrayNight : UIColor.white
        vehiclesListTableView.backgroundColor = isNightMode ? Appearance.backgroundViewNight : UIColor.white
        setupNavigationController()
        rememberMyChoiceLabel.textColor = isNightMode ? UIColor.white : UIColor(red255: 71,
                                                                                green255: 73,
                                                                                blue255: 85)
    }
    
    func setupNavigationController(){
        let isNightMode = ThemeManager.isNightMode()
        guard let navigationBar = self.navigationController?.navigationBar else {
            return
        }
        
        navigationBar.barTintColor = isNightMode ? Appearance.darkGrayNight : UIColor.white
        navigationBar.titleTextAttributes = [NSAttributedStringKey.font: Font.robotoBoldFont(size: 17), NSAttributedStringKey.foregroundColor: isNightMode ? UIColor.white : UIColor.darkGray]
        navigationBar.tintColor = isNightMode ? UIColor.white : Appearance.greenColor
    }
    
    func showReviewForm(with data: [AnyHashable:Any]){
        let reviewPickupViewController = ReviewPickupViewController.create()
        reviewPickupViewController.modalPresentationStyle = .overCurrentContext
        reviewPickupViewController.data = data
        self.showModal(reviewPickupViewController, animated: false, completion: nil)
    }
    
    private func getScrollPosition() -> HomeControllerPosition{
        let width = self.view.frame.width
        let offset = scroll.contentOffset.x
        
        if offset >= 0  && offset < width{
            return .toMap
        }else{
            return .toPickupsList
        }
    }
    
    func updateCurrentPosition(animated: Bool = true){
        self.view.endEditing(true)
        let shouldGoRight = currentScrollPosition == .toPickupsList
        let offset: CGFloat
        if #available(iOS 11.0, *) {
            offset = shouldGoRight ? view.safeAreaLayoutGuide.layoutFrame.width : 0.0
        } else {
            offset = shouldGoRight ? view.frame.width : 0.0
        }
        
        scroll.setContentOffset(CGPoint.init(x: offset, y: 0), animated: animated)
        if shouldGoRight {
            listController?.view.layoutIfNeeded()
            listController?.view.setNeedsDisplay()
            listController?.view.setNeedsLayout()
            listController?.scrollView.layoutIfNeeded()
            
        } else {
            if let isNavigationActive = mapController?.isNavigationActive, let isNavigationBoardShown = mapController?.isNavigationBoardShown {
                
                if shouldGoRight {
                    if isNavigationActive {
                        hideNavigationBoardHeader()
                    }
                } else {
                    if isNavigationActive {
                        showNavigationBoardHeader()
                    }
                }
            }
        }
        // Hide and show bar buttons based on current position
        if !shouldGoRight { // If it's left
           self.navigationItem.rightBarButtonItems = [listBarButton]
        } else {
            self.navigationItem.rightBarButtonItems = [mapBarButton, emptyBarButton, notificationsBarButton]
        }
    }
    
    func goOffline(){
        dispatch {
            let selectReasonViewController = SelectReasonViewController.create()
            selectReasonViewController.modalPresentationStyle = .overCurrentContext
            selectReasonViewController.delegate = self
            self.showModal(selectReasonViewController, animated: false, completion: nil)
        }
    }
    
    func goOnline(){
        if NavigationManager.offlineReasonId == OfflineReasonType.leaveTruck.rawValue {
            dispatch {
                let chooseTruckViewController = ChooseTruckViewController.create()
                chooseTruckViewController.delegate = self
                self.showModal(chooseTruckViewController)
            }
        } else {
            DriverREST.changeAvailability(isOnline: 1, reason: 1) { (success, error) in
                if success {
                    NavigationManager.offlineReasonId = OfflineReasonType.notSpecified.rawValue
                    Preferences.isWorkingStatusOn = true
                    self.selectVehicleView?.removeFromSuperview()
                    self.selectVehicleView = nil
                    self.listController?.getPickups(completion: { (_) in})
                    self.enableNavigationButtons()
                    self.mapController?.enableNavigationButtons()
                } else {
                    print(error?.message ?? "Error getting back online")
                }
            }
        }
    }
    
    func showOfflineMode(){
        let offlineController = OfflineController.create()
        offlineController.modalPresentationStyle = .overCurrentContext
        dispatch {
            self.showModal(offlineController, animated: false, completion: nil)
        }
    }
    
    func setupDelegates(){
        mapController?.homeMapDelegate = listController
        mapController?.pickupDelegate = self
        listController?.mapMarkersDelegate = mapController
        listController?.pickupDelegate = self
    }
    
    func isAnyPickupOnGoing(completion: @escaping (_ id: Int,_ state:Bool)->Void){
        guard let pickups = mapController?.pickupRequests else {
            completion(-1,false)
            return
        }
        for pickup in pickups {
            if pickup.pickupStatus == .ongoing {
                completion(pickup.id!,true)
                return
            }
        }
        completion(-1,false)
    }
    
    func startPickupWith(_ id:Int, _ route:String, completion: @escaping (_ didComplete:Bool)->Void){
        showHud()
        isAnyPickupOnGoing { (cancelPickupId, status) in
            if status{
                PickupREST.startAnotherPickup(id, cancelPickupId, completionHandler: { (success, error) in
                    self.hideHud()
                    if success {
                        print("The new pickup successfully started. Canceled the one ongoing")
                        completion(true)
                    } else {
                        print(#function)
                        print("Error on startAnotherPickup : \(String(describing: error))")
                        completion(false)
                    }
                })
            } else {
                PickupREST.startPickup(id, route) { (success, error) in
                    self.hideHud()
                    if success {
                        print("The new pickup successfully started")
                        completion(true)
                    } else {
                        print(#function)
                        print("Error on completePickup : \(String(describing: error))")
                        completion(false)
                    }
                }
            }
        }
    }
    
    // MARK: - IBACTIONS
    @IBAction func mapBarButtonPressed(_ sender: UIBarButtonItem) {
        mapController?.checkMapScheme()
        mapController?.checkDispatcherMessages()
        currentScrollPosition = .toMap
    }
    @IBAction func notificationsBarButtonPressed(_ sender: UIBarButtonItem) {
        mapController?.checkMapScheme()
        mapController?.checkDispatcherMessages()
        guard let pickups = mapController?.pickupRequests else {
            print("Could't get pickups from mapController. Pickups = nil")
            return
        }
        let notificationsViewController = NotificationsViewController.create()
        notificationsViewController.pickupsRequests = pickups
        self.push(notificationsViewController)
        
    }
    
    @IBAction func listButtonPressed(_ sender: UIBarButtonItem) {
        mapController?.checkMapScheme()
        mapController?.checkDispatcherMessages()
        if position != .toPickupsList {
            currentScrollPosition = .toPickupsList
        }
    }
    
    @IBAction func menuButtonPressed(_ sender: UIBarButtonItem) {
        mapController?.checkMapScheme()
        mapController?.checkDispatcherMessages()
        let menuVC = SideMenuManager.defaultManager.menuLeftNavigationController
        self.showModal(menuVC!)
    }
    @IBAction func doneButtonPressed(_ sender: UIButton) {
        print("Done pressed")
        self.showHud()
        if vehicleID != nil {
            if let vehicleID = vehicleID {
                DriverREST.changeAvailability(isOnline: 1, reason: 1, completionHandler: { (success, error) in
                    if success {
                        Preferences.isWorkingStatusOn = true
                        NavigationManager.offlineReasonId = OfflineReasonType.notSpecified.rawValue
                        DriverREST.chooseVehicle(vehicleID) { (success, error) in
                            self.hideHud()
                            if success {
                                let shouldRememberLastTruck = self.tickButton.isSelected
                                NavigationManager.shouldRememberLastTruck = shouldRememberLastTruck
                                AccountManager.isApplicationTerminated = false
                                NavigationManager.vehicleID = vehicleID
                                if let vehicleTypeID = self.currentVehicle?.truckTypeID {
                                    NavigationManager.vehicleTypeID = "\(vehicleTypeID)"
                                    let vehicleType = VehicleType.init(type: vehicleTypeID)
                                    if vehicleType.rawValue > 1 && vehicleType.rawValue < 5 {
                                        switch vehicleTypeID {
                                        case 2: NavigationManager.transportMode = 0
                                        case 3: NavigationManager.transportMode = 7
                                        case 4: NavigationManager.transportMode = 8
                                        default: break
                                        }
                                    } else {
                                        NavigationManager.transportMode = 5
                                    }
                                }
                                self.showLoadingTruckPickups(NavigationManager.vehicleName)
                                self.hideVehiclesTableView(onFulfilled: {
                                    self.selectVehicleView?.removeFromSuperview()
                                    self.enableNavigationButtons()
                                    self.mapController?.enableNavigationButtons()
                                    self.listController?.getPickups(completion: { (_) in})
                                })
                                self.findEarliestAndLatestTimeForFilter()
                            }
                        }
                    }
                })
            }
        }
    }
    
    func findEarliestAndLatestTimeForFilter(){
        listController?.findEarliestAndLatestTimeForFilter()
    }
    
    func showLoadingTruckPickups(_ truckName: String){
        let loadTruckPickupsVC = RemoveFromTruckViewController.create()
        loadTruckPickupsVC.tempText = "Wait a minute until we load the \(truckName) pickups"
        loadTruckPickupsVC.modalPresentationStyle = .overFullScreen
        showModal(loadTruckPickupsVC, animated: false, completion: nil)
    }
    
    @IBAction func rememberMyChoiceLabelPressed(_ sender: UITapGestureRecognizer) {
        rememberMyChoice()
    }
    @IBAction func rememberMyChoicePressed(_ sender: UITapGestureRecognizer) {
        rememberMyChoice()
    }
    func rememberMyChoice(){
        tickButton.isSelected = !tickButton.isSelected
    }
    
    
    
    // MARK: - Functions
    func addObservers(){
        registerNotification(notification: Notification.Name.changeTheme, selector: #selector(self.setupUI))
        registerNotification(notification: Notification.Name.showHome, selector: #selector(self.showHome))
        registerNotification(notification: Notification.Name.showSettings, selector: #selector(self.showSettings))
        registerNotification(notification: Notification.Name.signOut, selector: #selector(self.promptForSignOut))
        registerNotification(notification: Notification.Name.disableNavigationButtons, selector: #selector(self.disableNavigationButtons))
        registerNotification(notification: Notification.Name.enableNavigationButtons, selector: #selector(self.enableNavigationButtons))
        registerNotification(notification: Notification.Name.schedulePickupRequest, selector: #selector(self.schedulePickupRequest))
        registerNotification(notification: Notification.Name.navigateToDropOff, selector: #selector(self.showMap))
        registerNotification(notification: Notification.Name.workingSwitchPressed, selector: #selector(self.workingSwitchPressed(_:)))
        registerNotification(notification: Notification.Name.navigationStarted, selector: #selector(self.showNavigationBoardHeader))
        registerNotification(notification: Notification.Name.navigationStopped, selector: #selector(self.hideNavigationBoardHeader))
        registerNotification(notification: Notification.Name.editPickup, selector: #selector(self.editPickup(not:)) )
        registerNotification(notification: NSNotification.Name.UIDeviceOrientationDidChange, selector: #selector(self.setupScrollView))
        registerNotification(notification: Notification.Name.navigationBoardRoadDetailsPressed, selector: #selector(self.openRoadDetailsViewController))
    }
    
    @objc func openRoadDetailsViewController(){
        let roadDetailsVC = RoadDetailsViewController.create()
        roadDetailsVC.modalPresentationStyle = .overCurrentContext
        roadDetailsVC.delegate = mapController
        if let isLandscape = mapController?.navigationBoard?.isLandscape {
            roadDetailsVC.isLandscape = isLandscape
        }
        if let previousOrientiation = mapController?.navigationBoard?.previousOrientiation {
            roadDetailsVC.previousOrientiation = previousOrientiation
        }
        self.showModal(roadDetailsVC, animated: false, completion: nil)
    }
    
    @objc func setupScrollView(){
        if #available(iOS 11.0, *) {
            
            scroll.contentInsetAdjustmentBehavior = .never
        } else {
            // Fallback on earlier versions
        }
        self.view.layoutIfNeeded()
    }
    
    func checkDriverAvailability(){
        if NavigationManager.offlineReasonId == OfflineReasonType.leaveTruck.rawValue {
            showTruckSelectionView()
        } else if NavigationManager.isGoingToDropOff {
            disableNavigationButtons()
        } else {
            enableNavigationButtons()
        }
    }
    
    func initializeTrucksTableView(){
        selectVehicleView?.alpha = Preferences.isWorkingStatusOn ? 1 : 0
        vehiclesListTableView.register(VehicleTableViewCell.self)
        vehiclesListTableView.estimatedRowHeight = 100
        vehiclesListTableView.contentInset = UIEdgeInsetsMake(0, 0, 120, 0)
        vehiclesListTableView.rowHeight = UITableViewAutomaticDimension
        if NavigationManager.shouldRememberLastTruck && NavigationManager.vehicleID != 0 {
            vehicleID = NavigationManager.vehicleID
        }
    }
    
    @objc func showHome(){
        currentScrollPosition = .toMap
        popToRoot()
    }
    
    @objc func showSettings(){
        let settingsViewController = SettingsViewController.create()
        self.push(settingsViewController)
    }
    
    @objc func promptForSignOut(){
        let alert = UIAlertController(title: "Are you sure you want to sign out?", message: "", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (_) in
        }))
        alert.addAction(UIAlertAction(title: "Sign Out", style: .default, handler: { (_) in
            self.mapController?.stopNavigation()
            self.proceedLogOut()
        }))
        
        alert.view.tintColor = Appearance.greenColor
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = Appearance.greenColor
    }
    
    @objc func disableNavigationButtons(){
        menuVC?.sideMenuManager.menuWidth = 0
        
        menuBarButton.isEnabled = false
        notificationsBarButton.isEnabled = false
        mapBarButton.isEnabled = false
        listBarButton.isEnabled = false
        
    }
    
    @objc func enableNavigationButtons(){
        menuVC?.sideMenuManager.menuWidth = Constants.leftMenuWidth
        menuBarButton.isEnabled = true
        notificationsBarButton.isEnabled = true
        mapBarButton.isEnabled = true
        listBarButton.isEnabled = true
        
    }
    
    @objc func schedulePickupRequest(){
        let addPickupViewController = AddPickupViewController.create()
        self.push(addPickupViewController)
    }
    
    @objc func showMap(){
        currentScrollPosition = .toMap
    }
    
    @objc func workingSwitchPressed(_ notification: NSNotification){
        if let dict = notification.userInfo as NSDictionary? {
            if let isGoingToWork = dict["isGoingToWork"] as? Bool{
                if isGoingToWork {
                    goOnline()
                } else {
//                    if !NavigationManager.isGoingToDropOff &&
//                        NavigationManager.offlineReasonId != OfflineReasonType.leaveTruck.rawValue {
//                        goOnline()
//                    } else {
//                        goOffline()
//                    }
                    goOffline()
                }
            }
        }
    }
    
    @objc func editPickup(not: Notification){
        // userInfo is the payload send by sender of notification
        if let userInfo = not.userInfo {
            // Safely unwrap the name sent out by the notification sender
            if let pickup = userInfo["pickup"] as? PickupRequest {
                print("Editing Pickup")
                let addPickupViewController = AddPickupViewController.create()
                addPickupViewController.pickupMode = .edit
                let convertetPickup = PMPickupRequest(pickup)
                addPickupViewController.pickupRequest = convertetPickup
                push(addPickupViewController)
            }
        }
    }
    
    func openMessageCenter(){
        mapController?.checkMapScheme()
        mapController?.checkDispatcherMessages()
        mapController?.chatButton.badgeString = nil
        let dispatcherChatViewController = DispatcherChatViewController.create()
        dispatcherChatViewController.delegate = self
        self.push(dispatcherChatViewController)
    }
    
    func openPickupNotesViewVontroller(_ pickupId:Int){
        if AccountManager.isLogged {
            if let pickup = mapController?.pickupRequests.filter({$0.id == pickupId}).first {
                let pickupNotesVC = PickupNotesViewController.create()
                pickupNotesVC.modalPresentationStyle = .overCurrentContext
                pickupNotesVC.pickupRequest = pickup
                showModal(pickupNotesVC)
            }
        }
    }
    
    func openPickupProfile(_ pickupId:Int){
        if let pickup = mapController?.pickupRequests.filter({$0.id == pickupId}).first {
            switch pickup.pickupStatus {
            case .assigned:
                let currentPickupVC = CurrentPickupViewController.create()
                currentPickupVC.pickupRequest = pickup
                currentPickupVC.modalPresentationStyle = .overCurrentContext
                currentPickupVC.cancelPickupDelegate = self
                dispatch {
                    self.present(currentPickupVC, animated: false, completion: {
                        print("AssignedPickupViewController to be presented")
                    })
                }
                break
            // Cancelled Pickup
            case .canceled:
                let cancelledPickupVC = CancelledPickupViewController.create()
                cancelledPickupVC.pickupRequest = pickup
                cancelledPickupVC.modalPresentationStyle = .overCurrentContext
                cancelledPickupVC.delegate = self
                dispatch {
                    self.present(cancelledPickupVC, animated: false, completion: {
                        print("CancelledPickupViewController to be presented")
                    })
                }
                let overduePickupVC = OverduePickupViewController.create()
                overduePickupVC.pickupRequest = pickup
                overduePickupVC.modalPresentationStyle = .overCurrentContext
                overduePickupVC.overdueDelegate = self
                overduePickupVC.confirmDelegate = self
                dispatch {
                    self.present(overduePickupVC, animated: false, completion: {
                        print("OverduePickupViewController to be presented")
                    })
                }
            default: break
            }
        }
    }
    
    func proceedLogOut(){
        showHud()
        UserREST.leaveTruck { (success, error) in
            if success {
                UserREST.logOut { 
                    if success {
                        self.hideHud()
                        AccountManager.clear()
                        NavigationManager.clear()
                        FirebaseService.shared.removeFirebaseToken()
                        AccountManager.updateRootWindow()
                    }
                }
            }
        }
    }
}

// MARK: - EXTENSIONS
extension HomeController: UISideMenuNavigationControllerDelegate{
    func sideMenuWillAppear(menu: UISideMenuNavigationController, animated: Bool) {
        console("sideMenuWillAppear")
    }
    
    func sideMenuDidDisappear(menu: UISideMenuNavigationController, animated: Bool) {
        console("sideMenuDidDisappear")
    }
}
// MARK: - Navigation Board Header Delegate extension
extension HomeController: NavigationBoardHeaderDelegate {
    func showManeuvers() {
        showManueversController()
    }
    
    func showManueversController(){
        if let manuevers = mapController?.mapRoute?.route.maneuvers {
            let manueverViewController = ManueversViewController.create()
            manueverViewController.manuevers = manuevers
            showModal(manueverViewController)
        }
    }
    
    func setupNavigationBoardHeader(){
        navigationBoardHeader = UINib(nibName: "NavigationBoardHeader", bundle: nil).instantiate(withOwner: self, options: nil).first as? NavigationBoardHeader
        navigationBoardHeader?.delegate = self
        navigationBoardHeaderHeight = 100
        navigationBoardHeader?.frame = CGRect(x: UIScreen.main.nativeBounds.minX,
                                              y: -navigationBoardHeaderHeight,
                                              width: UIScreen.main.bounds.width,
                                              height: navigationBoardHeaderHeight)
    }
    
    @objc func showNavigationBoardHeader(){
        if currentScrollPosition == .toMap && mapController?.isNavigationActive ?? true {
            navigationBoardHeader?.frame = CGRect(x: 0,
                                                  y: -navigationBoardHeaderHeight,
                                                  width: UIScreen.main.bounds.width,
                                                  height: navigationBoardHeaderHeight)
            let height = calculateNavigationBoardHeaderViewHeight()
            
            navigationBoardHeader?.changeHeight(to: height)
            self.navigationController?.view.addSubview(navigationBoardHeader!)
            UIView.animate(withDuration: 0.2) {
                self.navigationBoardHeader?.frame.origin.y = 0
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @objc func hideNavigationBoardHeader(){
        if let height = self.navigationBoardHeader?.frame.height {
            UIView.animate(withDuration: 0.2, animations: {
                self.navigationBoardHeader?.frame.origin.y = -height
                self.view.layoutIfNeeded()
            }) { (_) in
                self.navigationBoardHeader?.removeFromSuperview()
            }
        }
    }
    
    func calculateNavigationBoardHeaderViewHeight()->Int {
        switch UIDevice.current.orientation {
        case .portrait,.portraitUpsideDown:
            if UIDevice().userInterfaceIdiom == .phone {
                
                switch UIScreen.main.nativeBounds.height {
                case 1136,1334,1920, 2208:
                    return 90
                case 2436, 1792, 2688:
                    return 100
                default:
                    return 90
                }
            } else {
                return 90
            }
        case .landscapeLeft, .landscapeRight:
            return 90
        default:
            return 90
        }
    }
}

// MARK: - Pickup Delegate extension
extension HomeController: PickupDelegate {
    func editPickup(_ pickup: PickupRequest) {
        let addPickupViewController = AddPickupViewController.create()
        addPickupViewController.pickupMode = .edit
        let convertetPickup = PMPickupRequest(pickup)
        addPickupViewController.pickupRequest = convertetPickup
        push(addPickupViewController)
    }
    
    
    func triggerNavigation(_ id: Int, _ route:String) {
        if mapController?.pickupRequests.filter({$0.pickupStatus == PickupStatusType.ongoing}).first != nil {
            dispatch {
                self.currentScrollPosition = .toMap
            }
            self.mapController?.startToNavigate()
        } else {
            startPickupWith(id, route) { (isCompleted) in
                if isCompleted {
                    print("Pickup started")
                    dispatch {
                        self.currentScrollPosition = .toMap
                    }
                    self.mapController?.startToNavigate()
                }
            }
        }
    }
    
    func refreshPickupDelegateFromCell() {
        pickupDelegate?.refreshPickupDelegateFromCell()
    }
}

// MARK: - Reason Delegate extension
extension HomeController: ReasonDelegate {
    
    func openDropOffTable(_ reason: OfflineReason) {
        let dropOffLocationViewController = DropOffViewController.create()
        dropOffLocationViewController.reason = reason
        dispatch {
            self.showModal(dropOffLocationViewController)
        }
    }
    
    func showTruckSelectionView(){
        var height: CGFloat = 0
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                height = (navigationController?.navigationBar.frame.height)! - 20
            case 1334:
                print("iPhone 6/6S/7/8")
                height = (navigationController?.navigationBar.frame.height)! - 20
            case 1920, 2208:
                height = (navigationController?.navigationBar.frame.height)! - 20
                print("iPhone 6+/6S+/7+/8+")
            case 2436:
                height = (navigationController?.navigationBar.frame.height)! + 10
                print("iPhone X")
            default:
                print("unknown")
            }
        }
        dispatch {
            let frame = CGRect(x: self.view.frame.width / 2 - 50,
                               y: height,
                               width: 130,
                               height: 30)
            self.selectVehicleView = SelectTruckView(frame: frame)
            self.navigationController?.view.addSubview(self.selectVehicleView!)
            self.selectVehicleView?.delegate = self
            self.mapController?.removeMapMarkers(onComplete: nil)
            self.mapController?.disableNavigationButtons()
            self.disableNavigationButtons()
            self.menuBarButton.isEnabled = true
        }
    }
    
    func showTrucksLoader(){
        let removeFromTruckViewController = RemoveFromTruckViewController.create()
        removeFromTruckViewController.modalPresentationStyle = .overFullScreen
        removeFromTruckViewController.tempText = "Please wait until we remove your from \(NavigationManager.vehicleName)"
        showModal(removeFromTruckViewController, animated: false, completion: nil)
        showTruckSelectionView()
    }
    
    func reasonForGoingOffline(_ reason: OfflineReason?) {
        if reason == nil {
            print("Selected: No reason to go offline")
        } else {
            let reasonId = reason?.id ?? -1
            DriverREST.changeAvailability(isOnline: 0, reason: reasonId) { (success, error) in
                self.hideHud()
                if success{
                    Preferences.isWorkingStatusOn = false
                    self.currentScrollPosition = .toMap
                    NavigationManager.offlineReasonId = reason?.id ?? -1
                    if reason?.type ==  OfflineReasonType.leaveTruck {
                        self.showTrucksLoader()
                        //self.addTrucks on Title View
                    } else {
                        self.showOfflineMode()
                    }
                }
            }
        }
    }
}

extension HomeController: HomeMapPickupDelegate {
    func delegatePickups(_ pickups: [PickupRequest]) {
        // Does Nothing here
    }
    
    func delegatePickup(_ pickup: PickupRequest) {
        switch pickup.pickupStatus {
            case .assigned:
                let currentPickupViewController = CurrentPickupViewController.create()
                currentPickupViewController.pickupRequest = pickup
                currentPickupViewController.cancelPickupDelegate = self
                currentPickupViewController.modalPresentationStyle = .overCurrentContext
                dispatch {
                    self.showModal(currentPickupViewController, animated: false, completion: nil)
                }
                break
            case .completedWithDelays,.completedWithoutDelays:
                let uncompletePickupViewController = UncompletePickupViewController.create()
                uncompletePickupViewController.pickupRequest = pickup
                uncompletePickupViewController.confirmActionDelegate = self
                uncompletePickupViewController.uncancelPickupDelegate = self
                uncompletePickupViewController.modalPresentationStyle = .overCurrentContext
                dispatch {
                    self.showModal(uncompletePickupViewController, animated: false, completion: nil)
                }
                break
            case .canceled:
                let cancelledPickupViewController = CancelledPickupViewController.create()
                cancelledPickupViewController.pickupRequest = pickup
                cancelledPickupViewController.delegate = self
                cancelledPickupViewController.modalPresentationStyle = .overCurrentContext
                dispatch {
                    self.showModal(cancelledPickupViewController, animated: false, completion: nil)
                }
                break
            case .overdue:
                let overduePickupViewController = OverduePickupViewController.create()
                overduePickupViewController.pickupRequest = pickup
                overduePickupViewController.overdueDelegate = self
                overduePickupViewController.confirmDelegate = self
                overduePickupViewController.modalPresentationStyle = .overCurrentContext
                dispatch {
                    self.showModal(overduePickupViewController, animated: false, completion: nil)
                }
                break
            default:
                break
        }
    }
}

extension HomeController: ConfirmPickupActionDelegate, UncancelPickupPickupDelegate, CancelPickupStatusDelegate, OverduePickupDelegate, CancelPickupDelegate, CancelledPickupDelegate {
    
    // ConfirmPickupActionDelegate
    func refreshPickups() {
        listController?.getPickups(completion: { (_) in
            self.mapController?.checkNextPickupToBeStarted()
        })
        
    }
    
    // UncancelPickupPickupDelegate
    func uncompletePickup() {
        listController?.getPickups(completion: { (_) in
        })
    }
    
    // CancelPickupStatusDelegate
    func shouldCancelPickup(_ status: Bool) {
        listController?.getPickups(completion: { (_) in
        })
    }
    
    // OverduePickupDelegate
    func isOverduedPickupCanceled(_ status: Bool) {
        listController?.getPickups(completion: { (_) in
        })
    }
    
    func startOverduePickup(_ status: Bool, _ id: Int, _ route:String) {
        if status {
            startPickupWith(id, route) { (isCompleted) in
                if isCompleted {
                    self.listController?.getPickups(completion: { (success) in
                        if success {
                            self.mapController?.startToNavigate()
                        }
                    })
                }
            }
        }
    }
    
    func isOverduePickupCompleted(_ status: Bool) {
        listController?.getPickups(completion: { (success) in
        })
    }
    
    // CancelPickupDelegate
    func isPickupCanceled(_ status: Bool) {
        listController?.getPickups(completion: { (_) in
        })
    }
    
    func startPickup(_ status: Bool, _ id: Int, _ route: String) {
        if status {
            startPickupWith(id, route) { (isCompleted) in
                if isCompleted {
                    self.listController?.getPickups(completion: { (success) in
                        if success {
                            guard let pickups = self.mapController?.pickupRequests else {return}
                            for pickup in pickups {
                                if pickup.id == id {
                                    self.listController?.getPickups(completion: { (success) in
                                        if success {
                                            self.mapController?.currentPickupRequest = pickup
                                            self.mapController?.startToNavigate()
                                        }
                                    })
                                }
                            }
                        }
                    })
                }
            }
        }
    }
    
    func isPickupCompleted(_ status: Bool) {
        listController?.getPickups(completion: { (success) in
        })
    }
    
    // CancelledPickupDelegate
    func isCancelPickupUndone(_ status: Bool) {
        listController?.getPickups(completion: { (_) in
        })
    }
}

// MARK: - Vehicle List Extension
extension HomeController: ChooseVehicleDelegate {
    func enableButtons(_ status: Bool) {
        if status {
            selectVehicleView?.removeFromSuperview()
            enableNavigationButtons()
            mapController?.enableNavigationButtons()
            mapController?.soundsButton.alpha = 0
            listController?.getPickups(completion: { (_) in})
        }
    }
}

extension HomeController: SelectVehicleDelegate {
    func shouldOpenTrucksList(_ status: Bool) {
        if status {
            getVehicles { (vehiclesReceived) in
                if vehiclesReceived {
                    self.showVehiclesTableView()
                } else {
                    self.hideVehiclesTableView(onFulfilled: nil)
                }
            }
        } else {
            hideVehiclesTableView(onFulfilled: nil)
            
        }
    }
    
    func showVehiclesTableView(){
        vehiclesListTableView.frame.size.height = 0
        self.vehiclesListTableView.alpha = 1

        UIView.animate(withDuration: 0.5) {
            self.vehiclesListTableView.frame.size.height = self.scroll.frame.height
            self.footerView.alpha = 1
            self.view.layoutIfNeeded()
        }
        vehiclesListTableView.reloadData()
    }
    
    func hideVehiclesTableView(onFulfilled: (()->())?){
        UIView.animate(withDuration: 0.5, animations: {
            self.vehiclesListTableView.frame.size.height = 0
            self.footerView.alpha = 0
            self.view.layoutIfNeeded()
        }) { (_) in
            self.vehiclesListTableView.alpha = 0
            self.doneButton.alpha = 0.5
            onFulfilled?()
        }
    }
    
    func getVehicles(completionHandler: @escaping (_ status:Bool)->Void){
        DriverREST.getVehicles { (success, vehicles, error) in
            if success {
                guard let vehicles = vehicles else {return}
                self.vehicles = vehicles
                self.separateVehiclesIntoSections()
                completionHandler(true)
            } else {
                completionHandler(false)
            }
        }
    }
    
    func separateVehiclesIntoSections(){
        trucks.removeAll()
        cars.removeAll()
        bikes.removeAll()
        bicycles.removeAll()
        separatedVehicles.removeAll()
        for vehicle in self.vehicles {
            switch vehicle.vehicleType {
            case .truck?, .frontloaderTruck?, .rearloaderTruck?, .sideLoaderTruck?:
                trucks.append(vehicle)
                break
            case .car?:
                cars.append(vehicle)
                 break
            case .bike?:
                bikes.append(vehicle)
                 break
            case .bicycle?:
                bicycles.append(vehicle)
            default :
                break
            }
        }
        if !trucks.isEmpty {
            separatedVehicles.append(trucks)
        }
        if !cars.isEmpty {
            separatedVehicles.append(cars)
        }
        if !bikes.isEmpty {
            separatedVehicles.append(bikes)
        }
        if !bicycles.isEmpty {
            separatedVehicles.append(bicycles)
        }
    }
}

extension HomeController: UITableViewDelegate, UITableViewDataSource {
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        return "
//    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if NavigationManager.shouldRememberLastTruck && vehicles.count > 0 {
            return 1 + separatedVehicles.count
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if NavigationManager.shouldRememberLastTruck && vehicles.count > 0 {
            if section == 0 {
                return 1
            } else {
                return separatedVehicles[section].count
            }
        } else {
            return separatedVehicles[section].count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(VehicleTableViewCell.self, for: indexPath)
        cell.delegate = self
        if NavigationManager.shouldRememberLastTruck && vehicles.count > 0 {
            if indexPath.section == 0 {
                var selectedTruck:Vehicle?
                for truck in vehicles {
                    if truck.id == NavigationManager.vehicleID {
                        selectedTruck = truck
                    }
                }
                if selectedTruck != nil {
                    cell.vehicle = selectedTruck
                    cell.isVehicleSelected = cell.vehicle.id == vehicleID
                }
            } else {
                let vehicle = separatedVehicles[indexPath.section][indexPath.row]
                cell.vehicle = vehicle
                cell.isVehicleSelected = cell.vehicle.id == vehicleID
            }
            cell.selectionStyle = .none
            return cell
        } else {
            if separatedVehicles[indexPath.section].count > indexPath.row {
                let vehicle = separatedVehicles[indexPath.section][indexPath.row]
                cell.vehicle = vehicle
                cell.isVehicleSelected = cell.vehicle.id == vehicleID
                cell.selectionStyle = .none
                return cell
            } else {
                return UITableViewCell()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let backgroundColor = UIColor.white
        let containerView = UIView(frame: CGRect(x: 10, y: 0, width: view.frame.width-10, height: 30))
        containerView.backgroundColor = ThemeManager.isNightMode() ? Appearance.backgroundViewNight : backgroundColor
        let label = UILabel(frame: CGRect(x: 10, y: 0, width: 80, height: containerView.frame.height))
        label.textColor = UIColor(red255: 71, green255: 73, blue255: 85)
        let horizontalLine = UIView(frame: CGRect(x: 90, y: 14, width: view.frame.width-100, height: 1))
        horizontalLine.backgroundColor = UIColor.lightGray
        containerView.addSubview(horizontalLine)
        
        let vehicleType:String
        switch separatedVehicles[section].first?.vehicleType {
        case .truck?, .rearloaderTruck?, .frontloaderTruck?, .sideLoaderTruck?, .rollOffTruck?:
            vehicleType = "Trucks"
        case .car?:
            vehicleType = "Cars"
        case .bike?:
            vehicleType = "Bikes"
        case .bicycle?:
            vehicleType = "Bicycles"
        default:
            vehicleType = ""
        }
        
        if NavigationManager.shouldRememberLastTruck && vehicles.count > 0 {
            if section == 0 {
                label.text = "Your last truck:"
                containerView.addSubview(label)
                return containerView
            } else {
                label.text = vehicleType
                containerView.addSubview(label)
                return containerView
            }
        } else {
            label.text = vehicleType
            containerView.addSubview(label)
            return containerView
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! VehicleTableViewCell
        // If truck is not taken or selected before from current driver
        if cell.vehicle.status != 0 && cell.vehicle.id != NavigationManager.vehicleID {
            select(vehicle: cell.vehicle)
        } else {
            cell.tickTapped()
        }
    }
    

    
    func select( vehicle: Vehicle) {
        if let vehicleID = vehicle.id {
            self.vehicleID = vehicleID
            NavigationManager.vehicleID = vehicleID
            currentVehicle = vehicle
        }
        
        if let vehicleName = vehicle.name {
            selectVehicleView?.vehicleName = vehicleName
            NavigationManager.vehicleName = vehicleName
        }
        vehiclesListTableView.reloadData()
    }
}

extension HomeController: VehicleCellDelegate {
    func didSelect(vehicle: Vehicle, status: Bool) {
        if status {
            select(vehicle: vehicle)
        }
    }
}

extension HomeController: DispatcherChatDelegate {
    func chatUpdates() {
        print("DispatcherChatDelegate")
        mapController?.chatButton.badgeString = nil
    }
}

