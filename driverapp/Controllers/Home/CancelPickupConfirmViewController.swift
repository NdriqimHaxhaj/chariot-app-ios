//
//  StartNewPickupConfirmViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 4/23/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

protocol CancelConfirmPickupDelegate:class {
    func cancelPickup(_ state:Bool)
}

class CancelPickupConfirmViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var closeButtonView: UIView!
    @IBOutlet weak var closeIcon: UIImageView!
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    // MARK: - Constraints
    @IBOutlet weak var yAxisPopupConstraint: NSLayoutConstraint!
    
    
    // MARK: - Properties
    var cancelPickupId:Int!
    weak var delegate:CancelConfirmPickupDelegate?
    var buildingName:String!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareForAnimation()
        setTexts()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        showSelf()
    }
    
    // MARK - IBActions
    @IBAction func yesButtonPressed(_ sender: UIButton) {
        cancelPickup()
    }
    
    @IBAction func noButtonPressed(_ sender: UIButton) {
        hideSelf(delegatingPickup: false)
    }
    
    @IBAction func closeButtonPressed(_ sender: UITapGestureRecognizer) {
        hideSelf(delegatingPickup: false)
    }
    
    @IBAction func backgroundButtonPressed(_ sender: UITapGestureRecognizer) {
        hideSelf(delegatingPickup: false)
    }
    
    // MARK: - Functions
    
    func setupUI(){
        if ThemeManager.isNightMode() {
            popupView.backgroundColor = Appearance.darkGrayNight
            descriptionLabel.textColor = UIColor.white
            closeButtonView.borderColor = Appearance.darkGrayNight
            closeButtonView.backgroundColor = Appearance.darkGrayNight
            closeIcon.image = #imageLiteral(resourceName: "X")
        }
    }
    func cancelPickup(){
        self.hideSelf(delegatingPickup: true)
    }
    
    func prepareForAnimation(){
        yAxisPopupConstraint.constant = self.view.frame.height
        backgroundView.alpha = 0
        view.layoutIfNeeded()
    }
    
    func setTexts(){
        descriptionLabel.text = "Are you sure you want to Cancel ‘\(buildingName!)’? "
    }
    
    func showSelf(){
        yAxisPopupConstraint.constant = 0
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 10, options: [.curveEaseIn], animations: {
            self.backgroundView.alpha = 0.5
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    func hideSelf(delegatingPickup:Bool){
        yAxisPopupConstraint.constant = self.view.frame.height
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0, options: [.curveEaseIn], animations: {
            self.backgroundView.alpha = 0
            self.view.layoutIfNeeded()
        }, completion: { _ in
            self.dismiss(animated: false, completion: {
                print("Dissmised StartNewPickupConfirmViewController")
                if delegatingPickup {
                    self.delegate?.cancelPickup(true)
                }
            })
        })
    }
    
}

// MARK: - View controller extension
extension CancelPickupConfirmViewController{
    static func create() -> CancelPickupConfirmViewController{
        return UIStoryboard.home.instantiate(CancelPickupConfirmViewController.self)
    }
}
