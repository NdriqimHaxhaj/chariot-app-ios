//
//  RoadDetailsViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 2/7/19.
//  Copyright © 2019 Zombie Soup. All rights reserved.
//

import UIKit

protocol RoadDetailsDelegate:class {
    func overviewButtonPressed()
    func completeButtonPressed()
    func cancelButtonPressed()
}

class RoadDetailsViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var backgroundView: UIView!
    
    // MARK: - Properties
    var previousOrientiation:UIDeviceOrientation = UIDevice.current.orientation
    var isLandscape = false
    var delegate:RoadDetailsDelegate?
    
    // MAKR: - Constraints
    @IBOutlet weak var overviewVerticalConstraint: NSLayoutConstraint! // -180
    @IBOutlet weak var overviewHorizontalConstraint: NSLayoutConstraint!
    @IBOutlet weak var completeVerticalConstraint: NSLayoutConstraint!
    @IBOutlet weak var completeHorizontalConstraint: NSLayoutConstraint!
    @IBOutlet weak var cancelHorizontalConstraint: NSLayoutConstraint!
    @IBOutlet weak var cancelVerticalConstraint: NSLayoutConstraint! // 180
    @IBOutlet weak var closeHorizontalConstraint: NSLayoutConstraint!
    @IBOutlet weak var closeBottomConstraint: NSLayoutConstraint! // 20
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        
        addObservers()
        prepareForAnimation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        checkOrientation()
        layoutButtons()
    }
    
    // MARK: - Initial functions
    func setupUI(){
        self.backgroundView.backgroundColor = ThemeManager.isNightMode() ? Appearance.backgroundViewNight : UIColor.white
    }
    
    func addObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(viewRotated), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
    }
    
    func prepareForAnimation(){
        backgroundView.alpha = 0
        let height = view.frame.height
        overviewVerticalConstraint.constant = height
//        overviewHorizontalConstraint.constant = -height
        completeVerticalConstraint.constant = height
//        completeHorizontalConstraint.constant = -height
        cancelVerticalConstraint.constant = height
//        cancelHorizontalConstraint.constant = -height
    }
    
    // MARK: - IBActions
    @IBAction func overviewButtonPressed(_ sender: UIButton) {
        hideSelf {
            self.delegate?.overviewButtonPressed()
        }
    }
    
    @IBAction func completeButtonPressed(_ sender: UIButton) {
        hideSelf {
            self.delegate?.completeButtonPressed()
        }
    }
    
    @IBAction func cancelPickupButtonPressed(_ sender: UIButton) {
        hideSelf {
            self.delegate?.cancelButtonPressed()
        }
    }
    
    @IBAction func closeButtonPressed(_ sender: UIButton) {
        hideSelf {
            print(#function)
        }
    }
    
    // MARK: - Functions
    @objc func viewRotated(){
        checkOrientation()
        layoutButtons()
    }
    
    func layoutButtons(){
        if isLandscape {
            overviewVerticalConstraint.constant = 0
            overviewHorizontalConstraint.constant = -180
            UIView.animate(withDuration: 0.2) {
                self.backgroundView.alpha = 0.5
                self.view.layoutIfNeeded()
            }
            completeVerticalConstraint.constant = 0
            completeHorizontalConstraint.constant = 0
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            cancelHorizontalConstraint.constant = 180
            cancelVerticalConstraint.constant = 0
            UIView.animate(withDuration: 0.4) {
                self.view.layoutIfNeeded()
            }
            closeHorizontalConstraint.constant = 0
            closeBottomConstraint.constant = 20
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
        } else {
            overviewVerticalConstraint.constant = -180
            overviewHorizontalConstraint.constant = 0
            UIView.animate(withDuration: 0.2) {
                self.backgroundView.alpha = 0.5
                self.view.layoutIfNeeded()
            }
            completeVerticalConstraint.constant = 0
            completeHorizontalConstraint.constant = 0
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            cancelHorizontalConstraint.constant = 0
            cancelVerticalConstraint.constant = 180
            UIView.animate(withDuration: 0.4) {
                self.view.layoutIfNeeded()
            }
            
            closeHorizontalConstraint.constant = 0
            closeBottomConstraint.constant = 20
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func hideSelf(onCompletion: @escaping ()->Void){
        let height = view.frame.height
        overviewVerticalConstraint.constant = height
        UIView.animate(withDuration: 0.5, animations: {
            self.view.layoutIfNeeded()
        }) { (_) in
            self.dismiss(animated: false, completion: {
                onCompletion()
            })
        }
        completeVerticalConstraint.constant = height
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }
        cancelVerticalConstraint.constant = height
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        closeBottomConstraint.constant = -height
        UIView.animate(withDuration: 0.2) {
            self.backgroundView.alpha = 0
            self.view.layoutIfNeeded()
        }
    }
    
    func checkOrientation(){
        switch UIDevice.current.orientation {
        case .faceDown, .faceUp:
            if previousOrientiation == .landscapeLeft || previousOrientiation == .landscapeRight {
                isLandscape = true
            } else {
                isLandscape = false
            }
        case .portrait, .portraitUpsideDown:
            isLandscape = false
            previousOrientiation = UIDevice.current.orientation
        case .landscapeLeft, .landscapeRight:
            isLandscape = true
            previousOrientiation = UIDevice.current.orientation
        case .unknown:
            // Do nothing
            break
        }
    }
}

extension RoadDetailsViewController {
    class func create()->RoadDetailsViewController{
        let controller = UIStoryboard.home.instantiate(RoadDetailsViewController.self)
        return controller
    }
}
