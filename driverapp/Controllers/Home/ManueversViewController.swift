//
//  ManueversViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 10/6/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import NMAKit

class ManueversViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var doneButtonContainerView: UIView!
    
    // MARK: - Properties
    var manuevers:[NMAManeuver]!
    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupTableView()
        addObservers()
    }
    
    // MARK: - IBActions
    @IBAction func doneButtonPressed(_ sender: UIButton) {
       self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Initial functions
    func setupUI(){
        if ThemeManager.isNightMode() {
            mainView.backgroundColor = Appearance.backgroundViewNight
            footerView.backgroundColor = Appearance.backgroundViewNight
        }
        
    }
    func setupTableView(){
        tableView.register(ManueverTableViewCell.self)
    }
    
    func addObservers(){
        
    }
    
    // MARK: - Functions
    
}

// MARK: - Table view extension
extension ManueversViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return manuevers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(ManueverTableViewCell.self, for: indexPath)
        cell.manuever = manuevers[indexPath.row]
        cell.isOddCell = indexPath.row % 2 == 0
        
        cell.selectionStyle = .none
        return cell
    }
}

// MARK: - Create extension
extension ManueversViewController {
    static func create() -> ManueversViewController{
        return UIStoryboard.home.instantiate(ManueversViewController.self)
    }
}
