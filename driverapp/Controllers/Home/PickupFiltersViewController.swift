//
//  PickupFiltersViewController.swift
//  Superintendent
//
//  Created by Arben Pnishi on 31/01/2018.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import FSCalendar
import SCLAlertView

protocol PickupFiltersDelegate:class {
    func filteredPickups(pickups:[PickupRequest], startDate:Date, endDate:Date, createdByFilters:[Int], pickupStatusFilters:[PickupStatusType])
}

class PickupFiltersViewController: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var startDateTimeContainerView: UIView!
    @IBOutlet weak var startDateTimeHeaderView: UIView!
    @IBOutlet weak var startDateTimeScrollView: UIScrollView!
    @IBOutlet weak var startDateTimeCalendarView: FSCalendar!
    
    @IBOutlet weak var endDateTimeContainerView: UIView!
    @IBOutlet weak var endDateTimeHeaderView: UIView!
    @IBOutlet weak var startDateLabel: UILabel!
    @IBOutlet weak var innerStartDateLabel: UILabel!
    @IBOutlet weak var selectStartDateAndTimeLabel: UILabel!
    
    @IBOutlet weak var endDateTimeScrollView: UIScrollView!
    @IBOutlet weak var endDateTimeCalendarView: FSCalendar!
    
    @IBOutlet weak var endDateLabel: UILabel!
    @IBOutlet weak var innerEndDateLabel: UILabel!
    @IBOutlet weak var selectEndDateAndTimeLabel: UILabel!
    
    @IBOutlet weak var dispatcherButton: UIButton!
    @IBOutlet weak var meButton: UIButton!
    @IBOutlet weak var postponedButton: UIButton!
    @IBOutlet weak var canceledButton: UIButton!
    @IBOutlet weak var overdueButton: UIButton!
    
    @IBOutlet weak var doneButton: UIButton!
    
    // MARK: - Constraints
    @IBOutlet weak var startDateTimeViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var startDateCalendarHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var startDateTimeScrollViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var endDateTimeViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var endDateCalendarHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var endDateTimeScrollViewHeightConstraint: NSLayoutConstraint!
    
    //MARK: - Properties
    var delegate:PickupFiltersDelegate?
    var hours = Array(0...23)
    var minutes = Array(0...59)
    let oneHour:TimeInterval = 1*60*60
    let twentyFourHours:TimeInterval = 24*60*60
   
    var configuredStartDate = Date.init()
    var configuredEndDate = Date.init().addingTimeInterval(24*60*60)
    fileprivate lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "h:mm a"
        return dateFormatter
    }()
    fileprivate var isStartDateTimeOpen = true
    fileprivate var isEndDateTimeOpen = true
    fileprivate var didLayoutSubviews = false
    
    fileprivate let gregorian: Calendar = Calendar(identifier: .gregorian)
    fileprivate var startTimeLabelInitialPosition = CGPoint.zero
    fileprivate var startDateLabelInitialPosition = CGPoint.zero
    fileprivate var endTimeLabelInitialPosition = CGPoint.zero
    fileprivate var endDateLabelInitialPosition = CGPoint.zero
    
    var pickupRequests:[PickupRequest]!
    var filteredPickupRequests:[PickupRequest] = []
    var createdByFilters:[Int] = []
    var pickupStatusFilters:[PickupStatusType] = []
    var isFirstInit = true
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLocalizables()
        addObservers()
        setValues()
        getPickups()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getInitialPositions()
        forceHideEndDateTimeSection()
        self.view.layoutIfNeeded()
        checkTheme()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if !didLayoutSubviews{
            didLayoutSubviews = true
            setupCalendarView()
            getInitialPositions()
        }
    }
    
    
    // MARK: - IBActions
    
    @IBAction func startDateTimeTap(_ sender: UITapGestureRecognizer) {
        UIView.animate(withDuration: 0.3) {
            self.innerStartDateLabel.backgroundColor = UIColor.clear
        }
    }
    
    @IBAction func endDateTimeTap(_ sender: UITapGestureRecognizer) {
        UIView.animate(withDuration: 0.3) {
            self.innerEndDateLabel.backgroundColor = UIColor.clear
        }
    }
    
    
    @IBAction func doneButtonPressed(_ sender: UIButton) {
        delegate?.filteredPickups(pickups: filteredPickupRequests,
                                  startDate: configuredStartDate,
                                  endDate: configuredEndDate,
                                  createdByFilters: createdByFilters,
                                  pickupStatusFilters: pickupStatusFilters)
        pop()
    }
    
    @IBAction func startDateTimeViewTap(_ sender: UITapGestureRecognizer) {
        if isStartDateTimeOpen {
            hideStartDateTimeSection { (_) in }
        } else {
            hideEndDateTimeSection { (completed) in
                if completed {
                    self.showStartDateTimeSection()
                }
            }
        }
    }
    
    @IBAction func endDateTimeViewTap(_ sender: UITapGestureRecognizer) {
        if isEndDateTimeOpen {
            hideEndDateTimeSection { (_) in}
        } else {
            hideStartDateTimeSection { (completed) in
                if completed {
                    self.showEndDateTimeSection()
                }
            }
        }
    }
    
    @IBAction func createdByButtonsPressed(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        sender.isHighlighted = false
        sender.layer.borderWidth = sender.isSelected ? 2 : 0
        switch sender.tag {
        case 2:
            // For sender with tag 2, we mean type PM and Dispatcher
            if sender.isSelected {
                if !createdByFilters.contains(1){
                    createdByFilters.append(sender.tag)
                }
                if !createdByFilters.contains(2){
                    createdByFilters.append(sender.tag)
                }
            } else {
                if createdByFilters.contains(1){
                    if let index = createdByFilters.firstIndex(of: 1) {
                        createdByFilters.remove(at: index)
                    }
                }
                if createdByFilters.contains(2){
                    if let index = createdByFilters.firstIndex(of: 2) {
                        createdByFilters.remove(at: index)
                    }
                }
            }
        case 3:
            if sender.isSelected {
                if !createdByFilters.contains(sender.tag){
                    createdByFilters.append(sender.tag)
                }
            } else {
                if createdByFilters.contains(sender.tag){
                    if let index = createdByFilters.firstIndex(of: sender.tag) {
                        createdByFilters.remove(at: index)
                    }
                }
            }
        default:
            break
        }
        if !isFirstInit{
            filterPickups()
        }
        
    }
    
    @IBAction func pickupStatusButtonPressed(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        sender.isHighlighted = false
        sender.layer.borderWidth = sender.isSelected ? 2 : 0
        switch sender.tag {
        case 1:
            if pickupStatusFilters.contains(.postponed) {
                if let index = pickupStatusFilters.firstIndex(of: .postponed){
                    pickupStatusFilters.remove(at: index)
                }
            } else {
                pickupStatusFilters.append(.postponed)
            }
        case 2:
            if pickupStatusFilters.contains(.overdue) {
                if let index = pickupStatusFilters.firstIndex(of: .overdue){
                    pickupStatusFilters.remove(at: index)
                }
            } else {
                pickupStatusFilters.append(.overdue)
            }
        case 3:
            if pickupStatusFilters.contains(.canceled) {
                if let index = pickupStatusFilters.firstIndex(of: .canceled){
                    pickupStatusFilters.remove(at: index)
                }
            } else {
                pickupStatusFilters.append(.canceled)
            }
        default:
            break
        }
        if !isFirstInit{
            filterPickups()
        }
    }
    
    
    // MARK: - Functions
    func getPickups(){
        PickupREST.getPickupRequests(.all) { (success, pickups, error) in
            if success {
                self.pickupRequests = pickups
            }
        }
    }
    
    func setValues(){
        startDateTimeCalendarView.setCurrentPage(configuredStartDate, animated: false)
        endDateTimeCalendarView.setCurrentPage(configuredEndDate, animated: false)
        for filter in createdByFilters {
            if filter == 2 {
                dispatcherButton.isSelected = true
                dispatcherButton.isHighlighted = false
                dispatcherButton.layer.borderWidth = dispatcherButton.isSelected ? 2 : 0
            } else if filter == 3 {
                meButton.isSelected = true
                meButton.isHighlighted = false
                meButton.layer.borderWidth = dispatcherButton.isSelected ? 2 : 0
            }
        }
        for filter in pickupStatusFilters {
            switch filter {
            case .postponed:
                postponedButton.isSelected = true
                postponedButton.isHighlighted = false
                postponedButton.layer.borderWidth = dispatcherButton.isSelected ? 2 : 0
            case .canceled:
                canceledButton.isSelected = true
                canceledButton.isHighlighted = false
                canceledButton.layer.borderWidth = dispatcherButton.isSelected ? 2 : 0
            case .overdue:
                overdueButton.isSelected = true
                overdueButton.isHighlighted = false
                overdueButton.layer.borderWidth = dispatcherButton.isSelected ? 2 : 0
            default:
                break
            }
        }
        isFirstInit = false
    }
    
    func filterPickups(){
        var filteredPickups:[PickupRequest] = []
        for pickup in pickupRequests {
            if let startTime = pickup.startPickupTime, let endtime = pickup.endPickupTime {
                if startTime >= configuredStartDate && endtime <= configuredEndDate {
                    filteredPickups.append(pickup)
                }
            }
        }
        
            for pickup in filteredPickups {
                if !createdByFilters.contains(pickup.requestedBy?.rawValue ?? 0){
                    if let index = filteredPickups.firstIndex(of: pickup) {
                        filteredPickups.remove(at: index)
                    }
                }
            }
        
            for pickup in filteredPickups {
                if !pickupStatusFilters.contains(pickup.pickupStatus){
                    if let index = filteredPickups.firstIndex(of: pickup) {
                        filteredPickups.remove(at: index)
                    }
                }
            }
        self.filteredPickupRequests = filteredPickups
    }
    func addObservers(){
        registerNotification(notification: Notification.Name.changeTheme, selector: #selector(checkTheme))
    }
    
    @objc func checkTheme(){
        if ThemeManager.isNightMode() {
            backgroundView.backgroundColor = Appearance.backgroundViewNight
            mainView.backgroundColor = Appearance.darkGrayNight
            
            startDateTimeContainerView.backgroundColor = Appearance.backgroundViewNight
            startDateTimeHeaderView.backgroundColor = Appearance.darkGrayNight
            
            startDateLabel.textColor = UIColor.white
            startDateLabel.backgroundColor = Appearance.darkGrayNight
            innerStartDateLabel.textColor = UIColor.white
            innerStartDateLabel.backgroundColor = Appearance.darkGrayNight
            
            startDateTimeCalendarView.appearance.titleDefaultColor = UIColor.white
            startDateTimeCalendarView.appearance.todayColor = UIColor.white
            
            endDateTimeContainerView.backgroundColor = Appearance.backgroundViewNight
            endDateTimeHeaderView.backgroundColor = Appearance.darkGrayNight
            endDateTimeScrollView.backgroundColor = Appearance.darkGrayNight
            
            endDateLabel.textColor = UIColor.white
            endDateLabel.backgroundColor = Appearance.darkGrayNight
            innerEndDateLabel.textColor = UIColor.white
            innerEndDateLabel.backgroundColor = Appearance.darkGrayNight
            
            endDateTimeCalendarView.appearance.titleDefaultColor = UIColor.white
            endDateTimeCalendarView.appearance.todayColor = UIColor.white
            
            dispatcherButton.backgroundColor = Appearance.backgroundViewNight
            dispatcherButton.setTitleColor(.white, for: .normal)
            meButton.backgroundColor = Appearance.backgroundViewNight
            meButton.setTitleColor(.white, for: .normal)
            postponedButton.backgroundColor = Appearance.backgroundViewNight
            postponedButton.setTitleColor(.white, for: .normal)
            canceledButton.backgroundColor = Appearance.backgroundViewNight
            canceledButton.setTitleColor(.white, for: .normal)
            overdueButton.backgroundColor = Appearance.backgroundViewNight
            overdueButton.setTitleColor(.white, for: .normal)
        }
    }
    
    func getInitialPositions(){
        startDateLabelInitialPosition.x = startDateLabel.center.x
        startDateLabelInitialPosition.y = startDateLabel.center.y
        endDateLabelInitialPosition.x = endDateLabel.center.x
        endDateLabelInitialPosition.y = endDateLabel.center.y
    }
    
    func forceHideEndDateTimeSection(){
        self.selectEndDateAndTimeLabel.alpha = 0
        self.innerEndDateLabel.center = self.endDateLabelInitialPosition
        self.endDateLabel.center = self.endDateLabelInitialPosition
        self.endDateTimeCalendarView.alpha = 0
        self.innerEndDateLabel.alpha = 0
        self.endDateLabel.alpha = 1
        self.endDateTimeViewHeightConstraint.constant = 60
        self.endDateTimeScrollViewHeightConstraint.constant = 0
        isEndDateTimeOpen = false
    }
    
    func setupCalendarView(){
        startDateTimeCalendarView.register(FSCalendarCell.self, forCellReuseIdentifier: "cell")
        endDateTimeCalendarView.register(FSCalendarCell.self, forCellReuseIdentifier: "cell")
        
        startDateTimeCalendarView.delegate = self
        startDateTimeCalendarView.dataSource = self
        startDateTimeCalendarView.scope = .month
//        startDateTimeCalendarView.locale = Locale.current
        
        
        endDateTimeCalendarView.delegate = self
        endDateTimeCalendarView.dataSource = self
        endDateTimeCalendarView.scope = .month
//        endDateTimeCalendarView.locale = Locale.current
        
        startDateTimeCalendarView.setCurrentPage(configuredStartDate, animated: false)
        startDateTimeCalendarView.select(configuredStartDate)
        endDateTimeCalendarView.setCurrentPage(configuredEndDate, animated: false)
        endDateTimeCalendarView.select(configuredEndDate)
        updateTimeLabels()
    }
    

    
    func setupLocalizables(){
        //        pickupTimeStaticLabel.text = "pickup_time".localized
        //        returnTimeStaticLabel.text = "return_time".localized
        //        durationStaticLabel.text = "duration".localized
        //        recurringButton.setTitle("this_is_recurring_request".localized, for: .normal)
        //        everydayButton.setTitle("every_day".localized, for: .normal)
        //        everyweekButton.setTitle("every_week".localized, for: .normal)
        //        everymonthButton.setTitle("every_month".localized, for: .normal)
        //        warningLabel.text = "unless_you_change".localized()
        //        doneButton.setTitle("done".localized, for: .normal)
    }
    
    func setConfiguredDate(){
//        var calendar = Calendar.autoupdatingCurrent
//        calendar.timeZone = TimeZone(abbreviation: "UTC")!
//        var dateComponents = calendar.dateComponents([.year,.month,.day,.hour,.minute], from: configuredStartDate)
//        dateComponents.hour = selectedStartHour!
//        dateComponents.minute = selectedStartMinute
//        configuredStartDate = calendar.date(from: dateComponents)
//        pickupRequest.startPickupTime =  configuredStartDate ?? selectedDate
//
//        dateComponents = calendar.dateComponents([.year,.month,.day,.hour,.minute], from: pickupRequest.endPickupTime)
//        dateComponents.hour = selectedEndHour!
//        dateComponents.minute = selectedEndMinute
//        configuredEndDate = calendar.date(from: dateComponents)
//        pickupRequest.endPickupTime = configuredEndDate ?? selectedDate
//
//        if (pickupRequest.startPickupTime.timeIntervalSinceNow - pickupRequest.endPickupTime.timeIntervalSinceNow) > -3599.9999989271164 {
//            pickupRequest.endPickupTime = pickupRequest.startPickupTime.addingTimeInterval(oneHour)
//            selectedEndHour = hours[pickupRequest.endPickupTime.inGMTRegion().hour]
//            selectedEndMinute = minutes[pickupRequest.endPickupTime.inGMTRegion().minute]
//            setConfiguredDate()
//            updateTimeLabels()
//        }
    }
    
    func updateTimeLabels() {
        dateFormatter.dateFormat = "EEEE, MMM dd"
        startDateLabel.text = "  From: \(dateFormatter.string(from: configuredStartDate))  "
        innerStartDateLabel.text = "  \(dateFormatter.string(from: configuredStartDate))  "
        dateFormatter.dateFormat = "H:mm"
        
        dateFormatter.dateFormat = "EEEE, MMM dd"
        endDateLabel.text = "  To: \(dateFormatter.string(from: configuredEndDate))  "
        innerEndDateLabel.text = "  \(dateFormatter.string(from: configuredEndDate))  "
        
    }


    
    func reloadCalendarData(){
        dispatch {
            self.startDateTimeCalendarView.reloadData()
            self.endDateTimeCalendarView.reloadData()
        }
    }
    

    
    
    
    func hideStartDateTimeSection(completionHandler: @escaping (_ completed:Bool)->()){
        UIView.animateKeyframes(withDuration: 0.4, delay: 0, animations: {
            
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.1, animations: {
                self.selectStartDateAndTimeLabel.alpha = 0
            })
            
            UIView.addKeyframe(withRelativeStartTime: 0.1, relativeDuration: 0.1, animations: {
                self.innerStartDateLabel.center.y = self.startDateLabelInitialPosition.y
                self.startDateLabel.center.y = self.startDateLabelInitialPosition.y
            })
            
            UIView.addKeyframe(withRelativeStartTime: 0.2, relativeDuration: 0.1, animations: {
                self.startDateTimeCalendarView.alpha = 0
                self.innerStartDateLabel.alpha = 0
                self.startDateLabel.alpha = 1
            })
            
            self.startDateTimeViewHeightConstraint.constant = 60
            self.startDateTimeScrollViewHeightConstraint.constant = 0
            UIView.addKeyframe(withRelativeStartTime: 0.3, relativeDuration: 0.1, animations: {
                self.view.layoutIfNeeded()
            })
        }, completion:{ _ in
            completionHandler(true)
            print("StartDateTimeHidden")
        })
        isStartDateTimeOpen = false
    }
    
    func hideEndDateTimeSection(completionHandler: @escaping (_ completed:Bool)->()){
        UIView.animateKeyframes(withDuration: 0.4, delay: 0, animations: {
            
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.1, animations: {
                self.selectEndDateAndTimeLabel.alpha = 0
            })
            
            UIView.addKeyframe(withRelativeStartTime: 0.1, relativeDuration: 0.1, animations: {
                self.innerEndDateLabel.center.y = self.endDateLabelInitialPosition.y
                self.endDateLabel.center.y = self.endDateLabelInitialPosition.y
            })
            
            UIView.addKeyframe(withRelativeStartTime: 0.2, relativeDuration: 0.1, animations: {
                self.endDateTimeCalendarView.alpha = 0
                self.innerEndDateLabel.alpha = 0
                self.endDateLabel.alpha = 1
            })
            
            self.endDateTimeViewHeightConstraint.constant = 60
            self.endDateTimeScrollViewHeightConstraint.constant = 0
            UIView.addKeyframe(withRelativeStartTime: 0.3, relativeDuration: 0.1, animations: {
                self.view.layoutIfNeeded()
            })
        }, completion:{ _ in
            completionHandler(true)
            print("EndDateTimeHidden")
        })
        isEndDateTimeOpen = false
    }
    
    func showStartDateTimeSection(){
        UIView.animateKeyframes(withDuration: 0.4, delay: 0, animations: {
            
            self.startDateTimeViewHeightConstraint.constant = 390
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.1, animations: {
                self.view.layoutIfNeeded()
            })
            
            self.startDateTimeScrollViewHeightConstraint.constant = 250
            self.startDateCalendarHeightConstraint.constant = 250
            UIView.addKeyframe(withRelativeStartTime: 0.1, relativeDuration: 0.1, animations: {
                self.startDateLabel.center.y = self.innerStartDateLabel.center.y
                self.view.layoutIfNeeded()
            })
            
            UIView.addKeyframe(withRelativeStartTime: 0.2, relativeDuration: 0.1, animations: {
                self.startDateLabel.alpha = 0
                self.innerStartDateLabel.alpha = 1
                self.startDateTimeCalendarView.alpha = 1
            })
            
            UIView.addKeyframe(withRelativeStartTime: 0.3, relativeDuration: 0.1, animations: {
                self.selectStartDateAndTimeLabel.alpha = 1
            })
            
        }, completion:{ _ in
            print("StartDateTimeShown")
        })
        isStartDateTimeOpen = true
    }
    
    func showEndDateTimeSection(){
        UIView.animateKeyframes(withDuration: 0.4, delay: 0, animations: {
            
            self.endDateTimeViewHeightConstraint.constant = 390
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.1, animations: {
                self.view.layoutIfNeeded()
            })
            
            
            self.endDateTimeScrollViewHeightConstraint.constant = 250
            self.endDateCalendarHeightConstraint.constant = 250
            UIView.addKeyframe(withRelativeStartTime: 0.1, relativeDuration: 0.1, animations: {
                self.endDateLabel.center.y = self.innerEndDateLabel.center.y
                self.view.layoutIfNeeded()
            })
            
            UIView.addKeyframe(withRelativeStartTime: 0.2, relativeDuration: 0.1, animations: {
                self.endDateTimeCalendarView.alpha = 1
                self.innerEndDateLabel.alpha = 1
                self.endDateLabel.alpha = 0
            })
            
            UIView.addKeyframe(withRelativeStartTime: 0.3, relativeDuration: 0.1, animations: {
                self.selectEndDateAndTimeLabel.alpha = 1
            })
            
        }, completion:{ _ in
            print("EndDateTimeShown")
        })
        isEndDateTimeOpen = true
    }

    
    func showWrongTimeAlert(){
        let alert = UIAlertController(title: "Wrong date", message: "The selected date/time is not correct. Please review", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_) in
            self.doneButton.isEnabled = true
        }))
        self.showModal(alert)
        print("The selected date/time is not correct. Please review")
    }
}


extension PickupFiltersViewController: FSCalendarDelegate, FSCalendarDataSource, FSCalendarDelegateAppearance {
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
     
        if calendar == startDateTimeCalendarView {
            configuredStartDate = date.addingTimeInterval(twentyFourHours)
        } else {
            configuredEndDate = date.addingTimeInterval(twentyFourHours)
        }
        
        reloadCalendarData()
        updateTimeLabels()
        filterPickups()
    }
    
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        return 0
    }
    
//    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, eventColorFor date: Date) -> UIColor? {
//        return pickupRequest.recurringFrequency == 1 ? Appearance.greenColor : nil
//    }
    
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        if calendar == self.startDateTimeCalendarView {
            self.startDateCalendarHeightConstraint.constant = bounds.height
            
        } else {
            self.endDateCalendarHeightConstraint.constant = bounds.height
        }
        dispatch {
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            })
        }
    }
    
    func calendar(_ calendar: FSCalendar, cellFor date: Date, at position: FSCalendarMonthPosition) -> FSCalendarCell {
        let cell = calendar.dequeueReusableCell(withIdentifier: "cell", for: date, at: position)
        let isStartDateTimeCalendarView = calendar == startDateTimeCalendarView
        
//        if isStartDateTimeCalendarView {
//            cell.alpha = date.isNotInThePastFrom(date: configuredStartDate) ? 1.0 : 0.5
//        } else {
//            cell.alpha = date.isNotInThePastFrom(date: configuredEndDate) ? 1.0 : 0.5
//        }
        return cell
    }
    
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
//        if calendar == startDateTimeCalendarView {
//            return date.isNotInThePastFrom(date: startDate)
//        } else {
//            return date.isNotInThePastFrom(date: endDate)
//        }
        return true
    }
    
    func calendar(_ calendar: FSCalendar, shouldDeselect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        return false
    }
}

//MARK: - Extensions
extension PickupFiltersViewController{
    static func create() -> PickupFiltersViewController{
        return UIStoryboard.home.instantiate(PickupFiltersViewController.self)
    }
}
