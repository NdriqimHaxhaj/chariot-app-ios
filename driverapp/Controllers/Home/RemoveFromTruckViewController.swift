//
//  RemoveFromTruckViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 9/6/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

class RemoveFromTruckViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    // MARK: - Constraints
    @IBOutlet weak var logoYConstraint: NSLayoutConstraint!
    @IBOutlet weak var descriptionYLabel: NSLayoutConstraint!
    
    // MARK: - Properties
    var tempText:String!
    
    // MARK: - Lifecycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        prepareForAnimation()
        checkTheme()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setValues()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        animateView()
        autoHideView()
    }

    // MARK: - Functions
    func setValues(){
        descriptionLabel.text = tempText
    }
    func checkTheme(){
        if ThemeManager.isNightMode(){
            mainView.backgroundColor = Appearance.backgroundViewNight
            descriptionLabel.textColor = UIColor.white
        }
    }
    
    func prepareForAnimation(){
        logoImageView.alpha = 0
        descriptionLabel.alpha = 0
        logoYConstraint.constant = -100
        descriptionYLabel.constant = 180
    }
    
    
    func animateView(){
        UIView.animateKeyframes(withDuration: 0.8, delay: 0, options: [], animations: {
            self.logoYConstraint.constant = -40
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.9, animations: {
                self.logoImageView.alpha = 1
                self.view.layoutIfNeeded()
            })
            self.descriptionYLabel.constant = 90
            UIView.addKeyframe(withRelativeStartTime: 0.3, relativeDuration: 0.5, animations: {
                self.descriptionLabel.alpha = 1
                self.view.layoutIfNeeded()
            })
        }, completion: nil)
    }
    
    func autoHideView(){
        delay(delay: 2) {
            self.hideView()
        }
    }
    
    func hideView(){
        UIView.animate(withDuration: 0.5, animations: {
            self.logoImageView.alpha = 0
            self.descriptionLabel.alpha = 0
            self.view.alpha = 0
            self.view.layoutIfNeeded()
        }) { (_) in
            self.dismiss(animated: false, completion: nil)
        }
    }
}

//MARK: View controller extensions
extension RemoveFromTruckViewController{
    static func create() -> RemoveFromTruckViewController{
        return UIStoryboard.home.instantiate(RemoveFromTruckViewController.self)
    }
}
