//
//  MenuController.swift
//  driverapp
//
//  Created by Arben Pnishi on 20/02/2018.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import SideMenu
import Kingfisher

// MARK: - Enums
enum MenuType {
    case Home
    case Settings
    case SignOut
}

class MenuController: UIViewController {
    //MARK: - OUTLETS
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var menuWidthConstraing: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var workingSwitchView: UIView!
    @IBOutlet weak var templateSwitchView: UIView!
    @IBOutlet weak var profilePicture: UIImageView!
    @IBOutlet weak var fullnameLabel: UILabel!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var scheduleButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var currentVehicleNameLabel: UILabel!
    
    //MARK: - VARIABLES
    var selectedIndex = 0
    var items = ["Home", "Settings", "Sign Out"]
    var templateSwitch = CustomSwitch()
    var workingSwitch = CustomSwitch()
    
    //MARK: - LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        setLeftMenuWidth()
        initTableView()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let isDayTheme = NavigationManager.mapTheme == 0
        templateSwitch.setOn(on: isDayTheme, animated: false)
        workingSwitch.setOn(on: Preferences.isWorkingStatusOn, animated: false)
        setValues()
        checkTheme()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    // MARK: - IBActions
    @IBAction func profileTapped(_ sender: UITapGestureRecognizer) {
        closeMenu()
        postNotification(notification: Notification.Name.showProfile, object: nil)
    }
    
    
    @IBAction func editProfilePressed(_ sender: UIButton) {
        closeMenu()
        postNotification(notification: Notification.Name.showProfile, object: nil)
    }
    
    
    //MARK: - Functions
    @objc func checkTheme(){
        let isNightMode = ThemeManager.isNightMode()
        backgroundView.backgroundColor = isNightMode ? Appearance.darkGrayNight : UIColor.white
        fullnameLabel.textColor = isNightMode ? UIColor.white : UIColor(red255: 71, green255: 73, blue255: 85)
        if isNightMode {
            workingSwitch.offTintColor = UIColor(red255: 93, green255: 93, blue255: 100)
            templateSwitch.offTintColor = UIColor(red255: 93, green255: 93, blue255: 100)
        }
        
    }
    
    func setupUI(){
        registerNotification(notification: Notification.Name.changeTheme, selector: #selector(self.checkTheme))
        addTemplateSwitch()
        addWorkingSwitch()
    }
    
    func setValues(){
        if let profile = AccountManager.currentUser{
            indicator.startAnimating()
            profilePicture.kf.setImage(with: URL(string: profile.photo), placeholder: #imageLiteral(resourceName: "User Avatar"), options: nil, progressBlock: nil, completionHandler: { (image, error, cache, url) in
                self.indicator.stopAnimating()
                self.indicator.alpha = 0
            })
            fullnameLabel.text = "\(profile.name) \(profile.lastname)"
            currentVehicleNameLabel.text = NavigationManager.vehicleName
        }
    }
    
    func setLeftMenuWidth() {
        menuWidthConstraing.constant = Constants.leftMenuWidth
        view.layoutIfNeeded()
    }
    
    func initTableView(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(MenuCell.self)
    }
    
    func closeMenu(){
        SideMenuManager.defaultManager.menuLeftNavigationController?.hideModal()
    }
    
    func addTemplateSwitch(){
        templateSwitch = CustomSwitch(frame: templateSwitchView.bounds)
        templateSwitch.onTintColor = UIColor(red: 124/255, green: 213/255, blue: 130/255, alpha: 1)
        templateSwitch.offTintColor = Appearance.lightGray
        templateSwitch.cornerRadius = 0.5
        templateSwitch.thumbCornerRadius = 0.5
        templateSwitch.thumbSize = CGSize(width: 35, height: 35)
        templateSwitch.thumbTintColor = UIColor(red: 62/255, green: 193/255, blue: 79/255, alpha: 1)
        templateSwitch.padding = 0
        templateSwitch.animationDuration = 0.25
        templateSwitch.onImage = #imageLiteral(resourceName: "sun")
        templateSwitch.offImage =  #imageLiteral(resourceName: "moon")
        templateSwitch.addTarget(self, action: #selector(self.templateSwitchPressed(_:)), for: .touchUpInside)
        self.templateSwitchView.addSubview(templateSwitch)
    }
    
    func addWorkingSwitch(){
        workingSwitch = CustomSwitch(frame: workingSwitchView.bounds)
        workingSwitch.onTintColor = UIColor(red: 124/255, green: 213/255, blue: 130/255, alpha: 1)
        workingSwitch.offTintColor = Appearance.lightGray
        workingSwitch.cornerRadius = 0.5
        workingSwitch.thumbCornerRadius = 0.5
        workingSwitch.thumbSize = CGSize(width: 35, height: 35)
        workingSwitch.thumbTintColor = UIColor(red: 62/255, green: 193/255, blue: 79/255, alpha: 1)
        workingSwitch.padding = 0
        workingSwitch.animationDuration = 0.25
        workingSwitch.onImage = #imageLiteral(resourceName: "on switch")
        workingSwitch.offImage =  #imageLiteral(resourceName: "off switch")
        workingSwitch.addTarget(self, action: #selector(self.workingStatusSwitchPressed(_:)), for: .touchUpInside)
        self.workingSwitchView.addSubview(workingSwitch)
    }
    
    //MARK: - IBACTIONS
    @IBAction func schedulePickupPressed(_ sender: UIButton) {
        postNotification(notification: Notification.Name.schedulePickupRequest, object: nil)
        closeMenu()
    }
    @IBAction func closeButtonPressed(_ sender: UIButton) {
        closeMenu()
    }
    
    @objc func templateSwitchPressed(_ sender: CustomSwitch) {
        // If mode isn't set to auto
        if NavigationManager.mapView != 0 {
            NavigationManager.mapTheme = templateSwitch.isOn ? 0 : 1
            ThemeManager.themeMode = templateSwitch.isOn ? Mode.Day.rawValue : Mode.Night.rawValue
            NavigationManager.mapView = templateSwitch.isOn ? 1 : 2
            checkTheme()
            postNotification(notification: Notification.Name.changeMapTheme, object: nil)
            postNotification(notification: Notification.Name.changeTheme, object: nil)
        }
        closeMenu()
    }
    
    @objc func workingStatusSwitchPressed(_ sender: CustomSwitch) {
        let workingSwitchDict:[String: Bool] = ["isGoingToWork": self.workingSwitch.isOn]
        NotificationCenter.default.post(name: Notification.Name.workingSwitchPressed, object: nil, userInfo: workingSwitchDict)
        closeMenu()
    }
}

//MARK: - EXTENSIONS
extension MenuController {
    static func create() -> MenuController {
        return UIStoryboard.menu.instantiate(MenuController.self)
    }
}

extension MenuController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: MenuCell = tableView.dequeue(MenuCell.self, for: indexPath)
        cell.titleLabel.text = items[indexPath.row]
        cell.isCellSelected = selectedIndex == indexPath.row
        cell.updateState()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch indexPath.row {
        case 0:
            closeMenu()
            postNotification(notification: Notification.Name.showHome, object: nil)
            break
        case 1:
            closeMenu()
            postNotification(notification: Notification.Name.showSettings, object: nil)
            break
        case 2:
            closeMenu()
            postNotification(notification: Notification.Name.signOut, object: nil)
            break
        default:
            break
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}

extension MenuController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.scrollView {
            scrollView  .contentOffset.x = 0
        }
    }
}
