//
//  OfflineController.swift
//  driverapp
//
//  Created by Arben Pnishi on 23/02/2018.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

class OfflineController: UIViewController {

    //MARK: OUTLETS
    @IBOutlet weak var offLabel: UILabel!
    @IBOutlet weak var offlineLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var goOnlineButton: UIButton!
    
    //MARK: LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        introAnimation()
    }
    
    //MARK: FUNCTIONS
    fileprivate func introAnimation() {
        
    }
    
    fileprivate func outroAnimation(){
        hideModal()
    }
    fileprivate func goOnline(){
        showHud()
        DriverREST.changeAvailability(isOnline: 1, reason: 1) { (success, error) in
            self.hideHud()
            if success{
                NavigationManager.offlineReasonId = 0
                Preferences.isWorkingStatusOn = true
                self.outroAnimation()
            }else{
                Preferences.isWorkingStatusOn = false
            }
        }
    }
    //MARK: IBACTIONS
    @IBAction func goOnlinePressed(_ sender: UIButton) {
        goOnline()
    }
}

//MARK: EXTENSIONS
extension OfflineController{
    static func create() -> OfflineController{
        return UIStoryboard.home.instantiate(OfflineController.self)
    }
}

