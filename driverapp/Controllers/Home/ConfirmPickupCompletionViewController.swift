//
//  ConfirmPickupCompletionViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 4/4/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

enum ConfirmType {
    case Complete
    case Uncomplete
    case DropOff
    case Postpone
    
}

protocol ConfirmPickupActionDelegate:class {
    func refreshPickups()
}

class ConfirmPickupCompletionViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var confirmView: UIView!
    @IBOutlet weak var closeView: UIView!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var closeIcon: UIImageView!
    @IBOutlet weak var sliderView: SliderView!
    @IBOutlet weak var sliderViewContainer: UIView!
    @IBOutlet weak var swipeRightToConfirmLabel: UILabel!
    
    // MARK: - Properties
    var confirmType:ConfirmType = .Complete
    var pickupId:String!
    var pickupRequest:PickupRequest?
    var routeCoordinates:String!
    weak var delegate:ConfirmPickupActionDelegate?
    var isCompletingFromPickupProfile = true
    
    // MARK: - Constraints
    @IBOutlet weak var sliderViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var confirmViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var confirmViewWidthConstraint: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupView()
        sliderView.delegate = self
    }
    
    override func viewDidLayoutSubviews() {
//        setupView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        scrollToPage(1)
    }
    
    @IBAction func cancelConfirmation(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
    // MARK: - Functions
    
    func setupUI(){
        if ThemeManager.isNightMode() {
            sliderViewContainer.backgroundColor = Appearance.darkGrayNight
            sliderView.backgroundColor = Appearance.darkGrayNight
            closeButton.backgroundColor = Appearance.darkGrayNight
            closeIcon.image = closeIcon.image?.tint(with: UIColor.white)
            swipeRightToConfirmLabel.textColor = UIColor.white
        }
    }
    
    func setupView(){
        if confirmType == .Complete{
            //
        }
    }
    
    private func uncompletePickup(){
        let id = Int(NSString.init(string: pickupId!).intValue)
        PickupREST.uncompletePickup(id) { (success, error) in
            if success {
                self.showSuccess()
                delay(delay: 1, closure: {
                    self.delegate?.refreshPickups()
                    self.hideSelf()
                })
                print("Uncompletion successfully done")
            } else {
                print("Uncompletion went wrong")
            }
        }
    }
    
    private func completePickup(){
        let id = Int(NSString.init(string: pickupId!).intValue)
        PickupREST.completePickup("\(id)", routeCoordinates) { (success, error) in
            if success {
                self.showSuccess()
                delay(delay: 1, closure: {
                    self.delegate?.refreshPickups()
                    self.hideSelf()
                })
            } else {
                 print("ConfirmPickupCompletionViewController: Couldn't complete pickup with id: \(self.pickupId ?? "x")")
            }
        }
    }
    
    private func resumePickup(){
        guard let id = pickupId else {return}
        
        PickupREST.resumePickup(id) { (success, error) in
            if success {
                self.showSuccess()
                delay(delay: 1, closure: {
                    self.delegate?.refreshPickups()
                    self.hideSelf()
                })
                print("Pickup successfully resumed")
            } else {
                self.hideSelf()
                print("Resume went wrong")
            }
        }
    }
    
    private func completeDropOff(){
        self.delegate?.refreshPickups()
        self.hideSelf()
    }
    
    func showSuccess(){
        UIView.animate(withDuration: 0.1, animations: {
            self.closeView.alpha = 0
        }) { (_) in
            self.sliderViewHeightConstraint.constant = 0
            self.confirmViewHeightConstraint.constant = 120
            self.confirmViewWidthConstraint.constant = 120
            self.confirmView.cornerRadius = 60
            UIView.animate(withDuration: 0.2, animations: {
                self.confirmView.alpha = 1
                self.sliderView.alpha = 0
                self.sliderViewContainer.alpha = 0
                self.view.layoutIfNeeded()
            })
        }
    }
    
    func hideSelf(){
        self.confirmViewHeightConstraint.constant = 0
        self.confirmViewWidthConstraint.constant = 0
        UIView.animate(withDuration: 0.2, animations: {
            self.confirmView.alpha = 0
            self.backgroundView.alpha = 0
            self.view.layoutIfNeeded()
        }) { (_) in
            dispatch {
                self.dismiss(animated: false, completion: nil)
            }
        }
    }
    
    func showSelf(){
        //
    }
}

//MARK: EXTENSIONS
extension ConfirmPickupCompletionViewController{
    static func create() -> ConfirmPickupCompletionViewController{
        return UIStoryboard.home.instantiate(ConfirmPickupCompletionViewController.self)
    }
}

extension ConfirmPickupCompletionViewController: SliderDelegate {
    func progressCompleted() {
        if confirmType == .Uncomplete {
            sliderView.isUserInteractionEnabled = false
            uncompletePickup()
        } else if confirmType == .Complete {
            sliderView.isUserInteractionEnabled = false
//            completePickup()
            self.showSuccess()
            delay(delay: 1, closure: {
                if self.isCompletingFromPickupProfile {
                    self.delegate?.refreshPickups()
                    self.hideSelf()
                } else {
                    self.completePickup()
                }
            })
        } else if confirmType == .DropOff {
            sliderView.isUserInteractionEnabled = false
            completeDropOff()
        } else if confirmType == .Postpone{
            sliderView.isUserInteractionEnabled = false
            resumePickup()
        }
    }
}


