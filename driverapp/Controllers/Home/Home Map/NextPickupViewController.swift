//
//  NextPickupViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 5/15/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

protocol NextPickupDelegate:class {
    func startNextPickup(_ status:Bool)
}

class NextPickupViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var closeView: UIView!
    @IBOutlet weak var closeIcon: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var bagCountStaticLabel: UILabel!
    @IBOutlet weak var bagCountLabel: UILabel!
    @IBOutlet weak var preferredPickupTimeStaticLabel: UILabel!
    @IBOutlet weak var preferredPickupTimeLabel: UILabel!
    @IBOutlet weak var locationStaticLabel: UILabel!
    @IBOutlet weak var loadLocationLabel: UILabel!
    @IBOutlet weak var pickupTypeStaticLabel: UILabel!
    @IBOutlet weak var pickupTypeLabel: UILabel!
    
    // MARK: - Constraint
    @IBOutlet weak var yAxisPopupConstraint: NSLayoutConstraint!
    
    // MARK: - Properties
    var pickupRequest:PickupRequest!
    weak var nextPickupDelegate:NextPickupDelegate?
    
    //Helper Properties
    lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter
    }()
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setInfos()
        prepareForAnimation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        showSelf()
    }
    
    // MARK: - IBActions
    @IBAction func noButtonPressed(_ sender: UIButton) {
        hideSelf(delegatingPickup: false)
    }
    @IBAction func startPickupPressed(_ sender: UIButton) {
        hideSelf(delegatingPickup: true)
    }
    @IBAction func closeViewPressed(_ sender: UITapGestureRecognizer) {
        hideSelf(delegatingPickup: false)
    }
    
    // MARK: - Functions
    
    func setupUI(){
        if ThemeManager.isNightMode() {
            nameLabel.textColor = UIColor.white
            titleLabel.textColor = UIColor.white
            popupView.backgroundColor = Appearance.darkGrayNight
            closeView.backgroundColor = Appearance.darkGrayNight
            closeIcon.image = #imageLiteral(resourceName: "X")
            addressLabel.textColor = UIColor.white
            bagCountLabel.textColor = UIColor.white
            bagCountStaticLabel.textColor = UIColor.white
            preferredPickupTimeLabel.textColor = UIColor.white
            preferredPickupTimeStaticLabel.textColor = UIColor.white
            locationStaticLabel.textColor = UIColor.white
            loadLocationLabel.textColor = UIColor.white
            pickupTypeLabel.textColor = UIColor.white
            pickupTypeStaticLabel.textColor = UIColor.white
        }
    }
    
    func setInfos(){
        nameLabel.text = pickupRequest.buildingName
        addressLabel.text = pickupRequest.buildingAddress
        bagCountLabel.text = "\(pickupRequest.bagCount!)"
        self.dateFormatter.dateFormat = "hh:mm a"
        let startTime = dateFormatter.string(from: pickupRequest.startPickupTime!)
        let endTime = dateFormatter.string(from: pickupRequest.endPickupTime!)
        preferredPickupTimeLabel.sizeToFit()
        preferredPickupTimeLabel.text = "\(startTime)-\(endTime)"
        loadLocationLabel.text = pickupRequest.loadLocationName
        pickupTypeLabel.text = pickupRequest.loadTypesNames.joined(separator: ", ")
    }
    
    func prepareForAnimation(){
        yAxisPopupConstraint.constant = self.view.frame.height
        backgroundView.alpha = 0
        view.layoutIfNeeded()
    }
    
    func showSelf(){
        yAxisPopupConstraint.constant = 0
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 10, options: [.curveEaseIn], animations: {
            self.backgroundView.alpha = 0.5
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    func hideSelf(delegatingPickup:Bool){
        yAxisPopupConstraint.constant = self.view.frame.height
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0, options: [.curveEaseIn], animations: {
            self.backgroundView.alpha = 0
            self.view.layoutIfNeeded()
        }, completion: { _ in
            self.dismiss(animated: false, completion: {
                print("Dissmised StartNewPickupConfirmViewController")
                self.nextPickupDelegate?.startNextPickup(delegatingPickup)
            })
        })
    }
}

//MARK: Extensions
extension NextPickupViewController{
    static func create() -> NextPickupViewController{
        return UIStoryboard.home.instantiate(NextPickupViewController.self)
    }
}
