//
//  PickupsCollectionViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 10/18/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

private let reuseIdentifier = "SliderPickupCollectionViewCell"

class PickupsCollectionViewController: HorizontalPeekingPagesCollectionViewController {
    
    // MARK: - Properties
    var pickupRequests:[PickupRequest] = [] {
        didSet {
            collectionView?.reloadData()
        }
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        addObservers()
    }
    
    // MARK: - Initial functionas
    func addObservers(){
        registerNotification(notification: Notification.Name.transferPickups, selector: #selector(self.updatePickups(not:)))
        registerNotification(notification: Notification.Name.centerPickupsSlider, selector: #selector(self.centerSlider))
    }
    
    // MARK: - Functions
    @objc func updatePickups(not: Notification){
        // userInfo is the payload send by sender of notification
        if let userInfo = not.userInfo {
            // Safely unwrap the name sent out by the notification sender
            if let pickups = userInfo["pickups"] as? [PickupRequest] {
                self.pickupRequests = pickups
                self.synchronizePriorityWithBackend()
            }
        }
    }
    
    override func calculateSectionInset() -> CGFloat {
        return 40
    }
    
    func synchronizePriorityWithBackend(){
        var pickups:[String] = []
        var priority:[String] = []
        var index = 1
        for pickup in self.pickupRequests {
            guard let id = pickup.id else {continue}
            pickups.append("\(id)")
            priority.append("\(index)")
            index += 1
        }
        PickupREST.changePickupsPriority(pickups, priority) { (success, error) in
            if success {
                print("DEBUG: Priority synchronized for \(self.pickupRequests.count) pickups")
            }
        }
    }
}

// MARK: - CollectionView extension

extension PickupsCollectionViewController {
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pickupRequests.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! SliderPickupCollectionViewCell
        cell.pickupRequest = pickupRequests[indexPath.row]
        return cell
    }
}

// MARK: - HomeMapPickupDelegate extension
extension PickupsCollectionViewController: HomeMapPickupDelegate {
    func delegatePickup(_ pickup: PickupRequest) {
        // Does nothing here
    }
    
    func delegatePickups(_ pickups: [PickupRequest]) {
        self.pickupRequests = pickups
    }
    
    
}
