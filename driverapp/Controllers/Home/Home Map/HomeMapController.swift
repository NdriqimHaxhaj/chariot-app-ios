
//
//  HomeMapController.swift
//  driverapp
//
//  Created by Arben Pnishi on 20/02/2018.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import CoreLocation
import NMAKit
import UserNotifications
import Alamofire
import SwiftyJSON

protocol HomeMapControllerDelegate:class {
    func refreshPickupsFromMap()
}

protocol HomeMapPickupDelegate:class {
    func delegatePickup(_ pickup:PickupRequest)
    func delegatePickups(_ pickups:[PickupRequest])
}

protocol VoiceLanguageDelegate:class {
    func downloadProgress(_ progress:Float)
    func uncompressProgress(_ progress:Float)
    func installCompleted(_ state:Bool)
}

class HomeMapController: UIViewController{
    
    //MARK: - Outlets
    @IBOutlet weak var mapView: NMAMapView!
    @IBOutlet weak var navigationControlButton: UIButton!
    @IBOutlet weak var notificationButton: BadgeButton!
    @IBOutlet weak var notificationAlert: UIView!
    @IBOutlet weak var notificationAlertArrow: UIView!
    @IBOutlet weak var notificationAlertText: UILabel!
    @IBOutlet weak var soundsButton: UIButton!
    @IBOutlet weak var chatButton: BadgeButton!
    @IBOutlet var navigationBoardView: PassthroughView!
    @IBOutlet weak var locationButton: UIButton!
    @IBOutlet weak var seeMoreButton: BadgeButton!
    @IBOutlet weak var createPickupButton: BadgeButton!
    @IBOutlet weak var pickupsContainerView: UIView!
    @IBOutlet weak var leftPickupsSliderButton: UIButton!
    @IBOutlet weak var rightPickupsSliderButton: UIButton!
    @IBOutlet weak var speedoMeterContainerView: UIView!
    
    // MARK: - Constraints
    @IBOutlet weak var navigationBarHeight: NSLayoutConstraint!
    @IBOutlet weak var navigationBarBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var notificationViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var notificationsButtonTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var pickupSliderHeightConstraint: NSLayoutConstraint! // 150
    @IBOutlet weak var pickupSliderBottomConstraint: NSLayoutConstraint! // 30
    
    //MARK: - Properties
    var locationManager = CLLocationManager()
    var userLocation : CLLocation?
    var mapCenter: CLLocationCoordinate2D?
    var isLocationAllowed: Bool = false
    var geoCoord: NMAGeoCoordinates?
    var router:NMACoreRouter?
    var mapRoute: NMAMapRoute?
    var navigationManager: NMANavigationManager?
    var route: NMARoute?
    var geoBoundingBox: NMAGeoBoundingBox?
    var pickupRequests = [PickupRequest]()
    var MRFBuildings = [DropOffLocation]()
    var currentPickupRequest:PickupRequest?
    var navigationBoard:NavigationBoard?
    var isNavigationBoardShown = false
    var isNavigationBoardHeaderShown = false
    var isPickupsSliderShown = true
    var isNavigationActive = false
    var areSoundsEnabled = true
    var timer:Timer?
    let thirtySeconds:Double = 30
    var mapMarkers:[NMAMapMarker] = []
    var MRFMapMarkers:[NMAMapMarker] = []
    weak var homeMapDelegate:HomeMapControllerDelegate?
    weak var pickupDelegate:HomeMapPickupDelegate?
    var voiceCatalog = NMAVoiceCatalog.sharedInstance()
    weak var voiceLanguageDelegate:VoiceLanguageDelegate?
    var languageId:UInt?
    var isAnyPickupAvailable = false
    var isFirstPickupsInitialization = true
    var shouldFollowRoute = true
    lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter
    }()
    var pickupDateSections:[String] = []
    var pickupRequestsBySections: [String:[PickupRequest]] = [:]
    let dispatchGroup = DispatchGroup()
    var routes:[PickupRequest : NMARoute] = [:]
    var positionIndicator:NMAMapOverlay?
    var speedometerView: SpeedometerView?
    var isFreeZoomEnabled = false
    var routeCoordinates:String = "["
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        checkNavigationButtons()
        setupNavigationManager()
        setupLocationManager()
        setupMapView()
        // Navigation board header is initialized on parent view controller
        setupNavigationBoardFooter()
        initializeSpeedoMeter()
        setupPositioningManager()
        addObservers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkMapScheme()
        checkDispatcherMessages()
        showPickupsSlider()
        // This prevents position indicator animation stop
        // by recreating a new one
//        dispatch {
//            self.mapView.remove(mapOverlay: self.positionIndicator!)
//            if let positionView = self.positionIndicator?.subviews[0] as? MapPositionIndicator {
//                self.positionIndicator = NMAMapOverlay(MapPositionIndicator(frame: positionView.frame), (self.positionIndicator?.coordinates)!)
//                self.mapView.add(mapOverlay: self.positionIndicator!)
//            }
//        }
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        (positionIndicator?.subviews[0] as? MapPositionIndicator)?.stopAnimating()
//        positionIndicator?.refresh()
        if let homeController = findHomeController(), isNavigationActive  {
            homeController.hideNavigationBoardHeader()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let homeController = findHomeController(), isNavigationActive  {
            homeController.showNavigationBoardHeader()
        }
    }
    
    //MARK: - Initial functions
    @objc func setupUI(){
        checkMapScheme()
        checkDispatcherMessages()
        navigationBoard?.setupUI()
        checkButtonsUI()
//        navigationBoardView.backgroundColor = ThemeManager.isNightMode() ? Appearance.darkGrayNight : .white
        navigationBoardView.backgroundColor = UIColor.clear
        seeMoreButton.backgroundColor = ThemeManager.isNightMode() ? Appearance.darkGrayNight : .white
    }
    
    @objc func updateNavigationManagerTraffic(){
        let trafficModeAvoidance = NavigationManager.trafficEnabled == 0 ? NMATrafficAvoidanceMode.disabled : NMATrafficAvoidanceMode.dynamic
        navigationManager?.setTrafficAvoidanceMode(trafficModeAvoidance)
    }
    
    func checkNavigationButtons(){
        // If driver was dropping off on last application seasson, hide navigation buttons
        if NavigationManager.isGoingToDropOff &&
            NavigationManager.offlineReasonId != OfflineReasonType.leaveTruck.rawValue {
            hideNavigationButtons()
        } else {
            unHideNavigationButtons()
        }
    }
    
    func checkButtonsUI(){
        let isNightMode = ThemeManager.isNightMode()
        createPickupButton.backgroundColor = isNightMode ? Appearance.darkGrayNight : .white
        createPickupButton.borderColor = isNightMode ? Appearance.darkGrayNight : Appearance.defaultGrayBorder
        notificationButton.backgroundColor = isNightMode ? Appearance.darkGrayNight : .white
        notificationButton.borderColor = isNightMode ? Appearance.darkGrayNight : Appearance.defaultGrayBorder
        chatButton.backgroundColor = isNightMode ? Appearance.darkGrayNight : .white
        chatButton.borderColor = isNightMode ? Appearance.darkGrayNight : Appearance.defaultGrayBorder
        locationButton.backgroundColor = isNightMode ? Appearance.darkGrayNight : .white
        locationButton.borderColor = isNightMode ? Appearance.darkGrayNight : Appearance.defaultGrayBorder
        leftPickupsSliderButton.backgroundColor = isNightMode ? Appearance.darkGrayNight : .white
        rightPickupsSliderButton.backgroundColor = isNightMode ? Appearance.darkGrayNight : .white
    }
    
    fileprivate func goOnline(){
        DriverREST.changeAvailability(isOnline: 1, reason: 1) { (success, error) in
            if success{
                NavigationManager.offlineReasonId = 0
                Preferences.isWorkingStatusOn = true
            }else{
                Preferences.isWorkingStatusOn = false
            }
        }
    }
    
    func setupNavigationManager(){
        navigationManager = NMANavigationManager.sharedInstance()
        navigationManager?.mapTrackingTilt = NMAMapTrackingTilt.tilt3D
        
        navigationManager?.delegate = self
        navigationManager?.voicePackageMeasurementSystem = NMAMeasurementSystem.imperialUS
        NavigationManager.actualLanguageId = Int(bitPattern: (navigationManager?.voicePackage.packageId)!)
        setupVoicePackages()
    }
    
    func setupVoicePackages(){
        voiceCatalog?.delegate = self
    }
    
    func setupLocationManager() {
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.distanceFilter = 3
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startMonitoringSignificantLocationChanges()
        locationManager.delegate = self
        updateFromLocationPermission()
    }
    
    func setupMapView(){
        mapView.zoomLevel = 13.2
        mapView.copyrightLogoPosition = NMALayoutPosition.bottomCenter
        mapView.delegate = self
        mapView.gestureDelegate = self
        // Display the position indicator on map
        mapView.positionIndicator.isVisible = true
        mapView.isRenderAllowed = true
        
//        positionIndicator = NMAMapOverlay(MapPositionIndicator(frame: CGRect(x: 0, y: 0, width: 110, height: 110)))
//        positionIndicator?.clipsToBounds = false
//        mapView.twoFingerPanTiltingEnabled = false
//
//
//        mapView.add(mapOverlay: positionIndicator!)
    }
    
    func setupNavigationBoardFooter(){
        navigationBoard = UINib(nibName: "NavigationBoard", bundle: nil).instantiate(withOwner: self, options: nil).first as? NavigationBoard
        navigationBoard?.delegate = self
        
        navigationBoard?.frame = CGRect(x: UIScreen.main.nativeBounds.minX, y: 30, width: self.navigationBoardView.frame.width, height: navigationBoardView.frame.height)
        
        navigationBoardView.addSubview(navigationBoard!)
        self.seeMoreButton.transform = CGAffineTransform(rotationAngle: .pi)
        self.view.layoutIfNeeded()
    }
    
    func setupPositioningManager(){
        // Start positioning and register for position update notifications
        if NMAPositioningManager.sharedInstance().startPositioning() {
            // Register to positioning manager notifications
            NotificationCenter.default.addObserver(self, selector: #selector(self.positionDidUpdate), name: NSNotification.Name.NMAPositioningManagerDidUpdatePosition, object: NMAPositioningManager.sharedInstance())
            
            NotificationCenter.default.addObserver(self, selector: #selector(self.didLosePosition), name: NSNotification.Name.NMAPositioningManagerDidLosePosition, object: NMAPositioningManager.sharedInstance())
        }
    }
    
    func addObservers(){
        registerNotification(notification: Notification.Name.changeMapTheme, selector: #selector(self.checkMapScheme))
        registerNotification(notification: Notification.Name.changeTheme, selector: #selector(self.setupUI))
        registerNotification(notification: Notification.Name.showProfile, selector: #selector(self.showProfile))
        registerNotification(notification: Notification.Name.downloadLanguage, selector: #selector(self.downloadLanguage(not:)))
        registerNotification(notification: Notification.Name.navigateToDropOff, selector: #selector(self.navigateToDropOff(not:)))
        registerNotification(notification: Notification.Name.dropOffCompleted, selector: #selector(self.refreshPickups))
        registerNotification(notification: Notification.Name.removeRouteFromMap, selector: #selector(self.removeRouteFromMap))
        registerNotification(notification: Notification.Name.removeRouteFromMap, selector: #selector(self.updateNavigationManagerTraffic))
        registerNotification(notification: Notification.Name.stopNavigation, selector: #selector(self.stopNavigation))
        registerNotification(notification: Notification.Name.navigationBoardLocationPressed, selector: #selector(self.navigationButtonPressed(_:)))
        registerNotification(notification: Notification.Name.navigationBoardNotificationsButtonPressed, selector: #selector(self.notificationsButtonPressed(_:)))
        registerNotification(notification: Notification.Name.navigationBoardSoundsButtonPressed, selector: #selector(self.soundsButtonPressed(_:)))
    }
    
    func initializeSpeedoMeter(){
        speedometerView = UINib(nibName: "SpeedometerView", bundle: nil).instantiate(withOwner: self, options: nil).first as? SpeedometerView
        speedometerView?.frame = CGRect(x: 0, y: 40, width: 60, height: 60)
        speedometerView?.alpha = 0
        if AccountManager.showSpeedometer! {
            speedoMeterContainerView.addSubview(speedometerView!)
            speedoMeterContainerView.alpha = 1
        }
        
    }
    
    func getPickups(completion: @escaping (_ status:Bool)->Void) {
        let isFilterOn = findHomeController()?.listController?.toDoListViewController?.isFilterOn ?? false
        PickupREST.getPickupRequests(isFilterOn ? .all : .current) { (success, data, error) in
            if success, let pickupRequests = data {
                if NavigationManager.isGoingToDropOff {
                    self.checkDropOff(completion: { (success, error) in
                        if success {
                            self.startNavigation()
                            completion(true)
                        } else {
                            completion(false)
                        }
                    })
                } else {
                    if pickupRequests.filter({$0.pickupStatus != .completedWithDelays && $0.pickupStatus != .completedWithoutDelays}).isEmpty, !pickupRequests.isEmpty {
                        print("DEBUG: DROPPING OFF")
                        self.askUserToDropOff()
                    } else {
                        self.pickupRequests = pickupRequests
                        self.seperatePickupsRequestsIntoSections()
                        self.createPickupsMapMarkers()
                        self.createMRFMapMarkers()
                        self.checkOnGoingPickup()
                        self.showPickupsSlider()
                        if self.currentPickupRequest != nil && self.isFirstPickupsInitialization {
                            self.isFirstPickupsInitialization = false
                            self.startToNavigate()
                        }
                    }
                    completion(true)
                }
            } else {
                self.stopNavigation()
                completion(false)
                self.showMessage("No internet connection")
            }
        }
        // We get and draw MRF pins on the map
        getMRFBuildings(onCompletion: nil)
    }
    
    func seperatePickupsRequestsIntoSections(){
        pickupDateSections.removeAll()
        pickupRequestsBySections.removeAll()
        createSections()
        fillSections()
    }
    
    func createSections(){
        for pickup in pickupRequests {
            let date = pickup.startPickupTime
            dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
            let key = dateFormatter.string(from: date!)
            if !pickupDateSections.contains(key){
                pickupDateSections.append(key)
            }
        }
    }
    
    func fillSections(){
        for key in pickupDateSections {
            pickupRequestsBySections[key] = pickupRequests.filter({ (pickup) -> Bool in
                dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
                let formattedDate = dateFormatter.string(from: pickup.startPickupTime!)
                return key == formattedDate
            })
        }
    }
    
    func getMRFBuildings(onCompletion: ((_ status:Bool)->Void)?){
        UserREST.getDropOffLocations { (success, MRFBuildings, error) in
            if success {
                guard let buildings = MRFBuildings else {
                    onCompletion?(false)
                    return
                }
                self.MRFBuildings = buildings
                self.createMRFMapMarkers()
                onCompletion?(true)
            } else {
                onCompletion?(false)
            }
        }
    }
    
    func checkOnGoingPickup(){
        for pickup in pickupRequests {
            if pickup.pickupStatus == .ongoing {
                currentPickupRequest = pickup
                break
            }
        }
    }
    
    func checkDropOff(completion: @escaping (_ success:Bool,_ error:APError?)->Void){
        UserREST.getDropOffLocations(completionHandler: { (success, drops, error) in
            if success{
                if let lastDropOffId = NavigationManager.lastDropOffId {
                    guard let drops = drops else {return}
                    for drop in drops {
                        if drop.id == lastDropOffId {
                            self.stopNavigation()
                            self.removeMapMarkers(onComplete: nil)
                            
                            let buildingName = drop.name!
                            let latitude = NSString(string: drop.latitude!).floatValue
                            let longitude = NSString(string: drop.longitude!).floatValue
                            
                            self.currentPickupRequest = PickupRequest()
                            self.currentPickupRequest?.buildingName = buildingName
                            self.currentPickupRequest?.latitude = latitude
                            self.currentPickupRequest?.longitude = longitude
                            
                            self.createRoute(completed: { (success) in
                                if success {
                                    self.createMapMarker(latitude: latitude,
                                                         longitude: longitude)
                                    self.navigationBoard?.hideCancelButtons()
                                    NavigationManager.isGoingToDropOff = true
                                    Preferences.isWorkingStatusOn = false
                                    completion(true, nil)
                                } else {
                                    let error = APError.init(message: "Route not created", code: -2)
                                    completion(false, error)
                                }
                            })
                            
                        }
                    }
                    completion(false, error)
                }
            } else {
                completion(false, error)
            }
        })
    }
    
    func checkDispatcherMessages(){
        ChatREST.getChatMessages { (success, data, error) in
            self.hideHud()
            if success {
                guard let status = data?.last?.status, let userId = data?.last?.idUser else {return}
                if status == 0 && userId != 0 {
                    self.chatButton.badgeString = "1"
                    self.chatButton.badgeEdgeInsets = UIEdgeInsetsMake(20, 5, 0, 20)
                } else {
                    self.chatButton.badgeString = nil
                }
            }
        }
    }
    
    func askUserToDropOff(){
        let alert = UIAlertController(title: "Congratulations", message: "All your pickups are finished. Do you want to drop off ?", preferredStyle: .alert)
        let dropOffAcion = UIAlertAction(title: "Drop off", style: .default) { (_) in
            self.showHud()
            DriverREST.changeAvailability(isOnline: 0, reason: 2, completionHandler: { (success, error) in
                self.hideHud()
                if success {
                    let dropOffLocationViewController = DropOffViewController.create()
                    dropOffLocationViewController.reason = OfflineReason(2, "At full capacity", .atFullCapacity)
                    dispatch {
                        self.showModal(dropOffLocationViewController)
                    }
                }
            })
        }
        let cancelAction = UIAlertAction(title: "No", style: .default, handler: nil)
        alert.addAction(dropOffAcion)
        alert.addAction(cancelAction)
        showModal(alert)
    }
    
    //MARK: - IBActions
    @IBAction func leftPickupsSliderButtonPressed(_ sender: UIButton) {
        postNotification(notification: Notification.Name.previousSliderPickup, object: nil)
    }
    
    @IBAction func rightPickupsSliderButtonPressed(_ sender: UIButton) {
        postNotification(notification: Notification.Name.nextSliderPickup, object: nil)
    }
    
    
    @IBAction func cancelDropOff(_ sender: UITapGestureRecognizer) {
//         This is a hack to prevent app freeze when drop off has wrong coordinates
        stopNavigation()
        NavigationManager.isGoingToDropOff = false
        dismissNavigationBar(onCompletion: nil)
        unHideNavigationButtons()
        postNotification(notification: Notification.Name.enableNavigationButtons, object: nil)
        if !Preferences.isWorkingStatusOn {
            goOnline()
        }
        removeRouteFromMap()
        currentPickupRequest = nil
    }
    @IBAction func createPickupButtonPressed(_ sender: UIButton) {
        postNotification(notification: Notification.Name.schedulePickupRequest, object: nil)
    }
    
    @objc @IBAction func navigationButtonPressed(_ sender: UIButton?) {
        DispatchQueue.main.async {
            self.checkMapScheme()
            self.checkDispatcherMessages()
        }
        // Restore the map orientation to show entire route on screen
        if let shouldReposition = navigationManager?.mapTrackingAutoZoomEnabled {
            if !shouldReposition {
                mapView?.set(geoCenter: NMAGeoCoordinates(latitude: Double((userLocation?.coordinate.latitude)!), longitude: Double((userLocation?.coordinate.longitude)!)), animation: .linear)
            }
        }
        
        if isFreeZoomEnabled {
            mapView.set(zoomLevel: 14.9, animation: .linear)
            navigationManager?.mapTrackingAutoZoomEnabled =  true
            navigationManager?.mapTrackingEnabled =  true
            isFreeZoomEnabled = false
        }
        
    }
    
    
    @IBAction func messagesButtonPressed(_ sender: UIButton) {
        checkMapScheme()
        checkDispatcherMessages()
        chatButton.badgeString = nil
        let dispatcherChatViewController = DispatcherChatViewController.create()
        dispatcherChatViewController.delegate = self
        self.push(dispatcherChatViewController)
    }
    
    
    @objc @IBAction func notificationsButtonPressed(_ sender: UIButton) {
        checkMapScheme()
        checkDispatcherMessages()
        let notificationsViewController = NotificationsViewController.create()
        notificationsViewController.pickupDelegate = self
        notificationsViewController.pickupsRequests = pickupRequests
        notificationButton.badgeString = nil
        self.push(notificationsViewController)
        
    }
    
    @objc @IBAction func soundsButtonPressed(_ sender: UIButton) {
        checkMapScheme()
        checkDispatcherMessages()
        areSoundsEnabled = !areSoundsEnabled
        navigationManager?.setVoiceEnabledForAllManeuverActions(areSoundsEnabled)
        soundsButton.setImage(areSoundsEnabled ? #imageLiteral(resourceName: "sound on") : #imageLiteral(resourceName: "mute"), for: .normal)
        soundsButton.backgroundColor = areSoundsEnabled ? UIColor(red255: 186, green255: 233, blue255: 192) : UIColor(red255: 254, green255: 198, blue255: 200)
    }
    
    func showNotificationAlert(_ text:String){
        let estimatedFrame = calculateHeightBasedOnText(text)
        notificationAlertArrow.transform = CGAffineTransform.init(rotationAngle: CGFloat.pi/4)
        notificationViewHeightConstraint.constant = estimatedFrame.height + 50
        
        notificationAlert.alpha = 1
        notificationAlertArrow.alpha = 1
        notificationAlertText.text = text
        
        delay(delay: 6) {
            self.notificationAlertArrow.alpha = 0
            self.notificationAlert.alpha = 0
        }
        self.view.layoutIfNeeded()
    }
    
    fileprivate func calculateHeightBasedOnText(_ text: String)->CGRect{
        let size = CGSize(width: 200, height: 1000)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        let estimatedFrame = NSString(string: text).boundingRect(with: size, options: options, attributes: [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 12)], context: nil)
        
        return estimatedFrame
    }
    
    //MARK: - Functions
    func createRoute(completed: ((_ success: Bool)->Void)?) {
        checkMapScheme()
        checkDispatcherMessages()
        var stops:[NMAGeoCoordinates] = []
        
        if userLocation != nil {
            
            let currentLocation:NMAGeoCoordinates = NMAGeoCoordinates(latitude: Double((userLocation?.coordinate.latitude)!), longitude: (userLocation?.coordinate.longitude)!)
            stops.append(currentLocation)
        } else {
            print("User location not available")
            if CLLocationManager.locationServicesEnabled() {
                locationManager.startUpdatingLocation()
                
                self.createRoute { (success) in
                    completed?(success ? true : false)
                }
                return
            } else {
                self.locationManager.requestAlwaysAuthorization()
                self.locationManager.requestWhenInUseAuthorization()
                return
            }
        }
        
        if currentPickupRequest != nil {
            let nextStop:NMAGeoCoordinates = NMAGeoCoordinates(latitude: Double(currentPickupRequest!.latitude!), longitude: Double(currentPickupRequest!.longitude!))
            stops.append(nextStop)
        } else {
            print("Current Pickup Request is nil")
        }
        
        
        let routingType = NMARoutingType(rawValue: UInt(NavigationManager.routingType)) ?? .fastest
        let transportMode = NMATransportMode(rawValue: UInt(NavigationManager.transportMode)) ?? .truck
        let routingOptions = NavigationManager.getRoutingOptions()

        let routingMode = NMARoutingMode(routingType: routingType,
                                         transportMode: transportMode,
                                         routingOptions: routingOptions)
        if NavigationManager.truckLength > 0, NavigationManager.truckHeight > 0 {
            routingMode.vehicleHeight = Float(NavigationManager.truckLength)
            routingMode.vehicleHeight = Float(NavigationManager.truckHeight)
        }
        
        // Initialize the NMACoreRouter
        if router == nil {
            router = NMACoreRouter()
        }
        
        //Trigger the route calculation
        router?.calculateRoute(withStops: stops, routingMode: routingMode, { (routeResult, error) in
            if error.rawValue == 0 {
                if routeResult != nil{
                    // Add the 1st result onto the map
                    guard let route: NMARoute = routeResult?.routes?.first else {
                        print("Could not set route")
                        return
                    }
                    
                    self.removeRouteFromMap()
                    dispatch {
                        self.mapRoute = NMAMapRoute(route)
                        self.setRouteInformationToNavigationBoard()
                        self.customiseMapRoute()
                        self.mapView?.add(mapObject: self.mapRoute!)
                        // In order to see the entire route, we orientate the map view accordingly
                        self.geoBoundingBox = self.mapRoute?.route.boundingBox
                        self.mapView?.set(boundingBox: route.boundingBox!, animation: NMAMapAnimation.linear)
                        completed?(true)
                    }
                }
                else {
                    completed?(false)
                    self.showMessage("Error: Route result is not valid")
                    return
                }
            }
            else {
                completed?(false)
//                self.showMessage("Error: Route calculation error ")
                return
            }
        })
    }
    
    func setRouteInformationToNavigationBoard(){
        if let distance = self.mapRoute?.route.length,
            let duration = self.mapRoute?.route.duration {
            let time = Date.now().addingTimeInterval(duration)
            let durationString = time.offsetFrom(date: Date.now())
            let durationAndDistance = "\(durationString) | \(String(format: "%.2f", Double(distance)*0.00062137)) miles"
            navigationBoard?.set(arrivalTime: time as NSDate, durationAndDistance: durationAndDistance)
        }
    }
    
    func createPickupsMapMarkers() {
        print("Pickup requests: \(pickupRequests.count)")
        mapView.layoutSubviews()
        
        for pickup in pickupRequests {
            guard let startPickupTime = pickup.startPickupTime else {continue}
            if startPickupTime.isToday || pickup.pickupStatus == .ongoing {
                guard let latitude = pickup.latitude,
                    let longitude = pickup.longitude else { continue }
                self.geoCoord = NMAGeoCoordinates(latitude: CLLocationDegrees(latitude),
                                                  longitude: CLLocationDegrees(longitude))
                
                //create NMAMapMarker located with geo coordinate and icon image
                var markerImage = UIImage()
                switch pickup.pickupStatus {
                case .assigned:
                    markerImage = #imageLiteral(resourceName: "pin-orange")
                    break
                case .ongoing:
                    markerImage = #imageLiteral(resourceName: "final destination pin")
                case .overdue:
                    markerImage = #imageLiteral(resourceName: "overdue pickup pin")
                    break
                case .canceled:
                    markerImage = #imageLiteral(resourceName: "canceled pickup pin")
                    break
                case .completedWithoutDelays:
                    markerImage = #imageLiteral(resourceName: "completed pickup pin")
                    break
                default:
                    break
                }
                
                let mapMarker = NMAMapMarker(geoCoordinates: geoCoord!,
                                             image: markerImage)
                mapMarkers.append(mapMarker)
                
                //add NMAMapMarker to map view
                mapView.add(mapObject: mapMarker)
            } else {
                continue
            }
        }
    }
    func createMRFMapMarkers(){
        print("Pickup requests: \(pickupRequests.count)")
        mapView.layoutSubviews()
        for MRFBuilding in MRFBuildings {
            guard let latitude = MRFBuilding.latitude,
                  let longitude = MRFBuilding.longitude
            else { continue }
            
            let geoCoord = NMAGeoCoordinates(latitude: CLLocationDegrees(latitude)!,
                                             longitude: CLLocationDegrees(longitude)!)
            let markerImage = #imageLiteral(resourceName: "locat")
            let mapMarker = NMAMapMarker(geoCoordinates: geoCoord,
                                         image: markerImage)
            MRFMapMarkers.append(mapMarker)
            mapView.add(mapObject: mapMarker)
        }
    }
    
    @objc fileprivate func removeRouteFromMap(){
        dispatch {
            if let route = self.mapRoute {
                self.mapView.remove(mapObject: route)
            }
        }
    }
    
    fileprivate func createMapMarker(latitude:Float, longitude:Float){
        // Add dropOff mapMarker
        self.geoCoord = NMAGeoCoordinates(latitude: CLLocationDegrees(latitude),
                                          longitude: CLLocationDegrees(longitude))
        
        //create NMAMapMarker located with geo coordinate and icon image
        let markerImage = #imageLiteral(resourceName: "pickup pin")
        let mapMarker = NMAMapMarker(geoCoordinates: geoCoord!,
                                     image: markerImage)
        mapMarkers.append(mapMarker)
        
        //add NMAMapMarker to map view
        mapView.add(mapObject: mapMarker)
    }
    
    func removeMapMarkers(onComplete: ((_ success:Bool)->Void)?){
        mapView.remove(mapObjects: mapMarkers)
        mapMarkers.removeAll()
        removeMRFMarkers { (removed) in
            if removed {
                onComplete?(true)
            }
        }
    }
    
    func removeMRFMarkers(completed: ((_ success:Bool)->Void)?){
        mapView.remove(mapObjects: MRFMapMarkers)
        MRFMapMarkers.removeAll()
        completed?(true)
    }
    
    func startToNavigate(){
        // If there is no route, create route/routes and start navigation
        if navigationManager?.navigationState.rawValue == 0 {
            createRoute { (success) in
                if success {
                    self.startNavigation()
                }
            }
        }
        else {
            // If navigation is running
            stopNavigation()
        }
    }
    
    func startNavigation(){
        if (self.mapRoute == nil) {
            print("mapRoute == nil")
            self.createRoute { (success) in
                if success {
                    self.redrawMapMarkers(completion: { (_) in })
                    self.customiseMapRoute()
                }
            }
            return
        }
        
        
        // Configure NavigationManager to launch navigation on current map
        navigationManager?.map = mapView
        let alert = UIAlertController(title: "Choose Navigation mode", message: "Please choose a mode", preferredStyle: .alert)
        
        //Add Buttons
        let deviceButton = UIAlertAction(title: "Navigation", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            // Start the turn-by-turn navigation.Please note if the transport mode of the passed-in
            // route is pedestrian, the NavigationManager automatically triggers the guidance which is
            // suitable for walking.
            guard let route = self.mapRoute?.route else {return}
            self.navigationManager?.startTurnByTurnNavigation(route)
            self.isNavigationActive = true
            // Set the map tracking properties
            self.navigationManager?.mapTrackingEnabled = true
            self.navigationManager?.mapTrackingAutoZoomEnabled = true
            self.navigationManager?.mapTrackingOrientation = NMAMapTrackingOrientation.dynamic
            self.navigationManager?.isSpeedWarningEnabled = AccountManager.playSpeedAlertSound!
        })
        
        let simulateButton = UIAlertAction(title: "Simulation", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            // Simulation navigation by init the PositionSource with route and set movement speed
            guard let route = self.mapRoute?.route else {return}
            let source = NMARoutePositionSource(route: route)
            source.movementSpeed = 60
            NMAPositioningManager.sharedInstance().dataSource = source
            self.navigationManager?.startTurnByTurnNavigation((self.mapRoute?.route)!)
            self.isNavigationActive = true
            // Set the map tracking properties
            self.navigationManager?.mapTrackingEnabled = true
            self.navigationManager?.mapTrackingAutoZoomEnabled = true
            self.navigationManager?.mapTrackingOrientation = NMAMapTrackingOrientation.dynamic
            self.navigationManager?.isSpeedWarningEnabled = AccountManager.playSpeedAlertSound!
        })
        
        alert.addAction(deviceButton)
        alert.addAction(simulateButton)
        dispatch {
            self.present(alert, animated: true, completion: {
                print("Start navigation alert presented")
            })
        }
        self.timer = Timer.scheduledTimer(timeInterval: self.thirtySeconds, target: self, selector: #selector(self.sendLocation), userInfo: nil, repeats: true)
    }
    
    @objc func stopNavigation(){
        // Stop navigation
        soundsButton.alpha = 0
        chatButton.alpha = 1
        navigationManager?.stop()
        isNavigationActive = false
        timer?.invalidate()
        dismissNavigationBar {
            self.showPickupsSlider()
        }
        
        removeRouteFromMap()
        if !(NMAPositioningManager.sharedInstance().dataSource is NMADevicePositionSource) {
            NMAPositioningManager.sharedInstance().dataSource = nil
        }
        // Restore the map orientation to show entire route on screen
        if geoBoundingBox != nil {
            mapView?.set(boundingBox: geoBoundingBox!, animation: NMAMapAnimation.linear)
            mapView?.orientation = 0
        }
        navigationManager?.mapTrackingAutoZoomEnabled = false
        navigationManager?.mapTrackingEnabled = false
        routeCoordinates.removeAll()
    }
    
    @objc func sendLocation(){
        guard let latitude = userLocation?.coordinate.latitude else {
            print("Couldn't get latitude")
            print("Location not sent")
            return
        }
        guard let longitude = userLocation?.coordinate.longitude else {
            print("Couldn't get latitude")
            print("Location not sent")
            return
        }
        
        // Add coordinate to route coordinates array
        let routeCoordinate = "{\"latitude\":\(Double(latitude)),\"longitude\":\(Double(longitude))},"
        routeCoordinates.append(routeCoordinate)
        
        DriverREST.sendLocation(latitude, longitude) { (success, error) in
            if success {
                print("Location sent")
                print("Latitude: \(latitude), Longitude: \(longitude)")
            } else {
                print("")
            }
        }
    }
    
    // Handle NMAPositioningManagerDidUpdatePositionNotification
    @objc func positionDidUpdate() {
        if let coordinates = NMAPositioningManager.sharedInstance().currentPosition?.coordinates {
//            positionIndicator?.coordinates = coordinates
//            positionIndicator?.refresh()
            userLocation = CLLocation(latitude: coordinates.latitude, longitude: coordinates.longitude)
        }
    }
    
    // Handle NMAPositioningManagerDidLosePositionNotification
    @objc func didLosePosition() {
    }
    
    func updateFromLocationPermission() {
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .restricted, .denied:
                console("No Access")
                isLocationAllowed = false
                
            case .notDetermined:
                console("Not Determined")
                isLocationAllowed = true
            case .authorizedAlways, .authorizedWhenInUse:
                console("Access")
                isLocationAllowed = true
                locationManager.startUpdatingLocation()
            }
        } else {
            isLocationAllowed = false
            console("Location services are not enabled")
        }
    }
    
    func customiseMapRoute(){
        mapRoute?.renderType = .userDefined
        mapRoute?.traveledColor = UIColor.gray
        mapRoute?.color = Appearance.greenColor
    }
    
    @objc func checkMapScheme(){
        // If it's Auto mode
        if NavigationManager.mapView == MapViewType.Auto.rawValue {
            let date = Date()
            let calendar = Calendar.current
            let hour = calendar.component(.hour, from: date)
            if hour >= 7 && hour < 19 {
                mapView.mapScheme = NMAMapSchemeNormalDay
                ThemeManager.themeMode = Mode.Day.rawValue
                NavigationManager.mapTheme = 0
            } else {
                mapView.mapScheme = NMAMapSchemeNormalNight
                ThemeManager.themeMode = Mode.Night.rawValue
                NavigationManager.mapTheme = 1
            }
        } else {
            if NavigationManager.mapTheme == 0 {
                mapView.mapScheme = NMAMapSchemeNormalDay
                ThemeManager.themeMode = Mode.Day.rawValue
            } else if NavigationManager.mapTheme == 1{
                mapView.mapScheme = NMAMapSchemeNormalNight
                ThemeManager.themeMode = Mode.Night.rawValue
            }
        }
    }

    
    @objc func showProfile(){
        let profileViewController = ProfileViewController.create()
        self.push(profileViewController)
    }
    
    @objc func downloadLanguage(not: Notification) {
        // userInfo is the payload send by sender of notification
        if let userInfo = not.userInfo {
            // Safely unwrap the name sent out by the notification sender
            if let languageId = userInfo["id"] as? Int {
                print("Language id: \(languageId)")
                self.languageId = UInt(languageId)
                voiceCatalog?.update()
            }
        }
    }
    
    @objc func navigateToDropOff(not: Notification) {
        // userInfo is the payload send by sender of notification
        if let userInfo = not.userInfo {
            // Safely unwrap the name sent out by the notification sender
            if let longitude = userInfo["longitude"] as? Float,
               let latitude = userInfo["latitude"] as? Float,
               let buildingName = userInfo["building_name"] as? String,
               let dropId = userInfo["id"] as? Int {
                stopNavigation()
                removeMapMarkers(onComplete: nil)
                if currentPickupRequest == nil {
                    currentPickupRequest = PickupRequest()
                }
                currentPickupRequest?.buildingName = buildingName
                currentPickupRequest?.longitude = longitude
                currentPickupRequest?.latitude = latitude
                DriverREST.changeAvailability(isOnline: 0, reason: 2) { (success, error) in
                    if success {
                        self.createRoute { (success) in
                            if success {
                                self.createMapMarker(latitude: latitude, longitude: longitude)
                                self.startNavigation()
                                
                                self.hideNavigationButtons()
                                self.postNotification(notification: Notification.Name.disableNavigationButtons, object: nil)
                                
                                NavigationManager.isGoingToDropOff = true
                                NavigationManager.lastDropOffId = dropId
                            }
                        }
                    }
                }
            }
        }
    }
    
    func hideNavigationButtons(){
        navigationBoard?.hideCancelButtons()
        notificationButton.alpha = 0

        seeMoreButton.alpha = 1
        locationButton.alpha = 1
//        chatButton.alpha = 0
        soundsButton.alpha = 0
    }
    
    func unHideNavigationButtons(){
        navigationBoard?.showCancelButton()
        soundsButton.alpha = 0
        seeMoreButton.alpha = 0
//        chatButton.alpha = 1
//        soundsButton.alpha = isNavigationActive ? 1 : 0
    }
    
    func findPickupWith(_ id: Int)->PickupRequest?{
        for pickup in self.pickupRequests {
            if pickup.id == id {
                return pickup
            }
        }
        return nil
    }
    
    func disableNavigationButtons(){
        soundsButton.isEnabled = false
        notificationButton.isEnabled = false
//        chatButton.isEnabled = false
        locationButton.isEnabled = false
    }
    
    func enableNavigationButtons(){
        soundsButton.isEnabled = true
        notificationButton.isEnabled = true
//        chatButton.isEnabled = true
        locationButton.isEnabled = true
    }
    
}

// MARK: - Location Manager Delegate extension
extension HomeMapController: CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .denied || status == .restricted {
            self.isLocationAllowed = false
        }else {
            self.isLocationAllowed = true
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if locations.count > 0, let uL = locations.first {
            locationManager.stopUpdatingLocation()
            self.userLocation = uL
            if self.mapView.isHidden {
                self.mapView.set(geoCenter: NMAGeoCoordinates(latitude:  uL.coordinate.latitude,
                                                              longitude: uL.coordinate.longitude),
                                 animation: .bow)
                self.mapView.isHidden = false
            } else {
                self.mapView.set(geoCenter: NMAGeoCoordinates(latitude:  uL.coordinate.latitude,
                                                              longitude: uL.coordinate.longitude),
                                 animation: .bow)
            }
            self.mapCenter = uL.coordinate
            self.geoCoord = NMAGeoCoordinates(latitude:  uL.coordinate.latitude,
                                              longitude: uL.coordinate.longitude)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Couldn't get location. Error: \(error.localizedDescription)")
    }
}


extension HomeMapController: NMAMapViewDelegate, NMAMapGestureDelegate{
    func mapViewDidSelectObjects(_ mapView: NMAMapView, objects: [NMAMapObject]) {
        print(#function)
        
    }
    
    func mapView(_ mapView: NMAMapView, didSelect objects: [NMAViewObject]) {
        print("Tapped mapMarker")
        for object in objects {
            if object is NMAMapMarker {
                print("Found map Marker")
                let mapMarker = object as! NMAMapMarker
                let markerCoordinates = mapMarker.coordinates
                for pickup in pickupRequests {
                    let pickupCoordinates = NMAGeoCoordinates(latitude: Double(pickup.latitude!), longitude: Double(pickup.longitude!))
                    
                    if markerCoordinates.latitude == pickupCoordinates.latitude &&
                        markerCoordinates.longitude == pickupCoordinates.longitude {
                        print("\(pickup.buildingName)")
                        print("\(pickup.pickupStatus)")
                        pickupDelegate?.delegatePickup(pickup)
                        return
                    }
                }
            }
        }
    }
    
    func mapViewDidEndMovement(_ mapView: NMAMapView) {
        if isNavigationActive {
            navigationManager?.mapTrackingAutoZoomEnabled = mapView.zoomLevel < Float(15) ? false : true
            navigationManager?.mapTrackingEnabled = mapView.zoomLevel < Float(15) ? false : true
            isFreeZoomEnabled = mapView.zoomLevel < Float(15) ? true : false
        }
        
        print("Zoom level: \(mapView.zoomLevel)")
    }
    
}



// MARK: - Navigation Manager extension
extension HomeMapController: NMANavigationManagerDelegate {
    
    func navigationManager(_ navigationManager: NMANavigationManager, didUpdateManeuvers maneuver: NMAManeuver?, _ nextManeuver: NMAManeuver?) {
        print("New maneuver is available")
        
        if let parent = self.parent as? HomeController {
            if let distance = maneuver?.distanceFromStart,
               let iconValue = maneuver?.icon.rawValue,
               let buildingName = currentPickupRequest?.buildingName,
               let buildingAddress = currentPickupRequest?.buildingAddress,
               let currentRoad = maneuver?.roadName as String? {
                parent.navigationBoardHeader?.set(distance: Double(distance)*0.00062137,
                                                  currentRoad: currentRoad,
                                                  buildingName: buildingName,
                                                  buildingAddress: buildingAddress,
                                                  iconValue: Int(iconValue))
            }
        }
        if !isNavigationBoardShown {
            hidePickupsSlider {
                self.bringNavigationBar()
            }
        }
        if !isNavigationBoardHeaderShown{
            postNotification(notification: Notification.Name.navigationStarted, object: nil)
            notificationsButtonTopConstraint.constant = 60
            isNavigationBoardHeaderShown = true
        }
    }
    
    // Signifies that the system has found a GPS signal
    func navigationManagerDidFindPosition(_ navigationManager: NMANavigationManager) {
        print("New position has been found")
    }
    
    func navigationManagerDidReachDestination(_ navigationManager: NMANavigationManager) {
        print(#function)
        if NavigationManager.isGoingToDropOff {
//            navigationBoard?.cancelButton.alpha = 1
            let completedPickupsViewController = CompletedPickupsViewController.create()
            completedPickupsViewController.modalPresentationStyle = .overCurrentContext
            self.showModal(completedPickupsViewController)
        } else {
            postNotification(notification: Notification.Name.navigationBoardRoadDetailsPressed, object: nil)
        }
        
    }
    
    func navigationManager(_ navigationManager: NMANavigationManager, shouldPlayVoiceFeedback text: String?) -> Bool {
        return areSoundsEnabled
    }
    
    func navigationManager(_ navigationManager: NMANavigationManager, didUpdateRealisticViewsForNextManeuver realisticViews: [String : NMAImage]) {
        print(#function)
    }
    func navigationManager(_ navigationManager: NMANavigationManager, didUpdateRealisticViewsForCurrentManeuver realisticViews: [String : NMAImage]) {
        print(#function)
    }
    func navigationManagerDidLosePosition(_ navigationManager: NMANavigationManager) {
        print(#function)
    }
    func navigationManagerDidSuspendDueToInsufficientMapData(_ navigationManager: NMANavigationManager) {
        print(#function)
    }
    func navigationManagerDidResumeDueToMapDataAvailability(_ navigationManager: NMANavigationManager) {
        print(#function)
    }
    func navigationManagerDidReroute(_ navigationManager: NMANavigationManager) {
        print(#function)
    }
    
    func navigationManager(_ navigationManager: NMANavigationManager, didUpdateRoute routeResult: NMARouteResult) {
        print(#function)
        checkMapScheme()
        checkDispatcherMessages()
        self.mapView.remove(mapObject: self.mapRoute!)
        self.mapRoute = nil
        guard let route: NMARoute = routeResult.routes?.first else {
            print("Could not set route")
            return
        }
        
        self.mapRoute = NMAMapRoute(route)
        setRouteInformationToNavigationBoard()
        
        self.customiseMapRoute()
        self.mapView?.add(mapObject: self.mapRoute!)
    }
    func navigationManagerWillReroute(_ navigationManager: NMANavigationManager)
    {
        print(#function)
    }
    func navigationManager(_ navigationManager: NMANavigationManager, didUpdateSpeedingStatus isSpeeding: Bool, forCurrentSpeed speed: Float, speedLimit: Float) {
        if AccountManager.showSpeedometer! {
            if speedometerView?.alpha == 0 {
                speedoMeterContainerView.addSubview(speedometerView!)
            }
            speedometerView?.set(currentSpeed: speed, limitedSpeed: speedLimit)
        }
    }
    func navigationManager(_ navigationManager: NMANavigationManager, didPlayVoiceFeedback text: String?) {
        print(#function)
    }
    func navigationManager(_ navigationManager: NMANavigationManager, didReachStopover stopover: NMAWaypoint) {
        print(#function)
        createPickupsMapMarkers()
        createMRFMapMarkers()
        self.view.setNeedsDisplay()
        //Update navigation board
    }
    func navigationManager(_ navigationManager: NMANavigationManager, didUpdateLaneInformation laneInformations: [NMALaneInformation], roadElement: NMARoadElement?) {
        print(#function)
    }
    
    func showMessage(_ message: String) {
        var frame = CGRect(x: 110, y: 200, width: 220, height: 120)
        let label = UILabel(frame: frame)
        label.backgroundColor = UIColor.groupTableViewBackground
        label.textColor = UIColor.blue
        label.text = message
        label.textAlignment = .center
        label.numberOfLines = 0
        let rect: CGRect? = label.text?.boundingRect(with: frame.size, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: [NSAttributedStringKey.font: label.font], context: nil)
        frame.size = (rect?.size)!
        label.frame = frame
        self.view.addSubview(label)
        UIView.animate(withDuration: 2.0, animations: {() -> Void in
            label.alpha = 0
        }, completion: {(_ finished: Bool) -> Void in
            label.removeFromSuperview()
        })
    }
}

// MARK: - Navigation board extension
extension HomeMapController {
    
    @IBAction func navigationBarSwipe(_ sender: UISwipeGestureRecognizer) {
        if isNavigationBoardShown {
            hideNavigationBar()
            isNavigationBoardShown = !isNavigationBoardShown
        }
    }
    @IBAction func hideNavigationBarSwipe(_ sender: UISwipeGestureRecognizer) {
        if !isNavigationBoardShown {
            showNavigationBar()
            isNavigationBoardShown = !isNavigationBoardShown
        }
    }
    
    @IBAction func navigationBarPressed(_ sender: UITapGestureRecognizer = UITapGestureRecognizer()) {
        if isNavigationBoardShown {
            hideNavigationBar()
        } else {
            showNavigationBar()
        }
        isNavigationBoardShown = !isNavigationBoardShown
    }
    
    @IBAction func seeMoreButtonPressed(_ sender: UIButton){
        navigationBarPressed()
    }
    
    
    fileprivate func showNavigationBar(){
        navigationBarHeight.constant += 70
        UIView.animate(withDuration: 0.2) {
            self.seeMoreButton.transform = CGAffineTransform(rotationAngle: 0)
            self.view.layoutIfNeeded()
        }
    }
    
    fileprivate func hideNavigationBar(){
        navigationBarHeight.constant -= 70
        UIView.animate(withDuration: 0.2) {
            self.seeMoreButton.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
            self.view.layoutIfNeeded()
        }
    }
    
    fileprivate func bringNavigationBar(){
        self.navigationBarBottomConstraint.constant = 30
        UIView.animate(withDuration: 0.2) {
//            self.soundsButton.alpha = NavigationManager.isGoingToDropOff ? 0 : 1
            self.soundsButton.alpha = 0
            self.notificationButton.alpha = 0
            self.locationButton.alpha = 0
            self.createPickupButton.alpha = 0
            self.chatButton.alpha = 0
            self.seeMoreButton.alpha = 0
            self.speedometerView?.alpha = AccountManager.showSpeedometer! ? 1 : 0
            self.speedoMeterContainerView.alpha = 1
            self.view.layoutIfNeeded()
        }
    }
    
    fileprivate func dismissNavigationBar(onCompletion: (()->Void)?){
        isNavigationBoardShown = false
        isNavigationBoardHeaderShown = false
        
        navigationBarBottomConstraint.constant = -navigationBarHeight.constant-30
        notificationsButtonTopConstraint.constant = 20
        
        postNotification(notification: Notification.Name.navigationStopped, object: nil)
        UIView.animate(withDuration: 0.2, animations: {
            self.notificationButton.alpha = 1
            self.locationButton.alpha = 1
            self.seeMoreButton.alpha = 0
            self.speedometerView?.alpha = 0
            self.speedoMeterContainerView.alpha = 0
            self.createPickupButton.alpha = 1
            self.view.layoutIfNeeded()
        }) { (_) in
            onCompletion?()
        }
    }
    
    func hidePickupsSlider(onCompletion: (()->Void)?){
        pickupSliderBottomConstraint.constant = 30
        pickupSliderHeightConstraint.constant = 0
        UIView.animate(withDuration: 0.2, animations: {
            self.pickupsContainerView.alpha = 0
            self.leftPickupsSliderButton.alpha = 0
            self.rightPickupsSliderButton.alpha = 0
            self.view.layoutIfNeeded()
        }) { (_) in
            onCompletion?()
        }
    }
    
    func showPickupsSlider(){
        if pickupRequests.count > 0 && !isNavigationActive {
            pickupSliderBottomConstraint.constant = 30
            pickupSliderHeightConstraint.constant = 150
            UIView.animate(withDuration: 0.2) {
                self.pickupsContainerView.alpha = 1
                self.leftPickupsSliderButton.alpha = 1
                self.rightPickupsSliderButton.alpha = 1
                self.view.layoutIfNeeded()
            }
        } else {
            hidePickupsSlider(onCompletion: nil)
        }
        
    }
    
    func checkNextPickupToBeStarted(){
        self.stopNavigation()
        // Search for an assigned or overdue pickup
        for pickup in self.pickupRequests  {
            if pickup.pickupStatus == .assigned || pickup.pickupStatus == .overdue {
                if pickup.id != self.currentPickupRequest?.id && self.currentPickupRequest != nil{
                    self.isAnyPickupAvailable = true
                    
                    dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
                    // Create key from pickup's start time
                    let formattedDate = dateFormatter.string(from: pickup.startPickupTime!)
                    // Fetch pickups that belong to formattedDate key
                    guard let pickups = self.pickupRequestsBySections[formattedDate] else { break }
                    
                    // Take only assigned and overdued pickups
                    let filteredPickups = pickups.filter {$0.pickupStatus == PickupStatusType.assigned || $0.pickupStatus == PickupStatusType.overdue }
                    
                    // If there is more than 1 pickup on this time, sort by route distance
                    if filteredPickups.count > 1 {
                        // Function is called recursively until the route distance
                        // is calculated for all pickups
                        getRouteDistance(forPickupAt: filteredPickups.count-1, from: filteredPickups)
                    } else {
                        // If there are not two pickups at the same time,
                        // just assign the first pickup on the list that's of type: assigned or overdue
                        self.currentPickupRequest = pickup
                        self.askUserToStartNewPickup()
                        dispatch {
                            self.removeMapMarkers(onComplete: nil)
                            self.removeRouteFromMap()
                            self.createPickupsMapMarkers()
                            self.createMRFMapMarkers()
                        }
                    }
                    break
                }
            }
        }
    }
    
    func getRouteDistance(forPickupAt i:Int, from pickups: [PickupRequest]){
        
        if i > -1 {
            dispatchGroup.enter()
            let routingType = NMARoutingType(rawValue: UInt(NavigationManager.routingType)) ?? .fastest
            let transportMode = NMATransportMode(rawValue: UInt(NavigationManager.transportMode)) ?? .truck

            // Create routing mode for route calculation
            let routingMode = NMARoutingMode.init(routingType: routingType,
                                                  transportMode: transportMode,
                                                  routingOptions: NavigationManager.getRoutingOptions())
            if NavigationManager.truckLength > 0, NavigationManager.truckHeight > 0 {
                routingMode.vehicleHeight = Float(NavigationManager.truckLength)
                routingMode.vehicleHeight = Float(NavigationManager.truckHeight)
            }
            
            
            var stops:[NMAGeoCoordinates] = []
            // Create points A and B
            // A = user's location
            // B = pickup's location
            
            // Initializing A
            if self.userLocation != nil {
                let currentLocation:NMAGeoCoordinates = NMAGeoCoordinates(latitude: Double((self.userLocation?.coordinate.latitude)!),
                                                                          longitude: (self.userLocation?.coordinate.longitude)!)
                stops.append(currentLocation)
            }
            // Initializing B
            let nextStop:NMAGeoCoordinates = NMAGeoCoordinates(latitude: Double(pickups[i].latitude!),
                                                               longitude: Double(pickups[i].longitude!))
            stops.append(nextStop)
            
            // Calculate the route from A to B
            self.router?.calculateRoute(withStops: stops, routingMode: routingMode, { (result, error) in
                if let result = result?.routes?.first {
                    let pickup = pickups[i]
                    self.routes[pickup] = result
                }
                self.dispatchGroup.leave()
                // Call function recursively until there is no more pickups to calculate it's route
                self.getRouteDistance(forPickupAt: i-1, from: pickups)
            })
        } else {
            // After all route calculations have been done
            dispatchGroup.notify(queue: .main) {
                // Sort the routes based on it's length and get the first route
                // It's key is the nearest pickup
                if let fastestRoute = self.routes.sorted(by: { $0.value.length < $1.value.length }).first {
                    let formattedDate = self.dateFormatter.string(from: pickups.first!.startPickupTime!)
                    // Get pickups assigned at the same time
                    guard let pickups = self.pickupRequestsBySections[formattedDate] else {return}
                    // Find the nearest pickup by id
                    for pickup in pickups {
                        if pickup.id == fastestRoute.key.id {
                            self.currentPickupRequest = pickup
                            self.askUserToStartNewPickup()
                            self.routes.removeAll()
                            return
                        }
                    }
                } else {
                    // If can't sort the list by route length, assign the first pickup instead
                    for pickup in self.pickupRequests  {
                        if pickup.pickupStatus == .assigned || pickup.pickupStatus == .overdue {
                            if pickup.id != self.currentPickupRequest?.id && self.currentPickupRequest != nil{
                                self.removeMRFMarkers(completed: nil)
                                self.removeRouteFromMap()
                                self.createPickupsMapMarkers()
                                self.createMRFMapMarkers()
                                self.currentPickupRequest = pickup
                                self.askUserToStartNewPickup()
                            }
                        }
                    }
                }
            }
        }
    }
    
    func askUserToStartNewPickup(){
        let nextPickupViewController = NextPickupViewController.create()
        nextPickupViewController.nextPickupDelegate = self
        nextPickupViewController.pickupRequest = currentPickupRequest
        nextPickupViewController.modalPresentationStyle = .overCurrentContext
        dispatch {
            self.showModal(nextPickupViewController, animated: false, completion: nil)
        }
    }
}

// MARK: - Navigation board buttons extension
extension HomeMapController: NavigationBoardDelegate {
    func buttonPressed(_ sender: UIButton) {
        //Set navigation board on default position
//        if isNavigationBoardShown {
//            hideNavigationBar()
//            isNavigationBoardShown = !isNavigationBoardShown
//        }
    }
}

extension HomeMapController: RoadDetailsDelegate {
    func overviewButtonPressed() {
        self.navigationManager?.mapTrackingEnabled = shouldFollowRoute ? false : true
        self.navigationManager?.mapTrackingAutoZoomEnabled = shouldFollowRoute ? false : true
        
        shouldFollowRoute = !shouldFollowRoute
        // Change overview button's text
        // In order to see the entire route, we orientate the map view accordingly
        // If mapTracking and mapTrackingAutoZoom are disabled
        if !shouldFollowRoute {
            guard let mapRoute = self.mapRoute else {return}
            self.geoBoundingBox = mapRoute.route.boundingBox
            self.mapView?.set(boundingBox: mapRoute.route.boundingBox!, animation: NMAMapAnimation.linear)
        }
    }
    
    func completeButtonPressed() {
        delay(delay: 0.5) {
            if NavigationManager.isGoingToDropOff {
                
                let completedPickupsViewController = CompletedPickupsViewController.create()
                completedPickupsViewController.modalPresentationStyle = .overCurrentContext
                self.showModal(completedPickupsViewController)
            } else {
                guard let id = self.currentPickupRequest?.id else {return}
                let confirmPickupCompletionViewController = ConfirmPickupCompletionViewController.create()
                confirmPickupCompletionViewController.pickupId = "\(id)"
                confirmPickupCompletionViewController.modalPresentationStyle = .overCurrentContext
                confirmPickupCompletionViewController.delegate = self
                // Prepare route coordinates
                
                if self.routeCoordinates.count > 2 {
                   self.routeCoordinates.remove(at: self.routeCoordinates.index(before: self.routeCoordinates.endIndex))
                }
                
                self.routeCoordinates.append("]")
                
                confirmPickupCompletionViewController.routeCoordinates = self.routeCoordinates
                if self.currentPickupRequest?.pickupStatus == .postponed {
                    confirmPickupCompletionViewController.confirmType = .Postpone
                    confirmPickupCompletionViewController.pickupRequest = self.currentPickupRequest
                }
                confirmPickupCompletionViewController.isCompletingFromPickupProfile = false
                self.showModal(confirmPickupCompletionViewController)
            }
        }
    }
    
    func cancelButtonPressed() {
        if NavigationManager.isGoingToDropOff {
            self.refreshPickups()
        } else {
            delay(delay: 0.3) {
                let cancellPickupViewController = CancellPickupViewController.create()
                cancellPickupViewController.buildingId = self.currentPickupRequest?.id
                cancellPickupViewController.cancelDelegate = self
                // pass coordinates
                
                cancellPickupViewController.modalPresentationStyle = .overCurrentContext
                self.showModal(cancellPickupViewController)
            }
        }
    }
}

extension HomeMapController: PickupDelegate {
    func editPickup(_ pickup: PickupRequest) {
        // Does nothing here
    }
    
    func findHomeController()->HomeController?{
        for controller in navigationController!.viewControllers  {
            if controller is HomeController {
                return controller as? HomeController
            }
        }
        return nil
    }
    
    func refreshPickupDelegateFromCell() {
        guard let homeController = findHomeController() else {
            print("Couldn't convert to HomeController")
            return
        }
        print("Found Home Controller")
        guard let listController = homeController.listController else {
            print("Couldn't get ListController")
            return
        }
        homeController.mapController?.stopNavigation()
        listController.getPickups { (_) in
        }
    }
    
    func triggerNavigation(_ id: Int, _ route:String) {
        guard let homeController = findHomeController() else {
            print("Couldn't find HomeController")
            return
        }
        print("Found Home Controller")
        homeController.listController?.getPickups(completion: { (success) in
            if success {
                guard let listController = homeController.listController else {
                    print("Couldn't get ListController")
                    return
                }
                
                self.setCurrentPickupRequest(withID: id)
                listController.pickupDelegate?.triggerNavigation(id,route)
                
            }
        })
        
    }
    
    func setCurrentPickupRequest(withID: Int){
        for pickup in pickupRequests {
            if pickup.id == withID {
                currentPickupRequest = pickup
                return
            }
        }
        return
    }
}

extension HomeMapController: DispatcherChatDelegate {
    func chatUpdates() {
        print("DispatcherChatDelegate")
        chatButton.badgeString = nil
    }
}

extension HomeMapController: MapMarkersDelegate {
    func redrawMapMarkers(completion: @escaping (Bool) -> Void) {
        removeMapMarkers { (success) in
            if success {
                self.getPickups(completion: { (success) in
                    if success {
                        // Also creates map markers
                        completion(true)
                    } else {
                        completion(false)
                    }
                })
            }
        }
    }
}

extension HomeMapController:ConfirmPickupActionDelegate {
    @objc func refreshPickups() {
        dismissNavigationBar {
            self.showPickupsSlider()
        }
        if NavigationManager.isGoingToDropOff {
            delay(delay: 0.5) {
                self.stopNavigation()
                NavigationManager.isGoingToDropOff = false
                self.removeRouteFromMap()
            }
        }
        
        unHideNavigationButtons()
        postNotification(notification: Notification.Name.enableNavigationButtons, object: nil)
        
        if !Preferences.isWorkingStatusOn {
            goOnline()
        }
        
        guard let parent:HomeController = self.parent as? HomeController else {return}
        parent.listController?.getPickups(completion: { (success) in
            //This also redraws map markers
            if !NavigationManager.isGoingToDropOff {
                self.checkNextPickupToBeStarted()
            } else{
                self.stopNavigation()
                NavigationManager.isGoingToDropOff = false
                self.currentPickupRequest = nil
            }
        })
        
        // Reset route coordinates for next navigation
        routeCoordinates.removeAll()
    }
}

extension HomeMapController: CancelPickupStatusDelegate {
    func shouldCancelPickup(_ status: Bool) {
        stopNavigation()
        getPickups { (success) in
            guard let parent:HomeController = self.parent as? HomeController else {return}
            parent.listController?.getPickups(completion: { (_) in
            })
        }
    }
}

extension HomeMapController: NextPickupDelegate {
    func startNextPickup(_ status: Bool) {
        if status {
//            removeRouteFromMap()
//            createRoute { (success) in
//                if success {
//                    self.startToNavigate()
//                }
//            }
            if let homeController = findHomeController(), let listController = homeController.listController, let toDoListController = listController.toDoListViewController {
                let toDoPickups = listController.toDoPickups
                for pickup in toDoPickups {
                    if pickup.id == currentPickupRequest?.id, let pickupId = pickup.id {
                        toDoListController.startPickup(true, pickupId, "[]")
                    }
                }
            }
        } else {
            self.stopNavigation()
            self.redrawMapMarkers(completion: {(_) in
                self.removeRouteFromMap()
            })
        }
    }
}

extension HomeMapController: NMAVoiceCatalogDelegate {
    func voiceCatalog(_ voiceCatalog: NMAVoiceCatalog, didInstallPackage package: NMAVoicePackage, withError error: Error?) {
        if error == nil {
            print("Package installed")
            navigationManager?.voicePackage = voiceCatalog.voicePackage(withId: languageId!)
            // Update cell
            NotificationCenter.default.post(name: Notification.Name.installVoiceLanguage, object: nil, userInfo: ["state": true,"id":languageId!])
            NavigationManager.actualLanguageId = Int(bitPattern: languageId!)
            postNotification(notification: Notification.Name.downloadedLanguage, object: nil)
        } else {
            NotificationCenter.default.post(name: Notification.Name.installVoiceLanguage, object: nil, userInfo: ["state": false,"id": languageId! ])
            print("Package not installed because:")
            print(error ?? "Error")
        }
    }
    
    func voiceCatalog(_ voiceCatalog: NMAVoiceCatalog, didUpdate error: Error?) {
        print(#function)
        if error == nil {
            print("Package didupdate")
            print(voiceCatalog)
            let isLanguageInstalled = voiceCatalog.voicePackage(withId: languageId!)?.isInstalled
            if isLanguageInstalled! {
                navigationManager?.voicePackage = voiceCatalog.voicePackage(withId: languageId!)
                // Update cell
                NotificationCenter.default.post(name: Notification.Name.installVoiceLanguage, object: nil, userInfo: ["state": true,"id":languageId!])
                NavigationManager.actualLanguageId = Int(bitPattern: languageId!)
                postNotification(notification: Notification.Name.downloadedLanguage, object: nil)
                
            } else {
                guard let downloadedLanguage = voiceCatalog.voicePackage(withId: languageId!) else {
                    print("Couldn't find language")
                    return
                }
                voiceCatalog.installVoicePackage(downloadedLanguage)
            }
            
        } else {
            print("Package did not update becuse: ")
            print(error ?? "Error")
        }
    }
    
    func voiceCatalog(_ voiceCatalog: NMAVoiceCatalog, didUpdateDownloadProgress progress: Float, for package: NMAVoicePackage) {
        print(voiceCatalog)
        print("Download progress \(progress)")
        NotificationCenter.default.post(name: Notification.Name.downloadVoiceLanguageProgress, object: nil, userInfo: ["progress":progress,"id":languageId!])
    }
    
    func voiceCatalog(_ voiceCatalog: NMAVoiceCatalog, didUpdateUncompressProgress progress: Float, for package: NMAVoicePackage) {
        print("Uncompress progress \(progress)")
        NotificationCenter.default.post(name: Notification.Name.uncompressVoiceLanguageProgress, object: nil, userInfo: ["progress":progress,"id":languageId!])
    }
}


