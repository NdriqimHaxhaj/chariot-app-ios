
//
//  ToDoListViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 3/23/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//


import UIKit
import NMAKit

enum RouteSort {
    case distance
    case timeThenDistance
}

enum PickupRequestsTime {
    case current
    case all
}

class ToDoListViewController: UIViewController {
    
    //MARK: Outlets
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var pickupsTableView: UITableView!
    @IBOutlet weak var filterOffView: UIView!
    @IBOutlet weak var filterOffButton: UIButton!
    @IBOutlet weak var filterOnButton: UIButton!
    @IBOutlet var sortBySwitch: UISwitch!
    @IBOutlet var sortByLabel: UILabel!
    @IBOutlet var sortByActivityIndicator: UIActivityIndicatorView!
    
    // MARK: - Helper Properties
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.refreshPickupsFromPull),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = Appearance.greenColor
        return refreshControl
    }()
    lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter
    }()
    lazy var mapController:HomeMapController? = {
        return (navigationController?.viewControllers[0].childViewControllers[0] as? HomeMapController) ?? nil
    }()
    lazy var completedViewController: CompletedListViewController? = {
       return self.parent?.childViewControllers[1] as? CompletedListViewController ?? nil
    }()
    fileprivate var filterOffButtonInitialPosition = CGPoint.zero
    fileprivate var routeSort = RouteSort.timeThenDistance
    
    //MARK: Properties
    var dispatchGroup = DispatchGroup()
    var router:NMACoreRouter?
    var pickupRequests = [PickupRequest](){
        didSet{
            filterPickups()
            toDoPickupsDelegate?.numberOfToDoPickups(self.filteredPickupRequests.count)
            seperatePickupsRequestsIntoSections()
            delay(delay: 0.3) {
                self.passPickupsToSlider()
            }
        }
    }
    
    var filteredPickupRequests:[PickupRequest] = []
    var startTimeFilter:Date = Date.now()
    var endTimeFilter:Date = Date.now().addingTimeInterval(24*60*60)
    var createdByFilters:[Int] = [2,3]
    var pickupStatusFilters:[PickupStatusType] = [.postponed, .canceled, .overdue, .assigned, .completedWithDelays, .completedWithoutDelays, .ongoing, .paused]
    var pickupDateSections:[String] = []
    var pickupRequestsBySections: [String:[PickupRequest]] = [:]
    var pickupRequestsByDistanceSections: [(key: String, value: PickupRequest)] = [(key: String, value: PickupRequest)]()
    var tempPickupRequestsByDistanceSections:[PickupRequest] = []
    var isFilterOn = false {
        didSet{
            showFilterButton(isFilterOn ? true : false)
        }
    }
    var routes:[PickupRequest : NMARoute] = [:]
    weak var toDoPickupsDelegate:ToDoPickupsDelegate?
    weak var pickupDelegate:PickupDelegate?
    
    //MARK: Life cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupUI()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addObservers()
        initPickupsTableView()
        addNotificationObservers()
    }
    
    //MARK: - Initial functions
    func addObservers(){
        registerNotification(notification: Notification.Name.changeTheme, selector: #selector(self.setupUI))
    }
    
    @objc func setupUI(){
        let isNightMode = ThemeManager.isNightMode()
        backgroundView.backgroundColor = isNightMode ? Appearance.backgroundViewNight : UIColor(red255: 242, green255: 242, blue255: 242)
        sortByLabel.textColor = isNightMode ? UIColor.white : UIColor.darkGray
//        pickupsTableView.reloadData()
    }
    
    func initPickupsTableView(){
        pickupsTableView.delegate = self
        pickupsTableView.dataSource = self
        pickupsTableView.register(PickupCell.self)
        pickupsTableView.insertSubview(refreshControl, at: 0)
        pickupsTableView.rowHeight = UITableViewAutomaticDimension
        pickupsTableView.estimatedRowHeight = 130
    }
    
    
    
    func addNotificationObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.getPickups), name: Notification.Name("refreshPickups"), object: nil)
    }
    
    // MARK: - IBActions
    @IBAction func openFiltersButtonPressed(_ sender: UIButton) {
        print("DEBUG: Opening Filters")
        let pickupFiltersViewController = PickupFiltersViewController.create()
        pickupFiltersViewController.delegate = self
        pickupFiltersViewController.configuredStartDate = startTimeFilter
        pickupFiltersViewController.configuredEndDate = endTimeFilter
        pickupFiltersViewController.createdByFilters = createdByFilters
        pickupFiltersViewController.pickupStatusFilters = pickupStatusFilters
        pickupFiltersViewController.pickupRequests = self.pickupRequests
        push(pickupFiltersViewController)
    }
    @IBAction func sortBySwitchPressed(_ sender: UISwitch) {
        sortByLabel.text = sender.isOn ? "Order by distance" : "Order by time than distance"
        sortByLabel.textColor = sender.isOn ? Appearance.greenColor : ThemeManager.isNightMode() ? UIColor.white : UIColor.darkGray
        routeSort = sender.isOn ? RouteSort.distance : RouteSort.timeThenDistance
        
        routeOptimization()
    }
    
    @IBAction func hideFiltersButtonPressed(_ sender: UIButton) {
        isFilterOn = false
    }
    
    
    
    // MARK: - Functions
    
    @objc fileprivate func getPickups(completionHandler: (()->Void)?) {
        pickupRequests.removeAll()
        
        PickupREST.getPickupRequests(isFilterOn ? .all : .current) { (success, data, error) in
            if success, let data = data {
                for pickup in data {
                    if pickup.pickupStatus != PickupStatusType.completedWithoutDelays && pickup.pickupStatus != PickupStatusType.completedWithDelays {
                        self.pickupRequests.append(pickup)
                    }
                }
                self.filteredPickupRequests = self.pickupRequests
                self.toDoPickupsDelegate?.numberOfToDoPickups(self.filteredPickupRequests.count)
                self.seperatePickupsRequestsIntoSections()
                self.passPickupsToSlider()
                completionHandler?()
            }
        }
    }
    
    @objc func refreshPickupsFromPull(){
        guard let completedPickupsViewController = self.parent as? HomeListController else {
            print("Couldn't get parent !")
            return
        }
        completedPickupsViewController.getPickups { (success) in
            print("Succesfully refreshed pickups on HomeListController via Pull to Refresh")
        }
    }
    
    func seperatePickupsRequestsIntoSections(){
        pickupDateSections.removeAll()
        pickupRequestsBySections.removeAll()
        createSections()
        fillSections()
        routeOptimization()
    }
    
    func createSections(){
        for pickup in filteredPickupRequests {
            let date = pickup.startPickupTime
            dateFormatter.dateFormat = "MM-dd-yyyy"
            let key = dateFormatter.string(from: date!)
            if !pickupDateSections.contains(key){
                pickupDateSections.append(key)
            }
        }
    }
    
    func fillSections(){
        for key in pickupDateSections {
            pickupRequestsBySections[key] = filteredPickupRequests.filter({ (pickup) -> Bool in
                dateFormatter.dateFormat = "MM-dd-yyyy"
                let formattedDate = dateFormatter.string(from: pickup.startPickupTime!)
                return key == formattedDate
            })
        }
        reloadRowsAndSections()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    }
    
    func routeOptimization(){
        if routeSort == .timeThenDistance {
            var pickups:[PickupRequest] = []
            for dateSection in pickupDateSections {
                pickups = pickupRequestsBySections[dateSection] ?? []
                if sameTimePickupsExistIn(pickups){
                    sortByActivityIndicator.alpha = 1
                    sortByActivityIndicator.startAnimating()
                    calculateRouteDistance(forPickupAt: pickups.count-1, from: pickups)
                }
            }
        } else {
            pickupRequestsByDistanceSections.removeAll()
            let pickupsToBeOptimized = pickupRequests.filter({($0.startPickupTime ?? Date.now()).isToday || ($0.startPickupTime ?? Date.now()).isInThePast })
            if !pickupsToBeOptimized.isEmpty {
                sortByActivityIndicator.alpha = 1
                sortByActivityIndicator.startAnimating()
                calculateRouteDistance(forPickupAt: pickupsToBeOptimized.count-1, from: pickupsToBeOptimized)
            } else {
                for pickup in self.pickupRequests {
                    let formattedDate = self.dateFormatter.string(from: pickup.startPickupTime!)
                    self.pickupRequestsByDistanceSections.append((formattedDate, pickup))
                }
                self.reloadRowsAndSections()
                self.passPickupsToSlider()
            }
        }
        
    }
    
    func sameTimePickupsExistIn(_ pickups:[PickupRequest]) -> Bool{
        var pickupsOnTheSameTimeExists = false
        for outterPickup in pickups {
            for innerPickup in pickups {
                if innerPickup.startPickupTime == outterPickup.startPickupTime && innerPickup.id != outterPickup.id {
                    pickupsOnTheSameTimeExists = true
                    return pickupsOnTheSameTimeExists
                }
            }
        }
        return pickupsOnTheSameTimeExists
    }
    
    func calculateRouteDistance(forPickupAt i:Int, from pickups: [PickupRequest]){
        dispatch {
            
            if i > -1 {
                self.dispatchGroup.enter()
                
                var stops:[NMAGeoCoordinates] = []
                // Create points A and B
                // A = user's location
                // B = pickup's location
                
                // Initializing A
                if self.mapController?.userLocation != nil {
                    let currentLocation:NMAGeoCoordinates = NMAGeoCoordinates( latitude: Double((self.mapController?.userLocation?.coordinate.latitude)!), longitude: (self.mapController?.userLocation?.coordinate.longitude)!)
                    stops.append(currentLocation)
                }
                // Initializing B
                let nextStop:NMAGeoCoordinates = NMAGeoCoordinates(latitude: Double(pickups[i].latitude!), longitude: Double(pickups[i].longitude!))
                stops.append(nextStop)
                if self.router == nil {
                    self.router = NMACoreRouter()
                }
                // Create routing mode for route calculation
                guard let routingType = NMARoutingType(rawValue: UInt(NavigationManager.routingType)) else {return}
                guard let transportMode = NMATransportMode(rawValue: UInt(NavigationManager.transportMode)) else {return}
                let routingOptions = NavigationManager.getRoutingOptions()
                
                let routingMode = NMARoutingMode(routingType: routingType,
                                                 transportMode: transportMode,
                                                 routingOptions: routingOptions)
                if NavigationManager.truckLength > 0,
                   NavigationManager.truckHeight > 0 {
                    routingMode.vehicleHeight = Float(NavigationManager.truckLength)
                    routingMode.vehicleHeight = Float(NavigationManager.truckHeight)
                }
                // Calculate the route from A to B
                self.router?.calculateRoute(withStops: stops, routingMode: routingMode, { (result, error) in
                    
                    if let result = result?.routes?.first {
                        let pickup = pickups[i]
                        self.routes[pickup] = result
                    }
                    self.dispatchGroup.leave()
                    // Call function recursively until there is no more pickups to calculate it's route
                    self.calculateRouteDistance(forPickupAt: i-1, from: pickups)
                })
            } else {
                self.sortByActivityIndicator.alpha = 0
                self.sortByActivityIndicator.stopAnimating()
                // After all route calculations have been done
                self.dispatchGroup.notify(queue: .main) {
                    // Sort the routes based on it's length and get the first route
                    // It's key is the nearest pickup
                    var tempRoutes:[(key: PickupRequest, value: NMARoute)] = [(key: PickupRequest, value: NMARoute)]()
                    if self.routeSort == .distance {
                        tempRoutes = self.routes.sorted(by: { $0.value.length < $1.value.length })
                    } else {
                        tempRoutes = self.routes.sorted(by: { $0.value.length < $1.value.length && $0.key.startPickupTime! < $1.key.startPickupTime! })
                    }
                    self.dateFormatter.dateFormat = "MM-dd-yyyy"
                    let formattedDate = self.dateFormatter.string(from: pickups.first!.startPickupTime!)
                    var tempPickups:[PickupRequest] = []
                    for route in tempRoutes {
                        tempPickups.append(route.key)
                    }
                    if self.routeSort == .timeThenDistance {
                        if self.pickupRequestsBySections[formattedDate]?.count == tempPickups.count {
                            self.pickupRequestsBySections[formattedDate] = tempPickups
                        }
                    } else {
                        // If all the routes have been calculated
                        if pickups.count == tempPickups.count {
                            self.pickupRequestsByDistanceSections.removeAll()
                            self.tempPickupRequestsByDistanceSections.removeAll()
                            for pickup in tempPickups {
                                self.dateFormatter.dateFormat = "MM-dd-yyyy"
                                let formattedDate = self.dateFormatter.string(from: pickups.first!.startPickupTime!)
                                self.pickupRequestsByDistanceSections.append((formattedDate, pickup))
                            }
                            for pickup in self.tempPickupRequestsByDistanceSections {
                                self.dateFormatter.dateFormat = "MM-dd-yyyy"
                                let formattedDate = self.dateFormatter.string(from: pickups.first!.startPickupTime!)
                                self.pickupRequestsByDistanceSections.append((formattedDate, pickup))
                            }
                            let pickupsToBeShifted = self.pickupRequests.filter({!($0.startPickupTime ?? Date.now()).isInToday && !($0.startPickupTime ?? Date.now()).isInThePast})
                            if !pickupsToBeShifted.isEmpty {
                                for pickup in pickupsToBeShifted {
                                    let formattedDate = self.dateFormatter.string(from: pickups.first!.startPickupTime!)
                                    self.pickupRequestsByDistanceSections.append((formattedDate, pickup))
                                }
                            }
                        } else {
                            // If any error ocurred while calculating routes,
                            // where one of them is skipped, just reset routes
                            self.pickupRequestsByDistanceSections.removeAll()
                            for pickup in self.pickupRequests {
                                let formattedDate = self.dateFormatter.string(from: pickups.first!.startPickupTime!)
                                self.pickupRequestsByDistanceSections.append((formattedDate, pickup))
                            }
                        }
                    }
                    self.reloadRowsAndSections()
                    self.routes.removeAll()
                    self.passPickupsToSlider()
                    print("DEBUG: Route optimized")
                }
            }
        }
    }
    
    func passPickupsToSlider(){
        var pickups:[PickupRequest] = []
        if routeSort == .timeThenDistance {
            for key in pickupDateSections {
                if let sectionPickups = pickupRequestsBySections[key] {
                    for pickup in sectionPickups {
                        pickups.append(pickup)
                    }
                }
            }
        } else {
            for i in 0..<pickupRequestsByDistanceSections.count {
                let pickup = pickupRequestsByDistanceSections[i].value
                pickups.append(pickup)
            }
        }
        
        // Get sorted completed pickups
        if let completedViewController = completedViewController {
            for key in completedViewController.pickupDateSections {
                if let sectionPickups = completedViewController.pickupRequestsBySections[key] {
                    for pickup in sectionPickups {
                        pickups.append(pickup)
                    }
                }
            }
        }
        
        NotificationCenter.default.post(name: Notification.Name.transferPickups, object: self, userInfo: ["pickups": pickups])
    }
    
    
    func reloadRowsAndSections(){
        dispatch {
            self.pickupsTableView.beginUpdates()
            // Remove current sections and fields if there is any
            if self.pickupsTableView.numberOfSections > 0 {
                for i in 0..<self.pickupsTableView.numberOfSections{
                    self.pickupsTableView.deleteSections(IndexSet(integer: i), with: .none)
                    let numberOfRows = self.pickupsTableView.numberOfRows(inSection: i)
                    for j in 0..<numberOfRows{
                        self.pickupsTableView.deleteRows(at: [IndexPath(row: j, section: i)], with: .none)
                    }
                }
            }
            if self.routeSort == .timeThenDistance {
                // Add new sections and rows
                let numberOfSections = self.pickupDateSections.count
                for i in 0..<numberOfSections{
                    self.pickupsTableView.insertSections(IndexSet(integer: i), with: .fade)
                    let key = self.pickupDateSections[i]
                    if let numberOfRows = self.pickupRequestsBySections[key]?.count {
                        for j in 0..<numberOfRows{
                            self.pickupsTableView.insertRows(at: [IndexPath(row: j, section: i)], with: .fade)
                        }
                    }
                }
            } else {
                let numberOfSections = self.pickupRequestsByDistanceSections.count
                for i in 0..<numberOfSections{
                    self.pickupsTableView.insertSections(IndexSet(integer: i), with: .fade)
                    for j in 0..<1{
                        self.pickupsTableView.insertRows(at: [IndexPath(row: j, section: i)], with: .fade)
                    }
                }
            }
            
            self.refreshControl.endRefreshing()
            self.pickupsTableView.endUpdates()
        }
    }
    
    func isAnyPickupOnGoing(completion: @escaping (_ id: Int,_ state:Bool)->Void){
        for pickup in pickupRequests {
            if pickup.pickupStatus == .ongoing {
                completion(pickup.id!,true)
                return
            }
        }
        completion(-1,false)
    }
    
    func startPickupWith(_ id:Int, _ route:String, completion: @escaping (_ didComplete:Bool)->Void){
        showHud()
        isAnyPickupOnGoing { (cancelPickupId, status) in
            if status{
                self.hideHud()
                dispatch {
                    let startNewPickupVC = StartNewPickupConfirmViewController.create()
                    startNewPickupVC.currentPickupId = cancelPickupId
                    startNewPickupVC.newPickupId = id
                    startNewPickupVC.modalPresentationStyle = .overCurrentContext
                    startNewPickupVC.delegate = self
                    self.showModal(startNewPickupVC, animated: false, completion: nil)
                }
            } else {
                PickupREST.startPickup(id, route) { (success, error) in
                    self.hideHud()
                    if success {
                        print("The new pickup successfully started")
                        completion(true)
                    } else {
                        print(#function)
                        print("Error on completePickup : \(String(describing: error))")
                        completion(false)
                    }
                }
            }
        }
    }
    
    func showFilterButton(_ status:Bool){
        if status {
            UIView.animate(withDuration: 0.3) {
                self.filterOffButton.alpha = 1
                self.filterOnButton.alpha = 0
                self.filterOffView.frame.size.width = 125
                self.filterOffView.alpha = 1
            }
        } else {
            self.getPickups {
                self.resetFilterSettings()
            }
            UIView.animate(withDuration: 0.3) {
                self.filterOffButton.alpha = 0
                //filterOffButtonInitialPosition
                self.filterOffView.frame.size.width = self.filterOnButton.frame.width
                self.filterOffView.alpha = 0
                self.filterOnButton.alpha = 1
            }
        }
    }
    
    func resetFilterSettings(){
        self.startTimeFilter = self.pickupRequests.min { (pickup, nextPickup) -> Bool in
            return pickup.startPickupTime! < nextPickup.startPickupTime!
        }?.startPickupTime ?? Date.now()
        
        self.endTimeFilter = self.pickupRequests.max { (pickup, nextPickup) -> Bool in
            return pickup.endPickupTime! < nextPickup.endPickupTime!
        }?.endPickupTime ?? Date.now().addingTimeInterval(24*60*60)
        self.createdByFilters = [2,3]
        self.pickupStatusFilters = [.overdue, .postponed, .canceled, .assigned]
        self.filterPickups()
    }
    
    func filterPickups(){
        var filteredPickups:[PickupRequest] = []
        for pickup in pickupRequests {
            if let startTime = pickup.startPickupTime, let endtime = pickup.endPickupTime {
                if startTime >= startTimeFilter && endtime <= endTimeFilter {
                    filteredPickups.append(pickup)
                }
            }
        }
        for pickup in filteredPickups {
            if !createdByFilters.contains(pickup.requestedBy?.rawValue ?? 0){
                if let index = filteredPickups.firstIndex(of: pickup) {
                    filteredPickups.remove(at: index)
                }
            }
        }
        
        for pickup in filteredPickups {
            if !pickupStatusFilters.contains(pickup.pickupStatus){
                if let index = filteredPickups.firstIndex(of: pickup) {
                    filteredPickups.remove(at: index)
                }
            }
        }
        self.filteredPickupRequests = filteredPickups
        self.reloadRowsAndSections()
    }
}

//MARK: TableView Extensions
extension ToDoListViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if routeSort == .timeThenDistance {
            return pickupDateSections.count
        } else {
            return pickupRequestsByDistanceSections.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if routeSort == .timeThenDistance {
            return self.pickupRequestsBySections[pickupDateSections[section]]?.count ?? 1
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = pickupsTableView.dequeue(PickupCell.self, for: indexPath)
        var pickupRequest:PickupRequest
        if routeSort == .timeThenDistance {
            let key = pickupDateSections[indexPath.section]
            pickupRequest = pickupRequestsBySections[key]![indexPath.row]
        } else {
            pickupRequest = pickupRequestsByDistanceSections[indexPath.section].value
        }
        
        cell.delegate = self
        // Check if it's first pickup
        let firstSectionKey = pickupDateSections[0]
        cell.isFirstPickup = pickupRequest == pickupRequestsBySections[firstSectionKey]?.first
        
        // Check if it's last pickup
        let lastSection = pickupRequestsBySections.count-1
        let lastSectionKey = pickupDateSections[lastSection]
        
        cell.isLastPickup = pickupRequest == pickupRequestsBySections[lastSectionKey]?.last
        
        // Asign current pickup to cell
        cell.pickupRequest = pickupRequest
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? PickupCell else { return }
        print("type: \(cell.pickupRequest.pickupStatusName!)")
        let pickupRequest = cell.pickupRequest
        //Check Pickup type and open their custom View Controller
        switch cell.pickupRequest.pickupStatus {
        // Assigned Pickup
        case .assigned:
            let vc:CurrentPickupViewController = CurrentPickupViewController.create()
            vc.pickupRequest = pickupRequest
            vc.modalPresentationStyle = .overCurrentContext
            vc.cancelPickupDelegate = self
            dispatch {
                self.present(vc, animated: false, completion: {
                    print("AssignedPickupViewController to be presented")
                })
            }
            break
        // Cancelled Pickup
        case .canceled:
            let vc:CancelledPickupViewController = CancelledPickupViewController.create()
            vc.pickupRequest = pickupRequest
            vc.modalPresentationStyle = .overCurrentContext
            vc.delegate = self
            dispatch {
                self.present(vc, animated: false, completion: {
                    print("CancelledPickupViewController to be presented")
                })
            }
            break
        case .overdue:
            let vc:OverduePickupViewController = OverduePickupViewController.create()
            vc.pickupRequest = pickupRequest
            vc.modalPresentationStyle = .overCurrentContext
            vc.overdueDelegate = self
            vc.confirmDelegate = self
            dispatch {
                self.present(vc, animated: false, completion: {
                    print("OverduePickupViewController to be presented")
                })
            }
            break
        default:
            break
        }
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let isNightMode = ThemeManager.isNightMode()
        let grayColor = UIColor(red255: 93, green255: 93, blue255: 100)
        let view = UIView(frame: CGRect(x: 0,
                                        y: 0,
                                        width: self.view.frame.width,
                                        height: 40))
        view.backgroundColor = isNightMode ? Appearance.backgroundViewNight : UIColor(red255: 242, green255: 242, blue255: 242)
        let dateLabel = UILabel(frame: CGRect(x: 50,
                                              y: 0,
                                              width: view.frame.width,
                                              height: view.frame.height))
        if routeSort == .timeThenDistance {
            dateLabel.text = pickupDateSections[section]
        } else {
            dateFormatter.dateFormat = "MM-dd-yyyy"
            dateLabel.text = dateFormatter.string(from: pickupRequestsByDistanceSections[section].value.startPickupTime ?? Date.now())
        }
        
        dateLabel.textColor = isNightMode ? .white : grayColor
        dateLabel.clipsToBounds = true
        let lineView = UIView(frame: CGRect(x: 10,
                                            y: view.frame.height / 2,
                                            width: 30,
                                            height: 1))
        lineView.backgroundColor = isNightMode ? .white : grayColor
        lineView.alpha = 0.3
        
        view.addSubview(dateLabel)
        view.addSubview(lineView)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    
    
}

//MARK: - View controller extension
extension ToDoListViewController{
    static func create() -> HomeListController{
        return UIStoryboard.home.instantiate(HomeListController.self)
    }
}

//MARK: - Canceled/UndoneCancel for Pickup delegate
extension ToDoListViewController: CancelPickupDelegate, CancelledPickupDelegate, OverduePickupDelegate, StartNewPickupDelegate {
    func startNewPickup(_ state: Bool, _ newPickupId: Int, _ cancelPickupId: Int) {
        if state {
            self.showHud()
            PickupREST.startAnotherPickup(newPickupId, cancelPickupId) { (success, error) in
                if success {
                    self.hideHud()
                    print("Starting another pickup")
                    self.pickupDelegate?.triggerNavigation(newPickupId, "")
                }
            }
        }
    }
    
    // Delegate comes from CurrentPickupViewController,
    // when pickup is canceled
    func isPickupCanceled(_ status: Bool) {
        if status {
            pickupDelegate?.refreshPickupDelegateFromCell()
        }
    }
    // Delegate comes from CancelledPickupViewController
    // when pickup cancel is undone
    func isCancelPickupUndone(_ status: Bool) {
        if status {
            pickupDelegate?.refreshPickupDelegateFromCell()
        }
    }
    
    //Delegate come from CurrentPickupViewController
    func startPickup(_ status: Bool, _ id: Int, _ route:String) {
        if status {
            startPickupWith(id, route) { (isCompleted) in
                if isCompleted {
                    print("Pickup started")
                    self.pickupDelegate?.triggerNavigation(id, route)
                }
            }
        }
    }
    
    
    // Delegate comes from CancelledPickupViewController
    // when pickup cancel is undone
    func isOverduedPickupCanceled(_ status: Bool) {
        if status {
            pickupDelegate?.refreshPickupDelegateFromCell()
        }
    }
    
    //Delegate comes from OverduePickupViewController
    func startOverduePickup(_ status: Bool, _ id: Int, _ route:String) {
        if status {
            startPickupWith(id, route) { (isCompleted) in
                if isCompleted {
                    print("Pickup started")
                    self.pickupDelegate?.triggerNavigation(id, route)
                }
            }
        }
    }
    
    func isOverduePickupCompleted(_ status: Bool) {
        if status {
            pickupDelegate?.refreshPickupDelegateFromCell()
        }
    }
    
    func isPickupCompleted(_ status: Bool) {
        if status {
            pickupDelegate?.refreshPickupDelegateFromCell()
        }
    }
}

extension ToDoListViewController: PickupFiltersDelegate{
    
    func filteredPickups(pickups: [PickupRequest], startDate: Date, endDate: Date, createdByFilters: [Int], pickupStatusFilters: [PickupStatusType]) {
        self.startTimeFilter = startDate
        self.endTimeFilter = endDate
        self.createdByFilters = createdByFilters
        self.pickupStatusFilters = pickupStatusFilters
        self.filteredPickupRequests = pickups
        self.toDoPickupsDelegate?.numberOfToDoPickups(self.filteredPickupRequests.count)
        self.seperatePickupsRequestsIntoSections()
        self.isFilterOn = true
    }
    
//    func filteredPickups(_ pickups: [PickupRequest]) {
//        self.filteredPickupRequests = pickups
//        self.toDoPickupsDelegate?.numberOfToDoPickups(self.filteredPickupRequests.count)
//        self.seperatePickupsRequestsIntoSections()
//    }
}


// Protocol extensions
protocol ToDoPickupsDelegate:class {
    func numberOfToDoPickups(_ value:Int)
}

extension ToDoListViewController: PickupDelegate, ConfirmPickupActionDelegate {
    func editPickup(_ pickup: PickupRequest) {
        let addPickupViewController = AddPickupViewController.create()
        addPickupViewController.pickupMode = .edit
        let convertetPickup = PMPickupRequest(pickup)
        addPickupViewController.pickupRequest = convertetPickup
        push(addPickupViewController)
    }
    
    func refreshPickups() {
        pickupDelegate?.refreshPickupDelegateFromCell()
    }
    
    func refreshPickupDelegateFromCell() {
        pickupDelegate?.refreshPickupDelegateFromCell()
    }
    
    func triggerNavigation(_ id: Int, _ route: String) {
        startPickupWith(id, route) { (isCompleted) in
            if isCompleted {
                print("Pickup started")
                self.pickupDelegate?.triggerNavigation(id, route)
            }
        }
    }
}

//extension ToDoListViewController: AddPickupTimingDelegate {
//    func applyCreatedPickupDateToFilter(startDate: Date, endDate: Date) {
//        if startDate < self.startTimeFilter {
//            startTimeFilter = startDate
//        }
//        if endDate > self.endTimeFilter {
//            endTimeFilter = endDate
//        }
//    }
//}
