//
//  ConfirmationViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 3/22/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

class ConfirmationViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet weak var textLabel: UILabel!
    
    // MARK: - Properties
    var timer:Timer?
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        timer = Timer(timeInterval: 2, target: self, selector: #selector(goBack), userInfo: nil, repeats: false)
    }
    
    // MARK: - IBActions
    @objc fileprivate func goBack() {
        self.dismiss(animated: true) {
            self.timer?.invalidate()
            self.timer = nil
        }
    }
    
    // MARK: - Functions
    func setText(_ text:String){
        textLabel.text = text
    }
}
