//
//  CancellPickupViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 2/28/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

protocol CancelPickupStatusDelegate:class {
    func shouldCancelPickup(_ status:Bool)
}

class CancellPickupViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var chooseCancelReasonLabel: UILabel!
    @IBOutlet weak var tickFullCapacity: UIImageView!
    @IBOutlet weak var tickRoadToThisPickupBlocked: UIImageView!
    @IBOutlet weak var tickCanNotFindTrash: UIImageView!
    @IBOutlet weak var tickPostpone: UIImageView!
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var cancelDetails: UITextView!
    @IBOutlet weak var btnFullCapacity: UIButton!
    @IBOutlet weak var btnRoadToThisPickupBlocked: UIButton!
    @IBOutlet weak var btnCanNotFindTrash: UIButton!
    @IBOutlet weak var btnPostpone: UIButton!
    @IBOutlet weak var btnSendFeedback: UIButton!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var reasonsListView: UIView!
    @IBOutlet weak var reasonDescriptionView: UIView!
    
    // MARK: - Properties
    var cancelReason:Int = 0
    var buildingId:Int!
    weak var cancelDelegate: CancelPickupStatusDelegate?
    weak var overdueDelegate: OverduePickupDelegate?
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        resetSelections()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupUI()
    }
    
    // MARK: - IBActions
    @IBAction func sendFeedbackPressed(_ sender: UIButton) {
        showHud()
        PickupREST.cancelPickup(buildingId, cancelReason, cancelDetails.text) { (success, error) in
            self.hideHud()
            if success {
                self.cancelDelegate?.shouldCancelPickup(true)
                self.overdueDelegate?.isOverduedPickupCanceled(true)
            } else {
                self.overdueDelegate?.isOverduedPickupCanceled(false)
                self.cancelDelegate?.shouldCancelPickup(false)
            }
            if self.modalPresentationStyle == .overCurrentContext {
                self.hideModal()
            } else {
                self.pop()
            }
        }
    }
    
    @IBAction func cancelPressed(_ sender: UIButton) {
        print(#function)
        dispatch {
            self.dismiss(animated: true) {
                //	
            }
        }
    }
    
    @IBAction func selectCancellOption(_ sender: UIButton) {
        print(#function)
        self.resetSelections()
        switch sender.tag {
        case 1:
            tickFullCapacity.isHidden = false
            cancelReason = 1
            btnFullCapacity.setTitleColor(Appearance.greenColor, for: .normal)
            print("2. Full/Out of capacity")
        case 2:
            tickRoadToThisPickupBlocked.isHidden = false
            cancelReason = 2
            btnRoadToThisPickupBlocked.setTitleColor(Appearance.greenColor, for: .normal)
            print("2. Road to this pickup is blocked")
        case 3:
            tickCanNotFindTrash.isHidden = false
            cancelReason = 3
            btnCanNotFindTrash.setTitleColor(Appearance.greenColor, for: .normal)
            print("2. Can't find trash")
        case 4:
            tickPostpone.isHidden = false
            cancelReason = 4
            btnPostpone.setTitleColor(Appearance.greenColor, for: .normal)
            print("2. Can't find trash")
        default:
            print("2. None")
        }
        self.validateFields()
    }
    
    @IBAction func resignFirstResponder(_ sender: UITapGestureRecognizer) {
        self.cancelDetails.resignFirstResponder()
    }
    
    
    // MARK: - Functions
    func setupUI(){
        if ThemeManager.isNightMode() {
            headerView.backgroundColor = Appearance.darkGrayNight
            mainView.backgroundColor = Appearance.backgroundViewNight
            chooseCancelReasonLabel.textColor = UIColor.white
            reasonsListView.backgroundColor = Appearance.darkGrayNight
            btnFullCapacity.setTitleColor(UIColor.white, for: .normal)
            btnRoadToThisPickupBlocked.setTitleColor(UIColor.white, for: .normal)
            btnCanNotFindTrash.setTitleColor(UIColor.white, for: .normal)
            reasonDescriptionView.backgroundColor = Appearance.darkGrayNight
            cancelDetails.textColor = UIColor.white
        }
    }
    
    func resetSelections(){
        print("1. Resetting selections")
        tickFullCapacity.isHidden = true
        tickRoadToThisPickupBlocked.isHidden = true
        tickCanNotFindTrash.isHidden = true
        tickPostpone.isHidden = true
        let isNightMode = ThemeManager.isNightMode()
        btnFullCapacity.setTitleColor(isNightMode ? UIColor.white : Appearance.cancellPickupGray, for: .normal)
        btnRoadToThisPickupBlocked.setTitleColor(isNightMode ? UIColor.white : Appearance.cancellPickupGray, for: .normal)
        btnCanNotFindTrash.setTitleColor(isNightMode ? UIColor.white : Appearance.cancellPickupGray, for: .normal)
        btnPostpone.setTitleColor(isNightMode ? UIColor.white : Appearance.cancellPickupGray, for: .normal)
        
    }
    
    func validateFields(){
        print("3. Validating Fields")
        guard cancelReason != 0 else { return }
        guard let cancelDetails = cancelDetails.text, cancelDetails != "" && cancelDetails != "Reason description" else { return }
        btnSendFeedback.isEnabled = true
        btnSendFeedback.alpha = 1.0
    }
}

//MARK: View controller extensions
extension CancellPickupViewController{
    static func create() -> CancellPickupViewController{
        return UIStoryboard.home.instantiate(CancellPickupViewController.self)
    }
}

// MARK: - Text view delegate
extension CancellPickupViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.text = ""
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "Reason description"
        }
        validateFields()
    }
}


