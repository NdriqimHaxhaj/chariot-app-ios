//
//  AssignedPickupViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 2/21/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import SCLAlertView

class UncompletePickupViewController: UIViewController {
    //MARK: Outlets
    @IBOutlet var headerTap: UITapGestureRecognizer!
    @IBOutlet var iconTap: UITapGestureRecognizer!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var contentScrollView: UIScrollView!
    @IBOutlet weak var footerView: UIView!
    
    //MARK: Constraints
    @IBOutlet weak var scrollViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var footerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var closeButtonBottomConstraint: NSLayoutConstraint!
    
    //MARK: Labels
    @IBOutlet weak var buildingNameLabel: UILabel!
    @IBOutlet weak var bagCountLabelLabel: UILabel!
    @IBOutlet weak var preferredPickupTimeLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var pickupTypeLabel: UILabel!
    
    //Helper variables
    lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter
    }()
    
    //MARK: Variables
    var pickupRequest:PickupRequest!
//    weak var delegate:UncancelPickupPickupDelegate?
    
    //MARK: Life cycle
    override func viewWillAppear(_ animated: Bool) {
        self.backgroundView.backgroundColor = UIColor.clear
        self.scrollViewTopConstraint.constant = self.view.frame.height + 30
        self.closeButtonBottomConstraint.constant = -50
        self.footerViewHeightConstraint.constant = 0
        self.view.layoutIfNeeded()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTaps()
        showSelf()
        updateLabels()
    }
    
    //MARK: Initial functions
    func setupTaps(){
        headerTap.addTarget(self, action: #selector(UncompletePickupViewController.hideSelf(_:)))
        iconTap.addTarget(self, action: #selector(UncompletePickupViewController.hideSelf(_:)))
    }
    
    func updateLabels(){
        self.buildingNameLabel.text = pickupRequest?.buildingName
        self.bagCountLabelLabel.text = "\(pickupRequest.bagCount!)"
        self.dateFormatter.dateFormat = "hh:mm a"
        self.preferredPickupTimeLabel.text = dateFormatter.string(from: (pickupRequest?.startPickupTime)!)
        self.locationLabel.text = pickupRequest?.loadLocationName
        self.pickupTypeLabel.text = pickupRequest?.loadTypesNames?.joined(separator: ", ")
    }
    
    //MARK: IBActions
    @IBAction func cancelPickupPressed(_ sender: UIButton) {
        print(#function)
    }
    
    @IBAction func startPickupPressed(_ sender: UIButton) {
        print(#function)
    }
    
    @IBAction func seeNotesPressed(_ sender: UIButton) {
        print(#function)
    }
    
    @IBAction func callSuperintendentPressed(_ sender: UIButton) {
        print(#function)
    }
    
    @IBAction func closeButtonPressed(_ sender: UIButton) {
        print(#function)
        self.hideSelf()
    }
    
    @IBAction func markupPickupAsUncompletedPressed(_ sender: UIButton) {
        print(#function)
        showHud()
        //  Here comes request
        //  If success
        let confirmPickupCompletionViewController = ConfirmPickupCompletionViewController.create()
        confirmPickupCompletionViewController.confirmType = ConfirmType.uncomplete
        confirmPickupCompletionViewController.pickupId = "\(String(describing: pickupRequest.id))"
        confirmPickupCompletionViewController.modalPresentationStyle = .overCurrentContext
        self.showModal(confirmPickupCompletionViewController)
        print("Uncompletion was successfully made")
        
        hideSelf()
        
    }
    
    //MARK: Show/Hide
    @objc func hideSelf(_ gestureRecognizer: UITapGestureRecognizer = UITapGestureRecognizer()){
        dispatch {
            self.scrollViewTopConstraint.constant = self.view.frame.height + 30
            self.footerViewHeightConstraint.constant = 0
            self.closeButtonBottomConstraint.constant = -50
            UIView.animate(withDuration: 0.2, animations: {
                self.view.layoutIfNeeded()
            }, completion: { (_) in
                UIView.animate(withDuration: 0.2, animations: {
                    self.backgroundView.backgroundColor = UIColor.clear
                }, completion: { (_) in
                    self.dismiss(animated: false, completion: {
                        print("Dismissed CurrentPickupViewController")
                    })
                })
            })
        }
    }
    
    func showSelf(){
        dispatch {
            self.scrollViewTopConstraint.constant = 150
            self.footerViewHeightConstraint.constant = 100
            self.closeButtonBottomConstraint.constant = 20
            UIView.animate(withDuration: 0.2, animations: {
                self.backgroundView.backgroundColor = UIColor.black
                self.view.layoutIfNeeded()
            }, completion: { (_) in
                print("Presented CurrentPickupViewController")
            })
        }
    }
}

//MARK: View controller extensions
extension UncompletePickupViewController {
    static func create() -> UncompletePickupViewController{
        return UIStoryboard.home.instantiate(UncompletePickupViewController.self)
    }
}

protocol UncancelPickupPickupDelegate {
    func uncompletePickup()
}
