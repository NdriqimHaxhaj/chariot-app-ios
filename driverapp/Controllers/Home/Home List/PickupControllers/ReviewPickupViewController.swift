//
//  ReviewPickupViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 8/7/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import IHKeyboardAvoiding

class ReviewPickupViewController: UIViewController {
    //MARK: Outlets
    @IBOutlet var headerTap: UITapGestureRecognizer!
    @IBOutlet var iconTap: UITapGestureRecognizer!
    @IBOutlet var mainViewTap: UITapGestureRecognizer!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var contentScrollView: UIScrollView!
    @IBOutlet weak var contentScrollInerView: UIView!
    @IBOutlet weak var reviewTextView: UITextView!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var closeView: UIView!
    @IBOutlet weak var closeIcon: UIImageView!
    @IBOutlet weak var starsStack: UIStackView!
    
    //MARK: Constraints
    @IBOutlet weak var scrollViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var footerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var buttonsStackBottomConstraint: NSLayoutConstraint!
    
    //MARK: Labels
    @IBOutlet weak var buildingNameLabel: UILabel!
    @IBOutlet weak var bagCountStaticLabel: UILabel!
    @IBOutlet weak var bagCountLabel: UILabel!
    @IBOutlet weak var preferredPickupTimeStaticLabel: UILabel!
    @IBOutlet weak var preferredPickupTimeLabel: UILabel!
    @IBOutlet weak var locationStaticLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var pickupTypeStaticLabel: UILabel!
    @IBOutlet weak var pickupTypeLabel: UILabel!
    
    //Helper variables
    lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter
    }()
    
    //MARK: Variables
    var data:[AnyHashable:Any]!
    var buildingId:Int?
    var toIdUser:Int?
    var currentRating = 1
    
    //MARK: Life cycle
    override func viewWillAppear(_ animated: Bool) {
        setupUI()
        prepareForAnimation()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTaps()
        showSelf()
        updateLabels()
        setupKeyboard()
    }
    
    //MARK: Initial functions
    func setupUI(){
        let isNightMode = ThemeManager.isNightMode()
        if isNightMode {
            closeView.backgroundColor = Appearance.darkGrayNight
            closeView.borderColor = Appearance.darkGrayNight
            closeIcon.image = #imageLiteral(resourceName: "X")
            contentScrollView.backgroundColor = Appearance.darkGrayNight
            buildingNameLabel.textColor = UIColor.white
            bagCountStaticLabel.textColor = UIColor.white
            bagCountLabel.textColor = UIColor.white
            preferredPickupTimeStaticLabel.textColor = UIColor.white
            preferredPickupTimeLabel.textColor = UIColor.white
            locationStaticLabel.textColor = UIColor.white
            locationLabel.textColor = UIColor.white
            pickupTypeStaticLabel.textColor = UIColor.white
            pickupTypeLabel.textColor = UIColor.white
            reviewTextView.backgroundColor = Appearance.darkGrayNight
            footerView.backgroundColor = Appearance.darkGrayNight
        }
    }
    
    func setupKeyboard(){
        KeyboardAvoiding.avoidingView = self.view
    }
    
    func setupTaps(){
        headerTap.addTarget(self, action: #selector(self.hideSelf(_:)))
        iconTap.addTarget(self, action: #selector(self.hideSelf(_:)))
    }
    
    @IBAction func startTapped(_ sender: UITapGestureRecognizer) {
        guard let starsNumber = sender.view?.tag else {return}
        fillStars(starsNumber)
    }
    
    func fillStars(_ quantity:Int){
        for subview in starsStack.subviews {
            guard let imageView = subview as? UIImageView else {continue}
            imageView.image = UIImage(named: "star not filled")
            
        }
        for i in 0...quantity {
            guard let imageView = starsStack.subviews[i] as? UIImageView else {continue}
            imageView.image = UIImage(named: "star filled")
        }
        currentRating = quantity+1
    }
    
    
    func updateLabels(){
        self.buildingId = (data["id_building"]  as? NSString)?.integerValue
        self.toIdUser = (data["to_id_user"]  as? NSString)?.integerValue
        self.buildingNameLabel.text = (data["building_name"] as? String) ?? ""
        self.bagCountLabel.text = "\((data["bag_count"] as? NSString)?.integerValue ?? 0)"
        self.dateFormatter.dateFormat = "hh:mm a"
        self.preferredPickupTimeLabel.text = dateFormatter.string(from: ((data["pickup_occurring_timestamp"] as? Date) ?? Date.init()))
        self.locationLabel.text = (data["load_location_name"] as? String) ?? ""
        self.pickupTypeLabel.text = (data["load_types_names"] as? String) ?? ""
    }
    
    //MARK: IBActions
    @IBAction func sendReviewPressed(_ sender: UIButton) {
        self.showHud()
        
        UserREST.reviewUser(buildingId!, toIdUser!, currentRating, reviewTextView.text) { (success, error) in
            self.hideHud()
            if success {
                self.hideSelf()
            } else {
                print(error?.message)
            }
        }
    }
    
    @IBAction func closeButtonPressed(_ sender: UIButton) {
        print(#function)
        self.hideSelf()
    }
    
    //MARK: Show/Hide
    
    @objc func hideSelf(_ gestureRecognizer: UITapGestureRecognizer = UITapGestureRecognizer()){
        dispatch {
            self.scrollViewTopConstraint.constant = self.view.frame.height + 30
            self.footerViewHeightConstraint.constant = 0
            self.buttonsStackBottomConstraint.constant = -50
            UIView.animate(withDuration: 0.2, animations: {
                self.view.layoutIfNeeded()
            }, completion: { (_) in
                UIView.animate(withDuration: 0.2, animations: {
                    self.backgroundView.backgroundColor = UIColor.clear
                }, completion: { (_) in
                    self.dismiss(animated: false, completion: {
                        print("Dismissed CurrentPickupViewController")
                    })
                })
            })
        }
    }
    
    func prepareForAnimation(){
        self.backgroundView.backgroundColor = UIColor.clear
        self.scrollViewTopConstraint.constant = self.view.frame.height + 30
        self.buttonsStackBottomConstraint.constant = -50
        self.footerViewHeightConstraint.constant = 0
        self.view.layoutIfNeeded()
    }
    
    func showSelf(){
        dispatch {
            self.scrollViewTopConstraint.constant = 80
            self.footerViewHeightConstraint.constant = 100
            self.buttonsStackBottomConstraint.constant = 25
            UIView.animate(withDuration: 0.2, animations: {
                self.backgroundView.backgroundColor = UIColor.black
                self.view.layoutIfNeeded()
            }, completion: { (_) in
                print("Presented CurrentPickupViewController")
            })
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
}

extension ReviewPickupViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Write something..." {
            textView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "Write something..."
        }
    }
}

//MARK: View controller extensions
extension ReviewPickupViewController{
    static func create() -> ReviewPickupViewController{
        return UIStoryboard.home.instantiate(ReviewPickupViewController.self)
    }
}
