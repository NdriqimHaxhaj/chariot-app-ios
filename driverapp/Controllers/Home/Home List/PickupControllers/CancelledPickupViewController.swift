//
//  CancelledPickupViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 2/21/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

class CancelledPickupViewController: UIViewController {

    //MARK: Outlets
    @IBOutlet var headerTap: UITapGestureRecognizer!
    @IBOutlet var iconTap: UITapGestureRecognizer!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var contentScrollView: UIScrollView!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var closeView: UIView!
    @IBOutlet weak var closeIcon: UIImageView!
    
    //MARK: Constraints
    @IBOutlet weak var scrollViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var closeButtonBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var footerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var contentViewHeightConstraint: NSLayoutConstraint!
    
    //MARK: Labels
    @IBOutlet weak var buildingNameLabel: UILabel!
    @IBOutlet weak var bagCountStaticLabel: UILabel!
    @IBOutlet weak var bagCountLabel: UILabel!
    @IBOutlet weak var preferredPickupTimeStaticLabel: UILabel!
    @IBOutlet weak var preferredPickupTimeLabel: UILabel!
    @IBOutlet weak var locationStaticLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var pickupTypeStaticLabel: UILabel!
    @IBOutlet weak var pickupTypeLabel: UILabel!
    
    //Helper variables
    lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter
    }()
    
    //MARK: Variables
    var pickupRequest:PickupRequest!
    weak var delegate:CancelledPickupDelegate?
    
     //MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTaps()
        showSelf()
        updateLabels()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupUI()
        prepareForAnimation()
    }
    
    //MARK: Initial functions
    func setupUI(){
        let isNightMode = ThemeManager.isNightMode()
        if isNightMode {
            closeView.backgroundColor = Appearance.darkGrayNight
            closeView.borderColor = Appearance.darkGrayNight
            closeIcon.image = #imageLiteral(resourceName: "X")
            contentScrollView.backgroundColor = Appearance.darkGrayNight
            buildingNameLabel.textColor = UIColor.white
            bagCountStaticLabel.textColor = UIColor.white
            bagCountLabel.textColor = UIColor.white
            preferredPickupTimeStaticLabel.textColor = UIColor.white
            preferredPickupTimeLabel.textColor = UIColor.white
            locationStaticLabel.textColor = UIColor.white
            locationLabel.textColor = UIColor.white
            pickupTypeStaticLabel.textColor = UIColor.white
            pickupTypeLabel.textColor = UIColor.white
            footerView.backgroundColor = Appearance.darkGrayNight
        }
    }
    
    func setupTaps(){
        headerTap.addTarget(self, action: #selector(self.hideSelf(_:)))
        iconTap.addTarget(self, action: #selector(self.hideSelf(_:)))
    }
    
    func updateLabels(){
        self.buildingNameLabel.text = pickupRequest?.buildingName
        self.bagCountLabel.text = "\(pickupRequest.bagCount!)"
        self.dateFormatter.dateFormat = "hh:mm a"
        self.preferredPickupTimeLabel.text = dateFormatter.string(from: (pickupRequest?.startPickupTime)!)
        self.locationLabel.text = pickupRequest?.loadLocationName
        self.pickupTypeLabel.text = pickupRequest?.loadTypesNames.joined(separator: ", ")
    }
    
    //MARK: IBActions
    @IBAction func undoCancelPickupPressed(_ sender: UIButton) {
        print(#function)
        showHud()
        PickupREST.undoCancelPickup(pickupRequest.id!) { (success, error) in
            self.hideHud()
            if success {
                print("Undo cancelation was successfully made")
                self.delegate?.isCancelPickupUndone(true)
            }
            self.hideSelf()
        }
    }
    
    @IBAction func closeButtonPressed(_ sender: UIButton) {
        print(#function)
        self.hideSelf()
    }
    
    @IBAction func markupPickupAsCompletedPressed(_ sender: UIButton) {
         print(#function)
    }
    
    //MARK: Show/Hide
    @objc func hideSelf(_ gestureRecognizer: UITapGestureRecognizer = UITapGestureRecognizer()){
        dispatch {
            self.scrollViewTopConstraint.constant = self.view.frame.height + 30
            self.footerViewHeightConstraint.constant = 0
            self.closeButtonBottomConstraint.constant = -50
            UIView.animate(withDuration: 0.2, animations: {
                self.removeBordersFrom([self.view, self.footerView])
                self.view.layoutIfNeeded()
            }, completion: { (_) in
                UIView.animate(withDuration: 0.2, animations: {
                    self.backgroundView.backgroundColor = UIColor.clear
                }, completion: { (_) in
                    self.dismiss(animated: false, completion: {
                        print("Dismissed CancelledPickupViewController")
                    })
                })
            })
            
        }
    }
    
    func prepareForAnimation(){
        self.backgroundView.backgroundColor = UIColor.clear
        self.scrollViewTopConstraint.constant = self.view.frame.height + 30
        self.closeButtonBottomConstraint.constant = -50
        self.footerViewHeightConstraint.constant = 0
        self.view.layoutIfNeeded()
    }
    
    func showSelf(){
        dispatch {
            self.scrollViewTopConstraint.constant = 80
            self.footerViewHeightConstraint.constant = 100
            self.closeButtonBottomConstraint.constant = 20
            UIView.animate(withDuration: 0.2, animations: {
                self.backgroundView.backgroundColor = UIColor.black
                self.view.layoutIfNeeded()
            }, completion: { (_) in
                self.addBorders()
                print("Presented CancelledPickupViewController")
            })
        }
    }
    
    //MARK: Border
    func addBorders(){
        UIView.animate(withDuration: 0.3) {
            self.footerView.addTopBorderWithColor(color: Appearance.salmonColor, width: 5.0, x: 0, y: -self.contentScrollView.frame.height-3)
            self.view.addLeftBorderWithColor(color: Appearance.salmonColor, width: 3.0, x: 0, y: 100)
            self.view.addRightBorderWithColor(color: Appearance.salmonColor, width: 3.0, x: self.view.frame.width, y: 100)
            self.view.addBottomBorderWithColor(color: Appearance.salmonColor, width: 3.0, x: 0, y: self.view.frame.height)
        }
    }
    
    func removeBordersFrom(_ views: [UIView]){
        for v in views {
            for layer in v.layer.sublayers! {
                switch layer.name {
                case "leftBorder"?,"topBorder"?,"rightBorder"?,"bottomBorder"?:
                    layer.removeFromSuperlayer()
                default:
                    continue
                }
            }
        }
    }
}

//MARK: View controller extensions
extension CancelledPickupViewController{
    static func create() -> CancelledPickupViewController{
        return UIStoryboard.home.instantiate(CancelledPickupViewController.self)
    }
}

// MARK: - Protocol extensions
protocol CancelledPickupDelegate:class {
    func isCancelPickupUndone(_ status:Bool)
}
