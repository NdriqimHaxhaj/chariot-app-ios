//
//  OverduePickupViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 4/15/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import SCLAlertView

class OverduePickupViewController: UIViewController {
    //MARK: Outlets
    @IBOutlet var headerTap: UITapGestureRecognizer!
    @IBOutlet var iconTap: UITapGestureRecognizer!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var contentScrollView: UIScrollView!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var notesNotificationBadgeView: UIView!
    @IBOutlet weak var closeView: UIView!
    @IBOutlet weak var closeIcon: UIImageView!
    
    //MARK: Constraints
    @IBOutlet weak var scrollViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var footerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var closeButtonBottomConstraint: NSLayoutConstraint!
    
    //MARK: Labels
    @IBOutlet weak var buildingNameLabel: UILabel!
    @IBOutlet weak var bagCountStaticLabel: UILabel!
    @IBOutlet weak var bagCountLabel: UILabel!
    @IBOutlet weak var preferredPickupTimeStaticLabel: UILabel!
    @IBOutlet weak var preferredPickupTimeLabel: UILabel!
    @IBOutlet weak var locationStaticLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var pickupTypeStaticLabel: UILabel!
    @IBOutlet weak var pickupTypeLabel: UILabel!
    
    //Helper variables
    lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter
    }()
    
    //MARK: Variables
    var pickupRequest:PickupRequest!
    weak var overdueDelegate:OverduePickupDelegate?
    weak var confirmDelegate:ConfirmPickupActionDelegate?
    var messages:[PickupNote]?
    
    //MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTaps()
        showSelf()
        updateLabels()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupUI()
        prepareForAnimation()
    }
    
    //MARK: Initial functions
    func setupUI(){
        let isNightMode = ThemeManager.isNightMode()
        if isNightMode {
            closeView.backgroundColor = Appearance.darkGrayNight
            closeView.borderColor = Appearance.darkGrayNight
            closeIcon.image = #imageLiteral(resourceName: "X")
            contentScrollView.backgroundColor = Appearance.darkGrayNight
            buildingNameLabel.textColor = UIColor.white
            bagCountStaticLabel.textColor = UIColor.white
            bagCountLabel.textColor = UIColor.white
            preferredPickupTimeStaticLabel.textColor = UIColor.white
            preferredPickupTimeLabel.textColor = UIColor.white
            locationStaticLabel.textColor = UIColor.white
            locationLabel.textColor = UIColor.white
            pickupTypeStaticLabel.textColor = UIColor.white
            pickupTypeLabel.textColor = UIColor.white
            footerView.backgroundColor = Appearance.darkGrayNight
        }
    }
    
    func setupTaps(){
        headerTap.addTarget(self, action: #selector(self.hideSelf(_:)))
        iconTap.addTarget(self, action: #selector(self.hideSelf(_:)))
    }
    
    func updateLabels(){
        self.buildingNameLabel.text = pickupRequest?.buildingName
        self.bagCountLabel.text = "\(pickupRequest.bagCount!)"
        self.dateFormatter.dateFormat = "hh:mm a"
        self.preferredPickupTimeLabel.text = dateFormatter.string(from: (pickupRequest?.startPickupTime)!)
        self.locationLabel.text = pickupRequest?.loadLocationName
        self.pickupTypeLabel.text = pickupRequest?.loadTypesNames.joined(separator: ", ")
    }
    
    //MARK: IBActions
    @IBAction func cancelPickupPressed(_ sender: UIButton) {
        print(#function)
        
        let cancelPickupConfirmVC = CancelPickupConfirmViewController.create()
        cancelPickupConfirmVC.buildingName = pickupRequest.buildingName
        cancelPickupConfirmVC.delegate = self
        cancelPickupConfirmVC.modalPresentationStyle = .overCurrentContext
        
        dispatch {
            self.showModal(cancelPickupConfirmVC, animated: false, completion: nil)
        }
        
    }
    
    @IBAction func startPickupPressed(_ sender: UIButton) {
        let route = "\(pickupRequest.longitude!), \(pickupRequest.latitude!)"
        overdueDelegate?.startOverduePickup(true, pickupRequest.id!, route)
        hideSelf()
    }
    
    @IBAction func seeNotesPressed(_ sender: UIButton) {
        let pickupNotesVC = PickupNotesViewController.create()
        pickupNotesVC.modalPresentationStyle = .overCurrentContext
        pickupNotesVC.pickupRequest = self.pickupRequest
        dispatch {
            self.present(pickupNotesVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func callSuperintendentPressed(_ sender: UIButton) {
        print(#function)
        if let url = NSURL(string: "tel://\(String(describing: pickupRequest.superintendentPhone))"),
            UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        }
        
    }
    
    @IBAction func closeButtonPressed(_ sender: UIButton) {
        print(#function)
        self.hideSelf()
    }
    
    @IBAction func markupPickupAsCompletedPressed(_ sender: UIButton) {
        print(#function)
        let confirmPickupCompletionViewController = ConfirmPickupCompletionViewController.create()
        confirmPickupCompletionViewController.routeCoordinates = "[]"
        confirmPickupCompletionViewController.confirmType = ConfirmType.Complete
        confirmPickupCompletionViewController.pickupId = "\(String(describing: pickupRequest.id!))"
        confirmPickupCompletionViewController.modalPresentationStyle = .overCurrentContext
        confirmPickupCompletionViewController.delegate = self
        self.showModal(confirmPickupCompletionViewController)
    }
    
    // MARK: - Functions
    @objc func hideSelf(_ gestureRecognizer: UITapGestureRecognizer = UITapGestureRecognizer()){
        dispatch {
            self.scrollViewTopConstraint.constant = self.view.frame.height + 30
            self.footerViewHeightConstraint.constant = 0
            self.closeButtonBottomConstraint.constant = -50
            UIView.animate(withDuration: 0.2, animations: {
                self.view.layoutIfNeeded()
            }, completion: { (_) in
                UIView.animate(withDuration: 0.2, animations: {
                    self.backgroundView.backgroundColor = UIColor.clear
                }, completion: { (_) in
                    self.dismiss(animated: false, completion: {
                        print("Dismissed OverduePickupViewController")
                    })
                })
            })
        }
        
    }
    
    func prepareForAnimation(){
        self.backgroundView.backgroundColor = UIColor.clear
        self.scrollViewTopConstraint.constant = self.view.frame.height + 30
        self.closeButtonBottomConstraint.constant = -50
        self.footerViewHeightConstraint.constant = 0
    }
    
    func showSelf(){
        dispatch {
            self.scrollViewTopConstraint.constant = 80
            self.footerViewHeightConstraint.constant = 100
            self.closeButtonBottomConstraint.constant = 20
            UIView.animate(withDuration: 0.2, animations: {
                self.backgroundView.backgroundColor = UIColor.black
                self.view.layoutIfNeeded()
            }, completion: { (_) in
                print("Presented CurrentPickupViewController")
            })
        }
    }
    
    func completePickup(){
        let id = "\(pickupRequest.id!)"
        PickupREST.completePickup(id, "[]") { (success, error) in
            self.hideHud()
            if success {
                self.hideSelf()
                self.overdueDelegate?.isOverduePickupCompleted(true)
            } else {
                print("Couldn't complete pickup")
            }
        }
    }
}

//MARK: - CancelPickupStatusDelegate extensions
extension OverduePickupViewController: OverduePickupDelegate, CancelConfirmPickupDelegate{
    func cancelPickup(_ state: Bool) {
        let cancellPickupVC = CancellPickupViewController.create()
        cancellPickupVC.modalPresentationStyle = .overCurrentContext
        cancellPickupVC.buildingId = pickupRequest.id
        cancellPickupVC.overdueDelegate = self
        
        dispatch {
            self.showModal(cancellPickupVC)
        }
    }
    
    func isOverduePickupCompleted(_ status: Bool) {
        overdueDelegate?.isOverduePickupCompleted(status)
        hideSelf()
    }
    
    func startOverduePickup(_ status: Bool, _ id: Int, _ route:String) {
        print(#function)
    }
    
    
    func isOverduedPickupCanceled(_ status: Bool) {
        if status {
            self.hideSelf()
            overdueDelegate?.isOverduedPickupCanceled(true)
        }
    }
}

//MARK: View controller extensions
extension OverduePickupViewController{
    static func create() -> OverduePickupViewController{
        return UIStoryboard.home.instantiate(OverduePickupViewController.self)
    }
}

extension OverduePickupViewController: ConfirmPickupActionDelegate {
    func refreshPickups() {
        completePickup()
    }
}

// MARK: - Protocol extensions
protocol OverduePickupDelegate:class {
    func isOverduedPickupCanceled(_ status:Bool)
    func startOverduePickup(_ status:Bool,_ id:Int, _ route:String)
    func isOverduePickupCompleted(_ status:Bool)
}



