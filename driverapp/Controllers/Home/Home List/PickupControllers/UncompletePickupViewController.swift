//
//  AssignedPickupViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 2/21/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import SCLAlertView

class UncompletePickupViewController: UIViewController {
    //MARK: Outlets
    @IBOutlet var headerTap: UITapGestureRecognizer!
    @IBOutlet var iconTap: UITapGestureRecognizer!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var contentScrollView: UIScrollView!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var closeView: UIView!
    @IBOutlet weak var closeIcon: UIImageView!
    
    //MARK: Constraints
    @IBOutlet weak var scrollViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var footerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var closeButtonBottomConstraint: NSLayoutConstraint!
    
    //MARK: Labels
    @IBOutlet weak var buildingNameLabel: UILabel!
    @IBOutlet weak var bagCountStaticLabel: UILabel!
    @IBOutlet weak var bagCountLabel: UILabel!
    @IBOutlet weak var preferredPickupTimeStaticLabel: UILabel!
    @IBOutlet weak var preferredPickupTimeLabel: UILabel!
    @IBOutlet weak var locationStaticLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var pickupTypeStaticLabel: UILabel!
    @IBOutlet weak var pickupTypeLabel: UILabel!
    
    //Helper variables
    lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter
    }()
    
    //MARK: Variables
    var pickupRequest:PickupRequest!
    weak var uncancelPickupDelegate:UncancelPickupPickupDelegate?
    weak var confirmActionDelegate:ConfirmPickupActionDelegate?
    
    //MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTaps()
        showSelf()
        updateLabels()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        prepareForAnimation()
        setupUI()
    }
    
    //MARK: Initial functions
    func setupUI(){
        let isNightMode = ThemeManager.isNightMode()
        if isNightMode {
            closeView.backgroundColor = Appearance.darkGrayNight
            closeView.borderColor = Appearance.darkGrayNight
            closeIcon.image = #imageLiteral(resourceName: "X")
            contentScrollView.backgroundColor = Appearance.darkGrayNight
            buildingNameLabel.textColor = UIColor.white
            bagCountStaticLabel.textColor = UIColor.white
            bagCountLabel.textColor = UIColor.white
            preferredPickupTimeStaticLabel.textColor = UIColor.white
            preferredPickupTimeLabel.textColor = UIColor.white
            locationStaticLabel.textColor = UIColor.white
            locationLabel.textColor = UIColor.white
            pickupTypeStaticLabel.textColor = UIColor.white
            pickupTypeLabel.textColor = UIColor.white
            footerView.backgroundColor = Appearance.darkGrayNight
        }
    }
    
    func setupTaps(){
        headerTap.addTarget(self, action: #selector(UncompletePickupViewController.hideSelf(_:)))
        iconTap.addTarget(self, action: #selector(UncompletePickupViewController.hideSelf(_:)))
    }
    
    func updateLabels(){
        self.buildingNameLabel.text = pickupRequest?.buildingName
        self.bagCountLabel.text = "\(pickupRequest.bagCount!)"
        self.dateFormatter.dateFormat = "hh:mm a"
        self.preferredPickupTimeLabel.text = dateFormatter.string(from: (pickupRequest?.startPickupTime)!)
        self.locationLabel.text = pickupRequest?.loadLocationName
        self.pickupTypeLabel.text = pickupRequest?.loadTypesNames.joined(separator: ", ")
    }
    
    //MARK: IBActions
    @IBAction func cancelPickupPressed(_ sender: UIButton) {
        print(#function)
    }
    
    @IBAction func startPickupPressed(_ sender: UIButton) {
        print(#function)
    }
    
    @IBAction func seeNotesPressed(_ sender: UIButton) {
        print(#function)
    }
    
    @IBAction func callSuperintendentPressed(_ sender: UIButton) {
        print(#function)
    }
    
    @IBAction func closeButtonPressed(_ sender: UIButton) {
        print(#function)
        self.hideSelf()
    }
    
    @IBAction func markupPickupAsUncompletedPressed(_ sender: UIButton) {
        print(#function)
        //  Here comes request
        //  If success
        let confirmPickupCompletionViewController = ConfirmPickupCompletionViewController.create()
        confirmPickupCompletionViewController.confirmType = ConfirmType.Uncomplete
        confirmPickupCompletionViewController.pickupId = "\(String(describing: pickupRequest.id!))"
        confirmPickupCompletionViewController.modalPresentationStyle = .overCurrentContext
        confirmPickupCompletionViewController.delegate = self
        self.showModal(confirmPickupCompletionViewController)
        
    }
    
    //MARK: Show/Hide
    func prepareForAnimation(){
        self.backgroundView.backgroundColor = UIColor.clear
        self.scrollViewTopConstraint.constant = self.view.frame.height + 30
        self.closeButtonBottomConstraint.constant = -50
        self.footerViewHeightConstraint.constant = 0
        self.view.layoutIfNeeded()
    }
    
    @objc func hideSelf(_ gestureRecognizer: UITapGestureRecognizer = UITapGestureRecognizer()){
        dispatch {
            self.scrollViewTopConstraint.constant = self.view.frame.height + 30
            self.footerViewHeightConstraint.constant = 0
            self.closeButtonBottomConstraint.constant = -50
            UIView.animate(withDuration: 0.2, animations: {
                self.view.layoutIfNeeded()
            }, completion: { (_) in
                UIView.animate(withDuration: 0.2, animations: {
                    self.backgroundView.backgroundColor = UIColor.clear
                }, completion: { (_) in
                    self.dismiss(animated: false, completion: {
                        print("Dismissed CurrentPickupViewController")
                    })
                })
            })
        }
    }
    
    func showSelf(){
        dispatch {
            self.scrollViewTopConstraint.constant = 80
            self.footerViewHeightConstraint.constant = 100
            self.closeButtonBottomConstraint.constant = 20
            UIView.animate(withDuration: 0.2, animations: {
                self.backgroundView.backgroundColor = UIColor.black
                self.view.layoutIfNeeded()
            }, completion: { (_) in
                print("Presented CurrentPickupViewController")
            })
        }
    }
    
    func uncompletePickup(){
        PickupREST.uncompletePickup(pickupRequest.id!) { (success, error) in
            if success {
                self.hideSelf()
                self.confirmActionDelegate?.refreshPickups()
            }
        }
    }
}

//MARK: View controller extensions
extension UncompletePickupViewController {
    static func create() -> UncompletePickupViewController{
        return UIStoryboard.home.instantiate(UncompletePickupViewController.self)
    }
}

extension UncompletePickupViewController: ConfirmPickupActionDelegate {
    func refreshPickups() {
        uncompletePickup()
    }
}

protocol UncancelPickupPickupDelegate:class {
    func uncompletePickup()
}

