//
//  AssignedPickupViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 2/21/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import SCLAlertView

class CurrentPickupViewController: UIViewController {
    //MARK: Outlets
    @IBOutlet var headerTap: UITapGestureRecognizer!
    @IBOutlet var iconTap: UITapGestureRecognizer!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var contentScrollView: UIScrollView!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var notesNotificationBadgeView: UIView!
    @IBOutlet weak var closeView: UIView!
    @IBOutlet weak var closeIcon: UIImageView!
    
    //MARK: Constraints
    @IBOutlet weak var scrollViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var footerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var closeButtonBottomConstraint: NSLayoutConstraint!
    
    //MARK: Labels
    @IBOutlet weak var buildingNameLabel: UILabel!
    @IBOutlet weak var bagCountStaticLabel: UILabel!
    @IBOutlet weak var bagCountLabel: UILabel!
    @IBOutlet weak var preferredPickupTimeStaticLabel: UILabel!
    @IBOutlet weak var preferredPickupTimeLabel: UILabel!
    @IBOutlet weak var locationStaticLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var pickupTypeStaticLabel: UILabel!
    @IBOutlet weak var pickupTypeLabel: UILabel!
    
    //Helper variables
    lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter
    }()
    
    //MARK: Variables
    var pickupRequest:PickupRequest!
    weak var cancelPickupDelegate:CancelPickupDelegate?
    
    //MARK: Life cycle
    override func viewWillAppear(_ animated: Bool) {
        setupUI()
        prepareForAnimation()
//        checkForNotes()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTaps()
        showSelf()
        updateLabels()
    }
    
    //MARK: Initial functions
    func setupUI(){
        let isNightMode = ThemeManager.isNightMode()
        if isNightMode {
            closeView.backgroundColor = Appearance.darkGrayNight
            closeView.borderColor = Appearance.darkGrayNight
            closeIcon.image = #imageLiteral(resourceName: "X")
            contentScrollView.backgroundColor = Appearance.darkGrayNight
            buildingNameLabel.textColor = UIColor.white
            bagCountStaticLabel.textColor = UIColor.white
            bagCountLabel.textColor = UIColor.white
            preferredPickupTimeStaticLabel.textColor = UIColor.white
            preferredPickupTimeLabel.textColor = UIColor.white
            locationStaticLabel.textColor = UIColor.white
            locationLabel.textColor = UIColor.white
            pickupTypeStaticLabel.textColor = UIColor.white
            pickupTypeLabel.textColor = UIColor.white
            footerView.backgroundColor = Appearance.darkGrayNight
        }
    }
    
    func setupTaps(){
        headerTap.addTarget(self, action: #selector(CurrentPickupViewController.cancelTapPressed(_:)))
        iconTap.addTarget(self, action: #selector(CurrentPickupViewController.cancelTapPressed(_:)))
    }
    
    func updateLabels(){
        self.buildingNameLabel.text = pickupRequest?.buildingName
        self.bagCountLabel.text = "\(pickupRequest.bagCount!)"
        self.dateFormatter.dateFormat = "hh:mm a"
        self.preferredPickupTimeLabel.text = dateFormatter.string(from: (pickupRequest?.startPickupTime)!)
        self.locationLabel.text = pickupRequest?.loadLocationName
        self.pickupTypeLabel.text = pickupRequest?.loadTypesNames.joined(separator: ", ")
    }
    
    //MARK: IBActions
    @IBAction func cancelPickupPressed(_ sender: UIButton) {
        print(#function)
        
        let cancelPickupConfirmVC = CancelPickupConfirmViewController.create()
        cancelPickupConfirmVC.buildingName = pickupRequest.buildingName
        cancelPickupConfirmVC.modalPresentationStyle = .overCurrentContext
        cancelPickupConfirmVC.delegate = self
        
        dispatch {
            self.present(cancelPickupConfirmVC, animated: true) {
                //
            }
        }
    }
    
    @IBAction func startPickupPressed(_ sender: UIButton) {
        self.hideSelf(delegatePickup: true)
    }
    
    @IBAction func seeNotesPressed(_ sender: UIButton) {
        let pickupNotesVC = PickupNotesViewController.create()
        pickupNotesVC.modalPresentationStyle = .overCurrentContext
        pickupNotesVC.pickupRequest = self.pickupRequest
        dispatch {
            self.present(pickupNotesVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func callSuperintendentPressed(_ sender: UIButton) {
        print(#function)
        if let url = NSURL(string: "tel://\(String(describing: pickupRequest.superintendentPhone))"),
            UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.openURL(url as URL)
        }
        
    }
    
    @IBAction func closeButtonPressed(_ sender: UIButton) {
        print(#function)
        self.hideSelf(delegatePickup: false)
    }
    
    @IBAction func markupPickupAsCompletedPressed(_ sender: UIButton) {
        print(#function)
        let confirmPickupCompletionViewController = ConfirmPickupCompletionViewController.create()
        confirmPickupCompletionViewController.routeCoordinates = "[]"
        confirmPickupCompletionViewController.confirmType = ConfirmType.Complete
        confirmPickupCompletionViewController.pickupId = "\(String(describing: pickupRequest.id!))"
        confirmPickupCompletionViewController.modalPresentationStyle = .overCurrentContext
        confirmPickupCompletionViewController.delegate = self
        self.showModal(confirmPickupCompletionViewController)
    }
    
    //MARK: Show/Hide
    @objc func cancelTapPressed(_ gestureRecognizer: UITapGestureRecognizer = UITapGestureRecognizer()){
            hideSelf(delegatePickup: false)
    }
    
    @objc func hideSelf(_ gestureRecognizer: UITapGestureRecognizer = UITapGestureRecognizer(), delegatePickup:Bool = false){
        dispatch {
            self.scrollViewTopConstraint.constant = self.view.frame.height + 30
            self.footerViewHeightConstraint.constant = 0
            self.closeButtonBottomConstraint.constant = -50
            UIView.animate(withDuration: 0.2, animations: {
                self.view.layoutIfNeeded()
            }, completion: { (_) in
                UIView.animate(withDuration: 0.2, animations: {
                    self.backgroundView.backgroundColor = UIColor.clear
                }, completion: { (_) in
                    self.dismiss(animated: false, completion: {
                        print("Dismissed CurrentPickupViewController")
                        if delegatePickup {
                            let route = "\(self.pickupRequest.longitude!), \(self.pickupRequest.latitude!)"
                            self.cancelPickupDelegate?.startPickup(true, self.pickupRequest.id!, route)
                        }
                    })
                })
            })
        }
    }
    
    func prepareForAnimation(){
        self.backgroundView.backgroundColor = UIColor.clear
        self.scrollViewTopConstraint.constant = self.view.frame.height + 30
        self.closeButtonBottomConstraint.constant = -50
        self.footerViewHeightConstraint.constant = 0
        self.view.layoutIfNeeded()
    }
    
    func showSelf(){
        dispatch {
            self.scrollViewTopConstraint.constant = 80
            self.footerViewHeightConstraint.constant = 100
            self.closeButtonBottomConstraint.constant = 20
            UIView.animate(withDuration: 0.2, animations: {
                self.backgroundView.backgroundColor = UIColor.black
                self.view.layoutIfNeeded()
            }, completion: { (_) in
                print("Presented CurrentPickupViewController")
            })
        }
    }
    
    func completePickup(){
        let id = "\(pickupRequest.id!)"
        PickupREST.completePickup(id, "[]") { (success, error) in
            self.hideHud()
            if success {
                self.cancelPickupDelegate?.isPickupCompleted(true)
                self.hideSelf(delegatePickup: false)
            } else {
                print("Couldn't complete pickup")
                self.cancelPickupDelegate?.isPickupCompleted(false)
            }
        }
    }
}

//MARK: - CancelPickupStatusDelegate extension
extension CurrentPickupViewController: CancelPickupStatusDelegate{
    func shouldCancelPickup(_ status: Bool) {
        if status {
            self.cancelPickupDelegate?.isPickupCanceled(true)
            self.hideSelf(delegatePickup: false)
        }
    }
}

//MARK: - CancelPickupDelegate extension
extension CurrentPickupViewController: CancelPickupDelegate, ConfirmPickupActionDelegate, CancelConfirmPickupDelegate{
    func cancelPickup(_ state: Bool) {
        if state {
            let cancellPickupVC = CancellPickupViewController.create()
            cancellPickupVC.modalPresentationStyle = .overCurrentContext
            cancellPickupVC.buildingId = pickupRequest.id
            cancellPickupVC.cancelDelegate = self
            dispatch {
                self.present(cancellPickupVC, animated: true) {
                    //
                }
            }
        }
    }
    
    func isPickupCompleted(_ status: Bool) {
        // Does nothing here
    }
    
    func isPickupCanceled(_ status: Bool) {
        // Does nothing here
    }
    
    func startPickup(_ status: Bool, _ id: Int, _ route: String) {
        // Does nothing here
    }
    
    func refreshPickups() {
        completePickup()
    }
}

//MARK: View controller extensions
extension CurrentPickupViewController{
    static func create() -> CurrentPickupViewController{
        return UIStoryboard.home.instantiate(CurrentPickupViewController.self)
    }
}

// MARK: - Protocol extensions
protocol CancelPickupDelegate:class {
    func isPickupCanceled(_ status:Bool)
    func startPickup(_ status:Bool,_ id:Int, _ route: String)
    func isPickupCompleted(_ status:Bool)
}
