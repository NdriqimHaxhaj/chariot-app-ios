//
//  PickupNotesViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 2/26/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import SCLAlertView

enum CommunicationType:Int {
    case DriverDispatcher = 1
    case DriverPropertyManager = 2
    case PropertyManagerDispatcher = 3
}

class PickupNotesViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollViewInnerView: UIView!
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var greenView: UIView!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageTextField: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var backBarButton: UIBarButtonItem!
    @IBOutlet weak var menuBarView: UIView!
    @IBOutlet weak var dispatcherButton: UIButton!
    @IBOutlet weak var propertyManagerButton: UIButton!
    
    // MARK: - Constraints
    @IBOutlet weak var textFieldBottomConstraint: NSLayoutConstraint!
    
    // MARK: - Properties
    var messages:[PickupNote] = []
    var pickupRequest:PickupRequest!
    private var animationDuration: Double = 0.3
    var dispatcherNotesViewController:PropertyManagerDriverNotesViewController?
    var propertyManagerNotesViewController:PropertyManagerDispatcherNotesViewController?
    var currentCommunicationType:CommunicationType = .DriverDispatcher
    
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        addPaddingToTextField()
        setupKeyboardObservers()
        setupChildViewController()
        self.titleLabel.text = pickupRequest.buildingName
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.messageTextField.resignFirstResponder()
    }
    
    // MARK: - IBActions
    @IBAction func goBack(_ sender: UIBarButtonItem) {
        dispatch {
            self.dismiss(animated: true) {
                //
            }
        }
    }
    @IBAction func dispatcherButtonPressed(_ sender: UIButton) {
        scrollToPage(0)
    }
    
    @IBAction func propertyManagerButtonPressed(_ sender: UIButton){
        scrollToPage(1)
    }
    
    
    
    @IBAction func postButtonPressed(_ sender: UIButton) {
        guard let note = messageTextField.text, note != "" else {return}
        self.view.endEditing(true)
        self.addNote(note)
        messageTextField.text = ""
    }
    
    
    // MARK: - Functions
    fileprivate func setupUI(){
        if ThemeManager.isNightMode() {
            headerView.backgroundColor = Appearance.darkGrayNight
            navigationBar.backgroundColor = Appearance.darkGrayNight
            menuBarView.backgroundColor = Appearance.darkGrayNight
            backBarButton.tintColor = UIColor.white
            backgroundView.backgroundColor = Appearance.backgroundViewNight
            scrollView.backgroundColor = Appearance.backgroundViewNight
            scrollViewInnerView.backgroundColor = Appearance.backgroundViewNight
            dispatcherButton.setTitleColor(.white, for: .normal)
            propertyManagerButton.setTitleColor(.white, for: .normal)
            titleLabel.textColor = UIColor.white
            messageTextField.borderColor = Appearance.darkGrayNight
            messageTextField.backgroundColor = Appearance.darkGrayNight
            messageTextField.placeHolderColor = UIColor.lightGray
            messageTextField.textColor = UIColor.white
            sendButton.imageView?.image = sendButton.imageView?.image?.tint(with: UIColor.white)
        }
    }
    
    func addPaddingToTextField(){
        let paddingView = UIView(frame: CGRect.init(x: 0, y: 0, width: 15, height: self.messageTextField.frame.height))
        messageTextField.leftView = paddingView
        messageTextField.leftViewMode = UITextFieldViewMode.always
    }
    
    func addNote(_ note: String) {
        PickupREST.addNote(pickupRequest.id!, currentCommunicationType.rawValue , note) { (success, error) in
            self.hideHud()
            if success {
                switch self.currentCommunicationType {
                    case .DriverDispatcher:
                        self.dispatcherNotesViewController?.getNotes()
                        break
                    case .DriverPropertyManager:
                        self.propertyManagerNotesViewController?.getNotes()
                        break
                    default:
                        break
                }
            }
            else {
                SCLAlertView().showNotice("Alert", subTitle: "Could not post the note. Please try again.") // Error
                print(error!)
                return
            }
        }
    }
    
    func setupChildViewController(){
        dispatcherNotesViewController = childViewControllers.first as? PropertyManagerDriverNotesViewController
        dispatcherNotesViewController?.pickupRequest = pickupRequest
        propertyManagerNotesViewController = childViewControllers.last as? PropertyManagerDispatcherNotesViewController
        propertyManagerNotesViewController?.pickupRequest = pickupRequest
    }
    
    func scrollToPage(_ page: Int) {
        UIView.animate(withDuration: animationDuration) {
            self.currentCommunicationType = page == 0 ? .DriverDispatcher : .DriverPropertyManager
            self.scrollView.contentOffset.x = self.scrollView.frame.width * CGFloat(page)
            self.scrollGreenViewToPage(page)
        }
    }
    
    func scrollGreenViewToPage(_ page: Int){
        if(page==0){
            self.greenView.frame = CGRect(x: 0, y: self.greenView.frame.origin.y, width: self.greenView.frame.size.width, height: self.greenView.frame.size.height)
        }else{
            self.greenView.frame = CGRect(x: self.greenView.frame.size.width, y: self.greenView.frame.origin.y, width: self.greenView.frame.size.width, height: self.greenView.frame.size.height)
        }
    }
    
    // MARK: - Keyboard functions
    func setupKeyboardObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardDidHide, object: nil)
    }
    
    @objc func keyboardWillShow(notification:NSNotification){
        let keyboardFrame = notification.userInfo![UIKeyboardFrameEndUserInfoKey] as! CGRect
        
        let keyboardDuration = notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! Double
        
        self.textFieldBottomConstraint.constant = keyboardFrame.height
        UIView.animate(withDuration: keyboardDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(notification:NSNotification){
        let keyboardDuration = notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! Double
        
        self.textFieldBottomConstraint.constant = 0
        UIView.animate(withDuration: keyboardDuration) {
            self.view.layoutIfNeeded()
        }
    }
}

//MARK: View controller general extensions
extension PickupNotesViewController{
    static func create() -> PickupNotesViewController{
        return UIStoryboard.home.instantiate(PickupNotesViewController.self)
    }
}


//MARK: Text field extensions
extension PickupNotesViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        guard let note = textField.text, note != "" else {return true}
        self.addNote(note)
        textField.text = ""
        return true
    }
    
}

extension PickupNotesViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        UIView.animate(withDuration: 0.3) {
            self.scrollGreenViewToPage(scrollView.currentPage)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if(scrollView.currentPage==0){
            UIView.animate(withDuration: 0.3, animations: {
                self.greenView.frame = CGRect(x: 0, y: self.greenView.frame.origin.y, width: self.greenView.frame.size.width, height: self.greenView.frame.size.height)
            })
        }else{
            UIView.animate(withDuration: 0.3, animations: {
                self.greenView.frame = CGRect(x: self.greenView.frame.size.width, y: self.greenView.frame.origin.y, width: self.greenView.frame.size.width, height: self.greenView.frame.size.height)
            })
        }
    }
}
