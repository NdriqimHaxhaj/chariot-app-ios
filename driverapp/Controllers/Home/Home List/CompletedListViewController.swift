//
//  CompletedListViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 3/23/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

protocol CompletedPickupsDelegate:class {
    func numberOfCompletedPickups(_ value:Int)
}

class CompletedListViewController: UIViewController {
    //MARK: Outlets
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var pickupsTableView: UITableView!
    
    //MARK: Variables
    var pickupRequests = [PickupRequest]() {
        didSet{
            seperatePickupsRequestsIntoSections()
        }
    }
    var pickupDateSections:[String] = []
    var pickupRequestsBySections: [String:[PickupRequest]] = [:]
    
    var completedPickupsDelegate:CompletedPickupsDelegate?
    var confirmPickupDelegate:ConfirmPickupActionDelegate?
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.refreshPickupsFromPull),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = Appearance.greenColor
        return refreshControl
    }()
    
    lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter
    }()
    
    //MARK: Life cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupUI()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addObservers()
        initPickupsTableView()
        addNotificationObservers()
    }
    
    //MARK: - Initial functions
    func addObservers(){
        registerNotification(notification: Notification.Name.changeTheme, selector: #selector(self.setupUI))
    }
    
    @objc func setupUI(){
        let isNightMode = ThemeManager.isNightMode()
        backgroundView.backgroundColor = isNightMode ? Appearance.backgroundViewNight : UIColor(red255: 242, green255: 242, blue255: 242)
        pickupsTableView.reloadData()
    }
    func initPickupsTableView(){
        pickupsTableView.delegate = self
        pickupsTableView.dataSource = self
        pickupsTableView.register(PickupCell.self)
        pickupsTableView.insertSubview(refreshControl, at: 0)
        pickupsTableView.rowHeight = UITableViewAutomaticDimension
        pickupsTableView.estimatedRowHeight = 130
    }
    
    @objc fileprivate func getPickups() {
        self.pickupRequests = []
        PickupREST.getPickupRequests(.current) { (success, data, error) in
            if success, let data = data{
                for pickup in data {
                    if pickup.pickupStatus == PickupStatusType.completedWithoutDelays || pickup.pickupStatus == PickupStatusType.completedWithDelays {
                        self.pickupRequests.append(pickup)
                    }
                }
                self.completedPickupsDelegate?.numberOfCompletedPickups(self.pickupRequests.count)
                dispatch {
                    self.seperatePickupsRequestsIntoSections()
                    self.reloadRowsAndSections()
                }
            }
        }
    }
    
    @objc func refreshPickupsFromPull(){
        guard let completedPickupsViewController = self.parent as? HomeListController else {
            print("Couldn't get parent !")
            return
        }
        completedPickupsViewController.getPickups { (success) in
            print("Succesfully refreshed pickups on HomeListController via Pull to Refresh")
        }
    }
    
    func seperatePickupsRequestsIntoSections(){
        pickupDateSections.removeAll()
        pickupRequestsBySections.removeAll()
        createSections()
        fillSections()
    }
    
    func createSections(){
        if pickupRequests.count > 0 {
            for pickup in pickupRequests {
                let date = pickup.startPickupTime
                dateFormatter.dateFormat = "dd-MM-yyyy"
                let key = dateFormatter.string(from: date!)
                if !pickupDateSections.contains(key){
                    pickupDateSections.append(key)
                }
            }
        } else {
            refreshControl.endRefreshing()
        }
    }
    
    func fillSections(){
        for key in pickupDateSections {
            pickupRequestsBySections[key] = pickupRequests.filter({ (pickup) -> Bool in
                dateFormatter.dateFormat = "dd-MM-yyyy"
                let formattedDate = dateFormatter.string(from: pickup.startPickupTime!)
                return key == formattedDate
            })
        }
        reloadRowsAndSections()
        refreshControl.endRefreshing()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    }
    
    func reloadRowsAndSections(){
        pickupsTableView.beginUpdates()
        
        // Remove current sections and fields if there is any
        if pickupsTableView.numberOfSections > 0 {
            for i in 0..<pickupsTableView.numberOfSections{
                self.pickupsTableView.deleteSections(IndexSet(integer: i), with: .none)
                let numberOfRows = pickupsTableView.numberOfRows(inSection: i)
                for j in 0..<numberOfRows{
                    self.pickupsTableView.deleteRows(at: [IndexPath(row: j, section: i)], with: .none)
                }
            }
        }
        // Add new sections and rows
        let numberOfSections = self.pickupDateSections.count
        for i in 0..<numberOfSections{
            self.pickupsTableView.insertSections(IndexSet(integer: i), with: .none)
            let key = pickupDateSections[i]
            if let numberOfRows = self.pickupRequestsBySections[key]?.count {
                for j in 0..<numberOfRows{
                    self.pickupsTableView.insertRows(at: [IndexPath(row: j, section: i)], with: .none)
                }
            }
        }
        pickupsTableView.endUpdates()
    }
    
    func addNotificationObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(CompletedListViewController.getPickups), name: Notification.Name("isCancelPickupUndone"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CompletedListViewController.getPickups), name: Notification.Name("isPickupCanceled"), object: nil)
    }
    
    // MARK: - Functions
    
}

//MARK: TableView Extensions
extension CompletedListViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return pickupDateSections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.pickupRequestsBySections[pickupDateSections[section]]?.count ?? 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = pickupsTableView.dequeue(PickupCell.self, for: indexPath)
        let key = pickupDateSections[indexPath.section]
        let pickupRequest = pickupRequestsBySections[key]![indexPath.row]
        cell.pickupRequest = pickupRequest
        cell.setInfos()
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? PickupCell else { return }
        print("type: \(cell.pickupRequest.pickupStatusName!)")
        let pickupRequest = cell.pickupRequest
        
        //Check Pickup type
        switch pickupRequest?.pickupStatus {
        // Completed Pickup
        case .completedWithoutDelays?:
            let vc:UncompletePickupViewController = UncompletePickupViewController.create()
            vc.pickupRequest = pickupRequest
            vc.modalPresentationStyle = .overCurrentContext
            vc.uncancelPickupDelegate = self
            vc.confirmActionDelegate = self
            dispatch {
                self.present(vc, animated: false, completion: {
                    print("AssignedPickupViewController to be presented")
                })
            }
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let isNightMode = ThemeManager.isNightMode()
        let grayColor = UIColor(red255: 93, green255: 93, blue255: 100)
        let view = UIView(frame: CGRect(x: 0,
                                        y: 0,
                                        width: self.view.frame.width,
                                        height: 40))
        view.backgroundColor = isNightMode ? Appearance.backgroundViewNight : UIColor(red255: 242, green255: 242, blue255: 242)
        let dateLabel = UILabel(frame: CGRect(x: 50,
                                              y: 0,
                                              width: view.frame.width,
                                              height: view.frame.height))
        dateLabel.text = pickupDateSections[section]
        dateLabel.textColor = isNightMode ? .white : grayColor
        dateLabel.clipsToBounds = true
        let lineView = UIView(frame: CGRect(x: 10,
                                            y: view.frame.height / 2,
                                            width: 30,
                                            height: 1))
        lineView.backgroundColor = isNightMode ? .white : grayColor
        lineView.alpha = 0.3
        
        view.addSubview(dateLabel)
        view.addSubview(lineView)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
}

//MARK: - View controller extension
extension CompletedListViewController{
    static func create() -> HomeListController{
        return UIStoryboard.home.instantiate(HomeListController.self)
    }
}

extension CompletedListViewController: ConfirmPickupActionDelegate {
    func refreshPickups() {
        getPickups()
        confirmPickupDelegate?.refreshPickups()
    }
}

//MARK: - Canceled/UndoneCancel for Pickup delegate
extension CompletedListViewController: UncancelPickupPickupDelegate {
    func uncompletePickup() {
        //
    }
}


