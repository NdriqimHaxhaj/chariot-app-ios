//
//  HomeListController.swift
//  driverapp
//
//  Created by Arben Pnishi on 20/02/2018.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

protocol MapMarkersDelegate:class {
    func redrawMapMarkers(completion: @escaping (_ status:Bool)->Void)
}

class HomeListController: UIViewController {
    //MARK: Outlets
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var greenView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var todoButton: UIButton!
    @IBOutlet weak var completedButton: UIButton!
    
    //MARK: Variables
    weak var pickupDelegate:PickupDelegate?
    private var animationDuration: Double = 0.3
    var toDoListViewController:ToDoListViewController?
    var toDoPickups = [PickupRequest]()
    var completedListViewController:CompletedListViewController?
    var completedPickups = [PickupRequest]()
    weak var mapMarkersDelegate:MapMarkersDelegate?
    var currentScrollPosition:Int = 0
    //MARK: Life cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupUI()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addObservers()
        setupChildViewControllers()
        if NavigationManager.offlineReasonId != OfflineReasonType.leaveTruck.rawValue {
            getPickups { (_) in}
        }
    }
    
    // MARK: - IBActions
    @IBAction func toDoPressed(_ sender: UIButton) {
        scrollToPage(0)
    }
    @IBAction func completedPressed(_ sender: UIButton) {
        scrollToPage(1)
    }
    
    
    //MARK: - Initial functions
    @objc func setupUI(){
        let isNightMode = ThemeManager.isNightMode()
        if isNightMode {
            mainView.backgroundColor =  Appearance.backgroundViewNight
        }
        todoButton.backgroundColor = isNightMode ? Appearance.darkGrayNight : UIColor.white
        todoButton.setTitleColor(isNightMode ? UIColor.white : UIColor(red255: 154, green255: 154, blue255: 154), for: .normal)
        completedButton.backgroundColor = isNightMode ?  Appearance.darkGrayNight : UIColor.white
        completedButton.setTitleColor(isNightMode ? UIColor.white : UIColor(red255: 154, green255: 154, blue255: 154), for: .normal)
        self.toDoListViewController?.pickupsTableView.reloadData()
        self.completedListViewController?.pickupsTableView.reloadData()
    }
    func addObservers(){
        registerNotification(notification: Notification.Name.changeTheme, selector: #selector(self.setupUI))
    }
    
    func scrollToPage(_ page: Int) {
        UIView.animate(withDuration: animationDuration) {
            self.currentScrollPosition = page
            self.scrollView.contentOffset.x = self.scrollView.frame.width * CGFloat(page)
            self.scrollGreenViewToPage(page)
        }
    }
    
    func getScrollPosition() -> Int {
        let width = self.view.frame.width
        let offset = scrollView.contentOffset.x
        
        if offset >= 0  && offset < width{
            return 1
        }else{
            return 0
        }
    }
    
    func scrollGreenViewToPage(_ page: Int){
        if(page==0){
            self.greenView.frame = CGRect(x: 0, y: self.greenView.frame.origin.y, width: self.greenView.frame.size.width, height: self.greenView.frame.size.height)
        }else{
            self.greenView.frame = CGRect(x: self.greenView.frame.size.width, y: self.greenView.frame.origin.y, width: self.greenView.frame.size.width, height: self.greenView.frame.size.height)
        }
    }
    
    fileprivate func setupChildViewControllers(){
        toDoListViewController = childViewControllers.first as? ToDoListViewController
        
        completedListViewController = childViewControllers.last as? CompletedListViewController
        
        toDoListViewController?.toDoPickupsDelegate = self
        toDoListViewController?.pickupDelegate = self
        completedListViewController?.completedPickupsDelegate = self
        completedListViewController?.confirmPickupDelegate = self
    }
    
    @objc func getPickups(completion: @escaping (_ status:Bool)->Void) {
        toDoPickups.removeAll()
        completedPickups.removeAll()
        let isFilterOn = toDoListViewController?.isFilterOn ?? false
        PickupREST.getPickupRequests(isFilterOn ? .all : .current) { (success, data, error) in
            if success, let data = data{
                self.onPickupSuccessResponse(data, completion: { (success) in
                    if success {
                        completion(true)
                    } else {
                        completion(false)
                    }
                })
            }else{
                self.onPickupFailResponse()
            }
        }
    }
    
    private func onPickupSuccessResponse(_ data: [PickupRequest], completion: @escaping (_ status:Bool)->Void){
        separatePickupsDependOnType(data)
        prepareUIForContainerViews { (success) in
            if success {
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    private func onPickupFailResponse(){
        
    }
    
    private func separatePickupsDependOnType(_ data: [PickupRequest]){
        for pickup in data {
            if pickup.pickupStatus == PickupStatusType.completedWithoutDelays || pickup.pickupStatus == PickupStatusType.completedWithDelays || pickup.pickupStatus == PickupStatusType.dropeedOff {
                self.completedPickups.append(pickup)
            } else {
                self.toDoPickups.append(pickup)
            }
        }
    }
    
    private func prepareUIForContainerViews(completion: @escaping (_ status:Bool)->Void){
        dispatch {
            // If start and end time is not set for filters, we create them
            // We choose earliest start pickup time from the pickup requests list
            self.findEarliestAndLatestTimeForFilter()
           // If start and end time are already set for filters, we pass only pickups
            self.completedListViewController?.pickupRequests = self.completedPickups
            self.toDoListViewController?.pickupRequests = self.toDoPickups
            self.completedButton.setTitle("Completed (\(self.completedPickups.count))", for: .normal)
            self.redrawMapMarkers(completion: { (success) in
                if success {
                    completion(true)
                } else {
                    completion(false)
                }
            })
        }
    }
    
    private func redrawMapMarkers(completion: @escaping (_ status:Bool)->Void){
        self.mapMarkersDelegate?.redrawMapMarkers(completion: { (success) in
            if success {
                completion(true)
            } else {
                completion(false)
            }
        })
    }
    
    private func onDrawMarkersCompleted(){
        
    }
    
    func findEarliestAndLatestTimeForFilter(){
        if self.toDoListViewController?.startTimeFilter == nil {
            self.toDoListViewController?.startTimeFilter = Date.now()
        }
        if self.toDoListViewController?.endTimeFilter == nil {
            self.toDoListViewController?.endTimeFilter = Date.now().addingTimeInterval(24*60*60)
        }
        
        self.toDoListViewController?.startTimeFilter = self.toDoPickups.min { (pickup, nextPickup) -> Bool in
            return pickup.startPickupTime! < nextPickup.startPickupTime!
            }?.startPickupTime ?? Date.now()
        
        // We choose latest end pickup time from the pickup requests list
        self.toDoListViewController?.endTimeFilter = self.toDoPickups.max { (pickup, nextPickup) -> Bool in
            return pickup.endPickupTime! < nextPickup.endPickupTime!
            }?.endPickupTime ?? Date.now().addingTimeInterval(24*60*60)
    }
    
    // MARK: - Functions
    
}

extension HomeListController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        UIView.animate(withDuration: 0.3) {
            self.scrollGreenViewToPage(scrollView.currentPage)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if(scrollView.currentPage==0){
            UIView.animate(withDuration: 0.3, animations: {
                self.greenView.frame = CGRect(x: 0, y: self.greenView.frame.origin.y, width: self.greenView.frame.size.width, height: self.greenView.frame.size.height)
            })
        }else{
            UIView.animate(withDuration: 0.3, animations: {
                self.greenView.frame = CGRect(x: self.greenView.frame.size.width, y: self.greenView.frame.origin.y, width: self.greenView.frame.size.width, height: self.greenView.frame.size.height)
            })
        }
    }
}

extension HomeListController: CompletedPickupsDelegate, ToDoPickupsDelegate {
    func numberOfToDoPickups(_ value: Int) {
        todoButton.setTitle("To do (\(value))", for: .normal)
    }
    
    func numberOfCompletedPickups(_ value: Int) {
        completedButton.setTitle("Completed (\(value))", for: .normal)
    }
}

// MARK: - Pickup Delegate extension
extension HomeListController: PickupDelegate {
    func editPickup(_ pickup: PickupRequest) {
       // Does nothing here
    }
    
    func refreshPickupDelegateFromCell() {
        getPickups { (_) in
        }
    }
    
    func triggerNavigation(_ id: Int, _ route:String) {
        getPickups { (success) in
            if success{
                self.pickupDelegate?.triggerNavigation(id,route)
            }
        }
    }
}

extension HomeListController: ConfirmPickupActionDelegate {
    func refreshPickups() {
        getPickups { (_) in
        }
    }
}



extension HomeListController: HomeMapControllerDelegate {
    func refreshPickupsFromMap() {
        getPickups { (_) in
        }
    }
}
