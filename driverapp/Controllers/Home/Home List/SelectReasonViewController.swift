//
//  SelectReasonViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 4/15/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

protocol ReasonDelegate:class {
    func reasonForGoingOffline(_ reason:OfflineReason?)
    func openDropOffTable(_ reason:OfflineReason)
}

class SelectReasonViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var reasonForGoingOfflineLabel: UILabel!
    @IBOutlet weak var closeButtonView: UIView!
    @IBOutlet weak var closeButtonIcon: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Constraints
    @IBOutlet weak var yAxisPopupConstraint: NSLayoutConstraint!
    
    // MARK: - Properties
    let lightGray = UIColor.HexToColor("BDBDBD")
    weak var delegate:ReasonDelegate?
    var offlineReasons:[OfflineReason] = []
    var reasonForGoingOffline:OfflineReason? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupTableView()
        prepareForAnimation()
        getOfflineReasons()
        addObservers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        showSelf()
    }
    
    // MARK: - IBActions
    @IBAction func cancelPressed(_ sender: UIButton) {
        self.hideSelf(shouldDelegate: false)
    }
    
    @IBAction func goOfflinePressed(_ sender: UIButton) {
        self.hideSelf(shouldDelegate: true)
    }
    
    @IBAction func closeButtonPressed(_ sender: UITapGestureRecognizer) {
        self.hideSelf(shouldDelegate: false)
    }
    
    // MARK: - Functions
    func setupUI(){
        let isNightMode = ThemeManager.isNightMode()
        
        closeButtonIcon.image = isNightMode ? #imageLiteral(resourceName: "X") : #imageLiteral(resourceName: "XGreen")
        if isNightMode {
            popupView.backgroundColor = Appearance.darkGrayNight
            reasonForGoingOfflineLabel.textColor = UIColor.white
            closeButtonView.backgroundColor = Appearance.darkGrayNight
            closeButtonView.borderColor = Appearance.darkGrayNight
            
        }
    }
    
    func setupTableView(){
        tableView.register(OfflineReasonCell.self)
        tableView.estimatedRowHeight = 40
        // Delegate and Data Source set via IB
    }
    
    func getOfflineReasons(){
        UserREST.getOfflineReasons { (success, offlineReasons, error) in
            if success {
                PickupREST.getCompletedPickups(completionHandler: { (success, completedPickups, error) in
                    if success {
                        guard let completedPickups = completedPickups else {
                            self.offlineReasons = offlineReasons!
                            self.reasonForGoingOffline = self.offlineReasons.first
                            self.tableView.reloadData(with: .top)
                            return
                        }
                        if completedPickups.isEmpty {
                            var reasons = offlineReasons!
                            for i in 0..<reasons.count {
                                if reasons[i].id == 2 {
                                    reasons.remove(at: i)
                                    break
                                }
                            }
                            self.offlineReasons = reasons
                            self.reasonForGoingOffline = self.offlineReasons.first
                        } else {
                            self.offlineReasons = offlineReasons!
                            self.reasonForGoingOffline = self.offlineReasons.first
                        }
                    } else {
                        self.offlineReasons = offlineReasons!
                        self.reasonForGoingOffline = self.offlineReasons.first
                    }
                    dispatch {
                        self.showSelf()
                        self.tableView.reloadData(with: .automatic)
                        
                    }
                    
                })
            } else {
                self.hideSelf(shouldDelegate: false)
                print("Couldn't get offline reasons")
            }
        }
    }
    
    func addObservers(){
        registerNotification(notification: Notification.Name.navigateToDropOff, selector: #selector(self.closeSelf))
    }
    
    @objc func closeSelf(){
        self.hideModal()
    }
    
}



// Offline Reasons table
extension SelectReasonViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return offlineReasons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(OfflineReasonCell.self, for: indexPath) as OfflineReasonCell
        let reason = offlineReasons[indexPath.row]
        cell.reason = reason
        cell.isReasonSelected = reason == reasonForGoingOffline ? true : false
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        reasonForGoingOffline = offlineReasons[indexPath.row]
        tableView.reloadData()
        let reason = offlineReasons[indexPath.row]
        if reason.type == .atFullCapacity {
            delegate?.openDropOffTable(reason)
            dismiss(animated: true, completion: nil)
        }
    }
}

// Show/Hide self extension
extension SelectReasonViewController {
    func prepareForAnimation(){
        yAxisPopupConstraint.constant = self.view.frame.height
        backgroundView.alpha = 0
        view.layoutIfNeeded()
    }
    
    func showSelf(){
        yAxisPopupConstraint.constant = 0
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 10, options: [.curveEaseIn], animations: {
            self.backgroundView.alpha = 0.5
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    func hideSelf(shouldDelegate: Bool){
        yAxisPopupConstraint.constant = self.view.frame.height
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0, options: [.curveEaseIn], animations: {
            self.backgroundView.alpha = 0
            self.view.layoutIfNeeded()
        }, completion: { _ in
            self.dismiss(animated: false, completion: {
                print("Dissmised SelectReasonViewController")
                if shouldDelegate {
                    self.delegate?.reasonForGoingOffline(self.reasonForGoingOffline)
                }
            })
        })
    }
}

//MARK: EXTENSIONS
extension SelectReasonViewController{
    static func create() -> SelectReasonViewController{
        return UIStoryboard.home.instantiate(SelectReasonViewController.self)
    }
}
