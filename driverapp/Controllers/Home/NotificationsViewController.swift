//
//  NotificationsViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 4/20/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import SCLAlertView

class NotificationsViewController: UIViewController {
    // MARK: IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var backgroundView: UIView!
    
    //MARK: - Properties
    var newNotifications:[NotificationType] = []
    var oldNotifications:[NotificationType] = []
    var pickupsRequests:[PickupRequest]!
    weak var pickupDelegate:PickupDelegate?
    weak var completedPickupsDelegate:CompletedPickupsDelegate?
    weak var confirmPickupDelegate:ConfirmPickupActionDelegate?
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        getNotifications()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupUI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        markAllNotificationsAsRead()
    }
    
    // MARK: Functions
    func setupUI(){
        Appearance.setup()
        if ThemeManager.isNightMode() {
            backgroundView.backgroundColor = Appearance.backgroundViewNight
        }
    }
    
    func setupTableView(){
        tableView.register(NotificationsTableViewCell.self)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 300
    }
    
    func getNotifications(){
        self.showHud()
        NotificationsREST.getNotifications { (success, receivedNotifications) in
            self.hideHud()
            if success {
                if let notifications = receivedNotifications {
                    for notification in notifications {
                        if notification.status == 1 {
                            self.oldNotifications.append(notification)
                        } else {
                            self.newNotifications.append(notification)
                        }
                    }
                    self.tableView.reloadData(with: .automatic)
                }
            }
        }
    }
    
    func getPickupFromHomeController(withID: Int)->PickupRequest{
        postNotification(notification: Notification.Name.requestPickupRequestWithId, object: nil)
        
        return PickupRequest()
    }
    
    func findPickupWith(_ id: Int)->PickupRequest?{
        for pickup in pickupsRequests {
            if pickup.id == id {
                return pickup
            }
        }
        return nil
    }
    
    func isAnyPickupOnGoing(completion: @escaping (_ id: Int,_ state:Bool)->Void){
        for pickup in pickupsRequests {
            if pickup.pickupStatus == .ongoing {
                completion(pickup.id!,true)
                return
            }
        }
        completion(-1,false)
    }
    
    func startPickupWith(_ id:Int, _ route:String, completion: @escaping (_ didComplete:Bool)->Void){
        showHud()
        isAnyPickupOnGoing { (cancelPickupId, status) in
            if status{
                self.hideHud()
                dispatch {
                    let startNewPickupVC = StartNewPickupConfirmViewController.create()
                    startNewPickupVC.currentPickupId = cancelPickupId
                    startNewPickupVC.newPickupId = id
                    startNewPickupVC.modalPresentationStyle = .overCurrentContext
                    startNewPickupVC.delegate = self
                    self.showModal(startNewPickupVC, animated: false, completion: nil)
                }
            } else {
                PickupREST.startPickup(id, route) { (success, error) in
                    self.hideHud()
                    if success {
                        print("The new pickup successfully started")
                        completion(true)
                    } else {
                        print(#function)
                        print("Error on completePickup : \(String(describing: error))")
                        completion(false)
                    }
                }
            }
        }
    }
    
    func printNotificationDetails(_ cell: NotificationsTableViewCell){
        print("--\nCouldn't get params from Notification")
        print("Params: \(String(describing: cell.notification.params))")
        print("Notification Type: \(cell.notification.notificationType)")
        print("Notification Type ID: \(cell.notification.notificationType.rawValue)\n--")
    }
    
    func markAllNotificationsAsRead(){
        NotificationsREST.markAllNotificationsAsRead { (success) in
            if success {
                print ("Notifications mark as Read")
            } else {
                print("Error marking notifications as read")
            }
        }
    }
    

}

extension NotificationsViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return newNotifications.count
        case 1:
            return oldNotifications.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(NotificationsTableViewCell.self, for: indexPath)
        switch indexPath.section {
        case 0:
            cell.notification = newNotifications[indexPath.row]
            break
        case 1:
            cell.notification = oldNotifications[indexPath.row]
            break
        default:
            break
        }
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! NotificationsTableViewCell
        switch cell.notification.notificationType {
        case .driverAssignPickup, .uncancelPickupDispatcher, .pickupOverdue, .pickupRescheduledDriver:
            guard let params = cell.notification.params else {
                printNotificationDetails(cell)
                return
            }
            guard let stringId = params["id_pickup_handler"] as? String else {
                print("Could not get id for pickup")
                return
            }
            let id = NSString(string: stringId).integerValue
            guard let pickup = findPickupWith(id) else {
                print("Could not find pickup with id: \(id)")
                return
            }
            
            switch pickup.pickupStatus {
            case .assigned:
                let currentPickupViewController = CurrentPickupViewController.create()
                currentPickupViewController.pickupRequest = pickup
                currentPickupViewController.cancelPickupDelegate = self
                currentPickupViewController.modalPresentationStyle = .overCurrentContext
                dispatch {
                    self.showModal(currentPickupViewController, animated: false) {
                        print("Presented CurrentPickupViewController from Notifications")
                    }
                }
                break
            case .canceled:
                let cancellPickupViewController = CancelledPickupViewController.create()
                cancellPickupViewController.pickupRequest = pickup
                cancellPickupViewController.delegate = self
                cancellPickupViewController.modalPresentationStyle = .overCurrentContext
                dispatch {
                    self.showModal(cancellPickupViewController, animated: false) {
                        print("Presented CancellPickupViewController from Notifications")
                    }
                }
                break
            case .completedWithDelays, .completedWithoutDelays:
                let ucompletePickupViewController = UncompletePickupViewController.create()
                ucompletePickupViewController.confirmActionDelegate = self
                ucompletePickupViewController.uncancelPickupDelegate = self
                ucompletePickupViewController.modalPresentationStyle = .overCurrentContext
                dispatch {
                    self.showModal(ucompletePickupViewController, animated: false) {
                        print("Presented CancellPickupViewController from Notifications")
                    }
                }
                break
            default:
                break
            }
            let currentPickupViewController = CurrentPickupViewController.create()
            currentPickupViewController.pickupRequest = pickup
            currentPickupViewController.cancelPickupDelegate = self
            currentPickupViewController.modalPresentationStyle = .overCurrentContext
            dispatch {
                self.showModal(currentPickupViewController, animated: false) {
                    print("Presented CurrentPickupViewController from Notifications")
                }
            }
            break
        case .newPickupNoteDriver:
            guard let params = cell.notification.params else {
                printNotificationDetails(cell)
                return
            }
            guard let id = params["id_pickup_handler"] as? Int else {
                print("Could not get id for pickup")
                return
            }
            guard let pickup = findPickupWith(id) else {
                print("Could not find pickup with id: \(id)")
                return
            }
            
            let pickupNotesVC = PickupNotesViewController.create()
            pickupNotesVC.modalPresentationStyle = .overCurrentContext
            pickupNotesVC.pickupRequest = pickup
            dispatch {
                self.present(pickupNotesVC, animated: true, completion: nil)
            }
            break
        case .driverCompletePickup:
            guard let params = cell.notification.params else {
                printNotificationDetails(cell)
                return
            }
            guard let id = params["id_pickup_handler"] as? Int else {
                print("Could not get id for pickup")
                return
            }
            
            guard let pickup = findPickupWith(id) else {
                print("Could not find pickup with id: \(id)")
                return
            }
            
            let uncompletePickupViewController = UncompletePickupViewController.create()
            uncompletePickupViewController.pickupRequest = pickup
            uncompletePickupViewController.confirmActionDelegate = self
            uncompletePickupViewController.modalPresentationStyle = .overCurrentContext
            dispatch {
                self.showModal(uncompletePickupViewController, animated: false) {
                    print("Presented UncompletePickupViewController from Notifications")
                }
            }
            break
        default:
            break
        }
    }

    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let isNightMode = ThemeManager.isNightMode()
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 40))
        headerView.backgroundColor = isNightMode ? Appearance.backgroundViewNight : backgroundView.backgroundColor
        let tableText = UILabel(frame: CGRect(x: 20, y: 0, width: 100, height: 40))
        tableText.textColor = isNightMode ? UIColor.white : UIColor(red255: 71, green255: 73, blue255: 85)
        headerView.addSubview(tableText)
        
        switch section {
        case 0:
            tableText.text = "New"
            return headerView
        case 1:
            tableText.text = "Old"
            let lineView = UIView(frame: CGRect(x: 60, y: 19, width: view.frame.width-80, height: 1))
            lineView.backgroundColor = isNightMode ? UIColor.white : UIColor.lightGray
            headerView.addSubview(lineView)
            return headerView
        default:
            return headerView
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let isNightMode = ThemeManager.isNightMode()
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 40))
        footerView.backgroundColor = isNightMode ? Appearance.backgroundViewNight : UIColor.clear
        
        let footerText = UILabel(frame: CGRect(x: 0, y: 0, width: footerView.frame.width, height: 40))
        footerText.textAlignment = .center
        footerText.textColor = isNightMode ? UIColor.white : UIColor.lightGray
        footerText.font = UIFont.systemFont(ofSize: 12)
        
        footerView.addSubview(footerText)
        
        let emptyView = UIView()
        emptyView.backgroundColor = UIColor.clear
        
        switch section {
        case 0:
            footerText.text = "There is no new notification at this time"
            return newNotifications.count == 0 ?  footerView :  emptyView
        case 1:
            footerText.text = "There is no old notification at this time"
            return oldNotifications.count == 0 ?  footerView :  emptyView
        default:
            return emptyView
        }
    }
    
}

// Delegations from Pickup View Controllers
extension NotificationsViewController: PickupDelegate, ConfirmPickupActionDelegate, StartNewPickupDelegate, CancelPickupDelegate, CancelPickupStatusDelegate, OverduePickupDelegate, UncancelPickupPickupDelegate, CancelledPickupDelegate {
    
    
    
    //CancelledPickupDelegate
    
    // UncancelPickupPickupDelegate
    func uncompletePickup() {
        var parent:HomeController? {
            for controller in navigationController!.viewControllers  {
                if controller is HomeController {
                    return controller as? HomeController
                }
            }
            return nil
        }
        guard let listController = parent?.listController else {
            print("Could not get listController from Notifications")
            return
        }
        listController.getPickups(completion: { (_) in
        })
    }
    
    
    //OverduePickupDelegate
    func isOverduedPickupCanceled(_ status: Bool) {
        if status {
            pickupDelegate?.refreshPickupDelegateFromCell()
        }
    }
    
    //Delegate comes from OverduePickupViewController
    func startOverduePickup(_ status: Bool, _ id: Int, _ route:String) {
        if status {
            startPickupWith(id, route) { (isCompleted) in
                if isCompleted {
                    print("Pickup started")
                    self.pickupDelegate?.triggerNavigation(id, route)
                }
            }
        }
    }
    
    func isOverduePickupCompleted(_ status: Bool) {
        if status {
            pickupDelegate?.refreshPickupDelegateFromCell()
        }
    }
    
    //CancelPickupStatusDelegate
    func shouldCancelPickup(_ status: Bool) {
        var parent:HomeController? {
            for controller in navigationController!.viewControllers  {
                if controller is HomeController {
                    return controller as? HomeController
                }
            }
            return nil
        }
        guard let listController = parent?.listController else {
            print("Could not get listController from Notifications")
            return
        }
        listController.getPickups(completion: { (_) in
        })
    }
    
    
    // CancelPickupDelegate
    
    // Delegate comes from CurrentPickupViewController,
    // when pickup is canceled
    func isPickupCanceled(_ status: Bool) {
        if status {
            pickupDelegate?.refreshPickupDelegateFromCell()
        }
    }
    // Delegate comes from CancelledPickupViewController
    // when pickup cancel is undone
    func isCancelPickupUndone(_ status: Bool) {
        if status {
            pickupDelegate?.refreshPickupDelegateFromCell()
        }
    }
    
    //Delegate come from CurrentPickupViewController
    func startPickup(_ status: Bool, _ id: Int, _ route:String) {
        if status {
            startPickupWith(id, route) { (isCompleted) in
                if isCompleted {
                    print("Pickup started")
                    self.pickupDelegate?.triggerNavigation(id, route)
                    self.pop()
                }
            }
        }
    }
    
    func isPickupCompleted(_ status: Bool) {
        if status {
            pickupDelegate?.refreshPickupDelegateFromCell()
        }
    }
    
    //StartNewPickupDelegate
    func startNewPickup(_ state: Bool, _ newPickupId: Int, _ cancelPickupId: Int) {
        if state {
            self.showHud()
            PickupREST.startAnotherPickup(newPickupId, cancelPickupId) { (success, error) in
                if success {
                    self.hideHud()
                    print("Starting another pickup")
                    self.pickupDelegate?.triggerNavigation(newPickupId, "")
                    self.pop()
                }
            }
        }
    }
    // PickupDelegate and StartNewPickupDelegate
    func refreshPickups() {
        pickupDelegate?.refreshPickupDelegateFromCell()
    }
    
    func refreshPickupDelegateFromCell() {
        pickupDelegate?.refreshPickupDelegateFromCell()
    }
    
    func triggerNavigation(_ id: Int, _ route: String) {
        startPickupWith(id, route) { (isCompleted) in
            if isCompleted {
                print("Pickup started")
                self.pickupDelegate?.triggerNavigation(id, route)
                self.pop()
            }
        }
    }
    
    func editPickup(_ pickup: PickupRequest) {
        // Does Nothing here
    }
}


// MARK: - View controller extension
extension NotificationsViewController{
    static func create() -> NotificationsViewController{
        return UIStoryboard.home.instantiate(NotificationsViewController.self)
    }
}
