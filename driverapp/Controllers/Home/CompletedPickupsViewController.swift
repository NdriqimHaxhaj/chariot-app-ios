//
//  CompletedPickupsViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 7/5/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

class CompletedPickupsViewController: UIViewController {
    // MARK: - IBOutlets
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var descriptionView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var footerView: UIView!
    
    
    // MARK: - Properties
    var completedPickups:[PickupRequest] = []
    var completedPickupsIds:[String] = []
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        setupUI()
        getCompletedPickups()
        
    }
    
    
    // MARK: - IBActions
    @IBAction func doneAndGoOnline(_ sender: UIButton) {
        self.showHud()
        let idArray = completedPickupsIds.joined(separator: ",")
        PickupREST.completeDropOff(completedPickupsIds: idArray) { (success, error) in
            self.hideHud()
            if success {
                self.postNotification(notification: Notification.Name.dropOffCompleted, object: nil)
                self.hideModal()
            } else {
                self.hideModal()
                print("Error completing drop off")
            }
        }
    }
    @IBAction func dismissButtonPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Functions
    
    func setupTableView(){
        tableView.register(CompletedPickupCell.self)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100
    }
    
    func setupUI(){
        if ThemeManager.isNightMode() {
            headerView.backgroundColor = Appearance.darkGrayColor
            descriptionView.backgroundColor = Appearance.darkGrayColor
            mainView.backgroundColor = Appearance.darkGrayNight
            headerLabel.textColor = UIColor.white
            descriptionLabel.textColor = UIColor.white
            footerView.backgroundColor = Appearance.darkGrayColor
        }
    }
    
    func getCompletedPickups(){
        PickupREST.getCompletedPickups { (success, pickups, error) in
            if success {
                guard let pickups = pickups else {return}
                self.completedPickups = pickups
                self.initCompletedPickupsIds()
                self.tableView.reloadData(with: .automatic)
            } else {
                print("Error downloading pickups")
            }
        }
    }
    
    func initCompletedPickupsIds(){
        for pickup in completedPickups {
            guard let id = pickup.id else {continue}
            completedPickupsIds.append("\(id)")
        }
        
    }
}

extension CompletedPickupsViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return completedPickups.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(CompletedPickupCell.self, for: indexPath)
        cell.pickupRequest = completedPickups[indexPath.row]
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
        cell.isSelected = true
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? CompletedPickupCell else {return}

        cell.isSelected = true
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
        guard let id = cell.pickupRequest.id else {return}
        if !completedPickupsIds.contains("\(id)") {
            completedPickupsIds.append("\(id)")
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? CompletedPickupCell else {return}
        tableView.deselectRow(at: indexPath, animated: true)
        cell.isSelected = false
        guard let id = cell.pickupRequest.id else {return}
        if completedPickupsIds.contains("\(id)"){
            for i in 0..<completedPickupsIds.count {
                if completedPickupsIds[i] == "\(id)" {
                    completedPickupsIds.remove(at: i)
                    return
                }
            }
        }
        
    }
}

extension CompletedPickupsViewController {
    static func create() -> CompletedPickupsViewController{
        return UIStoryboard.home.instantiate(CompletedPickupsViewController.self)
    }
}
