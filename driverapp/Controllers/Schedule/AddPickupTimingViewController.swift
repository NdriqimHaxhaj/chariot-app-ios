//
//  AddPickupTimingViewController.swift
//  Superintendent
//
//  Created by Arben Pnishi on 31/01/2018.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import FSCalendar
import SCLAlertView

enum DateTimeSelection {
    case date
    case time
}


class AddPickupTimingViewController: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var startDateTimeContainerView: UIView!
    @IBOutlet weak var startDateTimeHeaderView: UIView!
    @IBOutlet weak var startDateTimeScrollView: UIScrollView!
    @IBOutlet weak var startDateTimeCalendarView: FSCalendar!
    @IBOutlet weak var startDateTimePickerView: UIPickerView!
    @IBOutlet weak var startDateTimePickerViewContainer: UIView!
    
    @IBOutlet weak var endDateTimeContainerView: UIView!
    @IBOutlet weak var endDateTimeHeaderView: UIView!
    @IBOutlet weak var startDateLabel: UILabel!
    @IBOutlet weak var innerStartDateLabel: UILabel!
    @IBOutlet weak var startTimeLabel: UILabel!
    @IBOutlet weak var innerStartTimeLabel: UILabel!
    @IBOutlet weak var selectStartDateAndTimeLabel: UILabel!
    
    @IBOutlet weak var endDateTimeScrollView: UIScrollView!
    @IBOutlet weak var endDateTimeCalendarView: FSCalendar!
    @IBOutlet weak var endDateTimePickerView: UIPickerView!
    @IBOutlet weak var endDateTimePickerViewContainer: UIView!
    
    @IBOutlet weak var endDateLabel: UILabel!
    @IBOutlet weak var innerEndDateLabel: UILabel!
    @IBOutlet weak var endTimeLabel: UILabel!
    @IBOutlet weak var innerEndTimeLabel: UILabel!
    @IBOutlet weak var selectEndDateAndTimeLabel: UILabel!
    
    @IBOutlet weak var recurringButton: UIButton!
    @IBOutlet weak var recurringTypesHeaderView: UIView!
    
    @IBOutlet weak var warningLabel: UILabel!
    @IBOutlet weak var bottomTimeLabel: UILabel!
    @IBOutlet weak var doneButton: UIButton!
    
    // MARK: - Constraints
    @IBOutlet weak var startDateTimeViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var startDateCalendarHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var startDateTimeScrollViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var endDateTimeViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var endDateCalendarHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var endDateTimeScrollViewHeightConstraint: NSLayoutConstraint!
    
    //MARK: - Properties
    var hours = Array(0...23)
    var minutes = Array(0...59)
    let oneHour:TimeInterval = 1*60*60
    let twentyFourHours:TimeInterval = 24*60*60
    var selectedStartHour:Int?
    var selectedStartMinute:Int?
    var selectedEndHour:Int?
    var selectedEndMinute:Int?
    var configuredStartDate:Date?
    var configuredEndDate:Date?
    fileprivate var recurringTypes: [Int] = []
    fileprivate lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "h:mm a"
        return dateFormatter
    }()
    fileprivate var startSelectionMode:DateTimeSelection = .date
    fileprivate var endSelectionMode:DateTimeSelection = .date
    var pickupRequest: PMPickupRequest = PMPickupRequest()
    fileprivate var isStartDateTimeOpen = true
    fileprivate var isEndDateTimeOpen = true
    fileprivate var isRecurringFrequencyOpen = false
    fileprivate var isRecurringTypeSelected: Bool{
        return pickupRequest.recurringFrequency > -1
    }
    fileprivate var didLayoutSubviews = false
    fileprivate var firstDateInitialization = true
    fileprivate var selectedDate = Date()
    fileprivate var startDate = Date()
    fileprivate var endDate = Date().addingTimeInterval(1*60*60)
    fileprivate let gregorian: Calendar = Calendar(identifier: .gregorian)
    fileprivate var startTimeLabelInitialPosition = CGPoint.zero
    fileprivate var startDateLabelInitialPosition = CGPoint.zero
    fileprivate var endTimeLabelInitialPosition = CGPoint.zero
    fileprivate var endDateLabelInitialPosition = CGPoint.zero
    var progressView:UIProgressView!
    var pickupMode:CreatePickupMode = .create
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Create Pickup Request"
        updateRecurringState(selected: pickupRequest.recurringFrequency > -1)
        recurringFrequencies(recurringDays: pickupRequest.recurringDays, recurringFrequency: pickupRequest.recurringFrequency)
        recurringType(selectedAt: pickupRequest.recurringFrequency)
        setupLocalizables()
        setupPickerView()
        addObservers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getInitialPositions()
        forceHideEndDateTimeSection()
        self.view.layoutIfNeeded()
        checkTheme()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if !didLayoutSubviews{
            didLayoutSubviews = true
            setupCalendarView()
            updatePickupDayTimeFrequencyLabel()
            getInitialPositions()
        }
    }
    
    // MARK: - Functions
    
    func addObservers(){
        registerNotification(notification: Notification.Name.changeTheme, selector: #selector(checkTheme))
    }
    
    @objc func checkTheme(){
        if ThemeManager.isNightMode() {
            backgroundView.backgroundColor = Appearance.backgroundViewNight
            mainView.backgroundColor = Appearance.darkGrayNight
            
            startDateTimeContainerView.backgroundColor = Appearance.backgroundViewNight
            startDateTimeHeaderView.backgroundColor = Appearance.darkGrayNight
            
            startDateLabel.textColor = UIColor.white
            startDateLabel.backgroundColor = Appearance.darkGrayNight
            innerStartDateLabel.textColor = UIColor.white
            innerStartDateLabel.backgroundColor = Appearance.darkGrayNight
            
            startTimeLabel.textColor = UIColor.white
            startTimeLabel.backgroundColor = Appearance.darkGrayNight
            innerStartTimeLabel.textColor = UIColor.white
            innerStartTimeLabel.backgroundColor = Appearance.darkGrayNight
            
            startDateTimeCalendarView.appearance.titleDefaultColor = UIColor.white
            startDateTimeCalendarView.appearance.todayColor = UIColor.white
            
            endDateTimeContainerView.backgroundColor = Appearance.backgroundViewNight
            endDateTimeHeaderView.backgroundColor = Appearance.darkGrayNight
            endDateTimeScrollView.backgroundColor = Appearance.darkGrayNight
            
            endDateLabel.textColor = UIColor.white
            endDateLabel.backgroundColor = Appearance.darkGrayNight
            innerEndDateLabel.textColor = UIColor.white
            innerEndDateLabel.backgroundColor = Appearance.darkGrayNight
            
            endTimeLabel.textColor = UIColor.white
            endTimeLabel.backgroundColor = Appearance.darkGrayNight
            innerEndTimeLabel.textColor = UIColor.white
            innerEndTimeLabel.backgroundColor = Appearance.darkGrayNight
            
            endDateTimeCalendarView.appearance.titleDefaultColor = UIColor.white
            endDateTimeCalendarView.appearance.todayColor = UIColor.white
            
            recurringTypesHeaderView.backgroundColor = Appearance.darkGrayNight
            recurringButton.setTitleColor(.white, for: .normal)
            
            warningLabel.textColor = UIColor.white
        }
    }
    
    func getInitialPositions(){
        startTimeLabelInitialPosition.x = startTimeLabel.center.x
        startTimeLabelInitialPosition.y = startTimeLabel.center.y
        startDateLabelInitialPosition.x = startDateLabel.center.x
        startDateLabelInitialPosition.y = startDateLabel.center.y
        endTimeLabelInitialPosition.x = endTimeLabel.center.x
        endTimeLabelInitialPosition.y = endTimeLabel.center.y
        endDateLabelInitialPosition.x = endDateLabel.center.x
        endDateLabelInitialPosition.y = endDateLabel.center.y
    }
    
    func forceHideEndDateTimeSection(){
        self.selectEndDateAndTimeLabel.alpha = 0
        self.innerEndTimeLabel.center = self.endTimeLabelInitialPosition
        self.innerEndDateLabel.center = self.endDateLabelInitialPosition
        self.endTimeLabel.center = self.endTimeLabelInitialPosition
        self.endDateLabel.center = self.endDateLabelInitialPosition
        self.endDateTimeCalendarView.alpha = 0
        self.endDateTimePickerView.isHidden = true
        self.innerEndTimeLabel.alpha = 0
        self.innerEndDateLabel.alpha = 0
        self.endTimeLabel.alpha = 1
        self.endDateLabel.alpha = 1
        self.endDateTimeViewHeightConstraint.constant = 60
        self.endDateTimeScrollViewHeightConstraint.constant = 0
        isEndDateTimeOpen = false
    }
    
    func setupCalendarView(){
        startDateTimeCalendarView.register(FSCalendarCell.self, forCellReuseIdentifier: "cell")
        endDateTimeCalendarView.register(FSCalendarCell.self, forCellReuseIdentifier: "cell")
        
        startDateTimeCalendarView.delegate = self
        startDateTimeCalendarView.dataSource = self
        startDateTimeCalendarView.scope = .month
        startDateTimeCalendarView.locale = Locale.autoupdatingCurrent
        
        
        endDateTimeCalendarView.delegate = self
        endDateTimeCalendarView.dataSource = self
        endDateTimeCalendarView.scope = .month
        endDateTimeCalendarView.locale = Locale.autoupdatingCurrent
        
        if pickupMode != .edit {
            pickupRequest.startPickupTime = Date.now()
            pickupRequest.endPickupTime = Date.now().addingTimeInterval(oneHour)
        }
        configuredStartDate = pickupRequest.startPickupTime
        configuredEndDate = pickupRequest.endPickupTime
        startDateTimeCalendarView.setCurrentPage(configuredStartDate!, animated: false)
        startDateTimeCalendarView.select(configuredStartDate)
        endDateTimeCalendarView.setCurrentPage(configuredEndDate!, animated: false)
        endDateTimeCalendarView.select(configuredEndDate)
        updateTimeLabels()
    }
    
    func updatePickupDayTimeFrequencyLabel(){
        //Get day
        var day:String = ""
        
        switch configuredStartDate?.dayNumberOfWeek {
        case 2:
            day = "monday".localized
        case 3:
            day = "tuesday".localized
        case 4:
            day = "wednesday".localized
        case 5:
            day = "thursday".localized
        case 6:
            day = "friday".localized
        case 7:
            day = "saturday".localized
        case 1:
            day = "sunday".localized
        default:
            day = ""
        }
        
        //Get time
        dateFormatter.dateFormat = "H:mm"
        let time = "\(dateFormatter.string(from: configuredStartDate ?? selectedDate))"
        
        //Get frequency
        let frequency:String
        switch pickupRequest.recurringFrequency {
        case 1:
            frequency = "every day"
        case 2:
            frequency = "every week"
        case 3:
            frequency =  "every month"
        default:
            frequency = ""
        }
        
        
        bottomTimeLabel.text = "\(day), \(time) \(frequency)"
    }
    
    func recurringType(selectedAt index: Int){
        pickupRequest.recurringFrequency = index
        refreshRecurringViews()
        reloadCalendarData()
        updatePickupDayTimeFrequencyLabel()
    }
    
    func setupLocalizables(){
        //        pickupTimeStaticLabel.text = "pickup_time".localized
        //        returnTimeStaticLabel.text = "return_time".localized
        //        durationStaticLabel.text = "duration".localized
        //        recurringButton.setTitle("this_is_recurring_request".localized, for: .normal)
        //        everydayButton.setTitle("every_day".localized, for: .normal)
        //        everyweekButton.setTitle("every_week".localized, for: .normal)
        //        everymonthButton.setTitle("every_month".localized, for: .normal)
        //        warningLabel.text = "unless_you_change".localized()
        //        doneButton.setTitle("done".localized, for: .normal)
    }
    
    func setupPickerView(){
        var calendar = Calendar(identifier: .gregorian)
        calendar.timeZone = TimeZone(abbreviation: "UTC")!
        if pickupMode == .edit {
            let startDate = pickupRequest.startPickupTime
            let endDate = pickupRequest.endPickupTime
            let startHourRow = hours.index(of: startDate.inGMTRegion().hour)
            let endHourRow = hours.index(of: endDate.inGMTRegion().hour + 1)
            let startMinuteRow = minutes.index(of: startDate.minute)
            let endMinuteRow = minutes.index(of: endDate.minute)
            
            selectedStartHour = hours[startHourRow!]
            selectedStartMinute = minutes[startMinuteRow!]
            selectedEndHour = hours[endHourRow!]
            selectedEndMinute = minutes[startMinuteRow!]
//            setConfiguredDate()
            updateTimeLabels()
            
            startDateTimePickerView.selectRow(startHourRow!, inComponent: 0, animated: false)
            startDateTimePickerView.selectRow(startMinuteRow!, inComponent: 1, animated: false)
            
            endDateTimePickerView.selectRow(endHourRow!, inComponent: 0, animated: false)
            endDateTimePickerView.selectRow(endMinuteRow!, inComponent: 1, animated: false)
        } else {
            let date = calendar.date(from: calendar.dateComponents([.year, .month, .day, .hour, .minute, .second], from: Date.now().inGMTRegion().absoluteDate))!
            let startHourRow = hours.index(of: date.inGMTRegion().hour)
            let endHourRow = hours.index(of: date.inGMTRegion().hour + 1)
            let startMinuteRow = minutes.index(of: date.minute)
            
            selectedStartHour = hours[startHourRow!]
            selectedStartMinute = minutes[startMinuteRow!]
            selectedEndHour = hours[endHourRow!]
            selectedEndMinute = minutes[startMinuteRow!]
            setConfiguredDate()
            updateTimeLabels()
            
            startDateTimePickerView.selectRow(startHourRow!, inComponent: 0, animated: false)
            startDateTimePickerView.selectRow(startMinuteRow!, inComponent: 1, animated: false)
            
            endDateTimePickerView.selectRow(endHourRow!, inComponent: 0, animated: false)
            endDateTimePickerView.selectRow(startMinuteRow!, inComponent: 1, animated: false)
        }
        
        
    }
    
    func setConfiguredDate(){
        var calendar = Calendar.autoupdatingCurrent
        calendar.timeZone = TimeZone(abbreviation: "UTC")!
        var dateComponents = calendar.dateComponents([.year,.month,.day,.hour,.minute], from: pickupRequest.startPickupTime)
        dateComponents.hour = selectedStartHour!
        dateComponents.minute = selectedStartMinute
        configuredStartDate = calendar.date(from: dateComponents)
        pickupRequest.startPickupTime =  configuredStartDate ?? selectedDate
        
        dateComponents = calendar.dateComponents([.year,.month,.day,.hour,.minute], from: pickupRequest.endPickupTime)
        dateComponents.hour = selectedEndHour!
        dateComponents.minute = selectedEndMinute
        configuredEndDate = calendar.date(from: dateComponents)
        pickupRequest.endPickupTime = configuredEndDate ?? selectedDate
        
        if (pickupRequest.startPickupTime.timeIntervalSinceNow - pickupRequest.endPickupTime.timeIntervalSinceNow) > -3599.9999989271164 {
            pickupRequest.endPickupTime = pickupRequest.startPickupTime.addingTimeInterval(oneHour)
            selectedEndHour = hours[pickupRequest.endPickupTime.inGMTRegion().hour]
            selectedEndMinute = minutes[pickupRequest.endPickupTime.inGMTRegion().minute]
            endDateTimePickerView.selectRow(selectedEndHour!, inComponent: 0, animated: false)
            endDateTimePickerView.selectRow(selectedEndMinute!, inComponent: 1, animated: false)
            setConfiguredDate()
            updateTimeLabels()
        }
    }
    
    func updateTimeLabels(){
        dateFormatter.dateFormat = "EEEE, MMM dd"
        startDateLabel.text = "  \(dateFormatter.string(from: pickupRequest.startPickupTime))  "
        innerStartDateLabel.text = "  \(dateFormatter.string(from: pickupRequest.startPickupTime))  "
        dateFormatter.dateFormat = "H:mm"
        startTimeLabel.text = "  \(dateFormatter.string(from: pickupRequest.startPickupTime))  "
        innerStartTimeLabel.text = "  \(dateFormatter.string(from: pickupRequest.startPickupTime))  "
        
        dateFormatter.dateFormat = "EEEE, MMM dd"
        endDateLabel.text = "  \(dateFormatter.string(from: pickupRequest.endPickupTime))  "
        innerEndDateLabel.text = "  \(dateFormatter.string(from: pickupRequest.endPickupTime))  "
        dateFormatter.dateFormat = "H:mm"
        endTimeLabel.text = "  \(dateFormatter.string(from: pickupRequest.endPickupTime))  "
        innerEndTimeLabel.text = "  \(dateFormatter.string(from: pickupRequest.endPickupTime))  "
        
        updatePickupDayTimeFrequencyLabel()
    }
    
    func updateRecurringState(selected: Bool) {
        if !selected{
            pickupRequest.recurringFrequency = -1
            //
        }
        //        if selected {
        //            hideRecurringViewHolder()
        //        } else {
        //            showRecurringViewHolder()
        //        }
        recurringType(selectedAt: pickupRequest.recurringFrequency)
        updatePickupDayTimeFrequencyLabel()
    }
    
    func refreshRecurringViews() {
        recurringButton.setTitleColor(isRecurringTypeSelected ? Appearance.greenColor : ThemeManager.isNightMode() ? .white : UIColor.darkGray, for: .normal)
        switch pickupRequest.recurringFrequency {
        case 1:
            startDateTimeCalendarView.allowsSelection = true
            startDateTimeCalendarView.allowsMultipleSelection = false
//            everydayButton.setTitleColor(Appearance.greenColor, for: .normal)
//            everydayImageView.isHidden = false
            
        case 2:
            startDateTimeCalendarView.allowsSelection = true
            startDateTimeCalendarView.allowsMultipleSelection = true
//            everyweekButton.setTitleColor(Appearance.greenColor, for: .normal)
//            everyweekImageView.isHidden = false
            
        case 3:
            startDateTimeCalendarView.allowsSelection = true
            startDateTimeCalendarView.allowsMultipleSelection = true
//            everymonthButton.setTitleColor(Appearance.greenColor, for: .normal)
//            everymonthImageView.isHidden = false
            
        default:
            break
        }
    }
    
    func reloadCalendarData(){
        dispatch {
            self.startDateTimeCalendarView.reloadData()
            self.endDateTimeCalendarView.reloadData()
        }
    }
    
    fileprivate func createPickup(){
        if doubleCheckDates() {
            showHud()
            
            PickupREST.create(pickup: pickupRequest) { (success, error) in
                self.hideHud()
                if success {
                    self.applyCreateOrEditedPickupToPickupsFilter(from: self.pickupRequest)
                }
                self.popToRoot()
            }
        }
    }
    
    fileprivate func editPickup(){
        if doubleCheckDates() {
            showHud()
            
            PickupREST.edit(pickup: pickupRequest) { (success, pickup, error) in
                self.hideHud()
                if success {
                    self.applyCreateOrEditedPickupToPickupsFilter(from: self.pickupRequest)
                }
                self.popToRoot()
            }
        }
        
    }
    
    fileprivate func doubleCheckDates()->Bool{
        let startTime = pickupRequest.startPickupTime
        let endTime = pickupRequest.endPickupTime
        
        // Reset date format
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let timeIntervalDifference:TimeInterval = endTime.timeIntervalSinceNow - startTime.timeIntervalSinceNow
        if timeIntervalDifference >= oneHour &&
            timeIntervalDifference <= twentyFourHours {
            return true
        } else {
            showWrongTimeAlert()
            return false
        }
    }
    
    fileprivate func applyCreateOrEditedPickupToPickupsFilter(from pickup: PMPickupRequest){
        guard let navigationController = self.navigationController else {return}
        guard let homeController = navigationController.viewControllers[0] as? HomeController else {return}
        guard let homeListController = homeController.listController else {return}
        guard let toDoListViewController = homeListController.toDoListViewController else {return}
        if pickup.startPickupTime < toDoListViewController.startTimeFilter {
            toDoListViewController.startTimeFilter = pickup.startPickupTime
        }
        if pickup.endPickupTime > toDoListViewController.endTimeFilter {
            toDoListViewController.endTimeFilter = pickup.endPickupTime
        }
        homeListController.getPickups { (success) in
            if success {
                print("DEBUG: Pickups refreshed on HomeListController")
            }
        }
    }
    
    // MARK: - IBActions
    @IBAction func startDateTimeViewTap(_ sender: UITapGestureRecognizer) {
        if isStartDateTimeOpen {
            hideStartDateTimeSection { (_) in }
        } else {
            hideEndDateTimeSection { (completed) in
                if completed {
                    self.showStartDateTimeSection()
                }
            }
            
        }
    }
    @IBAction func endDateTimeViewTap(_ sender: UITapGestureRecognizer) {
        if isEndDateTimeOpen {
            hideEndDateTimeSection { (_) in}
        } else {
            hideStartDateTimeSection { (completed) in
                if completed {
                    self.showEndDateTimeSection()
                }
            }
            
        }
    }
    
    
    
    func hideStartDateTimeSection(completionHandler: @escaping (_ completed:Bool)->()){
        UIView.animateKeyframes(withDuration: 0.7, delay: 0, animations: {
            
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.1, animations: {
                self.selectStartDateAndTimeLabel.alpha = 0
            })
            
            UIView.addKeyframe(withRelativeStartTime: 0.1, relativeDuration: 0.3, animations: {
                self.innerStartTimeLabel.center.y = self.startTimeLabelInitialPosition.y
                self.innerStartDateLabel.center.y = self.startDateLabelInitialPosition.y
                self.startTimeLabel.center.y = self.startTimeLabelInitialPosition.y
                self.startDateLabel.center.y = self.startDateLabelInitialPosition.y
            })
            
            UIView.addKeyframe(withRelativeStartTime: 0.4, relativeDuration: 0.2, animations: {
                self.startDateTimeCalendarView.alpha = 0
                self.startDateTimePickerView.isHidden = true
                self.innerStartTimeLabel.alpha = 0
                self.innerStartDateLabel.alpha = 0
                self.startTimeLabel.alpha = 1
                self.startDateLabel.alpha = 1
            })
            
            self.startDateTimeViewHeightConstraint.constant = 60
            self.startDateTimeScrollViewHeightConstraint.constant = 0
            UIView.addKeyframe(withRelativeStartTime: 0.6, relativeDuration: 0.1, animations: {
                self.view.layoutIfNeeded()
            })
        }, completion:{ _ in
            completionHandler(true)
            print("StartDateTimeHidden")
        })
        isStartDateTimeOpen = false
    }
    
    func hideEndDateTimeSection(completionHandler: @escaping (_ completed:Bool)->()){
        UIView.animateKeyframes(withDuration: 0.7, delay: 0, animations: {
            
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.1, animations: {
                self.selectEndDateAndTimeLabel.alpha = 0
            })
            
            UIView.addKeyframe(withRelativeStartTime: 0.1, relativeDuration: 0.3, animations: {
                self.innerEndTimeLabel.center.y = self.endTimeLabelInitialPosition.y
                self.innerEndDateLabel.center.y = self.endDateLabelInitialPosition.y
                self.endTimeLabel.center.y = self.endTimeLabelInitialPosition.y
                self.endDateLabel.center.y = self.endDateLabelInitialPosition.y
            })
            
            UIView.addKeyframe(withRelativeStartTime: 0.4, relativeDuration: 0.2, animations: {
                self.endDateTimeCalendarView.alpha = 0
                self.endDateTimePickerView.isHidden = true
                self.innerEndTimeLabel.alpha = 0
                self.innerEndDateLabel.alpha = 0
                self.endTimeLabel.alpha = 1
                self.endDateLabel.alpha = 1
            })
            
            self.endDateTimeViewHeightConstraint.constant = 60
            self.endDateTimeScrollViewHeightConstraint.constant = 0
            UIView.addKeyframe(withRelativeStartTime: 0.6, relativeDuration: 0.1, animations: {
                self.view.layoutIfNeeded()
            })
        }, completion:{ _ in
            completionHandler(true)
            print("EndDateTimeHidden")
        })
        isEndDateTimeOpen = false
    }
    
    func showStartDateTimeSection(){
        UIView.animateKeyframes(withDuration: 0.7, delay: 0, animations: {
            
            self.startDateTimeViewHeightConstraint.constant = 390
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.1, animations: {
                self.view.layoutIfNeeded()
            })
            
            self.startDateTimeScrollViewHeightConstraint.constant = 250
            self.startDateCalendarHeightConstraint.constant = 250
            self.startDateTimePickerView.frame.size.height = 250
            UIView.addKeyframe(withRelativeStartTime: 0.1, relativeDuration: 0.3, animations: {
                self.startTimeLabel.center.y = self.innerStartTimeLabel.center.y
                self.startDateLabel.center.y = self.innerStartDateLabel.center.y
                self.view.layoutIfNeeded()
            })
            
            UIView.addKeyframe(withRelativeStartTime: 0.4, relativeDuration: 0.2, animations: {
                self.startTimeLabel.alpha = 0
                self.innerStartTimeLabel.alpha = 1
                self.startDateLabel.alpha = 0
                self.innerStartDateLabel.alpha = 1
                self.startDateTimeCalendarView.alpha = 1
                self.startDateTimePickerView.isHidden = false
            })
            
            UIView.addKeyframe(withRelativeStartTime: 0.6, relativeDuration: 0.1, animations: {
                self.selectStartDateAndTimeLabel.alpha = 1
            })
            
        }, completion:{ _ in
            print("StartDateTimeShown")
        })
        delay(delay: 0.1) {
            self.scrollToPage(self.startSelectionMode == .date ? 0 : 1, on: self.startDateTimeScrollView)
        }
        isStartDateTimeOpen = true
    }
    
    func showEndDateTimeSection(){
        UIView.animateKeyframes(withDuration: 0.7, delay: 0, animations: {
            
            self.endDateTimeViewHeightConstraint.constant = 390
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.1, animations: {
                self.view.layoutIfNeeded()
            })
            
            
            self.endDateTimeScrollViewHeightConstraint.constant = 250
            self.endDateCalendarHeightConstraint.constant = 250
            self.endDateTimePickerView.frame.size.height = 250
            UIView.addKeyframe(withRelativeStartTime: 0.1, relativeDuration: 0.3, animations: {
                self.endTimeLabel.center.y = self.innerEndTimeLabel.center.y
                self.endDateLabel.center.y = self.innerEndDateLabel.center.y
                self.view.layoutIfNeeded()
            })
            
            UIView.addKeyframe(withRelativeStartTime: 0.4, relativeDuration: 0.2, animations: {
                self.endDateTimeCalendarView.alpha = 1
                self.endDateTimePickerView.alpha = 1
                self.endDateTimePickerView.isHidden = false
                self.innerEndTimeLabel.alpha = 1
                self.innerEndDateLabel.alpha = 1
                self.endTimeLabel.alpha = 0
                self.endDateLabel.alpha = 0
            })
            
            UIView.addKeyframe(withRelativeStartTime: 0.6, relativeDuration: 0.1, animations: {
                self.selectEndDateAndTimeLabel.alpha = 1
            })
            
        }, completion:{ _ in
            print("EndDateTimeShown")
        })
        delay(delay: 0.1) {
            self.scrollToPage(self.endSelectionMode == .date ? 0 : 1, on: self.endDateTimeScrollView)
        }
        isEndDateTimeOpen = true
    }
    
    @IBAction func startDateTimeTap(_ sender: UITapGestureRecognizer) {
        guard let sender = sender.view as? UILabel else {return}
        UIView.animate(withDuration: 0.3) {
            self.innerStartDateLabel.backgroundColor = UIColor.clear
            self.innerStartTimeLabel.backgroundColor = UIColor.clear
        }
        scrollToPage(sender.tag, on: startDateTimeScrollView)
    }
    
    @IBAction func endDateTimeTap(_ sender: UITapGestureRecognizer) {
        guard let sender = sender.view as? UILabel else {return}
        UIView.animate(withDuration: 0.3) {
            self.innerEndDateLabel.backgroundColor = UIColor.clear
            self.innerEndTimeLabel.backgroundColor = UIColor.clear
        }
        scrollToPage(sender.tag, on: endDateTimeScrollView)
    }
    
    
    @IBAction func recurringButtonPressed(_ sender: UIButton) {
        let recurringFrequencyViewController = RecurringFrequencyViewController.create()
        recurringFrequencyViewController.recurringFrequency = pickupRequest.recurringFrequency
        recurringFrequencyViewController.delegate = self
        push(recurringFrequencyViewController)
    }

    
    @IBAction func doneButtonPressed(_ sender: UIButton) {
        
        if doneButton.isEnabled {
            let currentDate = Date.now()
            if pickupRequest.startPickupTime >= currentDate && pickupRequest.recurringFrequency != -1 {
                if pickupMode == .edit {
                    editPickup()
                } else {
                    createPickup()
                }
            } else {
                if pickupRequest.recurringFrequency == -1 {
                    if pickupMode == .edit {
                        editPickup()
                    } else {
                        createPickup()
                    }
                } else {
                    showWrongTimeAlert()
                }
            }
            doneButton.isEnabled = false
        }
        progressView.removeFromSuperview()
        
    }
    
    func showWrongTimeAlert(){
        let alert = UIAlertController(title: "Wrong date", message: "The selected date/time is not correct. Please review", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_) in
            self.doneButton.isEnabled = true
        }))
        self.showModal(alert)
        print("The selected date/time is not correct. Please review")
    }
    
    func scrollToPage(_ page: Int, on scrollView: UIScrollView) {
        UIView.animate(withDuration: 0.3) {
            scrollView.contentOffset.x = scrollView.frame.width * CGFloat(page)
            switch scrollView {
            case self.startDateTimeScrollView:
                if page == 0 {
                    self.innerStartDateLabel.backgroundColor = ThemeManager.isNightMode() ? Appearance.darkGrayNight : Appearance.selectedTimeLabelColor
                    self.startSelectionMode = .date
                } else {
                    self.innerStartTimeLabel.backgroundColor = ThemeManager.isNightMode() ? Appearance.darkGrayNight : Appearance.selectedTimeLabelColor
                    self.startSelectionMode = .time
                }
                break
            case self.endDateTimeScrollView:
                if page == 0 {
                    self.innerEndDateLabel.backgroundColor = ThemeManager.isNightMode() ? Appearance.darkGrayNight : Appearance.selectedTimeLabelColor
                    self.endSelectionMode = .date
                } else {
                    self.innerEndTimeLabel.backgroundColor = ThemeManager.isNightMode() ? Appearance.darkGrayNight : Appearance.selectedTimeLabelColor
                    self.endSelectionMode = .time
                }
                break
            default:
                break
            }
        }
    }
}

//MARK: - Extensions
extension AddPickupTimingViewController{
    static func create() -> AddPickupTimingViewController{
        return UIStoryboard.schedule.instantiate(AddPickupTimingViewController.self)
    }
}

extension AddPickupTimingViewController: FSCalendarDelegate, FSCalendarDataSource, FSCalendarDelegateAppearance {
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        
        selectedDate = date
        if calendar == startDateTimeCalendarView {
            pickupRequest.startPickupTime = pickupRequest.startPickupTime.getDMY(from: selectedDate)
            var components = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: selectedDate)
            components.hour = configuredStartDate?.inGMTRegion().hour
            components.minute = configuredEndDate?.inGMTRegion().minute
            var calendar = Calendar.autoupdatingCurrent
            calendar.timeZone = TimeZone(abbreviation: "UTC")!
            let calculatedDate = calendar.date(from: components)
            configuredStartDate = calculatedDate ?? selectedDate
        } else {
            pickupRequest.endPickupTime = pickupRequest.endPickupTime.getDMY(from: selectedDate)
            var components = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: selectedDate)
            components.hour = configuredEndDate?.inGMTRegion().hour
            components.minute = configuredEndDate?.inGMTRegion().minute
            var calendar = Calendar.autoupdatingCurrent
            calendar.timeZone = TimeZone(abbreviation: "UTC")!
            let calculatedDate = calendar.date(from: components)
            configuredEndDate = calculatedDate ?? selectedDate
        }
        
        reloadCalendarData()
        updatePickupDayTimeFrequencyLabel()
        updateTimeLabels()
    }
    
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        let recurringValue = pickupRequest.recurringFrequency == 1 ? 1 : 0
        return date.isNotInThePastFrom(date: startDate) ? recurringValue : 0
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, eventColorFor date: Date) -> UIColor? {
        return pickupRequest.recurringFrequency == 1 ? Appearance.greenColor : nil
    }
    
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        if calendar == self.startDateTimeCalendarView {
            self.startDateCalendarHeightConstraint.constant = bounds.height
            
        } else {
            self.endDateCalendarHeightConstraint.constant = bounds.height
        }
        dispatch {
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            })
        }
    }
    
    func calendar(_ calendar: FSCalendar, cellFor date: Date, at position: FSCalendarMonthPosition) -> FSCalendarCell {
        let cell = calendar.dequeueReusableCell(withIdentifier: "cell", for: date, at: position)
        let isStartDateTimeCalendarView = calendar == startDateTimeCalendarView
        switch pickupRequest.recurringFrequency {
        case 1:
            calendar.deselect(date)
            if selectedDate.isInSameDay(date: date){
                calendar.select(selectedDate)
            }
            
        case 2:
            if isStartDateTimeCalendarView {
                if selectedDate.dayNumberOfWeek == date.dayNumberOfWeek{
                    if date.isNotInThePastFrom(date: startDate){
                        calendar.select(date)
                    } else{
                        calendar.deselect(date)
                    }
                }
            } else {
                if date.isNotInThePastFrom(date: endDate){
                    calendar.select(date)
                } else{
                    calendar.deselect(date)
                }
            }
        case 3:
            if isStartDateTimeCalendarView {
                if selectedDate.dayNumberOfMonth == date.dayNumberOfMonth{
                    if date.isNotInThePastFrom(date: startDate){
                        calendar.select(date)
                    }
                }else{
                    calendar.deselect(date)
                }
            } else {
                if selectedDate.dayNumberOfMonth == date.dayNumberOfMonth{
                    if date.isNotInThePastFrom(date: endDate){
                        calendar.select(date)
                    }
                }else{
                    calendar.deselect(date)
                }
            }
        default:
            break
        }
        if isStartDateTimeCalendarView {
            cell.alpha = date.isNotInThePastFrom(date: startDate) ? 1.0 : 0.5
        } else {
            cell.alpha = date.isNotInThePastFrom(date: endDate) ? 1.0 : 0.5
        }
        
        return cell
    }
    
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        if calendar == startDateTimeCalendarView {
            return date.isNotInThePastFrom(date: startDate)
        } else {
            return date.isNotInThePastFrom(date: endDate)
        }
    }
    
    func calendar(_ calendar: FSCalendar, shouldDeselect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        return false
    }
}

extension AddPickupTimingViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch component {
        case 0:
            return hours.count
        case 1:
            return minutes.count
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch component {
        case 0:
            return "\(hours[row])"
        case 1:
            return "\(minutes[row])"
        default:
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        return 80
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        var titleData = ""
        switch component {
        case 0:
            titleData = "\(hours[row])"
        case 1:
            titleData = "\(minutes[row])"
        default:
            titleData = ""
        }
        let myTitle = NSAttributedString(
            string: titleData,
            attributes: [
                NSAttributedStringKey.font: UIFont.systemFontSize,
                NSAttributedStringKey.foregroundColor: Appearance.greenColor
            ]
        )
        return myTitle
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == startDateTimePickerView {
            switch component {
            case 0:
                selectedStartHour = hours[row]
            case 1:
                selectedStartMinute = minutes[row]
            default:
                break
            }
            setConfiguredDate()
            updateTimeLabels()
//            pickupRequest.startPickupTime = configuredStartDate ?? selectedDate
//            if pickupRequest.startPickupTime > pickupRequest.endPickupTime {
//                pickupRequest.endPickupTime = pickupRequest.startPickupTime.addingTimeInterval(oneHour)
//                selectedStartHour = hours[row+1]
//                startDateTimePickerView.selectRow(row+1, inComponent: 0, animated: false)
//                setConfiguredDate()
//                updateTimeLabels()
//            }
        } else {
            switch component {
            case 0:
                selectedEndHour = hours[row]
            case 1:
                selectedEndMinute = minutes[row]
            default:
                break
            }
            setConfiguredDate()
            updateTimeLabels()
//            pickupRequest.endPickupTime = configuredEndDate ?? selectedDate
        }
    }
    
}

extension AddPickupTimingViewController: RecurringFrequencyDelegate {
    func recurringFrequencies(recurringDays: [Int], recurringFrequency: Int) {
        self.pickupRequest.recurringDays = recurringDays
        self.pickupRequest.recurringFrequency = recurringFrequency
        warningLabel.frame.size.height = 34
        warningLabel.alpha = 1
        let frequency:String
        switch pickupRequest.recurringFrequency {
        case -1:
            frequency = "Does not repeat"
            warningLabel.frame.size.height = 0
            warningLabel.alpha = 0
        case 1:
            frequency = "Every day"
        case 2:
            frequency = "Every week"
        case 3:
            frequency =  "Every month"
        default:
            frequency = ""
        }
        
        recurringButton.setTitle(frequency, for: .normal)
        startDateTimeCalendarView.reloadData()
        endDateTimeCalendarView.reloadData()
        updatePickupDayTimeFrequencyLabel()
    }
}
