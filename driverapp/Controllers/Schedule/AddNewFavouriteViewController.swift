//
//  AddNewFavouriteViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 10/24/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import CoreLocation
import NMAKit

protocol FavouritePlaceDelegate {
    func addNewAddressToFavourite(_ place:NMAPlace)
}

class AddNewFavouriteViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchField: UITextField!
    
    // MARK: - Properties
    var addresses:[FavouriteAddress]!
    var locationManager = CLLocationManager()
    var userLocation : CLLocation?
    var geoCoord: NMAGeoCoordinates?
    var isLocationAllowed: Bool = false
    var navigationManager: NMANavigationManager?
    var results: NMADiscoveryPage? {
        didSet {
            for result in (results?.discoveryResults)! {
                print(result.name ?? "")
            }
        }
    }
    var suggestions:[NMAPlace] = [] {
        didSet{
            if !suggestions.isEmpty {
                tableView.reloadData()
                tableView.alpha = 1
            } else {
                tableView.alpha = 0
            }
        }
    }
    var currentPlace:NMAPlace?{
        
        didSet{
            func showWrongAddressAlert(){
                let alert = UIAlertController(title: "Wrong address", message: "The selected address is not valid, please choose another", preferredStyle: .alert)
                let action = UIAlertAction(title: "Ok", style: .default, handler : nil)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            
            guard let postalCode = currentPlace?.location?.address?.postalCode, !postalCode.isEmpty else {
                showWrongAddressAlert()
                return
            }
            guard let latitude = currentPlace?.location?.position?.latitude,
                !latitude.isZero else {
                    showWrongAddressAlert()
                    return
            }
            guard let longitude = currentPlace?.location?.position?.longitude,
                !longitude.isZero else {
                    showWrongAddressAlert()
                    return
            }
            
            delegate?.addNewAddressToFavourite(currentPlace!)
            createAndSaveAddressFrom(currentPlace!)
            pop()
        }
        
    }
    var delegate:FavouritePlaceDelegate?
    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupNavigationManager()
        setupLocationManager()
        setupTableView()
        addObservers()
        
    }
    
    // MARK: - Initial Functions
    func setupUI(){
        self.title = "Create Pickup Request"
        if ThemeManager.isNightMode() {
            mainView.backgroundColor = Appearance.backgroundViewNight
            searchField.backgroundColor = Appearance.darkGrayNight
            searchField.textColor = UIColor.white
            searchField.placeHolderColor = UIColor.lightGray
        }
    }
    
    func setupTableView(){
        tableView.register(MapResultViewCell.self)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 60
    }
    
    func setupNavigationManager(){
        navigationManager = NMANavigationManager.sharedInstance()
    }
    
    
    func setupLocationManager() {
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.distanceFilter = 20
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startMonitoringSignificantLocationChanges()
        locationManager.delegate = self
        updateFromLocationPermission()
    }
    
    func addObservers(){
        registerNotification(notification: Notification.Name.refreshFavouriteAddresses, selector: #selector(pop(_:)))
    }
    
    // MARK: - Functions
    func updateFromLocationPermission() {
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .restricted, .denied:
                console("No Access")
                isLocationAllowed = false
                
            case .notDetermined:
                console("Not Determined")
                isLocationAllowed = true
            case .authorizedAlways, .authorizedWhenInUse:
                console("Access")
                isLocationAllowed = true
                locationManager.startUpdatingLocation()
            }
        } else {
            isLocationAllowed = false
            console("Location services are not enabled")
        }
    }
    
    
    func startSearch(with place:String) {
        guard let location = geoCoord else {
            let alertController = UIAlertController(title: "Alert", message: "Make sure your location is turned on", preferredStyle: .alert)
            let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(action)
            self.present(alertController, animated: true, completion: nil)
            return
        }
        let request: NMAAutoSuggestionRequest? = NMAPlaces.sharedInstance()?.createAutoSuggestionRequest(location: location, partialTerm: place)
        
        // limit number of items in each result page to 10
        request?.collectionSize = 10
        let error: Error? = request?.start(withListener: self)
        if (error as NSError?)?.code != Int(NMARequestError.none.rawValue) {
            // Handle request error
            print(error)
        }
    }
    
    func checkIfAddressExtists(_ favouriteAddress:Address)->Bool{
        for address in self.addresses {
            if address.buildingName == favouriteAddress.buildingName {
                return true
            }
        }
        return false
    }

}

// MARK: - Location extension
extension AddNewFavouriteViewController: CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .denied || status == .restricted {
            self.isLocationAllowed = false
        }else {
            self.isLocationAllowed = true
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if locations.count > 0, let uL = locations.first {
            locationManager.stopUpdatingLocation()
            self.userLocation = uL
            self.geoCoord = NMAGeoCoordinates(latitude: uL.coordinate.latitude, longitude: uL.coordinate.longitude)
            delay(delay: 0.5, closure: {
            })
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Couldn't get location. Error: \(error.localizedDescription)")
    }
}
// MARK: - NMAResultListener extension
extension AddNewFavouriteViewController: NMAResultListener {
    func request(_ request: NMARequest, didCompleteWithData data: Any?, error: Error?) {
        
        if (request is NMAAutoSuggestionRequest) && error == nil {
            // Results are held in an array of NMAAutoSuggest objects
            // You can then check the subclass type using the NMAAutoSuggest.type property
            guard let  textAutoSuggestionResult = data as? [Any] else {return}
            suggestions = []
            tableView.reloadData()
            self.tableView.alpha = 1
            for result in textAutoSuggestionResult {
                if result is NMAAutoSuggestPlace {
                    guard let result = result as? NMAAutoSuggestPlace else {continue}
                    let detailsRequest:NMAPlaceRequest = result.placeDetailsRequest()
                    detailsRequest.start { (request, requestData, error) in
                        guard let place = requestData as? NMAPlace else {return}
                        self.suggestions.append(place)
                    }
                }
            }
        } else {
            // Handle error
        }
    }
    
    func calculateTableViewHeight(){
        DispatchQueue.main.async(execute: {
            var frame: CGRect = self.tableView.frame
            frame.size.height = self.tableView.contentSize.height
            self.tableView.frame = frame
        })
    }
}

// MARK: - Search field extension
extension AddNewFavouriteViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let place = textField.text else {return true}
        if !place.isEmpty {
            startSearch(with: place)
        } else {
            textField.placeholder = "search_address".localized
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //Editing
    }
}

// MARK: - TableView extension
extension AddNewFavouriteViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return suggestions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(MapResultViewCell.self, for: indexPath)
        cell.addressText = suggestions[indexPath.row].name
        cell.place = suggestions[indexPath.row]
        cell.formattedAddressText = suggestions[indexPath.row].location?.address?.formattedAddress?.replacingOccurrences(of: "<br/>", with: ", ")
        cell.selectionStyle = .none
        cell.cellType = .favourite
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let place = suggestions[indexPath.row]
//        guard let buildingName = place.name,
//            let buildingAddress = place.location?.address?.formattedAddress?.replacingOccurrences(of: "<br/>", with: ", "),
//            let zipCode = place.location?.address?.postalCode,
//            let latitude = place.location?.position?.latitude,
//            let longitude = place.location?.position?.longitude else {
//                return
//        }
//        currentPlace = place
    }
}

// MARK: - Favourite address extension
extension AddNewFavouriteViewController {
    func createAndSaveAddressFrom(_ place:NMAPlace){
        guard let buildingName = currentPlace?.name,
            let buildingAddress = currentPlace?.location?.address?.formattedAddress?.replacingOccurrences(of: "<br/>", with: ", "),
            let zipCode = currentPlace?.location?.address?.postalCode,
            let latitude = currentPlace?.location?.position?.latitude,
            let longitude = currentPlace?.location?.position?.longitude else {
                return
        }
        let newAddress = Address(id: 0, buildingName: buildingName, buildingAddress: buildingAddress, latitude: latitude, longitude: longitude, zipCode: NSString(string: zipCode).integerValue)
        
        let favouriteAddresses = getFavouriteAddresses()
        for address in favouriteAddresses {
            if address.buildingName == newAddress.buildingName {
                return
            }
        }
        UserREST.addFavouriteAddress(newAddress) { (success, error) in
            if success {
                UserREST.getFavouriteAddresses(completionHandler: { (success, favouriteAddresses, error) in
                    if success {
                        self.saveFavouriteAddresses(favouriteAddresses)
                        self.postNotification(notification: Notification.Name.refreshFavouriteAddresses, object: nil)
                    }
                })
            }
        }
    }
}

extension AddNewFavouriteViewController{
    static func create() -> AddNewFavouriteViewController{
        return UIStoryboard.schedule.instantiate(AddNewFavouriteViewController.self)
    }
}

