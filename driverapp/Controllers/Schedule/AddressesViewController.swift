//
//  AddressesViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 10/23/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import NMAKit

enum AddressType:String {
    case favourite = "favorite"
    case createdByDispatcher = "dispatcher"
    case history = "history"
}

protocol AddressViewControllerDelegate:class {
    func setCurrentPlace(_ pickupRequest:PMPickupRequest)
}

class AddressesViewController: UIViewController {
    
    
    // MARK: - IBOutlets
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Properties
    var addressType:AddressType!
    var addresses:[Address] = []
    weak var delegate:AddressViewControllerDelegate?
    
    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupTableView()
        getAddresses()
        setTitle()
        
    }
    
    // MARK: - Functions
    func setupUI(){
        self.title = "Create Pickup Request"
        if ThemeManager.isNightMode() {
            mainView.backgroundColor = Appearance.backgroundViewNight
        }
    }
    
    func setupTableView(){
        tableView.register(MapResultViewCell.self)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 60
    }
    
    func getAddresses(){
        switch addressType {
        case .favourite?:
            tableView.reloadData()
            syncFavouriteAddreses()
            break
        case .createdByDispatcher?:
            getCreatedByDispatcherAddresses()
            break
        case .history?:
            break
        case .none:
            break
        }
    }
    
    func setTitle(){
        switch addressType {
        case .favourite?:
            navigationItem.title = "Favorites"
        case .createdByDispatcher?:
            navigationItem.title = "Created by Dispatcher"
        case .history?:
            navigationItem.title = "History"
        case .none:
            navigationItem.title = ""
        }
    }
    
    func getCreatedByDispatcherAddresses(){
        UserREST.getAddresses(.createdByDispatcher) { (success, addresses, error) in
            if success {
                self.addresses = addresses
                self.tableView.reloadData()
            }
        }
    }
    
    func syncFavouriteAddreses(){
        UserREST.getAddresses(.favourite) { (success, favouriteAddresses, error) in
            if success {
                for favouriteAddress in favouriteAddresses {
                    if !self.checkIfAddressExtists(favouriteAddress) {
                        self.addresses.append(favouriteAddress)
                        self.tableView.reloadData(with: .fade)
                    }
                }
            }
        }
    }
    
    func checkIfAddressExtists(_ favouriteAddress:Address)->Bool{
        for address in self.addresses {
            if address.buildingName == favouriteAddress.buildingName {
                return true
            }
        }
        return false
    }
}

// MARK: - TableView extension
extension AddressesViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addresses.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(MapResultViewCell.self, for: indexPath)
        cell.addressText = addresses[indexPath.row].buildingName
        cell.formattedAddressText = addresses[indexPath.row].buildingAddress
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let address = addresses[indexPath.row]
        let place = PMPickupRequest()
        place.buildingAddress = address.buildingAddress
        place.buildingName = address.buildingName
        place.latitude = Double(address.latitude)
        place.longitude = Double(address.longitude)
        place.zipCode = "\(address.zipCode)"
        place.buildingId = address.buildingId
        
        delegate?.setCurrentPlace(place)
        if let addPickupViewController = navigationController?.viewControllers[1] as? AddPickupViewController {
            navigationController?.popToViewController(addPickupViewController, animated: true)
        }
    }
}

// MARK: - Creation extension
extension AddressesViewController{
    static func create() -> AddressesViewController{
        return UIStoryboard.schedule.instantiate(AddressesViewController.self)
    }
}
