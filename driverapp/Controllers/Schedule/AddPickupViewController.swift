//
//  ScheduleRequestViewController.swift
//  Superintendent
//
//  Created by Arben Pnishi on 17/01/2018.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import NMAKit

enum CreatePickupMode {
    case create
    case edit
}

class AddPickupViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var searchAddressView: UIView!
    @IBOutlet weak var searchAddressTextfield: UITextField!
    @IBOutlet weak var bagCountView: UIView!
    @IBOutlet weak var bagCountTextField: UITextField!
    @IBOutlet weak var pickupLocation: UILabel!
    @IBOutlet weak var pleaseSelectALocationLabel: UILabel!
    @IBOutlet weak var insideLocationView: UIView!
    @IBOutlet weak var insideButton: UIButton!
    @IBOutlet weak var outsideLocationView: UIView!
    @IBOutlet weak var outsideButton: UIButton!
    @IBOutlet weak var curbsideView: UIView!
    @IBOutlet weak var curbsideButton: UIButton!
    @IBOutlet weak var pickupTypeLabel: UILabel!
    @IBOutlet weak var pleaseSelectRecycableTypeLabel: UILabel!
    @IBOutlet weak var recyclableTypeView: UIView!
    @IBOutlet weak var recycableButton: UIButton!
    @IBOutlet weak var recyclableInfoIcon: UIImageView!
    @IBOutlet weak var garbageTypeView: UIView!
    @IBOutlet weak var garbageButton: UIButton!
    @IBOutlet weak var garbageInfoIcon: UIImageView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var bagCountTextfield: UITextField!
    @IBOutlet weak var recyclableTableView: UITableView!
    @IBOutlet weak var garbageTableView: UITableView!
    
    //location type
    @IBOutlet weak var insideLocationTypeButton: UIButton!
    @IBOutlet weak var outsideLocationTypeButton: UIButton!
    @IBOutlet weak var curbsideLocationTypeButton: UIButton!
    @IBOutlet weak var insideLocationTypeImageView: UIImageView!
    @IBOutlet weak var outsideLocationTypeImageView: UIImageView!
    @IBOutlet weak var curbsideLocationTypeImageView: UIImageView!
    
    //pickup type
    @IBOutlet weak var recycablePickupTypeButton: UIButton!
    @IBOutlet weak var garbagePickupTypeButton: UIButton!
    @IBOutlet weak var recycablePickupTypeImageView: UIImageView!
    @IBOutlet weak var garbagePickupTypeImageView: UIImageView!
    
    // MARK: - Constraints
    @IBOutlet weak var recyclableTypeViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var garbageTypeViewHeightConstraint: NSLayoutConstraint!
    
    // MARK: - Properties
    fileprivate var internalPickupRequest: PMPickupRequest = PMPickupRequest()
    var pickupRequest: PMPickupRequest?{
        didSet{
            if let pr = pickupRequest{
                internalPickupRequest.getValuesFrom(pickup: pr)
            }
        }
    }
    var currentPlace:NMAPlace?
    var progressView:UIProgressView?
    var pickupMode:CreatePickupMode = .create{
        didSet{
            if pickupMode == .edit {
                guard let pickup = pickupRequest else {return}
                internalPickupRequest = pickup
            }
        }
    }
    var materialSubtypes:[MaterialSubtype] = []
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        resetLocationTypes()
        resetPickTypes()
        setupLocalizables()
        setValues()
        checkGarbageType()
        addProgressBar()
        setupTableViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupUI()
    }
    
    // MARK: - Functions
    func setupUI(){
        self.title = "Create Pickup Request"
        if ThemeManager.isNightMode(){
            mainView.backgroundColor = Appearance.backgroundViewNight
            searchAddressView.backgroundColor = Appearance.darkGrayNight
            searchAddressTextfield.borderColor = UIColor.clear
            searchAddressTextfield.placeHolderColor = UIColor.darkGray
            searchAddressTextfield.textColor = UIColor.white
            bagCountView.backgroundColor = Appearance.darkGrayNight
            bagCountTextField.borderColor = UIColor.clear
            bagCountTextField.placeHolderColor = UIColor.darkGray
            bagCountTextField.textColor = UIColor.white
            pickupLocation.textColor = UIColor.white
            pleaseSelectALocationLabel.textColor = UIColor.lightGray
            insideLocationView.backgroundColor = Appearance.darkGrayNight
            insideButton.setTitleColor(UIColor.white, for: .normal)
            outsideLocationView.backgroundColor = Appearance.darkGrayNight
            outsideButton.setTitleColor(UIColor.white, for: .normal)
            curbsideView.backgroundColor = Appearance.darkGrayNight
            curbsideButton.setTitleColor(UIColor.white, for: .normal)
            pickupTypeLabel.textColor = UIColor.white
            pleaseSelectRecycableTypeLabel.textColor = UIColor.lightGray
            recyclableTypeView.backgroundColor = Appearance.darkGrayNight
            recycableButton.setTitleColor(UIColor.white, for: .normal)
            garbageTypeView.backgroundColor = Appearance.darkGrayNight
            garbageButton.setTitleColor(UIColor.white, for: .normal)
        } else {
            bagCountTextField.placeHolderColor = UIColor.darkGray
        }
    }
    
    func resetLocationTypes(){
        let isNightMode = ThemeManager.isNightMode()
        insideLocationTypeButton.setTitleColor(isNightMode ? .white : .darkGray, for: .normal)
        outsideLocationTypeButton.setTitleColor(isNightMode ? .white : .darkGray, for: .normal)
        curbsideLocationTypeButton.setTitleColor(isNightMode ? .white : .darkGray, for: .normal)
        
        insideLocationTypeImageView.isHidden = true
        outsideLocationTypeImageView.isHidden = true
        curbsideLocationTypeImageView.isHidden = true
        view.endEditing(true)
    }
    
    func resetPickTypes(){
        let isNightMode = ThemeManager.isNightMode()
        recycablePickupTypeButton.setTitleColor(isNightMode ? .white : .darkGray, for: .normal)
        garbagePickupTypeButton.setTitleColor(isNightMode ? .white : .darkGray, for: .normal)

        if materialSubtypes.count != internalPickupRequest.loadSubtypes.count {
            if recycableButton.isEnabled {
                recycablePickupTypeImageView.isHidden = true
            } else {
                garbagePickupTypeImageView.isHidden = true
            }
        }
        view.endEditing(true)
    }
    
    func locationTypeSelected(at index: Int){
        internalPickupRequest.locationType = index
        switch index {
        case 1:
            insideLocationTypeButton.setTitleColor(Appearance.greenColor, for: .normal)
            insideLocationTypeImageView.isHidden = false
        case 2:
            outsideLocationTypeButton.setTitleColor(Appearance.greenColor, for: .normal)
            outsideLocationTypeImageView.isHidden = false
        case 3:
            curbsideLocationTypeButton.setTitleColor(Appearance.greenColor, for: .normal)
            curbsideLocationTypeImageView.isHidden = false
        default:
            break
        }
    }
    
    func pickupTypeSelected(at index: Int){
        switch index {
        case 1:
//            recycablePickupTypeButton.setTitleColor(Appearance.greenColor, for: .normal)
            if !internalPickupRequest.loadSubtypes.isEmpty {
                recycablePickupTypeImageView.isHidden = false
            }
        case 2:
//            garbagePickupTypeButton.setTitleColor(Appearance.greenColor, for: .normal)
            garbagePickupTypeImageView.isHidden = false
        default:
            break
        }
    }
    
    func setupLocalizables(){
        searchAddressTextfield.text = "search_address".localized
        bagCountTextField.placeholder = "bag_count".localized
        pickupLocation.text = "pickup_location".localized
        pleaseSelectALocationLabel.text = "please_select_location".localized
        insideButton.setTitle("inside".localized, for: .normal)
        outsideButton.setTitle("outside".localized, for: .normal)
        curbsideButton.setTitle("curbside".localized, for: .normal)
        pickupTypeLabel.text = "pickup_type".localized
        pleaseSelectRecycableTypeLabel.text = "please_select_recycle_type".localized
        recycableButton.setTitle("recyclable".localized, for: .normal)
        garbageButton.setTitle("garbage".localized, for: .normal)
    }
    
    func setValues(){
        resetPickTypes()
        resetLocationTypes()
        locationTypeSelected(at: internalPickupRequest.locationType)
        reloadPickupTypes()
        if internalPickupRequest.bagCount > 0{
            bagCountTextfield.text = "\(internalPickupRequest.bagCount)"
        }
        if pickupMode == .edit {
            searchAddressTextfield.text = internalPickupRequest.buildingAddress
            bagCountTextField.text = "\(internalPickupRequest.loadSize)"
        }
        validateFields()
    }
    
    func checkGarbageType() {
        switch NavigationManager.vehicleTypeID {
        case "1":
            garbageButton.isUserInteractionEnabled = false
            garbageButton.alpha = 0.5
            garbageButton.isEnabled = false
            garbageInfoIcon.alpha = 0.5
            break
        case "2":
            recycableButton.isUserInteractionEnabled = false
            recycableButton.alpha = 0.5
            recycableButton.isEnabled = false
            recyclableInfoIcon.alpha = 0.5
            break
        default:
            break
        }
        garbagePickupTypeImageView.isHidden = true
        recycablePickupTypeImageView.isHidden = true
        getSubtypesFor(id: NavigationManager.vehicleTypeID)
    }
    
    func setupTableViews(){
        garbageTableView.register(SubtypeTableViewCell.self)
        garbageTableView.rowHeight = UITableViewAutomaticDimension
        garbageTableView.estimatedRowHeight = 40
        
        recyclableTableView.register(SubtypeTableViewCell.self)
        recyclableTableView.rowHeight = UITableViewAutomaticDimension
        recyclableTableView.estimatedRowHeight = 40
    }
    
    func getSubtypesFor(id: String){
        let idInt = NSString(string: id).integerValue
        PickupREST.getMaterialSubtypes { (success, materials, error) in
            if success {
                if let materials = materials {
                    self.materialSubtypes = materials.filter{$0.scrapTypeID == idInt && $0.active == 1}
                    self.calculateTablesHeight(with: id)
                    self.garbageTableView.reloadData()
                    self.recyclableTableView.reloadData()
                }
            }
        }
    }
    
    func calculateTablesHeight(with id: String){
        let multiplier = self.materialSubtypes.count
        switch id {
        case "1":
            garbageTableView.frame.size.height = CGFloat(multiplier * 40)
            garbageTypeViewHeightConstraint.constant = 44
            
            recyclableTableView.frame.size.height = 0
            recyclableTypeViewHeightConstraint.constant = 44 + CGFloat(multiplier * 40)
            break
        case "2":
            recyclableTableView.frame.size.height = CGFloat(multiplier * 40)
            recyclableTypeViewHeightConstraint.constant = 44
            recyclableTableView.reloadData()
            garbageTableView.frame.size.height = 0
            garbageTypeViewHeightConstraint.constant = 44 + CGFloat(multiplier * 40)
            break
        default:
            recyclableTypeViewHeightConstraint.constant = 0
            garbageTypeViewHeightConstraint.constant = 0
            break
        }
    }
    
    func addProgressBar(){
        if let navigationController = self.navigationController {
            progressView = UIProgressView(frame: CGRect(x: 0, y: navigationController.navigationBar.frame.height, width: view.frame.width, height: 20))
            //create gradient view the size of the progress view
            let gradientView = GradientView(frame: progressView!.bounds)
            
            //convert gradient view to image , flip horizontally and assign as the track image
            progressView?.trackImage = UIImage(view: gradientView).withHorizontallyFlippedOrientation()

            progressView?.progressViewStyle = .bar
            progressView?.trackTintColor = UIColor(red255: 208,
                                                   green255: 208,
                                                   blue255: 208)
            progressView?.progress = 0
            navigationController.navigationBar.addSubview(progressView!)
        }
    }
    
    func reloadPickupTypes(){
        for tag in internalPickupRequest.pickupTypes{
            pickupTypeSelected(at: tag)
        }
    }
    
    func shouldGoForward(_ flag: Bool){
        nextButton.isEnabled = flag
    }
    
    func validateFields(){
        guard !internalPickupRequest.zipCode.isEmpty else {
            shouldGoForward(false)
            updateProgressBar()
            return
        }
        
        guard internalPickupRequest.bagCount > 0 else {
            shouldGoForward(false)
            updateProgressBar()
            return
        }
        
        guard internalPickupRequest.locationType >= 0 else {
            shouldGoForward(false)
            updateProgressBar()
            return
        }
        
        if internalPickupRequest.loadSubtypes.isEmpty, !internalPickupRequest.pickupTypes.contains(2) {
            shouldGoForward(false)
            updateProgressBar()
            return
        }
//        guard internalPickupRequest.pickupTypes.count > 0 else {
//            shouldGoForward(false)
//            return
//        }
        
        shouldGoForward(true)
        updateProgressBar()
    }
    
    func updateProgressBar(){
        switch (!internalPickupRequest.zipCode.isEmpty,
                internalPickupRequest.bagCount > 0,
                internalPickupRequest.locationType >= 0)  {
        case (true, true, true):
            progressView?.setProgress(0.5, animated: true)
            break
        case (true, true, false),
             (true, false, true),
             (false, true, true):
            progressView?.setProgress(0.25, animated: true)
            break
        case (false, false, true),
             (false, true, false),
             (true, false, false):
            progressView?.setProgress(0.125, animated: true)
        case (false, false, false):
            progressView?.setProgress(0, animated: true)
        }
    }
    
    // MARK: - IBActions
    @IBAction func searchAddressPressed(_ sender: UITapGestureRecognizer) {
        let placeFinderViewController = PlaceFinderViewController.create()
        placeFinderViewController.delegate = self
        self.push(placeFinderViewController)
    }
    
    @IBAction func locationButtonPressed(_ sender: UIButton) {
        resetLocationTypes()
        locationTypeSelected(at: sender.tag)
        validateFields()
    }
    
    @IBAction func pickupButtonPressed(_ sender: UIButton) {
        resetPickTypes()
        if internalPickupRequest.pickupTypes.contains(sender.tag) {
            if let index = internalPickupRequest.pickupTypes.index(of: sender.tag){
                internalPickupRequest.pickupTypes.remove(at: index)
            }
        }else{
            if sender.tag == 2 {
                // If "Garbage" is selected, unselect other options
                internalPickupRequest.pickupTypes = []
                internalPickupRequest.pickupTypes.append(sender.tag)
                
            } else {
                // If Recycable or E-Waste is selected, unselect Garbage
                if internalPickupRequest.pickupTypes.contains(2){
                    if let index = internalPickupRequest.pickupTypes.index(of: 2) {
                        internalPickupRequest.pickupTypes.remove(at: index)
                    }
                }
                internalPickupRequest.pickupTypes.append(sender.tag)
            }
        }
        
        insertAllLoadSubtypes()
        reloadPickupTypes()
        validateFields()
    }
    
    func insertAllLoadSubtypes(){
        if internalPickupRequest.pickupTypes.contains(1){
            for material in materialSubtypes {
                guard let id = material.id else {continue}
                if !internalPickupRequest.loadSubtypes.contains(id) {
                    internalPickupRequest.loadSubtypes.append(id)
                }
            }
        }
        recyclableTableView.reloadData()
        garbageTableView.reloadData()
    }
    
    @IBAction func nextButtonPressed(_ sender: UIButton) {
        let pickupTimingVC = AddPickupTimingViewController.create()
        pickupTimingVC.pickupMode = pickupMode
        pickupTimingVC.pickupRequest = internalPickupRequest
        progressView?.setProgress(1, animated: true)
        pickupTimingVC.progressView = progressView
        push(pickupTimingVC)
    }
}


//MARK: - Extensions
extension AddPickupViewController: UITextFieldDelegate{
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        
        guard let text = textField.text, let count = Int(text) else {
            internalPickupRequest.bagCount = 0
            validateFields()
            return
        }
        
        internalPickupRequest.bagCount = count
        validateFields()
    }
}

// MARK: - Address delegates
extension AddPickupViewController: PlaceFinderDelegate {
    func setCurrentPlaceFromPlaceFinder(_ place: NMAPlace) {
        self.currentPlace = place
        internalPickupRequest.buildingAddress = currentPlace?.location?.address?.formattedAddress?.replacingOccurrences(of: "<br/>", with: ", ") ?? ""
        internalPickupRequest.latitude = currentPlace?.location?.position?.latitude ?? 0
        internalPickupRequest.longitude = currentPlace?.location?.position?.longitude ?? 0
        internalPickupRequest.zipCode = currentPlace?.location?.address?.postalCode ?? ""
        searchAddressTextfield.text = currentPlace?.location?.address?.formattedAddress?.replacingOccurrences(of: "<br/>", with: ", ") ?? ""
        validateFields()
    }
}

extension AddPickupViewController: AddressViewControllerDelegate {
    func setCurrentPlace(_ pickupRequest: PMPickupRequest) {
        internalPickupRequest = pickupRequest
        internalPickupRequest.buildingCreatedBy = .dispatcher
        searchAddressTextfield.text = internalPickupRequest.buildingAddress
        validateFields()
    }
}

extension AddPickupViewController: FavouriteViewControllerDelegate{
    func delegateFavouriteAddress(_ address: FavouriteAddress) {
        guard let buildingId = address.id,
              let buildingName = address.buildingName,
              let buildingAddress = address.buildingAddress,
              let longitude = address.longitude,
              let latitude = address.latitude,
              let zipCode = address.zipCode else {return}
        internalPickupRequest.buildingId = buildingId
        internalPickupRequest.buildingName = buildingName
        internalPickupRequest.buildingAddress = buildingAddress
        internalPickupRequest.longitude = longitude
        internalPickupRequest.latitude = latitude
        internalPickupRequest.zipCode = "\(zipCode)"
        searchAddressTextfield.text = internalPickupRequest.buildingAddress
        validateFields()
    }
}

extension AddPickupViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return materialSubtypes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(SubtypeTableViewCell.self, for: indexPath)
        cell.material = materialSubtypes[indexPath.row]
        cell.delegate = self
        for id in internalPickupRequest.loadSubtypes {
            if id == materialSubtypes[indexPath.row].id {
                cell.isMaterialSelected = true
            }
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? SubtypeTableViewCell {
            cell.tap()
        }
    }
}

extension AddPickupViewController: SubTypeTableViewCellDelegate {
    func selectedCell(shouldSelect: Bool, id: Int) {
        if shouldSelect {
            if !internalPickupRequest.loadSubtypes.contains(id) {
                internalPickupRequest.loadSubtypes.append(id)
            }
        } else {
            if internalPickupRequest.loadSubtypes.contains(id) {
                if let index = internalPickupRequest.loadSubtypes.firstIndex(of: id){
                    internalPickupRequest.loadSubtypes.remove(at: index)
                }
            }
        }
        if recycableButton.isEnabled {
            recycablePickupTypeImageView.isHidden = internalPickupRequest.loadSubtypes.isEmpty ?  true : false
        }
//        else {
//            garbagePickupTypeImageView.isHidden = internalPickupRequest.loadSubtypes.isEmpty ?  true : false
//        }
        validateFields()
    }
}


extension AddPickupViewController{
    static func create() -> AddPickupViewController{
        return UIStoryboard.schedule.instantiate(AddPickupViewController.self)
    }
}
