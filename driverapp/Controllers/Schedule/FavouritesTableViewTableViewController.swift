//
//  FavouritesTableViewTableViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 11/8/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

protocol FavouriteTableViewControllerDelegate:class {
    func delegateFavouriteAddress(_ address:FavouriteAddress)
}

class FavouritesTableViewTableViewController: UITableViewController {
    
    // MARK: - Properties
    var addresses:[FavouriteAddress] = [] {
        didSet{
            addresses = addresses.sorted(by: {$0.priority! < $1.priority!})
            if isFirstAddressesInit {
                separateAddresses()
            }
            
        }
    }
    var delegate:FavouriteTableViewControllerDelegate?
    var id:[String] = []
    var priority:[String] = []
    var deleted:[String] = []
    var isFirstAddressesInit = true
    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        addObservers()
        synchronizeFavouriteAddresses()
    }
    
    // MARK: - Initial functions
    func setupTableView(){
        self.tableView.register(MapResultViewCell.self)
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 70
    }
    
    func addObservers(){
        registerNotification(notification: Notification.Name.refreshFavouriteAddresses, selector: #selector(self.refreshFavouriteAddresses))
    }
    
    func separateAddresses(){
        id.removeAll()
        priority.removeAll()
        deleted.removeAll()
        for address in addresses {
            id.append("\(address.id!)")
            priority.append("\(address.priority!)")
            deleted.append("0")
        }
    }

    func synchronizeFavouriteAddresses(){
        UserREST.getFavouriteAddresses { (success, favouriteAddresses, error) in
            if success {
                let beforeNumberOfRows = self.addresses.count
                for favouriteAddress in favouriteAddresses {
                    if !self.checkIfAddressExtists(favouriteAddress){
                        self.addresses.append(favouriteAddress)
                    }
                }
                let afterNumberOfRows = self.addresses.count
                if afterNumberOfRows > beforeNumberOfRows {
                    self.tableView.reloadData()
                }
                self.isFirstAddressesInit = false
                self.saveFavouriteAddresses(self.addresses)
            }
        }
    }
    
    func changeFavouriteAddresses(){
        UserREST.editFavouriteAddresses(id, priority, deleted) { (success, error) in
            if success {
                print("Successfully updated Favourite Addresses")
                self.isFirstAddressesInit = true
                self.saveFavouriteAddresses(self.addresses)
                self.synchronizeFavouriteAddresses()
            } else {
                print("Error while updating Favourite Addresses")
            }
        }
    }
    
    // MARK: - Functions
    @objc func refreshFavouriteAddresses(){
        self.addresses = getFavouriteAddresses()
        self.tableView.reloadData()
    }
    
    func checkIfAddressExtists(_ favouriteAddress:FavouriteAddress)->Bool{
        for address in self.addresses {
            if address.id == favouriteAddress.id {
                return true
            }
        }
        return false
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addresses.count
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(MapResultViewCell.self, for: indexPath)
        cell.addressText = addresses[indexPath.row].buildingName
        cell.formattedAddressText = addresses[indexPath.row].buildingAddress
        cell.selectionStyle = .none
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let address = addresses[indexPath.row]
        delegate?.delegateFavouriteAddress(address)
        if let addPickupViewController = navigationController?.viewControllers[1] as? AddPickupViewController {
            navigationController?.popToViewController(addPickupViewController, animated: true)
        }
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
            let removedAddress = addresses.remove(at: indexPath.row)
            if let removedAddressIndex = id.firstIndex(of: "\(removedAddress.id!)") {
                deleted[removedAddressIndex] = "1"
            }
            
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    
    override func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    override func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        
        let sourceAddress = addresses[sourceIndexPath.row]
        let destinationAddress = addresses[destinationIndexPath.row]
        let tempPriority = sourceAddress.priority
        sourceAddress.priority = destinationAddress.priority
        destinationAddress.priority = tempPriority
        
        if let indexOfSourceAddress = id.firstIndex(of: "\(sourceAddress.id!)") {
            if let indexOfDestinationAddress = id.firstIndex(of: "\(destinationAddress.id!)") {
                let tempPriority = priority[indexOfSourceAddress]
                priority[indexOfSourceAddress] = priority[indexOfDestinationAddress]
                priority[indexOfDestinationAddress] = tempPriority
            }
        }
        
        addresses.remove(at: sourceIndexPath.row)
        addresses.insert(sourceAddress, at: destinationIndexPath.row)
        
    }

}
