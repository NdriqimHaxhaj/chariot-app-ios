//
//  FavouriteViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 10/24/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import NMAKit

protocol FavouriteViewControllerDelegate:class {
    func delegateFavouriteAddress(_ address:FavouriteAddress)
}

class FavouriteViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var editFavouritesButton: UIBarButtonItem!
    @IBOutlet weak var addressesContainerView: UIView!
    @IBOutlet weak var addNewFavouriteView: UIView!
    
    // MARK: - Properties
    var delegate:FavouriteViewControllerDelegate?
    var tableViewController:FavouritesTableViewTableViewController? {
        if !childViewControllers.isEmpty {
            if let tableViewController = childViewControllers[0] as? FavouritesTableViewTableViewController {
                return tableViewController
            } else {
                return nil
            }
        } else {
            return nil
        }
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewController?.delegate = self
        tableViewController?.addresses = getFavouriteAddresses()
        setupUI()
    }
    
    // MARK: - IBActions
    @IBAction func editFavouritesButtonPressed(_ sender: UIBarButtonItem) {
        guard let tableView = self.tableViewController?.tableView else {return}
        tableView.setEditing(!tableView.isEditing, animated: true)
        if !tableView.isEditing {
            tableViewController?.changeFavouriteAddresses()
        }
        editFavouritesButton.title = tableView.isEditing ? "Done" : "Edit"
    }
    
    @IBAction func addNewFavouriteTapped(_ sender: UITapGestureRecognizer) {
        guard let addresses = self.tableViewController?.addresses else {return}
        let addNewFavouriteViewController = AddNewFavouriteViewController.create()
        addNewFavouriteViewController.delegate = self
        addNewFavouriteViewController.addresses = addresses
        self.push(addNewFavouriteViewController)
    }
    
    // MARK: - Functions
    func setupUI(){
        if ThemeManager.isNightMode() {
            mainView.backgroundColor = Appearance.backgroundViewNight
            addNewFavouriteView.backgroundColor = Appearance.backgroundViewNight
//            searchField.backgroundColor = Appearance.darkGrayNight
//            searchField.textColor = UIColor.white
//            searchField.placeHolderColor = UIColor.lightGray
        }
    }
}


extension FavouriteViewController: FavouritePlaceDelegate {
    func addNewAddressToFavourite(_ fromPlace: NMAPlace) {
        guard let tableViewController = self.tableViewController else {return}
        tableViewController.addresses = getFavouriteAddresses()
        tableViewController.tableView.reloadData()
    }
}

extension FavouriteViewController: FavouriteTableViewControllerDelegate{
    func delegateFavouriteAddress(_ address: FavouriteAddress) {
        delegate?.delegateFavouriteAddress(address)
    }
}

extension FavouriteViewController{
    static func create() -> FavouriteViewController{
        return UIStoryboard.schedule.instantiate(FavouriteViewController.self)
    }
}


