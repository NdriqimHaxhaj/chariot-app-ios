//
//  MapViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 7/18/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit
import CoreLocation
import NMAKit

protocol PlaceFinderDelegate {
    func setCurrentPlaceFromPlaceFinder(_ place:NMAPlace)
}

class PlaceFinderViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var addresesTableView: UITableView!
    @IBOutlet weak var chooseTableView: UITableView!
    
    //MARK: - Properties
    var locationManager = CLLocationManager()
    var userLocation : CLLocation?
    var geoCoord: NMAGeoCoordinates?
    var isLocationAllowed: Bool = false
    var navigationManager: NMANavigationManager?
    var results: NMADiscoveryPage? {
        didSet {
            for result in (results?.discoveryResults)! {
                print(result.name ?? "")
            }
        }
    }
    var suggestions:[NMAPlace] = [] {
        didSet{
            if !suggestions.isEmpty {
                addresesTableView.reloadData()
                addresesTableView.alpha = 1
            } else {
                addresesTableView.alpha = 0
            }
        }
    }
    var currentPlace:NMAPlace?{
        
        didSet{
            func showWrongAddressAlert(){
                let alert = UIAlertController(title: "Wrong address", message: "The selected address is not valid, please choose another", preferredStyle: .alert)
                let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            
            guard let postalCode = currentPlace?.location?.address?.postalCode, !postalCode.isEmpty else {
                showWrongAddressAlert()
                return
            }
            guard let latitude = currentPlace?.location?.position?.latitude,
                      !latitude.isZero else {
                showWrongAddressAlert()
                return
            }
            guard let longitude = currentPlace?.location?.position?.longitude,
                      !longitude.isZero else {
                showWrongAddressAlert()
                return
            }

            delegate?.setCurrentPlaceFromPlaceFinder(currentPlace!)
            createAndSaveAddressFrom(currentPlace!)
            pop()
        }
    }
    
    var delegate:PlaceFinderDelegate?
    var chooseMenu:[String] = ["Favorites", "Created by Dispatcher", "History"]
    //MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationManager()
        setupLocationManager()
        setupTableView()
        addObservers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupUI()
    }

    //Initial functions
    func setupUI(){
        self.title = "Search Address"
        if ThemeManager.isNightMode() {
            mainView.backgroundColor = Appearance.backgroundViewNight
            searchField.backgroundColor = Appearance.darkGrayNight
            searchField.textColor = UIColor.white
            searchField.placeHolderColor = UIColor.lightGray
        }
    }
    func setupNavigationManager(){
        navigationManager = NMANavigationManager.sharedInstance()
    }
    
    func setupLocationManager() {
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.distanceFilter = 20
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startMonitoringSignificantLocationChanges()
        locationManager.delegate = self
        updateFromLocationPermission()
    }
    
    func setupTableView(){
        addresesTableView.register(MapResultViewCell.self)
        chooseTableView.register(ChooseAddressTableViewCell.self)
        chooseTableView.rowHeight = UITableViewAutomaticDimension
        chooseTableView.estimatedRowHeight = 60
    }
    
    func addObservers(){
        registerNotification(notification: Notification.Name.refreshFavouriteAddresses, selector: #selector(self.reloadData))
    }
    
    @objc func reloadData(){
//        addresesTableView.reloadData()
    }
    
    //MARK: - Functions
    func updateFromLocationPermission() {
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .restricted, .denied:
                console("No Access")
                isLocationAllowed = false
                
            case .notDetermined:
                console("Not Determined")
                isLocationAllowed = true
            case .authorizedAlways, .authorizedWhenInUse:
                console("Access")
                isLocationAllowed = true
                locationManager.startUpdatingLocation()
            }
        } else {
            isLocationAllowed = false
            console("Location services are not enabled")
        }
    }
    
    
    func startSearch(with place:String) {
        guard let location = geoCoord else {
            let alertController = UIAlertController(title: "Alert", message: "Make sure your location is turned on", preferredStyle: .alert)
            let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(action)
            self.present(alertController, animated: true, completion: nil)
            return
        }
        let request: NMAAutoSuggestionRequest? = NMAPlaces.sharedInstance()?.createAutoSuggestionRequest(location: location, partialTerm: place)
        
        // limit number of items in each result page to 10
        request?.collectionSize = 10
        let error: Error? = request?.start(withListener: self)
        if (error as NSError?)?.code != Int(NMARequestError.none.rawValue) {
            // Handle request error
            print(error)
        }
    }
    
    // MARK: - Overriden Functions
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
}

//MARK: - Extensions
extension PlaceFinderViewController: CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .denied || status == .restricted {
            self.isLocationAllowed = false
        }else {
            self.isLocationAllowed = true
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if locations.count > 0, let uL = locations.first {
            locationManager.stopUpdatingLocation()
            self.userLocation = uL
            self.geoCoord = NMAGeoCoordinates(latitude: uL.coordinate.latitude, longitude: uL.coordinate.longitude)
            delay(delay: 0.5, closure: {
            })
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Couldn't get location. Error: \(error.localizedDescription)")
    }
}

extension PlaceFinderViewController: NMAResultListener {
    func request(_ request: NMARequest, didCompleteWithData data: Any?, error: Error?) {
        
        if (request is NMAAutoSuggestionRequest) && error == nil {
            // Results are held in an array of NMAAutoSuggest objects
            // You can then check the subclass type using the NMAAutoSuggest.type property
            guard let  textAutoSuggestionResult = data as? [Any] else {return}
            suggestions = []
            addresesTableView.reloadData()
            self.addresesTableView.alpha = 1
            for result in textAutoSuggestionResult {
                if result is NMAAutoSuggestPlace {
                    guard let result = result as? NMAAutoSuggestPlace else {continue}
                    let detailsRequest:NMAPlaceRequest = result.placeDetailsRequest()
                    detailsRequest.start { (request, requestData, error) in
                        guard let place = requestData as? NMAPlace else {return}
                        self.suggestions.append(place)
                    }
                }
            }
        } else {
            // Handle error
        }
    }
    
    func calculateTableViewHeight(){
        DispatchQueue.main.async(execute: {
            var frame: CGRect = self.addresesTableView.frame
            frame.size.height = self.addresesTableView.contentSize.height
            self.addresesTableView.frame = frame
        })
    }
}

extension PlaceFinderViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let place = textField.text else {return true}
        chooseTableView.alpha   = place.isEmpty ? 1 : 0
        addresesTableView.alpha = place.isEmpty ? 0 : 1
        if !place.isEmpty {
            startSearch(with: place)
        } else {
            textField.placeholder = "search_address".localized
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        addresesTableView.alpha = suggestions.isEmpty ? 0 : 1
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //Editing
    }
    
}

extension PlaceFinderViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == chooseTableView {
            return chooseMenu.count
        } else {
            return suggestions.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == chooseTableView {
            let cell = tableView.dequeue(ChooseAddressTableViewCell.self, for: indexPath)
            cell.tempText = chooseMenu[indexPath.row]
            cell.selectionStyle = .none
            return cell
        } else {
            let cell = tableView.dequeue(MapResultViewCell.self, for: indexPath)
            cell.addressText = suggestions[indexPath.row].name
            cell.place = suggestions[indexPath.row]
            cell.formattedAddressText = suggestions[indexPath.row].location?.address?.formattedAddress?.replacingOccurrences(of: "<br/>", with: ", ")
            cell.selectionStyle = .none
            cell.cellType = .favourite
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == addresesTableView {
            guard let cell = tableView.cellForRow(at: indexPath) as? MapResultViewCell else {return}
            searchField.text = cell.formattedAddressText
            currentPlace = suggestions[indexPath.row]
            tableView.alpha = 0
        } else {
            switch indexPath.row {
            case 0:
                let favouriteViewController = FavouriteViewController.create()
                if let addPickupViewController = navigationController?.viewControllers[1] as? AddPickupViewController {
                    favouriteViewController.delegate = addPickupViewController
                }
                self.push(favouriteViewController)
                break
            case 1:
                let addressesViewController = AddressesViewController.create()
                addressesViewController.addressType = .createdByDispatcher
                if let addPickupViewController = navigationController?.viewControllers[1] as? AddPickupViewController {
                    addressesViewController.delegate = addPickupViewController
                }
                self.push(addressesViewController)
                break
            case 2:
                let addressesViewController = AddressesViewController.create()
                addressesViewController.addressType = .history
                let historyAddresses = getHistoryAddresses()
                addressesViewController.addresses = historyAddresses
                if let addPickupViewController = navigationController?.viewControllers[1] as? AddPickupViewController {
                    addressesViewController.delegate = addPickupViewController
                }
                self.push(addressesViewController)
                break
            default:
                break
            }
            
        }
    }
}

// MARK: - Address History extension
extension PlaceFinderViewController {
    func createAndSaveAddressFrom(_ place:NMAPlace){
        guard let buildingName = currentPlace?.name,
              let buildingAddress = currentPlace?.location?.address?.formattedAddress?.replacingOccurrences(of: "<br/>", with: ", "),
            let zipCode = currentPlace?.location?.address?.postalCode,
            let latitude = currentPlace?.location?.position?.latitude,
            let longitude = currentPlace?.location?.position?.longitude else {
                return
        }
        let newAddress = Address(id: 0, buildingName: buildingName, buildingAddress: buildingAddress, latitude: latitude, longitude: longitude, zipCode: NSString(string: zipCode).integerValue)
        
        var historyAddresses = getHistoryAddresses()
        for address in historyAddresses {
            if address.buildingName == newAddress.buildingName {
                return
            }
        }
        historyAddresses.append(newAddress)
        saveHistoryAddresses(historyAddresses)
    }
    
}

extension PlaceFinderViewController{
    static func create() -> PlaceFinderViewController{
        return UIStoryboard.schedule.instantiate(PlaceFinderViewController.self)
    }
}
