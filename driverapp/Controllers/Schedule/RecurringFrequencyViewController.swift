//
//  RecurringFrequencyViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 11/30/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

protocol RecurringFrequencyDelegate:class {
    func recurringFrequencies(recurringDays: [Int], recurringFrequency: Int)
}

class RecurringFrequencyViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var recurringImageView: UIImageView!
    @IBOutlet weak var recurringHeaderView: UIView!
    @IBOutlet weak var recurringDaysContainerView: UIView!
    @IBOutlet weak var everyDayImageView: UIImageView!
    @IBOutlet weak var everyWeekImageView: UIImageView!
    @IBOutlet weak var everyMonthImageView: UIImageView!
    @IBOutlet weak var recurringButton: UIButton!
    @IBOutlet weak var everyDayButton: UIButton!
    @IBOutlet weak var everyWeekButton: UIButton!
    @IBOutlet weak var everyMonthButton: UIButton!
    @IBOutlet weak var repeatingDaysView: UIView!
    @IBOutlet weak var mondayButton: UIButton!
    @IBOutlet weak var tuesdayButton: UIButton!
    @IBOutlet weak var wednesdayButton: UIButton!
    @IBOutlet weak var thursdayButton: UIButton!
    @IBOutlet weak var fridayButton: UIButton!
    @IBOutlet weak var saturdayButton: UIButton!
    @IBOutlet weak var sundayButton: UIButton!
    @IBOutlet weak var repeatOnLabel: UILabel!
    
    // MARK: - Properties
    var recurringFrequency = -1
    var recurringDays:[Int] = []
    var delegate:RecurringFrequencyDelegate?
    
    // MARK: - Constraints
    @IBOutlet weak var repeatingDaysViewTopConstraint: NSLayoutConstraint!
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Create Pickup Request"
        setupUI()
        resetRecurringTypes()
        recurringType(selectedAt: recurringFrequency)
        showRepeatingDays(status: false, animated: false)
    }
    
    // MARK: - IBActions
    @IBAction func recurringButtonPressed(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        updateRecurringState(selected: sender.isSelected)
    }
    
    @IBAction func recurringTypeButtonPressed(_ sender: UIButton) {
        resetRecurringTypes()
        recurringType(selectedAt: sender.tag)
    }
    
    @IBAction func recurringDayButtonPressed(_ sender: UIButton) {
        let backgroundColor = UIColor(red255: 218, green255: 218, blue255: 218)
        sender.isSelected = !sender.isSelected
        sender.backgroundColor = sender.isSelected ? Appearance.greenColor : backgroundColor
        sender.setTitleColor(sender.isSelected ? .white : .darkGray , for: .normal)
        if sender.isSelected {
            if !recurringDays.contains(sender.tag){
                recurringDays.append(sender.tag)
            }
        } else {
            if recurringDays.contains(sender.tag){
                if let index = recurringDays.firstIndex(of: sender.tag){
                    recurringDays.remove(at: index)
                }
            }
        }
    }
    
    @IBAction func doneButtonPressed(_ sender: UIButton) {
        delegate?.recurringFrequencies(recurringDays: recurringDays, recurringFrequency: recurringFrequency)
        pop()
    }
    
    
    // MARK: - Functions
    func setupUI(){
        if ThemeManager.isNightMode() {
            mainView.backgroundColor = Appearance.backgroundViewNight
            recurringHeaderView.backgroundColor = Appearance.darkGrayNight
            recurringDaysContainerView.backgroundColor = Appearance.darkGrayNight
            repeatingDaysView.backgroundColor = Appearance.darkGrayNight
            repeatOnLabel.textColor = UIColor.white
        }
    }
    
    func resetRecurringTypes(){
        let isNightMode = ThemeManager.isNightMode()
        recurringButton.setTitleColor(isNightMode ? .white : .darkGray, for: .normal)
        everyDayButton.setTitleColor(isNightMode ? .white : .darkGray, for: .normal)
        everyWeekButton.setTitleColor(isNightMode ? .white : .darkGray, for: .normal)
        everyMonthButton.setTitleColor(isNightMode ? .white : .darkGray, for: .normal)
        recurringImageView.isHidden =  true
        everyDayImageView.isHidden = true
        everyWeekImageView.isHidden = true
        everyMonthImageView.isHidden = true
    }

    
    func recurringType(selectedAt index: Int){
        recurringFrequency = index
        refreshRecurringViews()
//        reloadCalendarData()
//        updatePickupDayTimeFrequencyLabel()
    }
    
    func updateRecurringState(selected: Bool) {
        if !selected{
            recurringFrequency = -1
        }
        
        resetRecurringTypes()
        recurringType(selectedAt: recurringFrequency)
    }
    
    func refreshRecurringViews() {
        switch recurringFrequency {
        case -1:
            recurringButton.setTitleColor(Appearance.greenColor, for: .normal)
            recurringImageView.isHidden = false
            showRepeatingDays(status: false, animated: true)
        case 1:
//            startDateTimeCalendarView.allowsSelection = true
//            startDateTimeCalendarView.allowsMultipleSelection = false
            everyDayButton.setTitleColor(Appearance.greenColor, for: .normal)
            everyDayImageView.isHidden = false
            showRepeatingDays(status: false, animated: true)
            
        case 2:
//            startDateTimeCalendarView.allowsSelection = true
//            startDateTimeCalendarView.allowsMultipleSelection = true
            everyWeekButton.setTitleColor(Appearance.greenColor, for: .normal)
            everyWeekImageView.isHidden = false
            showRepeatingDays(status: true, animated: true)
            
        case 3:
//            startDateTimeCalendarView.allowsSelection = true
//            startDateTimeCalendarView.allowsMultipleSelection = true
            everyMonthButton.setTitleColor(Appearance.greenColor, for: .normal)
            everyMonthImageView.isHidden = false
            showRepeatingDays(status: false, animated: true)
        default:
            break
        }
    }
    
    func showRepeatingDays( status:Bool, animated:Bool){
        if animated {
            repeatingDaysViewTopConstraint.constant = status ? 0 : 100
            UIView.animate(withDuration: 0.2) {
                self.repeatingDaysView.alpha = status ? 1 : 0
                self.view.layoutIfNeeded()
            }
        } else {
            repeatingDaysViewTopConstraint.constant = status ? 0 : 100
            self.repeatingDaysView.alpha = status ? 1 : 0
            self.repeatingDaysView.layoutIfNeeded()
        }
        
    }
}

//MARK: - Extensions
extension RecurringFrequencyViewController{
    static func create() -> RecurringFrequencyViewController{
        return UIStoryboard.schedule.instantiate(RecurringFrequencyViewController.self)
    }
}
