//
//  ProfileViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 4/25/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit


class ProfileViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var fullnameLabel: UILabel!
    @IBOutlet weak var emailStaticLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var numberStaticLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var profilePicture: UIImageView!
    
    
    // MARK: - Properties
    var thisUser:User?
    
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        getProfile()
        setTexts()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setValues()
    }
    
    // MARK: - IBActions
    func setupUI(){
        if ThemeManager.isNightMode() {
            backgroundView.backgroundColor = Appearance.backgroundViewNight
            fullnameLabel.textColor = UIColor.white
            emailStaticLabel.textColor = UIColor.white
            numberStaticLabel.textColor = UIColor.white
            emailLabel.textColor = UIColor.white
            numberLabel.textColor = UIColor.white
        }
    }
    
    @IBAction func editButtonPressed(_ sender: UIButton) {
        let editProfileViewController = EditProfileViewController.create()
        editProfileViewController.tempUser = thisUser
        editProfileViewController.modalPresentationStyle = .overCurrentContext
        self.showModal(editProfileViewController, animated: false, completion: nil)
    }
    
    // MARK: - Functions
    func getProfile(){
        UserREST.getUserProfile(with: nil) { [weak self] (user, error) in
            guard let `self` = self else { return }
            
            if let user = user{
                AccountManager.currentUser = user
                self.thisUser = user
                self.setValues()
            }
        }
    }
    
    func setTexts(){
        navigationItem.title = "profile".localized
    }
    
    func setValues(){
        if let profile = AccountManager.currentUser{
            indicator.startAnimating()
            profilePicture.kf.setImage(with: URL(string: profile.photo), placeholder: #imageLiteral(resourceName: "User Avatar"), options: nil, progressBlock: nil, completionHandler: { (image, error, cache, url) in
                self.indicator.stopAnimating()
                self.indicator.alpha = 0
            })
            fullnameLabel.text = "\(profile.name) \(profile.lastname)"
            emailLabel.text = profile.email
            numberLabel.text  = profile.phone
            
        }
    }
}

//MARK: - Extensions
extension ProfileViewController{
    static func create() -> ProfileViewController{
        return UIStoryboard.profile.instantiate(ProfileViewController.self)
    }
}


