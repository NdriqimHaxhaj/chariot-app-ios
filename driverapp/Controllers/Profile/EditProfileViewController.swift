//
//  EditProfileViewController.swift
//  driverapp
//
//  Created by Ndriqim Haxhaj on 4/25/18.
//  Copyright © 2018 Zombie Soup. All rights reserved.
//

import UIKit

class EditProfileViewController: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var profilePicture: UIImageView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var changePictureLabel: UILabel!
    @IBOutlet weak var nameTextfield: UITextField!
    @IBOutlet weak var surnameTextfield: UITextField!
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var phoneTextfield: UITextField!
    
    // MARK: - Properties
    var tempUser:User!
    var shouldProceed = false
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setValues()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    // MARK: - IBOutlets
    @IBAction func saveChangesPressed(_ sender: UIButton) {
        validateTextfields()
        updateProfile()
    }
    @IBAction func cancelPressed(_ sender: UIButton) {
        self.hideModal(false, completion: nil)
    }
    
    @IBAction func viewTapped(_ sender: UITapGestureRecognizer) {
        endEditing()
    }
    
    @IBAction func profileImagePressed(_ sender: UITapGestureRecognizer) {
        let editPicture = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let removePicture = UIAlertAction(title: "Remove Current Photo", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in self.deleteProfilePicture() })
        let takeApicture = UIAlertAction(title: "Take a Photo", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in self.openCamera() })
        let chooseFromCameraRoll = UIAlertAction(title: "Choose From Camera Roll", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in  self.openPhotoLibraryButton() })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in })
        
        editPicture.addAction(removePicture)
        editPicture.addAction(takeApicture)
        editPicture.addAction(chooseFromCameraRoll)
        editPicture.addAction(cancelAction)
        editPicture.view.tintColor = Appearance.greenColor
        self.present(editPicture, animated: true, completion: nil)
    }
    
    
    // MARK: - Functions
    func setupUI(){
        if ThemeManager.isNightMode() {
            backgroundView.backgroundColor = Appearance.backgroundViewNight
            changePictureLabel.textColor = UIColor.white
            nameTextfield.backgroundColor = Appearance.textFieldNight
            nameTextfield.textColor = UIColor.white
            nameTextfield.placeHolderColor = UIColor.white
            surnameTextfield.backgroundColor = Appearance.textFieldNight
            surnameTextfield.textColor = UIColor.white
            surnameTextfield.placeHolderColor = UIColor.white
            emailTextfield.backgroundColor = Appearance.textFieldNight
            emailTextfield.textColor = UIColor.white
            emailTextfield.placeHolderColor = UIColor.white
            phoneTextfield.backgroundColor = Appearance.textFieldNight
            phoneTextfield.textColor = UIColor.white
            phoneTextfield.placeHolderColor = UIColor.white
        }
        
    }
    
    func setValues(){
        if let profile = AccountManager.currentUser{
            indicator.startAnimating()
            profilePicture.kf.setImage(with: URL(string: profile.photo), placeholder: #imageLiteral(resourceName: "User Avatar"), options: nil, progressBlock: nil, completionHandler: { (image, error, cache, url) in
                self.indicator.stopAnimating()
                self.indicator.alpha = 0
            })
            nameTextfield.text = profile.name
            surnameTextfield.text  = profile.lastname
            emailTextfield.text = profile.email
            phoneTextfield.text = profile.phone
        }
    }
    
    
    func updateProfile(){
        if shouldProceed {
            guard let name = nameTextfield.text,
                let surname = surnameTextfield.text,
                let email = emailTextfield.text,
                let phone = phoneTextfield.text else {return}
            showHud()
            UserREST.updateProfile(with: name, phone: phone) { (success, error) in
                if success {
                    self.updateUser()
                } else {
                    print("Error: \(error.debugDescription)")
                    self.hideHud()
                }
            }
        }
    }
    
    func updateUser(){
        UserREST.getUserProfile(with: nil) { [weak self] (user, error) in
            self?.hideHud()
            if let user = user{
                AccountManager.currentUser = user
                self?.hideModal(false, completion: {
                    print("User saved")
                })
            }
        }
    }
    
    func uploadProfile(image: UIImage){
        indicator.startAnimating()
        UserREST.uploadProfilePicture(image: image) {[weak self] (newImage, error) in
            guard let `self` = self else { return }
            
            self.indicator.stopAnimating()
            self.profilePicture.image = image
            AccountManager.currentUser?.photo = newImage ?? ""
            AccountManager.currentUser?.save()
        }
    }
    
    func deleteProfilePicture(){
        endEditing()
        
        self.indicator.startAnimating()
        let defaultImage = #imageLiteral(resourceName: "User Avatar")
        UserREST.uploadProfilePicture(image: defaultImage) { [weak self] (newImage, error) in
            guard let `self` = self else { return }
            
            self.profilePicture.image = #imageLiteral(resourceName: "User Avatar")
            let user = AccountManager.currentUser
            user?.photo = newImage ?? ""
            user?.save()
            self.indicator.stopAnimating()
        }
    }
    
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    func openPhotoLibraryButton() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            let imagePicker = UIImagePickerController()
            
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func validateTextfields(){
        if nameTextfield.text == "" {
            highlightMissingField(nameTextfield)
            shouldProceed = false
        } else {
            shouldProceed = true
        }
        
        if surnameTextfield.text == "" {
            highlightMissingField(surnameTextfield)
            shouldProceed = false
        } else {
            shouldProceed = true
        }
        
        if emailTextfield.text == "" {
            highlightMissingField(emailTextfield)
            shouldProceed = false
        } else {
            shouldProceed = true
        }
        
        if phoneTextfield.text == "" {
            highlightMissingField(phoneTextfield)
            shouldProceed = false
        } else {
            shouldProceed = true
        }
    }
    
    func highlightMissingField(_ field:UITextField){
        field.borderColor = Appearance.darkOrangeColor
    }
    
    func resetTextFieldsColors(){
        let color = UIColor(red255: 230, green255: 230, blue255: 230)
        nameTextfield.borderColor = color
        surnameTextfield.borderColor = color
        emailTextfield.borderColor = color
        phoneTextfield.borderColor = color
    }
    
    func endEditing(){
        nameTextfield.resignFirstResponder()
        surnameTextfield.resignFirstResponder()
        emailTextfield.resignFirstResponder()
        phoneTextfield.resignFirstResponder()
    }
}

// MARK: - Textfield Delegate
extension EditProfileViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        validateTextfields()
        endEditing()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        resetTextFieldsColors()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        validateTextfields()
    }
}

//MARK: - Create Extension
extension EditProfileViewController{
    static func create() -> EditProfileViewController{
        return UIStoryboard.profile.instantiate(EditProfileViewController.self)
    }
}

// MARK: - Image picker delegate
extension EditProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            uploadProfile(image: image)
        }
        
        picker.dismiss(animated: true, completion: {
        })

    }
}
